import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { StaticContentResource } from '../../module/admin/component/static-content/static-content.resource';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { RestResponseModel } from '../../module/core/model/rest-response.model';
import { StaticContentType } from '../../module/shared/model/static-content-type.model';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {

    text: string;

    constructor(
        public authService: AuthService,
        private staticContentResource: StaticContentResource,
        private sanitizer: DomSanitizer,
        private ref: ChangeDetectorRef
    ) { }

    ngOnInit(): void {
        this.loadCodeOfConduct();
    }

    private loadCodeOfConduct(): void {
        const localizationId = this.authService.isAuthorized ? this.authService.localizationId : '';

        this.staticContentResource.get(StaticContentType[StaticContentType.HOME].toString(), localizationId)
            .subscribe((data: RestResponseModel) => {
                this.text = data.data;
                this.ref.detectChanges();
            });
    }

    getHtmlText(): SafeHtml {
        return this.sanitizer.bypassSecurityTrustHtml(this.text);
    }
}
