import {Component, ChangeDetectionStrategy} from '@angular/core';

@Component({
    moduleId: module.id,
    changeDetection: ChangeDetectionStrategy.OnPush,
    template: `
        <div class="container">
            <router-outlet></router-outlet>
        </div>
    `
})
export class NoNavigationComponent {
    constructor() { }
}
