import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
    moduleId: module.id,
    templateUrl: 'error.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorComponent {

    constructor() {}
}
