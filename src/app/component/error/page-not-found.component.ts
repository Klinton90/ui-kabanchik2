import {AfterViewInit, Component, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy} from '@angular/core';
import {Subscription, timer} from 'rxjs';
import {Router} from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'page-not-found',
    templateUrl: 'page-not-found.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageNotFoundComponent implements AfterViewInit, OnDestroy {

    constructor(
        private router: Router,
        private ref: ChangeDetectorRef
    ) {}

    seconds: number = 5;
    private timer: Subscription;

    ngAfterViewInit(): void {
        this.timer = timer(1000, 1000).subscribe(x => {
            if (this.seconds > 0) {
                this.seconds--;
                this.ref.detectChanges();
            } else {
                this.router.navigateByUrl('/');
            }
        });
    }

    ngOnDestroy(): void {
        this.timer.unsubscribe();
    }

}
