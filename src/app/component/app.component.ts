import { Component, Inject, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AlertService } from '../module/core/component/alert/alert.service';
import { ILanguageService, LANGUAGE_SERVICE_TOKEN } from '../module/core/service/i-language.service';
import { APP_INITLALIZATION_ERROR } from '../config/app-initializer.config';
import { Title } from '@angular/platform-browser';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';

export type ILanguageServiceAlias = ILanguageService;

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.component.html',
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {

    constructor(
        private activatedRoute: ActivatedRoute,
        private notificationService: AlertService,
        private router: Router,
        @Inject(LANGUAGE_SERVICE_TOKEN) private languageService: ILanguageServiceAlias,
        private titleService: Title,
        private ref: ChangeDetectorRef,
    ) { }

    ngOnInit(): void {
        if (APP_INITLALIZATION_ERROR) {
            // Fail gracefully if backend is down when app is loaded
            this.router.navigateByUrl('/error');
        } else {
            // parse message received in link and show alert on screen
            this.activatedRoute.queryParams.subscribe((params: Params) => {
                const message: string = params['message'];
                if (message && message.length > 0) {
                    // that is OK to do navigation, it doesn't re-create component, just runs that callback once again
                    const url: string = this.router.url.split('?')[0];
                    const type: any = params['type'] || AlertType.DANGER;
                    this.router.navigate([url], {queryParams: {}});
                    this.notificationService.post({
                        body: message,
                        title: 'ui.message.default.serviceNotification.title',
                        type: type
                    }, 3000);
                }
            });

            this.languageService.reloading$.subscribe((reloading: boolean) => {
                if (reloading) {
                    const url: string = this.router.url;
                    this.router.navigateByUrl('/error', {skipLocationChange: true}).then(() => {
                        this.router.navigateByUrl(url);
                        this.setTitle();
                    });
                }
            });
        }

        this.ref.detectChanges();
        this.setTitle();
    }

    private setTitle(): void {
        this.titleService.setTitle(this.languageService.getMessage('ui.text.default.name'));
    }

}
