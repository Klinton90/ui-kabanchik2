import { ChangeDetectorRef, Component, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { Subscription } from 'rxjs';
import { ROLE } from '../../module/admin/component/user/user.model';
import { Router } from '@angular/router';
import { StickyService } from 'app/module/core/service/sticky.service';
import { NavigationMatMenuItem } from './navigation-mat-menu.component';

@Component({
    moduleId: module.id,
    templateUrl: 'navigation.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationComponent implements OnDestroy {

    Role: typeof ROLE = ROLE;
    private subscriptions: Subscription[] = [];

    timesheetItems: NavigationMatMenuItem[] = [
        {
            name: 'ui.text.report.name',
            routerLink: '/timesheet/report',
        },
        {
            name: 'ui.text.summary.name',
            routerLink: '/timesheet/summary',
            hidden: () => this.authService.roles.indexOf(ROLE.EXECUTOR) === -1
        }
    ];

    adminItems: NavigationMatMenuItem[] = [
        {
            name: 'ui.text.department.title',
            routerLink: '/admin/department',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.departmentAccess.title',
            routerLink: '/admin/departmentAccess',
        },
        {
            name: 'ui.text.localization.title',
            routerLink: '/admin/localization',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.user.title',
            routerLink: '/admin/user',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.timesheetCategory.title',
            routerLink: '/admin/timesheetCategory',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.timesheetAccess.title',
            routerLink: '/admin/timesheetAccess'
        },
        {
            name: 'ui.text.staticContent.title',
            routerLink: '/admin/staticContent',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.configuration.title',
            routerLink: '/admin/configuration',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.bonuspayment.title',
            routerLink: '/admin/bonuspayment',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.holiday.title',
            routerLink: '/admin/holiday',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.customer.title',
            routerLink: '/admin/customer',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.subcontractor.title',
            routerLink: '/admin/subcontractor',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.supplier.title',
            routerLink: '/admin/supplier',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.currency.title',
            routerLink: '/admin/currency',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.outcomeEntry.title',
            routerLink: '/admin/outcomeEntry',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.invoice.title',
            routerLink: '/admin/invoice',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.genericField.title',
            routerLink: '/admin/genericField',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        },
        {
            name: 'ui.text.genericFieldOption.title',
            routerLink: '/admin/genericFieldOption',
            hidden: () => this.authService.roles.indexOf(ROLE.ADMIN) === -1
        }
    ];

    psrItem: NavigationMatMenuItem[] = [
        {
            name: 'ui.text.psr.title',
            routerLink: '/psr/plan',
            hidden: () => this.authService.roles.indexOf(ROLE.EXECUTOR) === -1
        },
        {
            name: 'ui.text.psr.summary.name',
            routerLink: '/psr/summary',
        },
        {
            name: 'ui.text.debtorListing.name',
            routerLink: '/psr/debtorListing',
            hidden: () => this.authService.roles.indexOf(ROLE.EXECUTOR) === -1
        },
        {
            name: 'ui.text.projectSheet.name',
            routerLink: '/psr/projectSheet',
            hidden: () => this.authService.roles.indexOf(ROLE.EXECUTOR) === -1
        }
    ];

    constructor(
        public authService: AuthService,
        public router: Router,
        private ref: ChangeDetectorRef,
        public stickyService: StickyService
    ) {
        this.subscriptions.push(authService.$authPosted.subscribe(() => this.ref.detectChanges()));
    }

    public logout(event: MouseEvent): void {
        event.stopPropagation();
        this.authService.logout();
    }

    getUserName(): string {
        return this.authService.isAuthorized ? `${this.authService.user.firstName} ${this.authService.user.lastName}` : '';
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
