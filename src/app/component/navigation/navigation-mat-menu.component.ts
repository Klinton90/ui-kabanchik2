import { Component, ChangeDetectionStrategy, Input, OnDestroy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, RouterEvent, NavigationEnd } from '@angular/router';

export class NavigationMatMenuItem {
    routerLink: string;
    name: string;
    hidden?: () => boolean;
}

@Component({
    moduleId: module.id,
    selector: 'navigation-mat-menu',
    templateUrl: 'navigation-mat-menu.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationMatMenuComponent implements AfterViewInit, OnDestroy {

    private subscriptions: Subscription[] = [];

    @Input() name: string;

    @Input() hidden: boolean;

    @Input() items: NavigationMatMenuItem[] = [];

    constructor(
        private router: Router,
        private ref: ChangeDetectorRef,
    ) {
        this.subscriptions.push(this.router.events.subscribe({
            next: (event: RouterEvent) => {
                if (event instanceof NavigationEnd) {
                    this.ref.markForCheck();
                }
            }
        }));
    }

    ngAfterViewInit(): void {
        this.ref.markForCheck();
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
