import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {ROLE} from '../../module/admin/component/user/user.model';
import {AlertService} from '../../module/core/component/alert/alert.service';
import {UserResource} from '../../module/admin/component/user/user.resource';
import {RestResponseModel} from '../../module/core/model/rest-response.model';
import {HttpErrorResponse} from '@angular/common/http';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html'
})
export class RegisterComponent {

    role: ROLE;

    ROLE = ROLE;

    form: FormGroup = this.fb.group({
        email: ['', [Validators.required, Validators.email, Validators.minLength(8), Validators.maxLength(255)]]
    });

    constructor(
        private fb: FormBuilder,
        private location: Location,
        private userResource: UserResource,
        private notificationService: AlertService,
        private router: Router
    ) {}

    registerAction(): void {
        this.userResource.register({
            email: this.form.value.email,
            role: ROLE[this.role].toString()
        }).subscribe((data: RestResponseModel) => {
            this.notificationService.post({
                type: AlertType.SUCCESS,
                body: 'ui.message.user.register.body',
                title: 'ui.message.user.register.title'
            });
            this.router.navigateByUrl('/user/login');
        }, (response: HttpErrorResponse) => {
            this.notificationService.processFormFieldBackendValidation(response, this.form);
        });
    }

    cancelAction(): void {
        this.location.back();
    }

}
