import { ConverterService } from '../../module/core/service/converter.service';
import { UserResource } from '../../module/admin/component/user/user.resource';
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { RestResponseModel } from '../../module/core/model/rest-response.model';
import { AlertService } from '../../module/core/component/alert/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { LocalizationResource } from '../../module/admin/component/localization/localization.resource';
import { SortOrder } from '../../module/core/model/sort-order.model';
import { AuthService } from '../../service/auth.service';
import { passwordConfirmValidator } from '../../module/core/validator/password.validator';
import { UtilService } from '../../module/core/service/util.service';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { SelectItem } from 'primeng/api/selectitem';
import { Subscription } from 'rxjs';

@Component({
    moduleId: module.id,
    templateUrl: 'profile.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit, OnDestroy {

    localizations: SelectItem[] = [];
    subscriptions: Subscription[] = [];

    form: FormGroup = new FormGroup({
        firstName:      new FormControl('', [Validators.maxLength(255)]),
        lastName:       new FormControl('', [Validators.maxLength(255)]),
        localizationId:   new FormControl('', [Validators.required]),
        receiveReminder:    new FormControl('')
    });

    changePasswordForm: FormGroup = new FormGroup(
        {
            password:           new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(255)]),
            confirmPassword:    new FormControl('', [])
        },
        {
            updateOn: 'blur',
            validators: passwordConfirmValidator('password', 'confirmPassword')
        }
    );

    constructor(
        private auth: AuthService,
        private userResource: UserResource,
        private localizationResource: LocalizationResource,
        private notificationService: AlertService,
        private converterService: ConverterService,
        private utilService: UtilService,
        private ref: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.localizationResource
            .listAllPublic(true, {sort: [{field: 'name', order: SortOrder.ASC}]})
            .subscribe((response: RestResponseModel) => {
                this.localizations = this.converterService.convertObjectListToSelectItems(response.data, false, 'name', 'id');
                this.ref.detectChanges();
            });

        // set form values
        this.form.controls['firstName'].setValue(this.auth.user.firstName);
        this.form.controls['lastName'].setValue(this.auth.user.lastName);
        this.form.controls['receiveReminder'].setValue(this.auth.user.receiveReminder);
        this.form.controls['localizationId'].setValue(this.auth.user.localization.id);

        // show "form" error as "field" error
        this.subscriptions.push(this.changePasswordForm.valueChanges.subscribe(() => {
            const control: AbstractControl = this.changePasswordForm.controls['confirmPassword'];
            control.setErrors(this.changePasswordForm.errors);
            control.markAsTouched();
        }));
    }

    public updateProfileAction(): void {
        this.userResource.updateProfile(this.form.value).subscribe(
            (data: RestResponseModel) => {
                this.notificationService.post({
                    type: AlertType.SUCCESS,
                    body: 'ui.message.default.grid.save.body.edit'
                });
                this.auth.user.firstName = this.form.value.firstName;
                this.auth.user.lastName = this.form.value.lastName;
                this.auth.user.receiveReminder = this.form.value.receiveReminder;
                this.auth.user.localizationId = this.form.value.localizationId;
            },
            (response: HttpErrorResponse) => {
                this.notificationService.processFormFieldBackendValidation(response, this.form);
            }
        );
    }

    public updatePassword(): void {
        this.utilService.submitFormWithBlur(this.changePasswordForm, () => {
            this.userResource.updateOwnPassword(this.changePasswordForm.controls['password'].value).subscribe(
                (response: RestResponseModel) => {
                    this.notificationService.post({
                        type: AlertType.SUCCESS,
                        body: 'ui.message.default.grid.save.body.edit'
                    });
                    this.changePasswordForm.reset();
                },
                (response: HttpErrorResponse) => {
                    this.notificationService.processFormFieldBackendValidation(response, this.changePasswordForm);
                }
            );
        });
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
