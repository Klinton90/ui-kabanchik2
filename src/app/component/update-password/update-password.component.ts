import {Component, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {passwordConfirmValidator} from '../../module/core/validator/password.validator';
import {UserResource} from '../../module/admin/component/user/user.resource';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {RestResponseModel} from '../../module/core/model/rest-response.model';
import {Subscription, timer} from 'rxjs';

@Component({
    moduleId: module.id,
    templateUrl: 'update-password.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UpdatePasswordComponent implements OnDestroy {

    public form: FormGroup = this.fb.group({
        password:           ['', [Validators.required, Validators.minLength(8), Validators.maxLength(255)]],
        confirmPassword:    ['', []]
    }, {
        validator: passwordConfirmValidator('password', 'confirmPassword')
    });

    private timer: Subscription;
    public seconds: number = 5;
    public state: boolean | null = null;

    constructor(
        private router: Router,
        private fb: FormBuilder,
        private resource: UserResource,
        private activatedRoute: ActivatedRoute,
        private ref: ChangeDetectorRef
    ) {
        // show "form" error as "field" error
        this.form.valueChanges.subscribe(() => {
            this.form.controls['confirmPassword'].setErrors(this.form.errors);
        });
    }

    updatePassword() {
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            if (params['securityCode'] && params['securityCode'].length > 0) {
                this.resource.updatePassword(
                    params['securityCode'],
                    this.form.controls['password'].value
                ).subscribe(
                    (data: RestResponseModel) => {
                        this.state = true;
                        this.afterActivationCheck('/user/login');
                    },
                    (response: Response) => {
                        this.processErrorState();
                    }
                );
            } else {
                this.processErrorState();
            }
        });
    }

    private processErrorState() {
        this.state = false;
        this.afterActivationCheck('/home');
    }

    private afterActivationCheck(url: string): void {
        this.timer = timer(1000, 1000).subscribe(x => {
            if (this.seconds > 0) {
                this.seconds--;
                this.ref.detectChanges();
            } else {
                this.router.navigateByUrl(url);
            }
        });
    }

    ngOnDestroy(): void {
        this.timer.unsubscribe();
    }

}
