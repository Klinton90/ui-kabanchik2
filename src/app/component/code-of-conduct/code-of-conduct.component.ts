import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { RestResponseModel } from '../../module/core/model/rest-response.model';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { StaticContentResource } from '../../module/admin/component/static-content/static-content.resource';
import { StaticContentType } from '../../module/shared/model/static-content-type.model';

@Component({
    moduleId: module.id,
    templateUrl: 'code-of-conduct.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeOfConductComponent implements OnInit {

    text: string;

    constructor(
        public authService: AuthService,
        private staticContentResource: StaticContentResource,
        private sanitizer: DomSanitizer,
        private ref: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.loadCodeOfConduct();
    }

    private loadCodeOfConduct(): void {
        const localizationId = this.authService.isAuthorized ? this.authService.localizationId : '';

        this.staticContentResource.get(StaticContentType[StaticContentType.CODE_OF_CONDUCT].toString(), localizationId)
            .subscribe((data: RestResponseModel) => {
                this.text = data.data;
                this.ref.detectChanges();
            });
    }

    getHtmlText(): SafeHtml {
        return this.sanitizer.bypassSecurityTrustHtml(this.text);
    }

}
