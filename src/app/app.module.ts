import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_BASE_HREF } from '@angular/common';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { environment } from 'environments/environment';
import { AppComponent } from './component/app.component';
import { HomeComponent } from './component/home/home.component';
import { appRoutes } from './config/app-routes.config';
import { CoreModule } from './module/core/core.module';
import { AuthService } from './service/auth.service';
import { ErrorComponent } from './component/error/error.component';
import { LanguageService } from './service/language.service';
import { appInitializer } from './config/app-initializer.config';
import { LANGUAGE_SERVICE_TOKEN } from './module/core/service/i-language.service';
import { PageNotFoundComponent } from './component/error/page-not-found.component';
import { AuthGuardService } from './service/auth-guard.service';
import { AppSharedModule } from './module/shared/shared.module';
import { NavigationComponent } from './component/navigation/navigation.component';
import { NoNavigationComponent } from './component/no-navigation/no-navigation.component';
import { UpdatePasswordComponent } from './component/update-password/update-password.component';
import { RegisterComponent } from './component/register/register.component';
import { LoginComponent } from './component/login/login.component';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { DefaultRequestOptionsInterceptor } from './config/default-request-options.interceptor';
import { ProfileComponent } from './component/profile/profile.component';
import { CodeOfConductComponent } from './component/code-of-conduct/code-of-conduct.component';
import { ConfigurationResource } from './module/admin/component/configuration/configuration.resource';
import { LoaderInterceptor } from './config/loader.interceptor';
import { ErrorInterceptor } from './config/error.interceptor';
import { CleanupInterceptor } from './config/cleanup.interceptor';
import { LoginService } from './service/login.service';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogConfig } from '@angular/material/dialog';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarConfig } from '@angular/material/snack-bar';
import { CoalescingComponentFactoryResolver } from './service/coalescing-component-factory-resolver.service';
import { NavigationMatMenuComponent } from './component/navigation/navigation-mat-menu.component';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        ErrorComponent,
        NavigationComponent,
        NoNavigationComponent,
        PageNotFoundComponent,
        LoginComponent,
        RegisterComponent,
        UpdatePasswordComponent,
        ProfileComponent,
        CodeOfConductComponent,
        NavigationMatMenuComponent
    ],
    imports: [
        // @angular
        BrowserAnimationsModule,
        BrowserModule,
        // HttpModule,
        HttpClientModule,
        HttpClientXsrfModule.disable(),
        RouterModule.forRoot(appRoutes),

        // Material
        MatMomentDateModule,

        // custom
        CoreModule,
        AppSharedModule.forRoot(),
    ],
    providers: [
        // services
        AuthService,
        AuthGuardService,
        LoginService,
        CoalescingComponentFactoryResolver,
        {
            provide: LANGUAGE_SERVICE_TOKEN,
            useClass: LanguageService
        },

        // @angular
        {
            provide: HTTP_INTERCEPTORS,
            useClass: DefaultRequestOptionsInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: LoaderInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: CleanupInterceptor,
            multi: true
        },
        {
            provide: APP_BASE_HREF,
            useValue: environment.base_href
        },
        {
            provide: APP_INITIALIZER,
            deps: [AuthService, LANGUAGE_SERVICE_TOKEN, ConfigurationResource],
            useFactory: appInitializer,
            multi: true
        },
        {
            provide: MAT_DIALOG_DEFAULT_OPTIONS,
            useValue: {
                ... new MatDialogConfig(),
                width: '500px',
                disableClose: true,
            }
        },
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: {
                ... new MatSnackBarConfig(),
                horizontalPosition: 'right',
                verticalPosition: 'top'
            }
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(coalescingResolver: CoalescingComponentFactoryResolver) {
        coalescingResolver.init();
    }
}
