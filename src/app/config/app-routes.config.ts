import { CodeOfConductComponent } from '../component/code-of-conduct/code-of-conduct.component';
import { Routes } from '@angular/router';

import { HomeComponent } from '../component/home/home.component';
import { ErrorComponent } from '../component/error/error.component';
import { PageNotFoundComponent } from '../component/error/page-not-found.component';
import { AuthGuardService } from '../service/auth-guard.service';
import { NavigationComponent } from '../component/navigation/navigation.component';
import { NoNavigationComponent } from '../component/no-navigation/no-navigation.component';
import { LoginComponent } from '../component/login/login.component';
import { RegisterComponent } from '../component/register/register.component';
import { UpdatePasswordComponent } from '../component/update-password/update-password.component';
import { ProfileComponent } from '../component/profile/profile.component';

export const appRoutes: Routes = [
    {
        path: '',
        component: NavigationComponent,
        children: [
            {
                path: '',
                redirectTo: '/home',
                pathMatch: 'full'
            },
            {
                path: 'home',
                component: HomeComponent,
            },
            {
                path: 'user',
                canActivate: [AuthGuardService],
                children: [
                    {
                        path: 'login',
                        component: LoginComponent
                    },
                    {
                        path: 'register',
                        component: RegisterComponent
                    },
                ]
            },
            {
                path: 'admin',
                loadChildren: () => import('../module/admin/admin.module').then(m => m.AdminModule),
                canLoad: [AuthGuardService]
            },
            {
                path: 'profile',
                component: ProfileComponent,
                canActivate: [AuthGuardService]
            },
            {
                path: 'timesheet',
                loadChildren: () => import('../module/timesheet/timesheet.module').then(m => m.TimesheetModule),
                canLoad: [AuthGuardService]
            },
            {
                path: 'codeOfConduct',
                component: CodeOfConductComponent
            },
            {
                path: 'psr',
                loadChildren: () => import('../module/psr/psr.module').then(m => m.PsrModule),
                canLoad: [AuthGuardService]
            }
        ]
    },
    {
        path: '',
        component: NoNavigationComponent,
        children: [
            {
                path: 'user',
                canActivate: [AuthGuardService],
                children: [
                    {
                        path: 'update-password',
                        component: UpdatePasswordComponent
                    },
                ]
            },
            {
                path: 'error',
                component: ErrorComponent
            },
            {
                path: 'page-not-found',
                component: PageNotFoundComponent
            },
        ]
    },
    {
        path: '**',
        redirectTo: '/page-not-found'
    }
];
