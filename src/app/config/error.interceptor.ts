import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AlertService } from '../module/core/component/alert/alert.service';
import { AuthService } from '../service/auth.service';
import { ErrorRestResponseModel } from '../module/core/model/error-rest-response.model';
import { ValidationRestResponseModel } from '../module/core/model/validation-rest-response.model';
import { CUSTOM_HTTP_PARAMS, CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER } from './cleanup.interceptor';
import { LoginService } from 'app/service/login.service';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
        private notificationService: AlertService,
        private authService: AuthService,
        private loginService: LoginService
    ) {}

    intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.params
            && req.params.has(CUSTOM_HTTP_PARAMS)
            && req.params.get(CUSTOM_HTTP_PARAMS).indexOf(CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER) > -1
        ) {
            return next.handle(req);
        } else {
            return next.handle(req).pipe(tap(null, (err: HttpErrorResponse) => {
                if (err.error instanceof Error) {
                    console.warn('UI code error occurred:', err.error.message);
                    this.notificationService.post({
                        body: 'ui.message.default.error.body.default',
                        type: AlertType.DANGER
                    });
                } else {
                    const response: ErrorRestResponseModel | ValidationRestResponseModel = err.error;
                    switch (err.status) {
                        case 400:
                            this.notificationService.post({
                                body: response.message,
                                type: AlertType.DANGER
                            });
                            break;
                        case 401:
                            this.notificationService.post({
                                body: 'ui.message.default.error.body.401',
                                type: AlertType.DANGER
                            });
                            this.authService.isAuthorized = false;
                            this.loginService.show();
                            break;
                        case 403:
                            this.authService.logout('ui.message.default.error.body.403', AlertType.DANGER);
                            break;
                        case 404:
                            this.notificationService.post({
                                body: 'ui.message.default.error.body.404',
                                type: AlertType.DANGER
                            });
                            break;
                        case 422:
                            break;
                        default:
                            this.notificationService.post({
                                body: 'ui.message.default.error.body.default',
                                type: AlertType.DANGER
                            });
                            break;
                    }
                }
            }));
        }
    }
}
