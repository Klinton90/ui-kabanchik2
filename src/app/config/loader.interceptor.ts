import { LoaderService } from '../module/core/service/loader.service';
import { Observable } from 'rxjs';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CUSTOM_HTTP_PARAMS, CUSTOM_HTTP_PARAMS_NO_LOADER_HANDLER } from './cleanup.interceptor';
import { finalize } from 'rxjs/operators';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

    constructor(private loaderService: LoaderService) {}

    intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.params
            && req.params.has(CUSTOM_HTTP_PARAMS)
            && req.params.get(CUSTOM_HTTP_PARAMS).indexOf(CUSTOM_HTTP_PARAMS_NO_LOADER_HANDLER) > -1
        ) {
            return next.handle(req);
        } else {
            this.loaderService.show();

            return next.handle(req).pipe(
                finalize(() => {
                    this.loaderService.hide();
                })
            );
        }
    }

}
