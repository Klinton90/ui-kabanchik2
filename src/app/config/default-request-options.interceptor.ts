import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StaticService } from 'app/module/core/service/static.service';

@Injectable()
export class DefaultRequestOptionsInterceptor implements HttpInterceptor {

    constructor() {}

    private static isAppApi (req: HttpRequest<any>): boolean {
        return !req.url.startsWith('http') && !req.url.startsWith('HTTP') &&
        !req.url.startsWith('https') && !req.url.startsWith('HTTPS') &&
        !req.url.startsWith('ftp') && !req.url.startsWith('FTP');
    }

    intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let newReq: HttpRequest<any> = req;

        if (DefaultRequestOptionsInterceptor.isAppApi(newReq)) {
            newReq = newReq.clone({
                url: StaticService.backendHref + newReq.url,
                withCredentials: true,
                setHeaders: {
                    'X-Requested-With': 'XMLHttpRequest'
                }
            });
        }

        return next.handle(newReq);
    }

}
