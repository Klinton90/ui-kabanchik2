import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

export const CUSTOM_HTTP_PARAMS: string = '__CUSTOM_HTTP_PARAMS__';
export const CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER: string = 'CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER';
export const CUSTOM_HTTP_PARAMS_NO_LOADER_HANDLER: string = 'CUSTOM_HTTP_PARAMS_NO_LOADER_HANDLER';

@Injectable()
export class CleanupInterceptor implements HttpInterceptor {

    constructor () {}

    intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let newReq: HttpRequest<any> = req;

        if (req.params.has(CUSTOM_HTTP_PARAMS)) {
            newReq = newReq.clone({
                params: req.params.delete(CUSTOM_HTTP_PARAMS)
            });
        }

        return next.handle(newReq);
    }
}
