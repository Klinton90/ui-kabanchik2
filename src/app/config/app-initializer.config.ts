import { AuthService } from '../service/auth.service';
import { LanguageService } from '../service/language.service';
import { ConfigurationResource } from '../module/admin/component/configuration/configuration.resource';
import { finalize } from 'rxjs/operators';

export let APP_INITLALIZATION_ERROR: boolean = false;

export function appInitializer(
    auth: AuthService,
    languageService: LanguageService,
    configurationResource: ConfigurationResource
): () => Promise<any> {
    // old version with parallel calls, doesn't work anymore, as language service now depends on user profile
    /*let services: Promise<any>[] = [
        languageService.initMessages().toPromise(),
        auth.auth().toPromise()
    ];

    //Promises are resolved in exact order as they defined in `services` array
    //that's why use index to get result of specified service/promise
    return () => Promise.all(services.map(reflect)).then((results: PromiseReflectionResult[]) => {
        if(!results[0].status){
            //Main fallback point for application
            //If `MessageService` cannot get messageSource, then backend in down. Fail gracefully:
            //Set environment property. `AppComponent` checks it before rendering and redirects to error page.
            environment.global_error_occurred = true;
        }
    });*/
    return () => new Promise((resolve, reject) => {
        auth.auth().pipe(
            finalize(
                () => languageService.initMessages(auth.isAuthorized, auth.localizationId).subscribe(
                    () => {
                        configurationResource.get().subscribe(
                            () => resolve(true)
                        );
                    },
                    () => {
                        APP_INITLALIZATION_ERROR = true;
                        resolve(null);
                    }
                )
            )
        ).subscribe();
    });

}

class PromiseReflectionResult {
    data: any;
    status: boolean;
}

function reflect(promise: Promise<any>): Promise<PromiseReflectionResult> {
    return promise.then(
        function(v) {
            return {data: v, status: true};
        },
        function(e) {
            return {data: e, status: false};
        }
    );
}
