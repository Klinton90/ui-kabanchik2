import {UserPrincipalModel} from '../module/admin/component/user/user.model';

export class AuthenticationResponse {
    authenticated: boolean;
    authorities: Authority[];
    principal: UserPrincipalModel;
}

export class Authority {
    authority: string;
}
