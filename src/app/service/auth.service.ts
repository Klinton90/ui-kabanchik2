import { Injectable, Injector } from '@angular/core';
import { LocationStrategy } from '@angular/common';
import { ROLE, UserPrincipalModel } from '../module/admin/component/user/user.model';
import { RestResponseModel } from '../module/core/model/rest-response.model';
import { AlertService } from '../module/core/component/alert/alert.service';
import { AuthenticationResponse, Authority } from '../model/authentication-response.model';
import { ILanguageService, LANGUAGE_SERVICE_TOKEN } from '../module/core/service/i-language.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthResource } from '../module/core/resource/auth.resource';
import { StaticService } from '../module/core/service/static.service';
import { mergeMap, catchError } from 'rxjs/operators';
import { Subject, Observable, of, throwError } from 'rxjs';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';

type ILanguageServiceAlias = ILanguageService;

@Injectable()
export class AuthService {

    public roles: [ROLE] = [ROLE.GUEST];
    public isAuthorized: boolean = false;
    public user: UserPrincipalModel;

    public get localizationId(): string {
        if (this.user && this.user.localization.id) {
            return this.user.localization.id;
        } else {
            return localStorage.getItem('localization') || '';
        }
    }

    private authPoster: Subject<boolean> = new Subject<boolean>();
    public $authPosted: Observable<boolean> = this.authPoster.asObservable();

    constructor(
        private locationStrategy: LocationStrategy,
        private notificationService: AlertService,
        private injector: Injector,
        private resource: AuthResource
    ) {}

    public doLogin(login: string, password: string): Observable<any> {
        if (!this.isAuthorized) {
            return this.resource.login(login, password).pipe(
                mergeMap((response: any) => {
                    const prevLocalization: string = this.localizationId;
                    this.processSuccessfulResponse(response);
                    // refresh Messages only if localization indeed changed
                    if (prevLocalization !== this.localizationId) {
                        const languageService: ILanguageServiceAlias = this.injector.get(LANGUAGE_SERVICE_TOKEN);
                        languageService.refreshMessages(false);
                    }
                    this.notificationService.post({
                        body: 'ui.message.user.welcome.body',
                        title: 'ui.message.user.welcome.title',
                        type: AlertType.SUCCESS
                    });
                    return of(response);
                }),
                catchError((response: HttpErrorResponse) => {
                    this.processErrorResponse(response);
                    if (response.status === 401) {
                        this.notificationService.post({
                            body: 'ui.message.user.loginFailed.body',
                            title: 'ui.message.user.loginFailed.title',
                            type: AlertType.DANGER
                        }, 3000);
                        return throwError(response);
                    }
                }));
        } else {
            return of(null);
        }
    }

    public auth(): Observable<any> {
        if (!this.isAuthorized) {
            return this.resource.auth().pipe(
                mergeMap((response: any) => {
                    this.processSuccessfulResponse(response);
                    // TODO: fix this, as after changes LangVars might not be ready yet when we try to post this message
                    // this.notificationService.post({
                    //     body: 'ui.message.user.auth.body',
                    //     title: 'ui.message.user.auth.title',
                    //     type: AlertType.SUCCESS
                    // });
                    return of(response);
                }),
                catchError((response: HttpErrorResponse) => {
                    this.processErrorResponse(response);
                    return throwError(response);
                })
            );
        } else {
            return of(null);
        }
    }

    public logout(message?: string, type?: AlertType): void {
        this.isAuthorized = false;
        this.authPoster.next(false);
        this.user = null;
        this.roles = [ROLE.GUEST];

        this.resource.logout().subscribe(
            // Use `window.location` instead of `router` as page have to be refreshed.
            // TODO: have to navigate to `/home` path instead of empty `/` path,
            // as `Router` doesn't keep query params on redirects.
            // Fix when resolved globally https://github.com/angular/angular/issues/12664
            (response: any) => {
                const _message: string = '?message=' + (message && message.length > 0 ? message : 'ui.message.user.comeAgain.body');
                const _type: string = '&type=' + (type || AlertType.INFO);
                window.location.href = this.locationStrategy.getBaseHref() + 'home' + _message + _type;
            },
            (response: HttpErrorResponse) => {
                window.location.href = this.locationStrategy.getBaseHref() + 'home' + '?message=ui.message.default.error.body.default';
            }
        );
    }

    protected processSuccessfulResponse(json: RestResponseModel): RestResponseModel {
        if (json && json.data) {
            const auth: AuthenticationResponse = json.data;
            if (auth.authenticated && auth.authenticated === true) {
                this.isAuthorized = true;
                if (auth && auth.principal) {
                    if (auth.principal.localization.id && this.localizationId !== auth.principal.localization.id) {
                        localStorage.setItem('localization', auth.principal.localization.id);
                    }
                    // update principal after LocalStorage otherwise lang change detection will be broken
                    this.user = auth.principal;
                }
                auth.authorities.forEach((authority: Authority) => {
                    const role: number = StaticService.convertStringToEnumInt(ROLE, authority.authority, 5);
                    this.roles.push(role);
                });
                this.authPoster.next(true);
            }
        }
        return json;
    }

    protected processErrorResponse(response: HttpErrorResponse): void {
        if (response.status !== 401) {
            this.notificationService.post({
                body: 'ui.message.default.error.body.default',
                type: AlertType.DANGER
            });
        }
    }

}
