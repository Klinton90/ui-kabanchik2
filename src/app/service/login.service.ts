import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginModalComponent } from 'app/module/shared/component/login/login-modal.component';

// Need this service to prevent circular dependency
@Injectable({
    providedIn: 'root'
})
export class LoginService {

    private shown: boolean = false;

    constructor(private dialog: MatDialog) { }

    show(): void {
        if (!this.shown) {
          this.shown = true;
          this.dialog.open(LoginModalComponent)
            .afterClosed()
            .subscribe({
                next: () => this.shown = false
            });
        }
    }

}
