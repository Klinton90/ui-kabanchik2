import { Injectable } from '@angular/core';
import { ILanguageService } from '../module/core/service/i-language.service';
import { LocalizationResource } from '../module/admin/component/localization/localization.resource';
import { Subject, Observable } from 'rxjs';

const UI_VALIDATION_MESSAGE_PREFIX = 'ui.validation.default.';
const UI_VALIDATION_MESSAGE_KEYS = [
    'required',
    'minlength',
    'maxlength',
    'email',
    'min',
    'max'
];

@Injectable()
export class LanguageService implements ILanguageService {

    constructor(
        private resource: LocalizationResource,
    ) {}

    private static messageMap: Map<string, string> = new Map();

    reloadingPoster: Subject<boolean> = new Subject<boolean>();
    reloading$: Observable<boolean> = this.reloadingPoster.asObservable();

    public static getMessage(key: string): string {
        const _key: string = UI_VALIDATION_MESSAGE_KEYS.indexOf(key) >= 0 ? UI_VALIDATION_MESSAGE_PREFIX + key : key;
        return LanguageService.messageMap[_key] || _key;
    }

    public getMessage(key: string): string {
        const _key: string = UI_VALIDATION_MESSAGE_KEYS.indexOf(key) >= 0 ? UI_VALIDATION_MESSAGE_PREFIX + key : key;
        return LanguageService.messageMap[_key] || _key;
    }

    public initMessages(isAuthorized: boolean, localizationId?: string): Observable<any> {
        const o: Observable<Response> = isAuthorized
            ? this.resource.getMessagesBySession()
            : this.resource.getMessagesByLocalizationId(localizationId);

        o.subscribe(
            (data: any) => {
                LanguageService.messageMap = data;
            },
            (response: Response) => void 0 // this is required otherwise on error app crashes as nothing capture it
        );

        return o;
    }

    public refreshMessages(forceBackend: boolean) {
        const o: Observable<Response> = forceBackend ? this.resource.refreshLocalization() : this.resource.getMessagesBySession();

        o.subscribe(
            (data: any) => {
                LanguageService.messageMap = data;
                this.reloadingPoster.next(true);
            },
            (response: Response) => void 0 // this is required otherwise on error app crashes as nothing capture it
        );

        return o;
    }

}
