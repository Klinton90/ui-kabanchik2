import {Injectable} from '@angular/core';
import {
    ActivatedRouteSnapshot, CanActivate, CanLoad, Route, RouterStateSnapshot,
    UrlSegment
} from '@angular/router';
import {AuthService} from './auth.service';
import {ROLE} from '../module/admin/component/user/user.model';
import { UtilService } from '../module/core/service/util.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuardService implements CanActivate, CanLoad {

    constructor(
        private authService: AuthService,
        private utilService: UtilService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let allowed: boolean = true;

        for (let i = 0; i < route.url.length; i++) {
            const urlSegment: UrlSegment = route.url[i];
            if (urlSegment.path === 'user') {
                allowed = !this.authService.isAuthorized;
            } else if (urlSegment.path === 'profile') {
                allowed = this.authService.isAuthorized;
            }
        }

        if (!allowed) {
            this.utilService.processRouteNotAllowed();
        }

        return allowed;
    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        let allowed: boolean = true;

        if (['admin'].indexOf(route.path) >= 0) {
            allowed = this.authService.isAuthorized && this.authService.roles.indexOf(ROLE.EXECUTOR) >= 0;
        } else if (['timesheet', 'psr'].indexOf(route.path) >= 0) {
            allowed = this.authService.isAuthorized;
        }

        if (!allowed) {
            this.utilService.processRouteNotAllowed();
        }

        return allowed;
    }

}
