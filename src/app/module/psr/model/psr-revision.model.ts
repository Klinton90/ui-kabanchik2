export class PsrRevision {
    id: number;
    name: string;
    timesheetCategoryId: number;
    data: string;
}
