import { PsrDateColumn } from 'app/module/core/model/date-column.model';
import { OutcomeEntryModel } from 'app/module/admin/component/outcome-entry/outcome-entry.model';
import { BaseTimesheet } from 'app/module/timesheet/model/timesheet.model';
import { InvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { PaymentModel } from 'app/module/admin/component/payment/payment.model';
import { ProgressModel } from 'app/module/psr/component/progress/progress.model';

export class PsrCalculationResult {
    plan: PsrCalculationSubResult;
    fact: PsrCalculationSubResult;
    totals: PsrLineModelTotal[] = [];
    legend: PsrCalculationResultLegend;
}

export class PsrCalculationResultLegend {
    budgetAmount: number = 0;
    budgetAmountInPeriod: number = 0;

    spentAmount: number = 0;
    spentAmountinPeriod: number = 0;

    budgetSpent: number = 0;

    estimatedToComplete: number = 0;
    estimatedToCompleteInPeriod: number = 0;

    totalDaysOverdue: number = 0;
    maxDaysOverdue: number = 0;

    // this amount only set if `datePosted` exists
    invoiceAmount: number = 0;
    invoiceAmountInPeriod: number = 0;

    progressPlan: number = 0;
    progressFact: number = 0;

    expectedProfitLoss: number = 0;
    expectedProfitLossInPeriod: number = 0;
    expectedProfitLossPercentage: number = 0;
    expectedProfitLossPercentageInPeriod: number = 0;

    forecastSpentAtCompletion: number = 0;
    forecastSpentAtCompletionInPeriod: number = 0;

    forecastProfitLoss: number = 0;
    forecastProfitLossInPeriod: number = 0;
    forecastProfitLossPercentage: number = 0;
    forecastProfitLossPercentageInPeriod: number = 0;

    invoiceOutstanding: number = 0;
    invoiceToBeInvoiced: number = 0;
}

export abstract class PsrLineModelCore {
    key: PsrLineKey;
    name: string;
    description: string;
    additionalDescription?: string;
    invoice?: InvoiceModel;
    invoiceDaysOverdue?: number;
}

export class PsrLineModelTotal extends PsrLineModelCore {
    totalsPlan: PsrLineTotal;
    totalsFact: PsrLineTotal;
    totalsInPeriodPlan: PsrLineTotal;
    totalsInPeriodFact: PsrLineTotal;
}

export class PsrCalculationSubResult {
    rows: PsrLineModel[] = [];

    totalSubContractorProgressAmount: number = 0;
    totalTimesheetProgressAmount: number = 0;

    incomeSubTotalRow: PsrLineModel;
    outcomeSubTotalRow: PsrLineModel;
    timesheetSubTotalRow: PsrLineModel;

    outcomeTotalRow: PsrLineModel;
    totalRow: PsrLineModel;

    progressSubTotalRow: PsrLineModel;
    progressSubContractorSubTotalRow: PsrLineModel;
    progressTimesheetSubTotalRow: PsrLineModel;
}

export class PsrLineModel extends PsrLineModelCore {
    totals?: PsrLineTotal;
    totalsInPeriod?: PsrLineTotal;
    weeks: Map<PsrDateColumn, PsrLineItem> = new Map();
    weeksInPeriod: Map<PsrDateColumn, PsrLineItem> = new Map();
}

export class PsrLineKey {
    id: number;
    type: PsrLineType;
    isGroupTotal: boolean;
    subGroupId: number;
    isSubGroupTotal: boolean;
}

export declare type PsrLineModelEntry = OutcomeEntryModel | BaseTimesheet | PaymentModel | ProgressModel;

export class PsrLineItem {
    amount: number = 0;
    units: number = 0;
    description: string = '';
    entries: PsrLineModelEntry[] = [];
    hasInvoice?: boolean;

    // editable options
    isDirty?: boolean;
    uiId?: number;
    errorMessage?: string;
}

export class PsrLineTotal {
    amount: number = 0;     // money
    amount2: number = 0;    // invoice
    units?: number = 0;     // hours
}

export enum PsrLineType {
    INCOME,
    OUTCOME,
    TIMESHEET,
    TOTAL,
    PROGRESS
}
