export class PsrToXlsxTotal {
    company: string;
    planProjectTotal: string;
    factProjectTotal: string;
    planPeriodTotal: string;
    factPeriodTotal: string;
}

export class PsrToXlsx {
    company: string;
    invoiced: string;
    paid: string;
}

export class PsrToXlsxPreview {
    header?: string;
    value: string;
    description?: string;
}
