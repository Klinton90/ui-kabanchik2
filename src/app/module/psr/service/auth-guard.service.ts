import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, UrlSegment } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from 'app/service/auth.service';
import { UtilService } from 'app/module/core/service/util.service';
import { Observable } from 'rxjs';
import { ROLE } from 'app/module/admin/component/user/user.model';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private authService: AuthService,
        private utilService: UtilService
    ) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        let allowed: boolean = true;
        for (let i = 0; i < route.url.length; i++) {
            const urlSegment: UrlSegment = route.url[i];
            if (['plan', 'debtorListing', 'projectSheet'].indexOf(urlSegment.path) >= 0) {
                allowed = this.authService.roles.indexOf(ROLE.EXECUTOR) >= 0;
            }
        }

        if (!allowed) {
            this.utilService.processRouteNotAllowed();
        }
        return allowed;
    }

}
