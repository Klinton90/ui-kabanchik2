import { SubcontractorModel } from '../../admin/component/subcontractor/subcontractor.model';
import { PsrDateColumn } from 'app/module/core/model/date-column.model';
import { LocalizedEnum } from './../../shared/model/localized-enum.model';
import { OutcomeEntryModel, OutcomeEntryType } from '../../admin/component/outcome-entry/outcome-entry.model';
import { Injectable } from '@angular/core';
import { PsrTimesheet } from 'app/module/timesheet/model/timesheet.model';
import { clone, cloneDeep, flatMap, orderBy } from 'lodash';
import { StaticService, DEFAULT_DATE_FORMAT } from 'app/module/core/service/static.service';
import {
    PsrLineModel,
    PsrLineType,
    PsrLineKey,
    PsrLineItem,
    PsrLineTotal,
    PsrCalculationSubResult,
    PsrCalculationResult,
    PsrLineModelTotal,
    PsrCalculationResultLegend,
    PsrLineModelCore
} from '../model/psr-line.model';
import { UserSimpleModel } from 'app/module/admin/component/user/user.model';
import { UserService } from 'app/module/core/service/user.service';
import { SupplierModel } from 'app/module/admin/component/supplier/supplier.model';
import { ConverterService } from 'app/module/core/service/converter.service';
import { CurrencyModel } from 'app/module/admin/component/currency/currency.model';
import { OutcomeEntryParent } from 'app/module/admin/model/outcome-entry-parent.model';
import { DepartmentModel } from 'app/module/admin/component/department/department.model';
import { ExtendedInvoiceModel, InvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { CustomerModel } from 'app/module/admin/component/customer/customer.model';
import { PaymentModel } from 'app/module/admin/component/payment/payment.model';
import { ProgressModel, ProgressType } from 'app/module/psr/component/progress/progress.model';
import { ProgressParent } from 'app/module/admin/model/progress-parent.model';
import { Moment } from 'moment';
import * as moment from 'moment';
import { CurrencyRate } from 'app/module/admin/component/currency-rate/currency-rate.model';
import { DepartmentAccess } from 'app/module/admin/component/department-access/department-access.model';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';
import { PsrToXlsxTotal, PsrToXlsx, PsrToXlsxPreview } from '../model/psr-export.model';
import { TimesheetCategory } from 'app/module/admin/component/timesheet-category/timesheet-category.model';
import { GenericFieldValue } from 'app/module/admin/model/generic-field-value.model';
import { GenericFieldModel, GenericFieldType } from 'app/module/admin/component/generic-field/generic-field.model';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
import { OutcomeEntryResource } from 'app/module/admin/component/outcome-entry/outcome-entry.resource';
import { UserResource } from 'app/module/admin/component/user/user.resource';
import { CustomerResource } from 'app/module/admin/component/customer/customer.resource';
import { SupplierResource } from 'app/module/admin/component/supplier/supplier.resource';
import { SubcontractorResource } from 'app/module/admin/component/subcontractor/subcontractor.resource';
import { InvoiceResource } from 'app/module/admin/component/invoice/invoice.resource';
import { TimesheetResource } from 'app/module/timesheet/resource/timesheet.resource';
import { ProgressResource } from '../component/progress/progress.resource';
import { DepartmentResource } from 'app/module/admin/component/department/department.resource';
import { CurrencyRateResource } from 'app/module/admin/component/currency-rate/currency-rate.resource';
import { DepartmentAccessResource } from 'app/module/admin/component/department-access/department-access.resource';
import { MessageService } from 'app/module/core/service/message.service';
import { SortEvent } from 'primeng/api/sortevent';

const DESCRIPTION_SEPARATOR: string = '\r\n-------------------\r\n';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

export declare type PsrData = [
    Map<boolean, OutcomeEntryModel[]>,
    ExtendedInvoiceModel[],
    Map<boolean, PsrTimesheet[]>,
    Map<boolean, ProgressModel[]>,
    UserSimpleModel[],
    DepartmentModel[],
    CustomerModel[],
    SupplierModel[],
    SubcontractorModel[],
    CurrencyRate[],
    DepartmentAccess[]
];

class GroupedWeeks {
    weeks: Map<PsrDateColumn, PsrLineItem> = new Map();
    weeksInPeriod: Map<PsrDateColumn, PsrLineItem> = new Map();
}

export class DateLimitsResult {
    minFact: Date;
    maxFact: Date;
    minPlan: Date;
    maxPlan: Date;
    min: Date;
    max: Date;

    minDateFrom: Moment;
    maxDateTo: Moment;
}

@Injectable()
export class PsrService {

    constructor(
        private userService: UserService,
        private converterService: ConverterService,
        private messageService: MessageService,

        private outcomeEntryResource: OutcomeEntryResource,
        private invoiceResource: InvoiceResource,
        private timesheetResource: TimesheetResource,
        private progressResource: ProgressResource,
        private userResource: UserResource,
        private departmentResource: DepartmentResource,
        private customerResource: CustomerResource,
        private supplierResource: SupplierResource,
        private subcontractorResource: SubcontractorResource,
        private currencyReateResource: CurrencyRateResource,
        private departmentAccessResource: DepartmentAccessResource,
    ) {}

    private static getCompanyColumnForExport(entity: PsrLineModelCore): string {
        return entity.key.type === PsrLineType.INCOME && !entity.key.isGroupTotal && !entity.key.isSubGroupTotal
            ? ConverterService.formatDate(InvoiceModel.getDate(entity.invoice, false))
            : entity.additionalDescription || entity.name;
    }

    fetchDataForPsr(timesheetCategoryId: number): Observable<PsrData> {
        return zip(
            this.outcomeEntryResource.findAllByTimesheetCategoryId(timesheetCategoryId),
            this.invoiceResource.findAllByTimesheetCategoryId(timesheetCategoryId),
            this.timesheetResource.findAllForPsr(timesheetCategoryId),
            this.progressResource.getForPSR(timesheetCategoryId),
            this.userResource.getUsersByTimesheetCategoryId(timesheetCategoryId, true),
            this.departmentResource.getForPSR(timesheetCategoryId),
            this.customerResource.getForPSR(timesheetCategoryId),
            this.supplierResource.getForPSR(timesheetCategoryId),
            this.subcontractorResource.getForPSR(timesheetCategoryId),
            this.currencyReateResource.getForPSR(timesheetCategoryId),
            this.departmentAccessResource.findByTimesheetCategoryId(timesheetCategoryId)
        )
            .pipe(
                map(([
                    outcomeEntries,
                    invoices,
                    timesheets,
                    progressList,
                    usersResponse,
                    departmentsResponse,
                    customersResponse,
                    suppliersResponse,
                    subcontractorsResponse,
                    currencyRatesResponse,
                    departmentAccessResponse
                ]:
                [
                    OutcomeEntryModel[],
                    ExtendedInvoiceModel[],
                    PsrTimesheet[],
                    ProgressModel[],
                    RestResponseModel,
                    RestResponseModel,
                    RestResponseModel,
                    RestResponseModel,
                    RestResponseModel,
                    RestResponseModel,
                    RestResponseModel
                ]) => {
                    const groupedOutcomeEntries: Map<boolean, OutcomeEntryModel[]> = StaticService.groupBy(outcomeEntries,
                        (outcomeEntry: OutcomeEntryModel): boolean => {
                            return outcomeEntry.isForPlan;
                        });

                    const groupedTimesheets: Map<boolean, PsrTimesheet[]> = StaticService.groupBy(timesheets,
                        (timesheet: PsrTimesheet): boolean => {
                            return (<LocalizedEnum>timesheet.status).value === 'PLANNED';
                        });

                    const groupedProgressList: Map<boolean, ProgressModel[]> = StaticService.groupBy(progressList,
                        (progressModel: ProgressModel): boolean => {
                            return progressModel.isForPlan;
                        });

                    return [
                        groupedOutcomeEntries,
                        invoices,
                        groupedTimesheets,
                        groupedProgressList,
                        usersResponse.data,
                        departmentsResponse.data,
                        customersResponse.data,
                        suppliersResponse.data,
                        subcontractorsResponse.data,
                        currencyRatesResponse.data,
                        departmentAccessResponse.data
                    ];
                })
            );
    }

    calculateDateLimitsByData(
        invoices: ExtendedInvoiceModel[],
        outcomeEntries: Map<boolean, OutcomeEntryModel[]>,
        timesheets: Map<boolean, PsrTimesheet[]>,
        progressList: Map<boolean, ProgressModel[]>
    ): DateLimitsResult {
        const result: DateLimitsResult = new DateLimitsResult();

        if (invoices.length > 0
            || (outcomeEntries.get(true) && outcomeEntries.get(true).length > 0)
            || (outcomeEntries.get(false) && outcomeEntries.get(false).length > 0)
            || (timesheets.get(true) && timesheets.get(true).length > 0)
            || (timesheets.get(false) && timesheets.get(false).length > 0)
            || (progressList.get(true) && progressList.get(true).length > 0)
            || (progressList.get(false) && progressList.get(false).length > 0)
        ) {
            const payments: Map<boolean, PaymentModel[]> = StaticService.groupBy(
                flatMap(invoices, (invoice: ExtendedInvoiceModel) => invoice.paymentDTOS || []),
                (payment: PaymentModel) => payment.isForPlan);

            const paymentsSortedPlan: PaymentModel[] = orderBy(payments.get(true), 'date');
            const paymentsSortedFact: PaymentModel[] = orderBy(payments.get(false), 'date');
            const invoicesSortedPlan: ExtendedInvoiceModel[] = orderBy(invoices, 'datePlanned');
            const invoicesSortedFact: ExtendedInvoiceModel[] = orderBy(invoices,
                [(invoice: ExtendedInvoiceModel) => InvoiceModel.getDate(invoice, false)]);
            const outcomeEntriesSortedPlan: OutcomeEntryModel[] = orderBy(outcomeEntries.get(true), 'date');
            const outcomeEntriesSortedFact: OutcomeEntryModel[] = orderBy(outcomeEntries.get(false), 'date');
            const timesheetsSortedPlan: PsrTimesheet[] = orderBy(timesheets.get(true), 'date');
            const timesheetsSortedFact: PsrTimesheet[] = orderBy(timesheets.get(false), 'date');
            const progressListSortedPlan: ProgressModel[] = orderBy(progressList.get(true), 'date');
            const progressListSortedFact: ProgressModel[] = orderBy(progressList.get(false), 'date');

            if (paymentsSortedPlan.length
                || invoicesSortedPlan.length
                || outcomeEntriesSortedPlan.length
                || timesheetsSortedPlan.length
                || progressListSortedPlan.length
            ) {
                result.minPlan = new Date(Math.min(
                    paymentsSortedPlan.length > 0 ? paymentsSortedPlan[0].date.getTime() : Number.MAX_SAFE_INTEGER,
                    invoicesSortedPlan.length > 0 ? InvoiceModel.getMinDate(invoicesSortedPlan[0]).getTime() : Number.MAX_SAFE_INTEGER,
                    outcomeEntriesSortedPlan.length > 0 ? outcomeEntriesSortedPlan[0].date.getTime() : Number.MAX_SAFE_INTEGER,
                    timesheetsSortedPlan.length > 0 ? timesheetsSortedPlan[0].date.getTime() : Number.MAX_SAFE_INTEGER,
                    progressListSortedPlan.length > 0 ? progressListSortedPlan[0].date.getTime() : Number.MAX_SAFE_INTEGER
                ));

                result.maxPlan = new Date(Math.max(
                    paymentsSortedPlan.length > 0
                        ? paymentsSortedPlan[paymentsSortedPlan.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER,
                    invoicesSortedPlan.length > 0
                        ? InvoiceModel.getMinDate(invoicesSortedPlan[invoicesSortedPlan.length - 1]).getTime()
                        : Number.MIN_SAFE_INTEGER,
                    outcomeEntriesSortedPlan.length > 0
                        ? outcomeEntriesSortedPlan[outcomeEntriesSortedPlan.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER,
                    timesheetsSortedPlan.length > 0
                        ? timesheetsSortedPlan[timesheetsSortedPlan.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER,
                    progressListSortedPlan.length > 0
                        ? progressListSortedPlan[progressListSortedPlan.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER
                ));
            }

            if (paymentsSortedFact.length
                || invoicesSortedFact.length
                || outcomeEntriesSortedFact.length
                || timesheetsSortedFact.length
                || progressListSortedFact.length
            ) {
                result.minFact = new Date(Math.min(
                    paymentsSortedFact.length > 0 ? paymentsSortedFact[0].date.getTime() : Number.MAX_SAFE_INTEGER,
                    invoicesSortedFact.length > 0 ? InvoiceModel.getMinDate(invoicesSortedFact[0]).getTime() : Number.MAX_SAFE_INTEGER,
                    outcomeEntriesSortedFact.length > 0 ? outcomeEntriesSortedFact[0].date.getTime() : Number.MAX_SAFE_INTEGER,
                    timesheetsSortedFact.length > 0 ? timesheetsSortedFact[0].date.getTime() : Number.MAX_SAFE_INTEGER,
                    progressListSortedFact.length > 0 ? progressListSortedFact[0].date.getTime() : Number.MAX_SAFE_INTEGER
                ));

                result.maxFact = new Date(Math.max(
                    paymentsSortedFact.length > 0
                        ? paymentsSortedFact[paymentsSortedFact.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER,
                    invoicesSortedFact.length > 0
                        ? InvoiceModel.getMaxDate(invoicesSortedFact[invoicesSortedFact.length - 1]).getTime()
                        : Number.MIN_SAFE_INTEGER,
                    outcomeEntriesSortedFact.length > 0
                        ? outcomeEntriesSortedFact[outcomeEntriesSortedFact.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER,
                    timesheetsSortedFact.length > 0
                        ? timesheetsSortedFact[timesheetsSortedFact.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER,
                    progressListSortedFact.length > 0
                        ? progressListSortedFact[progressListSortedFact.length - 1].date.getTime()
                        : Number.MIN_SAFE_INTEGER
                ));
            }

            result.min = new Date(Math.min(
                result.minFact ? result.minFact.getTime() : Number.MAX_SAFE_INTEGER,
                result.minPlan ? result.minPlan.getTime() : Number.MAX_SAFE_INTEGER));
            result.max = new Date(Math.max(
                result.maxFact ? result.maxFact.getTime() : Number.MIN_SAFE_INTEGER,
                result.maxPlan ? result.maxPlan.getTime() : Number.MIN_SAFE_INTEGER));
        }

        const dateFromTmp: Date = new Date(result.min ? result.min.getTime() : Date.now());
        const dateToTmp: Date = new Date(result.max ? result.max.getTime() : Date.now());

        result.minDateFrom = moment([dateFromTmp.getFullYear(), dateFromTmp.getMonth(), 1]);
        result.maxDateTo = moment([dateToTmp.getFullYear(), dateToTmp.getMonth(), 1]).endOf('month');

        const today: Moment = moment();
        if (result.maxDateTo.isBefore(today)) {
            result.maxDateTo = today;
        }

        return result;
    }

    customSort(event: SortEvent): void {
        event.data.sort((data1: PsrLineModel, data2: PsrLineModel): number => {
            const typeResult: number = StaticService.compareVars(data1.key.type, data2.key.type);
            if (typeResult !== 0) {
                return typeResult;
            }

            const subGroupIdResult: number = StaticService.compareVars(data1.key.subGroupId, data2.key.subGroupId);
            if (subGroupIdResult) {
                return subGroupIdResult;
            }

            if (data1.key.isGroupTotal && !data2.key.isGroupTotal) {
                return -1;
            } else if (!data1.key.isGroupTotal && data2.key.isGroupTotal) {
                return 1;
            }

            if (data1.key.isSubGroupTotal && !data2.key.isSubGroupTotal) {
                return -1;
            } else if (!data1.key.isSubGroupTotal && data2.key.isSubGroupTotal) {
                return 1;
            }

            const finalResult = StaticService.compareVars(data1[event.field], data2[event.field]);
            return event.order * finalResult;
        });
    }

    preparePSR(invoices: ExtendedInvoiceModel[],
        outcomeEntries: Map<boolean, OutcomeEntryModel[]>,
        timesheets: Map<boolean, PsrTimesheet[]>,
        progressList: Map<boolean, ProgressModel[]>,
        weeksArray: PsrDateColumn[],
        users: UserSimpleModel[],
        customers: CustomerModel[],
        suppliers: SupplierModel[],
        subContractors: SubcontractorModel[],
        currencies: CurrencyModel[],
        departments: DepartmentModel[],
        currencyRates: CurrencyRate[],
        departmentAccessList: DepartmentAccess[],
        dateFrom: Date,
        dateTo: Date,
        contractDays: number,
        useTimesheetInternalRates: boolean
    ): PsrCalculationResult {
        weeksArray.forEach((dateColumn: PsrDateColumn) => {
            dateColumn.isInPeriod =
                dateFrom.getTime() <= dateColumn.weekEnd.getTime()
                && dateTo.getTime() >= dateColumn.weekStart.getTime();
        });

        const planSubResult: PsrCalculationSubResult = this.prepareRowsByData(
            invoices,
            outcomeEntries,
            timesheets,
            weeksArray,
            users,
            customers,
            suppliers,
            subContractors,
            currencies,
            departments,
            currencyRates,
            departmentAccessList,
            true,
            dateFrom,
            dateTo,
            contractDays,
            useTimesheetInternalRates);

        const factSubResult: PsrCalculationSubResult = this.prepareRowsByData(
            invoices,
            outcomeEntries,
            timesheets,
            weeksArray,
            users,
            customers,
            suppliers,
            subContractors,
            currencies,
            departments,
            currencyRates,
            departmentAccessList,
            false,
            dateFrom,
            dateTo,
            contractDays,
            useTimesheetInternalRates);

        this.prepareProgressByData(
            progressList.get(true),
            weeksArray,
            subContractors,
            departments,
            planSubResult,
            planSubResult);

        this.prepareProgressByData(
            progressList.get(false),
            weeksArray,
            subContractors,
            departments,
            planSubResult,
            factSubResult);

        const legend: PsrCalculationResultLegend = this.prepareLegend(
            planSubResult,
            factSubResult,
            weeksArray,
            dateTo);

        const totals: PsrLineModelTotal[] = this.prepareTotals(
            planSubResult,
            factSubResult,
            legend,
            dateFrom,
            dateTo);

        return {
            plan: planSubResult,
            fact: factSubResult,
            totals: totals,
            legend: legend
        };
    }

    exportData(
        data: PsrCalculationResult,
        timesheetCategory: TimesheetCategory,
        dateLimis: DateLimitsResult,
        genericFields: GenericFieldModel[]
    ): void {
        const previewSheetName: string = this.messageService.resolveMessage('ui.text.psr.mainFigures');
        const totalSheetName: string = this.messageService.resolveMessage('ui.text.psr.mode.summary');
        const planSheetName: string = this.messageService.resolveMessage('ui.text.psr.mode.plan');
        const factSheetName: string = this.messageService.resolveMessage('ui.text.psr.mode.fact');

        const workbook: XLSX.WorkBook = {
            Sheets: {
                [previewSheetName]: XLSX.utils.json_to_sheet(
                    this.convertToXlsxPreview(data, timesheetCategory, dateLimis, genericFields),
                    {skipHeader: true}),
                [totalSheetName]: XLSX.utils.json_to_sheet(this.convertToXlsxTotal(data.totals), {skipHeader: true}),
                [planSheetName]: XLSX.utils.json_to_sheet(this.convertToXlsxData(data.plan.rows, true), {skipHeader: true}),
                [factSheetName]: XLSX.utils.json_to_sheet(this.convertToXlsxData(data.fact.rows, false), {skipHeader: true}),
            },
            SheetNames: [previewSheetName, totalSheetName, planSheetName, factSheetName]
        };
        const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
        this.saveAsExcelFile(excelBuffer, timesheetCategory.name);
    }

    private saveAsExcelFile(buffer: any, fileName: string): void {
        const data: Blob = new Blob([buffer], {
            type: EXCEL_TYPE
        });
        saveAs(data, fileName + '_' + new Date().toLocaleString() + EXCEL_EXTENSION);
    }

    private convertToXlsxPreview(
        data: PsrCalculationResult,
        timesheetCategory: TimesheetCategory,
        dateLimis: DateLimitsResult,
        genericFields: GenericFieldModel[]
    ): PsrToXlsxPreview[] {
        const result: PsrToXlsxPreview[] = [];

        result.push({
            header: this.messageService.resolveMessage('ui.text.timesheetCategory.title'),
            value: timesheetCategory.name,
            description: timesheetCategory.description
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.timesheetCategory.managers'),
            value: this.userService.joinUserNames(timesheetCategory.managers),
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.projectStart')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            value: dateLimis && dateLimis.minPlan ? ConverterService.formatDate(dateLimis.minPlan) : ''
        });
        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.projectStart')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            value: dateLimis && dateLimis.minFact ? ConverterService.formatDate(dateLimis.minFact) : ''
        });
        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.projectFinish')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            value: dateLimis && dateLimis.maxPlan ? ConverterService.formatDate(dateLimis.maxPlan) : ''
        });
        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.projectFinish')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            value: dateLimis && dateLimis.maxFact ? ConverterService.formatDate(dateLimis.maxFact) : ''
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.progress.title')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            value: data.legend.progressPlan ? ConverterService.formatPercentage(data.legend.progressPlan) : ''
        });
        result.push({
            header: this.messageService.resolveMessage('ui.text.progress.title')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            value: data.legend.progressFact ? ConverterService.formatPercentage(data.legend.progressFact) : ''
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.contractValue'),
            value: data.plan.incomeSubTotalRow && data.plan.incomeSubTotalRow.totals && data.plan.incomeSubTotalRow.totals.amount2
                ? ConverterService.formatCurrency(data.plan.incomeSubTotalRow.totals.amount2)
                : '0',
            description: this.messageService.resolveMessage('ui.text.currency.default.code')
        });
        // TODO: add original currency

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.expectedProfitLoss'),
            value: ConverterService.formatCurrency(data.legend.expectedProfitLoss),
            description: ConverterService.formatPercentage(data.legend.expectedProfitLossPercentage)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.budgetAmount'),
            value: ConverterService.formatCurrency(data.legend.budgetAmount)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.spentAmount'),
            value: ConverterService.formatCurrency(data.legend.spentAmount)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.budgetSpent'),
            value: ConverterService.formatPercentage(data.legend.budgetSpent)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.estimatedToComplete'),
            value: ConverterService.formatCurrency(data.legend.estimatedToComplete)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.forecastSpentAtCompletion'),
            value: ConverterService.formatCurrency(data.legend.forecastSpentAtCompletion)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.forecastProfitLoss'),
            value: ConverterService.formatCurrency(data.legend.forecastProfitLoss),
            description: ConverterService.formatPercentage(data.legend.forecastProfitLossPercentage)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.invoiceAmount'),
            value: ConverterService.formatCurrency(data.legend.invoiceAmount),
            description: this.messageService.resolveMessage('ui.text.currency.default.code')
        });
        // TODO: add original currency

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.invoiceOutstanding'),
            value: ConverterService.formatCurrency(data.legend.invoiceOutstanding),
            description: this.messageService.resolveMessage('ui.text.currency.default.code')
        });
        // TODO: add original currency

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.invoicePaid'),
            value: data.fact.incomeSubTotalRow && data.fact.incomeSubTotalRow.totals && data.fact.incomeSubTotalRow.totals.amount
                ? ConverterService.formatCurrency(data.fact.incomeSubTotalRow.totals.amount)
                : '0',
            description: this.messageService.resolveMessage('ui.text.currency.default.code')
        });
        // TODO: add original currency

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.invoiceToBeInvoiced'),
            value: ConverterService.formatCurrency(data.legend.invoiceToBeInvoiced),
            description: this.messageService.resolveMessage('ui.text.currency.default.code')
        });
        // TODO: add original currency

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.totalDaysOverdue'),
            value: data.legend.totalDaysOverdue.toString()
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.psr.maxDaysOverdue'),
            value: data.legend.maxDaysOverdue.toString()
        });

        genericFields.forEach((genericField: GenericFieldModel) => {
            const match: GenericFieldValue = timesheetCategory.genericFieldValues
                    .find((genericFieldValue: GenericFieldValue) => genericFieldValue.genericFieldId === genericField.id);
            let value: string = match ? match.value : '';

            switch (StaticService.resolveEnumOnEntity(genericField.type, GenericFieldType)) {
                case GenericFieldType.BOOLEAN: {
                    value = this.messageService.resolveMessage(value.toLowerCase() === 'true'
                        ? 'ui.text.default.yes'
                        : 'ui.text.default.no');
                    break;
                }
                case GenericFieldType.DATE: {
                    value = ConverterService.guessMoment(value).format(DEFAULT_DATE_FORMAT);
                    break;
                }
                default: {
                    break;
                }
            }

            result.push({
                header: genericField.name,
                value: value
            });
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.timesheetCategory.initialBudget'),
            value: ConverterService.formatCurrency(timesheetCategory.initialBudget)
        });

        result.push({
            header: this.messageService.resolveMessage('ui.text.timesheetCategory.contractDays'),
            value: timesheetCategory.contractDays.toString()
        });

        return result;
    }

    private convertToXlsxTotal(data: PsrLineModelTotal[]): PsrToXlsxTotal[] {
        this.customSort({data, order: 0});
        const result: PsrToXlsxTotal[] = data.map((entity: PsrLineModelTotal): PsrToXlsxTotal => ({
            company: PsrService.getCompanyColumnForExport(entity),
            planProjectTotal: ConverterService.formatCurrency(entity.totalsPlan.amount)
                + ConverterService.getPercentagePrefix(entity),
            factProjectTotal: ConverterService.formatCurrency(entity.totalsFact.amount)
                + ConverterService.getPercentagePrefix(entity),
            planPeriodTotal: ConverterService.formatCurrency(entity.totalsInPeriodPlan.amount)
                + ConverterService.getPercentagePrefix(entity),
            factPeriodTotal: ConverterService.formatCurrency(entity.totalsInPeriodFact.amount)
                + ConverterService.getPercentagePrefix(entity),
        }));

        result.unshift({
            company: this.messageService.resolveMessage('ui.text.psr.company'),
            planProjectTotal: this.messageService.resolveMessage('ui.text.psr.mode.plan')
                + ' ' + this.messageService.resolveMessage('ui.text.psr.projectTotal'),
            factProjectTotal: this.messageService.resolveMessage('ui.text.psr.mode.fact')
                + ' ' + this.messageService.resolveMessage('ui.text.psr.projectTotal'),
            planPeriodTotal: this.messageService.resolveMessage('ui.text.psr.mode.plan')
                + ' ' + this.messageService.resolveMessage('ui.text.psr.periodTotal'),
            factPeriodTotal: this.messageService.resolveMessage('ui.text.psr.mode.fact')
                + ' ' + this.messageService.resolveMessage('ui.text.psr.periodTotal'),
        });

        return result;
    }

    private convertToXlsxData(data: PsrLineModel[], isPlan: boolean): PsrToXlsx[] {
        const weekTranslation: string = this.messageService.resolveMessage('ui.text.default.week');

        this.customSort({data, order: 0});
        const result: PsrToXlsx[] = data.map((entity: PsrLineModel): PsrToXlsx => {
            const invoiced: number = entity.totalsInPeriod && (
                entity.key.type === PsrLineType.INCOME
                ? entity.totalsInPeriod.amount2
                : (PsrLineType.TIMESHEET === entity.key.type ? entity.totalsInPeriod.units : 0));

            const rowResult: PsrToXlsx = {
                company: PsrService.getCompanyColumnForExport(entity),
                invoiced: ConverterService.formatCurrency(invoiced),
                paid: ConverterService.formatCurrency((entity.totalsInPeriod && entity.totalsInPeriod.amount))
                    + ConverterService.getPercentagePrefix(entity)
            };

            Array.from(data[0].weeks.keys()).forEach((col: PsrDateColumn) => {
                const item: PsrLineItem = entity.weeks.get(col);
                if (item) {
                    const weekValue: number = item && PsrLineType.TIMESHEET === entity.key.type ? item.units : item.amount;
                    rowResult[col.weekStart.toString()] = weekValue
                        ? (ConverterService.formatCurrency(weekValue) + ConverterService.getPercentagePrefix(entity))
                        : '';
                }
            });

            return rowResult;
        });

        const weekRow: PsrToXlsx = {
            company: '',
            invoiced: '',
            paid: ''
        };

        const headerRow: PsrToXlsx = {
            company: this.messageService.resolveMessage('ui.text.psr.company'),
            invoiced: this.messageService.resolveMessage(isPlan ? 'ui.text.psr.toBeInvoiced' : 'ui.text.psr.invoiced'),
            paid: this.messageService.resolveMessage(isPlan ? 'ui.text.psr.toBePaid' : 'ui.text.psr.paid')
        };

        Array.from(data[0].weeks.keys()).forEach((col: PsrDateColumn) => {
            headerRow[col.weekStart.toString()] =
                `${ConverterService.formatDate(col.weekStart)} ${ConverterService.formatDate(col.weekEnd)}`;
            weekRow[col.weekStart.toString()] = `${weekTranslation} ${col.week}`;
        });

        result.unshift(headerRow);
        result.unshift(weekRow);

        return result;
    }

    private prepareRowsByData(
        invoices: ExtendedInvoiceModel[],
        $outcomeEntries: Map<boolean, OutcomeEntryModel[]>,
        $timesheets: Map<boolean, PsrTimesheet[]>,
        weeksArray: PsrDateColumn[],
        users: UserSimpleModel[],
        customers: CustomerModel[],
        suppliers: SupplierModel[],
        subContractors: SubcontractorModel[],
        currencies: CurrencyModel[],
        departments: DepartmentModel[],
        currencyRates: CurrencyRate[],
        departmentAccessList: DepartmentAccess[],
        isForPlan: boolean,
        dateFrom: Date,
        dateTo: Date,
        contractDays: number,
        useTimesheetInternalRates: boolean
    ): PsrCalculationSubResult {
        const result: PsrCalculationSubResult = new PsrCalculationSubResult();
        const outcomeEntries: OutcomeEntryModel[] = $outcomeEntries.get(isForPlan);
        const timesheets: PsrTimesheet[] = $timesheets.get(isForPlan);

        if (outcomeEntries
            && outcomeEntries.length > 0
            || timesheets
            && timesheets.length > 0
            || invoices
            && invoices.length
        ) {
            const totalRow: PsrLineModel = this.generateTotalRow(PsrLineType.TOTAL);
            result.rows.push(totalRow);
            result.totalRow = totalRow;

            if (outcomeEntries && outcomeEntries.length > 0) {
                const outcomeSubTotalRow: PsrLineModel = this.generateTotalRow(PsrLineType.OUTCOME);
                result.rows.push(outcomeSubTotalRow);
                result.outcomeSubTotalRow = outcomeSubTotalRow;

                const outcomeEntryTypeGroup: Map<OutcomeEntryType, OutcomeEntryModel[]> = StaticService.groupBy(
                    outcomeEntries,
                    (outcomeEntry: OutcomeEntryModel): OutcomeEntryType => this.parseOutcomeType(outcomeEntry));

                Array.from(outcomeEntryTypeGroup.keys()).forEach((type: OutcomeEntryType) => {
                    const parents: OutcomeEntryParent[] = type === OutcomeEntryType.SUB_CONTRACTOR ? subContractors : suppliers;
                    const typeSubTotalRow: PsrLineModel = this.generateTotalRow(PsrLineType.OUTCOME, type);
                    typeSubTotalRow.name += this.messageService.resolveMessage('enum.OutcomeEntryType.' + OutcomeEntryType[type]);

                    const outcomeEntryParentIdGroup: Map<number, OutcomeEntryModel[]> = StaticService.groupBy(
                        outcomeEntryTypeGroup.get(type),
                        (outcomeEntry: OutcomeEntryModel): number => {
                            return outcomeEntry.parentId;
                        });

                    Array.from(outcomeEntryParentIdGroup.values()).forEach((_outcomeEntriesByParentId: OutcomeEntryModel[]) => {
                        const parent: OutcomeEntryParent = parents.find(item => item.id === _outcomeEntriesByParentId[0].parentId);

                        const outcomeItem: PsrLineModel = this.processOutcomeEntry(
                            _outcomeEntriesByParentId,
                            parent,
                            currencies,
                            currencyRates,
                            weeksArray,
                            typeSubTotalRow,
                            outcomeSubTotalRow,
                            totalRow
                        );
                        result.rows.push(outcomeItem);

                        if (type === OutcomeEntryType.SUB_CONTRACTOR
                            && (<SubcontractorModel>parent).progressReportable
                            && outcomeItem.totals
                        ) {
                            result.totalSubContractorProgressAmount += outcomeItem.totals.amount;
                        }
                    });

                    typeSubTotalRow.totals = this.processTotalColumn(typeSubTotalRow.weeks);
                    typeSubTotalRow.totalsInPeriod = this.processTotalColumn(typeSubTotalRow.weeksInPeriod);
                    result.rows.push(typeSubTotalRow);
                });
                outcomeSubTotalRow.totals = this.processTotalColumn(outcomeSubTotalRow.weeks);
                outcomeSubTotalRow.totalsInPeriod = this.processTotalColumn(outcomeSubTotalRow.weeksInPeriod);
            }

            if (timesheets && timesheets.length) {
                const timesheetSubTotalRow: PsrLineModel = this.generateTotalRow(PsrLineType.TIMESHEET);
                result.rows.push(timesheetSubTotalRow);
                result.timesheetSubTotalRow = timesheetSubTotalRow;

                const timesheetDepartmentGroups: Map<number, PsrTimesheet[]> = StaticService.groupBy(timesheets,
                    (timesheet: PsrTimesheet): number => {
                        return timesheet.departmentId;
                    });

                Array.from(timesheetDepartmentGroups.values()).forEach((_departmentTimesheets:  PsrTimesheet[]) => {
                    const department: DepartmentModel = departments
                        .find(_department => _department.id === _departmentTimesheets[0].departmentId);
                    const departmentAccess: DepartmentAccess = departmentAccessList
                        .find(_departmentAccess => _departmentAccess.departmentId === _departmentTimesheets[0].departmentId);
                    const departmentSubTotalRow: PsrLineModel = this.generateTotalRow(PsrLineType.TIMESHEET, department.id);
                    departmentSubTotalRow.name += department.name;

                    const timesheetUserGroups: Map<number, PsrTimesheet[]> = StaticService.groupBy(_departmentTimesheets,
                        (timesheet: PsrTimesheet): number => {
                            return timesheet.userId;
                        });

                    Array.from(timesheetUserGroups.values()).forEach((_userTimesheets:  PsrTimesheet[]) => {
                        result.rows.push(this.proccessTimesheets(
                            _userTimesheets,
                            users,
                            weeksArray,
                            department,
                            departmentSubTotalRow,
                            timesheetSubTotalRow,
                            totalRow,
                            useTimesheetInternalRates));
                    });

                    departmentSubTotalRow.totals = this.processTotalColumn(departmentSubTotalRow.weeks);
                    departmentSubTotalRow.totalsInPeriod = this.processTotalColumn(departmentSubTotalRow.weeksInPeriod);
                    result.rows.push(departmentSubTotalRow);

                    if (departmentAccess) {
                        result.totalTimesheetProgressAmount += departmentSubTotalRow.totals.amount;
                    }
                });

                timesheetSubTotalRow.totals = this.processTotalColumn(timesheetSubTotalRow.weeks);
                timesheetSubTotalRow.totalsInPeriod = this.processTotalColumn(timesheetSubTotalRow.weeksInPeriod);
            }

            if ((outcomeEntries && outcomeEntries.length > 0) || (timesheets && timesheets.length)) {
                const outcomeTotalRow = this.generateTotalRow(PsrLineType.TOTAL);
                outcomeTotalRow.key.isSubGroupTotal = true;
                outcomeTotalRow.name = this.messageService.resolveMessage('ui.text.psr.projectOutcome');
                totalRow.weeks.forEach((value, key) => {
                    outcomeTotalRow.weeks.set(key, clone(value));
                });
                totalRow.weeksInPeriod.forEach((value, key) => {
                    outcomeTotalRow.weeksInPeriod.set(key, clone(value));
                });
                this.postProcessTotalRow(outcomeTotalRow);
                result.rows.push(outcomeTotalRow);
                result.outcomeTotalRow = outcomeTotalRow;
            }

            if (invoices && invoices.length) {
                const incomeSubTotalRow: PsrLineModel = this.generateTotalRow(PsrLineType.INCOME);
                incomeSubTotalRow.totals = new PsrLineTotal();
                incomeSubTotalRow.totalsInPeriod = new PsrLineTotal();
                result.rows.push(incomeSubTotalRow);
                result.incomeSubTotalRow = incomeSubTotalRow;

                invoices.forEach((invoice: ExtendedInvoiceModel) => {
                    result.rows.push(this.processIncome(
                        invoice,
                        customers,
                        currencies,
                        currencyRates,
                        weeksArray,
                        incomeSubTotalRow,
                        totalRow,
                        isForPlan,
                        dateFrom,
                        dateTo,
                        contractDays));
                });

                incomeSubTotalRow.totals = this.processTotalColumn(incomeSubTotalRow.weeks, incomeSubTotalRow.totals);
                incomeSubTotalRow.totalsInPeriod = this.processTotalColumn(
                    incomeSubTotalRow.weeksInPeriod,
                    incomeSubTotalRow.totalsInPeriod);
            }

            this.postProcessTotalRow(totalRow);
        }
        return result;
    }

    private prepareProgressByData(
        progressList: ProgressModel[],
        weeksArray: PsrDateColumn[],
        subContractors: SubcontractorModel[],
        departments: DepartmentModel[],
        planSubResult: PsrCalculationSubResult,
        result: PsrCalculationSubResult
    ): void {
        const progressSubTotalRow: PsrLineModel = this.generateTotalRow(PsrLineType.PROGRESS);
        progressSubTotalRow.totals = new PsrLineTotal();
        progressSubTotalRow.totalsInPeriod = new PsrLineTotal();

        result.rows.push(progressSubTotalRow);
        result.progressSubTotalRow = progressSubTotalRow;

        if (progressList && progressList.length > 0) {
            const progressTypeGroups: Map<ProgressType, PsrTimesheet[]> = StaticService.groupBy(
                progressList,
                (progress: ProgressModel): number => {
                    return this.parseProgressType(progress);
                });

            Array.from(progressTypeGroups.keys()).forEach((type: ProgressType) => {
                let subGroupSubTotalAmount: number;
                const subGroupSubTotalRow: PsrLineModel = this.generateTotalRow(PsrLineType.PROGRESS, type);
                subGroupSubTotalRow.totals = new PsrLineTotal();
                subGroupSubTotalRow.totalsInPeriod = new PsrLineTotal();
                subGroupSubTotalRow.name = this.messageService.resolveMessage(
                    ProgressType.DEPARTMENT === type
                    ? 'ui.text.department.title'
                    : 'ui.text.subcontractor.title');
                result.rows.push(subGroupSubTotalRow);

                let parents: ProgressParent[];
                switch (type) {
                    case ProgressType.SUB_CONTRACTOR: {
                        result.progressSubContractorSubTotalRow = subGroupSubTotalRow;
                        subGroupSubTotalAmount = planSubResult.totalSubContractorProgressAmount;
                        parents = subContractors;
                        break;
                    }
                    case ProgressType.DEPARTMENT: {
                        result.progressTimesheetSubTotalRow = subGroupSubTotalRow;
                        subGroupSubTotalAmount = planSubResult.totalTimesheetProgressAmount;
                        parents = departments;
                        break;
                    }
                    default: {
                        throw Error('Unexpected type');
                    }
                }

                const progressParentIdGroup: Map<number, ProgressModel[]> = StaticService.groupBy(
                    progressTypeGroups.get(type),
                    (progress: ProgressModel): number => progress.parentId);

                Array.from(progressParentIdGroup.values()).forEach((progressListByParentId: ProgressModel[]) => {
                    const naturalResultRow: PsrLineModel = planSubResult.rows.find((_naturalResultRow: PsrLineModel) => {
                        switch (type) {
                            case ProgressType.DEPARTMENT: {
                                return _naturalResultRow.key.isGroupTotal
                                    && _naturalResultRow.key.isSubGroupTotal
                                    && _naturalResultRow.key.type === PsrLineType.TIMESHEET
                                    && _naturalResultRow.key.subGroupId === progressListByParentId[0].parentId;
                            }
                            case ProgressType.SUB_CONTRACTOR: {
                                return _naturalResultRow.key.subGroupId === OutcomeEntryType.SUB_CONTRACTOR
                                    && _naturalResultRow.key.type === PsrLineType.OUTCOME
                                    && _naturalResultRow.key.id === progressListByParentId[0].parentId;
                            }
                            default: {
                                throw Error('Unexpected Type');
                            }
                        }
                    });

                    result.rows.push(this.processProgressList(
                        progressListByParentId,
                        weeksArray,
                        parents,
                        progressSubTotalRow,
                        subGroupSubTotalRow,
                        naturalResultRow,
                        type));
                });

                this.postProccessProgressSubTotalRow(subGroupSubTotalRow, subGroupSubTotalAmount);
            });
        }

        const totalProgressAmount: number = planSubResult.totalSubContractorProgressAmount + planSubResult.totalTimesheetProgressAmount;
        this.postProccessProgressSubTotalRow(progressSubTotalRow, totalProgressAmount);
    }

    private prepareLegend(
        planSubResult: PsrCalculationSubResult,
        factSubResult: PsrCalculationSubResult,
        weeksArray: PsrDateColumn[],
        dateTo: Date
    ): PsrCalculationResultLegend {
        const legend: PsrCalculationResultLegend = new PsrCalculationResultLegend();

        const today: Date = new Date();
        const currentDateColumn: PsrDateColumn = weeksArray.find((dateColumn: PsrDateColumn) => {
            return today.getTime() <= dateColumn.weekEnd.getTime()
                && today.getTime() >= dateColumn.weekStart.getTime();
        });

        // progress
        legend.progressFact = factSubResult.progressSubTotalRow.totals.amount;
        legend.progressPlan = currentDateColumn
            && planSubResult.progressSubTotalRow
            && planSubResult.progressSubTotalRow.weeks
            && planSubResult.progressSubTotalRow.weeks.get(currentDateColumn)
            ? planSubResult.progressSubTotalRow.weeks.get(currentDateColumn).amount
            : planSubResult.progressSubTotalRow.totals.amount;

        // budgetAmount
        if (planSubResult.outcomeSubTotalRow) {
            legend.budgetAmount += planSubResult.outcomeSubTotalRow.totals.amount;
            legend.budgetAmountInPeriod += planSubResult.outcomeSubTotalRow.totalsInPeriod.amount;
        }
        if (planSubResult.timesheetSubTotalRow) {
            legend.budgetAmount += planSubResult.timesheetSubTotalRow.totals.amount;
            legend.budgetAmountInPeriod += planSubResult.timesheetSubTotalRow.totalsInPeriod.amount;
        }

        // expectedProfitLoss
        if (
            factSubResult.incomeSubTotalRow
            && factSubResult.incomeSubTotalRow.totals
            && factSubResult.incomeSubTotalRow.totals.amount2
        ) {
            legend.expectedProfitLoss += factSubResult.incomeSubTotalRow.totals.amount2;
            legend.expectedProfitLossInPeriod += factSubResult.incomeSubTotalRow.totalsInPeriod.amount2;
        }
        legend.expectedProfitLoss -= legend.budgetAmount;
        legend.expectedProfitLossInPeriod -= legend.budgetAmountInPeriod;
        // expectedProfitLossPercentage
        if (planSubResult.incomeSubTotalRow && planSubResult.incomeSubTotalRow.totals) {
            if (planSubResult.incomeSubTotalRow.totals.amount2) {
                legend.expectedProfitLossPercentage = legend.expectedProfitLoss / planSubResult.incomeSubTotalRow.totals.amount2 * 100;
            }
            if (planSubResult.incomeSubTotalRow.totalsInPeriod.amount2) {
                legend.expectedProfitLossPercentageInPeriod =
                    legend.expectedProfitLossInPeriod / planSubResult.incomeSubTotalRow.totalsInPeriod.amount2 * 100;
            }
        }

        // spentAmount
        if (factSubResult.outcomeSubTotalRow) {
            legend.spentAmount += factSubResult.outcomeSubTotalRow.totals.amount;
            legend.spentAmountinPeriod += factSubResult.outcomeSubTotalRow.totalsInPeriod.amount;
        }
        if (factSubResult.timesheetSubTotalRow) {
            legend.spentAmount += factSubResult.timesheetSubTotalRow.totals.amount;
            legend.spentAmountinPeriod += factSubResult.timesheetSubTotalRow.totalsInPeriod.amount;
        }

        // budgetSpent
        if (legend.spentAmount && legend.budgetAmount) {
            legend.budgetSpent = legend.spentAmount / legend.budgetAmount * 100;
        }

        // estimatedToComplete
        if (planSubResult.outcomeSubTotalRow) {
            legend.estimatedToComplete += planSubResult.outcomeSubTotalRow.totals.amount;
        }
        if (factSubResult.outcomeSubTotalRow) {
            legend.estimatedToComplete -= factSubResult.outcomeSubTotalRow.totals.amount;
        }

        let estimatedToCompleteProgress: number = 0;
        if (factSubResult.progressTimesheetSubTotalRow) {
            const mToday: Moment = moment();
            const matchWeek: PsrDateColumn = Array.from(factSubResult.progressTimesheetSubTotalRow.weeks.keys())
                .find((week: PsrDateColumn) => moment(week.weekStart).isBefore(mToday) && moment(week.weekEnd).isAfter(mToday));

            if (matchWeek && factSubResult.progressTimesheetSubTotalRow.weeks.get(matchWeek)) {
                estimatedToCompleteProgress =
                    ConverterService.roundCurrency(factSubResult.progressTimesheetSubTotalRow.weeks.get(matchWeek).amount);
            }

            if (
                planSubResult.progressTimesheetSubTotalRow
                && (!estimatedToCompleteProgress || estimatedToCompleteProgress > 100)
                && planSubResult.progressTimesheetSubTotalRow.weeks.get(matchWeek)
            ) {
                estimatedToCompleteProgress =
                    ConverterService.roundCurrency(planSubResult.progressTimesheetSubTotalRow.weeks.get(matchWeek).amount);
            }
        }
        legend.estimatedToComplete += ((100 - estimatedToCompleteProgress) * (planSubResult.totalTimesheetProgressAmount || 0) / 100);
        if (legend.estimatedToComplete < 0) {
            legend.estimatedToComplete = 0;
        }

        // estimatedToCompleteInPeriod
        if (planSubResult.outcomeSubTotalRow) {
            legend.estimatedToCompleteInPeriod += planSubResult.outcomeSubTotalRow.totalsInPeriod.amount;
        }
        if (factSubResult.outcomeSubTotalRow) {
            legend.estimatedToCompleteInPeriod -= factSubResult.outcomeSubTotalRow.totalsInPeriod.amount;
        }

        let estimatedToCompleteProgressInPeriod: number = 0;
        if (factSubResult.progressTimesheetSubTotalRow) {
            const lastDayOfPeriod: Moment = moment(dateTo);
            const matchWeek: PsrDateColumn = Array.from(factSubResult.progressTimesheetSubTotalRow.weeksInPeriod.keys())
                .find((week: PsrDateColumn) =>
                    moment(week.weekStart).isBefore(lastDayOfPeriod) && moment(week.weekEnd).isAfter(lastDayOfPeriod));

            if (matchWeek && factSubResult.progressTimesheetSubTotalRow.weeksInPeriod.get(matchWeek)) {
                estimatedToCompleteProgressInPeriod =
                    ConverterService.roundCurrency(factSubResult.progressTimesheetSubTotalRow.weeksInPeriod.get(matchWeek).amount);
            }

            if (
                planSubResult.progressTimesheetSubTotalRow
                && (!estimatedToCompleteProgressInPeriod || estimatedToCompleteProgressInPeriod > 100)
                && planSubResult.progressTimesheetSubTotalRow.weeksInPeriod.get(matchWeek)
            ) {
                estimatedToCompleteProgressInPeriod =
                    ConverterService.roundCurrency(planSubResult.progressTimesheetSubTotalRow.weeksInPeriod.get(matchWeek).amount);
            }
        }
        legend.estimatedToCompleteInPeriod += ((100 - estimatedToCompleteProgressInPeriod)
            * (planSubResult.totalTimesheetProgressAmount || 0) / 100);
        if (legend.estimatedToCompleteInPeriod < 0) {
            legend.estimatedToCompleteInPeriod = 0;
        }

        // forecastSpentAtCompletion
        legend.forecastSpentAtCompletion += legend.estimatedToComplete + legend.spentAmount;
        legend.forecastSpentAtCompletionInPeriod += legend.estimatedToCompleteInPeriod + legend.spentAmountinPeriod;

        // forecastProfitLoss
        if (
            factSubResult.incomeSubTotalRow
            && factSubResult.incomeSubTotalRow.totals
            && factSubResult.incomeSubTotalRow.totals.amount2
        ) {
            legend.forecastProfitLoss += factSubResult.incomeSubTotalRow.totals.amount2;
            legend.forecastProfitLossInPeriod += factSubResult.incomeSubTotalRow.totalsInPeriod.amount2;
        }
        legend.forecastProfitLoss -= legend.estimatedToComplete + legend.spentAmount;
        legend.forecastProfitLossInPeriod -= legend.estimatedToCompleteInPeriod + legend.spentAmountinPeriod;

        // forecastProfitLossPercentage
        if (factSubResult.incomeSubTotalRow && factSubResult.incomeSubTotalRow.totals) {
            if (factSubResult.incomeSubTotalRow.totals.amount2 && legend.forecastProfitLoss) {
                legend.forecastProfitLossPercentage = legend.forecastProfitLoss / factSubResult.incomeSubTotalRow.totals.amount2 * 100;
            }
            if (legend.forecastProfitLossInPeriod && factSubResult.incomeSubTotalRow.totalsInPeriod.amount2) {
                legend.forecastProfitLossPercentageInPeriod =
                    legend.forecastProfitLossInPeriod / factSubResult.incomeSubTotalRow.totalsInPeriod.amount2 * 100;
            }
        }

        return legend;
    }

    private prepareTotals(
        planSubResult: PsrCalculationSubResult,
        factSubResult: PsrCalculationSubResult,
        legend: PsrCalculationResultLegend,
        dateFrom: Date,
        dateTo: Date,
    ): PsrLineModelTotal[] {
        // These records are from PsrMode.SUMMARY
        const totals: PsrLineModelTotal[] = [];
        planSubResult.rows.forEach((model: PsrLineModel) => {
            const total: PsrLineModelTotal = {
                key: model.key,
                name: model.name,
                description: model.description,
                additionalDescription: model.additionalDescription,
                invoice: model.invoice,
                // no daysOverdue for Plan
                invoiceDaysOverdue: 0,
                totalsPlan: model.totals,
                totalsInPeriodPlan: model.totalsInPeriod,
                totalsFact: new PsrLineTotal(),
                totalsInPeriodFact: new PsrLineTotal()
            };

            totals.push(total);
        });

        factSubResult.rows.forEach((model: PsrLineModel) => {
            // this is Invoice row
            if (model.key.isGroupTotal === false && model.key.isSubGroupTotal === false && model.invoice) {
                if (model.invoice.datePosted) {
                    legend.invoiceAmount += model.totals.amount2;
                    if (
                        dateFrom.getTime() <= model.invoice.datePosted.getTime()
                        && dateTo.getTime() >= model.invoice.datePosted.getTime()
                    ) {
                        legend.invoiceAmountInPeriod += model.totals.amount2;
                    }
                }
                if (model.invoiceDaysOverdue) {
                    legend.totalDaysOverdue += model.invoiceDaysOverdue;
                    legend.maxDaysOverdue = Math.max(legend.maxDaysOverdue, model.invoiceDaysOverdue);
                }
            }

            const $model: PsrLineModelTotal = totals.find((_model: PsrLineModelTotal) => {
                return _model.key.id === model.key.id
                    && _model.key.type === model.key.type
                    && _model.key.isGroupTotal === model.key.isGroupTotal
                    && _model.key.isSubGroupTotal === model.key.isSubGroupTotal
                    && _model.key.subGroupId === model.key.subGroupId
                    && ((!_model.invoice && !model.invoice)
                    || (_model.invoice && model.invoice && _model.invoice.id === model.invoice.id));
            });

            if (!!$model) {
                $model.totalsFact = model.totals;
                $model.totalsInPeriodFact = model.totalsInPeriod;
                $model.invoiceDaysOverdue = model.invoiceDaysOverdue;
            } else {
                const total: PsrLineModelTotal = {
                    key: model.key,
                    name: model.name,
                    description: model.description,
                    additionalDescription: model.additionalDescription,
                    invoice: model.invoice,
                    invoiceDaysOverdue: model.invoiceDaysOverdue,
                    totalsPlan: new PsrLineTotal(),
                    totalsInPeriodPlan: new PsrLineTotal(),
                    totalsFact: model.totals,
                    totalsInPeriodFact: model.totalsInPeriod,
                };

                totals.push(total);
            }
        });

        // invoiceOutstanding
        legend.invoiceOutstanding += legend.invoiceAmount;
        if (factSubResult.incomeSubTotalRow && factSubResult.incomeSubTotalRow.totals && factSubResult.incomeSubTotalRow.totals.amount) {
            legend.invoiceOutstanding -= factSubResult.incomeSubTotalRow.totals.amount;
        }

        // invoiceToBeInvoiced
        if (factSubResult.incomeSubTotalRow && factSubResult.incomeSubTotalRow.totals && factSubResult.incomeSubTotalRow.totals.amount2) {
            legend.invoiceToBeInvoiced += factSubResult.incomeSubTotalRow.totals.amount2;
        }
        legend.invoiceToBeInvoiced -= legend.invoiceAmount;

        return totals;
    }

    private groupIncome(
        invoice: ExtendedInvoiceModel,
        weeksArray: PsrDateColumn[],
        currencies: CurrencyModel[],
        currencyRates: CurrencyRate[],
        subTotalRow: PsrLineModel,
        totalRow: PsrLineModel,
        isForPlan: boolean
    ): GroupedWeeks {
        const result: GroupedWeeks = new GroupedWeeks();
        const invoiceDate: Date = InvoiceModel.getDate(invoice, isForPlan);

        for (let i: number = 0; i < weeksArray.length; i++) {
            const currentWeek: PsrDateColumn = weeksArray[i];
            const item: PsrLineItem = new PsrLineItem();

            invoice.paymentDTOS.forEach((payment: PaymentModel) => {
                if (isForPlan === payment.isForPlan
                    && payment.date.getTime() >= currentWeek.weekStart.getTime()
                    && payment.date.getTime() <= currentWeek.weekEnd.getTime()
                ) {
                    item.entries.push(payment);
                    const rate: number = this.findRate(payment.currencyId, payment.date, currencies, currencyRates);
                    item.amount += payment.amount * rate;

                    if (payment.description) {
                        item.description += (item.description ? DESCRIPTION_SEPARATOR : '') + payment.description;
                    }
                }
            });

            item.hasInvoice = invoiceDate.getTime() >= currentWeek.weekStart.getTime()
                && invoiceDate.getTime() <= currentWeek.weekEnd.getTime();

            this.processSubTotalRow(subTotalRow, item, currentWeek);
            this.processTotalRow(totalRow, item, currentWeek, true);

            result.weeks.set(currentWeek, item);

            if (currentWeek.isInPeriod) {
                result.weeksInPeriod.set(currentWeek, item);
            }
        }

        return result;
    }

    private groupOutcomeEntries(
        outcomeEntries: OutcomeEntryModel[],
        weeksArray: PsrDateColumn[],
        currencies: CurrencyModel[],
        currencyRates: CurrencyRate[],
        subGroupSubTotalRow: PsrLineModel,
        subTotalRow: PsrLineModel,
        totalRow: PsrLineModel
    ): GroupedWeeks {
        const result: GroupedWeeks = new GroupedWeeks();

        for (let i: number = 0; i < weeksArray.length; i++) {
            const currentWeek: PsrDateColumn = weeksArray[i];
            const item: PsrLineItem = new PsrLineItem();

            outcomeEntries.forEach((outcomeEntry: OutcomeEntryModel) => {
                if (outcomeEntry.date.getTime() >= currentWeek.weekStart.getTime()
                    && outcomeEntry.date.getTime() <= currentWeek.weekEnd.getTime()
                ) {
                    item.entries.push(outcomeEntry);
                    const rate: number = this.findRate(outcomeEntry.currencyId, outcomeEntry.date, currencies, currencyRates);
                    item.amount += outcomeEntry.amount * rate;

                    if (outcomeEntry.description) {
                        item.description += (item.description ? DESCRIPTION_SEPARATOR : '') + outcomeEntry.description;
                    }
                }
            });

            this.processSubTotalRow(subGroupSubTotalRow, item, currentWeek);
            this.processSubTotalRow(subTotalRow, item, currentWeek);
            this.processTotalRow(totalRow, item, currentWeek, false);

            result.weeks.set(currentWeek, item);

            if (currentWeek.isInPeriod) {
                result.weeksInPeriod.set(currentWeek, item);
            }
        }

        return result;
    }

    private groupTimesheets(
        timesheets: PsrTimesheet[],
        weeksArray: PsrDateColumn[],
        subGroupSubTotalRow: PsrLineModel,
        subTotalRow: PsrLineModel,
        totalRow: PsrLineModel,
        useTimesheetInternalRates: boolean
    ): GroupedWeeks {
        const result: GroupedWeeks = new GroupedWeeks();

        for (let i: number = 0; i < weeksArray.length; i++) {
            const currentWeek: PsrDateColumn = weeksArray[i];
            const item: PsrLineItem = new PsrLineItem();

            timesheets.forEach((timesheet: PsrTimesheet) => {
                if (timesheet.date.getTime() >= currentWeek.weekStart.getTime()
                    && timesheet.date.getTime() <= currentWeek.weekEnd.getTime()
                ) {
                    item.entries.push(timesheet);
                    item.units += timesheet.amount;

                    if (timesheet.comment) {
                        item.description += (item.description ? DESCRIPTION_SEPARATOR : '') + timesheet.comment;
                    }
                }
            });
            item.amount = item.units * (useTimesheetInternalRates ? timesheets[0].internalRate : timesheets[0].rate);

            this.processSubTotalRow(subGroupSubTotalRow, item, currentWeek);
            this.processSubTotalRow(subTotalRow, item, currentWeek);
            this.processTotalRow(totalRow, item, currentWeek, false);

            result.weeks.set(currentWeek, item);

            if (currentWeek.isInPeriod) {
                result.weeksInPeriod.set(currentWeek, item);
            }
        }

        return result;
    }

    private groupProgressList(
        progressList: ProgressModel[],
        weeksArray: PsrDateColumn[],
        subTotalRow: PsrLineModel,
        subGroupSubTotalRow: PsrLineModel,
        naturalResultRow: PsrLineModel
    ): GroupedWeeks {
        const result: GroupedWeeks = new GroupedWeeks();
        let fallbackAmount: number = 0;
        let subGroupFallbackAmount: number = 0;

        for (let i: number = 0; i < weeksArray.length; i++) {
            const currentWeek: PsrDateColumn = weeksArray[i];
            const item: PsrLineItem = new PsrLineItem();

            progressList.forEach((progress: ProgressModel) => {
                if (progress.date.getTime() >= currentWeek.weekStart.getTime()
                    && progress.date.getTime() <= currentWeek.weekEnd.getTime()
                ) {
                    item.entries.push(progress);
                    item.amount += progress.amount;

                    if (progress.description) {
                        item.description += (item.description ? DESCRIPTION_SEPARATOR : '') + progress.description;
                    }
                }
            });

            fallbackAmount = this.processProgressSubTotalRow(
                subTotalRow,
                item,
                currentWeek,
                naturalResultRow,
                fallbackAmount);

            subGroupFallbackAmount = this.processProgressSubTotalRow(
                subGroupSubTotalRow,
                item,
                currentWeek,
                naturalResultRow,
                subGroupFallbackAmount);

            result.weeks.set(currentWeek, item);

            if (currentWeek.isInPeriod) {
                result.weeksInPeriod.set(currentWeek, item);
            }
        }

        return result;
    }

    private processTotalColumn(weeks: Map<PsrDateColumn, PsrLineItem>, total: PsrLineTotal = new PsrLineTotal()): PsrLineTotal {
        const _weeks: PsrLineItem[] = Array.from(weeks.values());

        for (let i = 0; i < _weeks.length; i++) {
            const week: PsrLineItem = _weeks[i];
            total.amount += week.amount;
            total.units += week.units;
        }

        return total;
    }

    private processTotalRow(totalRow: PsrLineModel, item: PsrLineItem, dateColumn: PsrDateColumn, isIncome: boolean) {
        if (totalRow) {
            let totalItem: PsrLineItem = totalRow.weeks.get(dateColumn);
            if (!totalItem) {
                totalItem = cloneDeep(item);
                if (!isIncome) {
                    totalItem.amount = -totalItem.amount;
                }

                totalRow.weeks.set(dateColumn, totalItem);

                if (dateColumn.isInPeriod) {
                    totalRow.weeksInPeriod.set(dateColumn, totalItem);
                }
            } else {
                if (!isIncome) {
                    totalItem.amount -= item.amount;
                } else {
                    totalItem.amount += item.amount;
                }
                totalItem.units += item.units;
            }
        }
    }

    private processSubTotalRow(subTotalRow: PsrLineModel, item: PsrLineItem, dateColumn: PsrDateColumn): void {
        if (subTotalRow) {
            let totalItem: PsrLineItem = subTotalRow.weeks.get(dateColumn);
            if (!totalItem) {
                totalItem = cloneDeep(item);

                subTotalRow.weeks.set(dateColumn, totalItem);

                if (dateColumn.isInPeriod) {
                    subTotalRow.weeksInPeriod.set(dateColumn, totalItem);
                }
            } else {
                totalItem.amount += item.amount;
                totalItem.units += item.units;
                totalItem.entries = totalItem.entries.concat(item.entries);
            }
        }
    }

    private processProgressSubTotalRow(
        progressSubTotalRow: PsrLineModel,
        item: PsrLineItem,
        dateColumn: PsrDateColumn,
        naturalResultRow: PsrLineModel,
        fallbackAmount: number
    ): number {
        if (progressSubTotalRow && naturalResultRow && naturalResultRow.totals && naturalResultRow.totals.amount) {
            let totalItem: PsrLineItem = progressSubTotalRow.weeks.get(dateColumn);
            if (!totalItem) {
                totalItem = cloneDeep(item);

                progressSubTotalRow.weeks.set(dateColumn, totalItem);

                if (dateColumn.isInPeriod) {
                    progressSubTotalRow.weeksInPeriod.set(dateColumn, totalItem);
                }
            }

            fallbackAmount = item.amount || fallbackAmount;
            totalItem.amount += fallbackAmount * naturalResultRow.totals.amount;

            if (progressSubTotalRow.totals.amount < totalItem.amount) {
                progressSubTotalRow.totals.amount = totalItem.amount;

                if (dateColumn.isInPeriod) {
                    progressSubTotalRow.totalsInPeriod.amount = totalItem.amount;
                }
            }
        }
        return fallbackAmount;
    }

    private processProgressTotalColumn(weeks: Map<PsrDateColumn, PsrLineItem>, forPeriod: boolean): PsrLineTotal {
        const total: PsrLineTotal = new PsrLineTotal();

        weeks.forEach((item: PsrLineItem, dateColumn: PsrDateColumn) => {
            if (total.amount < item.amount) {
                if (forPeriod) {
                    if (dateColumn.isInPeriod) {
                        total.amount = item.amount;
                    }
                } else {
                    total.amount = item.amount;
                }
            }
        });

        return total;
    }

    private postProcessTotalRow(totalRow: PsrLineModel): void {
        totalRow.totals = this.processTotalColumn(totalRow.weeks);
        totalRow.totalsInPeriod = this.processTotalColumn(totalRow.weeksInPeriod);

        const items: PsrLineItem[] = Array.from(totalRow.weeks.values());
        for (let i = 1; i < items.length; i++) {
            const week: PsrLineItem = items[i];

            const prevWeek: PsrLineItem = items[i - 1];
            week.amount += prevWeek.amount;
            week.units += prevWeek.units;
        }
    }

    private parseOutcomeType(outcomeEntry: OutcomeEntryModel): OutcomeEntryType {
        return StaticService.isInstanceOfLocalizedEnum(outcomeEntry.outcomeEntryType)
            ? OutcomeEntryType[(<LocalizedEnum>outcomeEntry.outcomeEntryType).value]
            : outcomeEntry.outcomeEntryType;
    }

    private parseProgressType(progress: ProgressModel): ProgressType {
        return StaticService.isInstanceOfLocalizedEnum(progress.progressType)
            ? ProgressType[(<LocalizedEnum>progress.progressType).value]
            : progress.progressType;
    }

    private postProccessProgressSubTotalRow(progressSubTotalRow: PsrLineModel, totalProgressAmount: number) {
        if (progressSubTotalRow && progressSubTotalRow.weeks) {
            progressSubTotalRow.totals.amount /= totalProgressAmount;
            progressSubTotalRow.totalsInPeriod.amount /= totalProgressAmount;

            progressSubTotalRow.weeks.forEach((item: PsrLineItem) => {
                if (totalProgressAmount) {
                    item.amount /= totalProgressAmount;
                } else {
                    item.amount = 0;
                }
            });
        }
    }

    private processIncome(
        invoice: ExtendedInvoiceModel,
        customers: CustomerModel[],
        currencies: CurrencyModel[],
        currencyRates: CurrencyRate[],
        weeksArray: PsrDateColumn[],
        subTotalRow: PsrLineModel,
        totalRow: PsrLineModel,
        isForPlan: boolean,
        dateFrom: Date,
        dateTo: Date,
        contractDays: number
    ): PsrLineModel {
        const key: PsrLineKey = {
            id: invoice.customerId,
            type: PsrLineType.INCOME,
            isGroupTotal: false,
            subGroupId: 0,
            isSubGroupTotal: false
        };

        const customer: CustomerModel = customers.find(item => item.id === key.id);
        const groupedWeeks: GroupedWeeks = this.groupIncome(
            invoice,
            weeksArray,
            currencies,
            currencyRates,
            subTotalRow,
            totalRow,
            isForPlan
        );

        const totalItem: PsrLineTotal = this.processTotalColumn(groupedWeeks.weeks);

        const invoiceDate: Date = InvoiceModel.getDate(invoice, isForPlan);
        const invoiceRate: number = this.findRate(invoice.currencyId, invoiceDate, currencies, currencyRates);
        totalItem.amount2 = invoice.amount * invoiceRate;
        subTotalRow.totals.amount2 += totalItem.amount2;

        const totalItemInPeriod: PsrLineTotal = this.processTotalColumn(groupedWeeks.weeksInPeriod);
        if (dateFrom.getTime() <= invoiceDate.getTime() && dateTo.getTime() >= invoiceDate.getTime()) {
            totalItemInPeriod.amount2 = invoice.amount * invoiceRate;
            subTotalRow.totalsInPeriod.amount2 += totalItemInPeriod.amount2;
        }

        let invoiceDaysOverdue: number = 0;
        if (totalItem.amount < totalItem.amount2) {
            const daysDiff: number = Math.round(moment.duration(moment().diff(moment(InvoiceModel.getDate(invoice, false)))).asDays());
            if (daysDiff > contractDays) {
                invoiceDaysOverdue = daysDiff;
            }
        }

        return {
            name: this.converterService.getLabelByPattern(customer, ConverterService.DEFAULT_PATTERN),
            description: customer.description,
            additionalDescription: invoice.description,
            key: key,
            weeks: groupedWeeks.weeks,
            weeksInPeriod: groupedWeeks.weeksInPeriod,
            totals: totalItem,
            totalsInPeriod: totalItemInPeriod,
            invoice: invoice,
            invoiceDaysOverdue: invoiceDaysOverdue
        };
    }

    private processOutcomeEntry(
        outcomeEntries: OutcomeEntryModel[],
        parent: OutcomeEntryParent,
        currencies: CurrencyModel[],
        currencyRates: CurrencyRate[],
        weeksArray: PsrDateColumn[],
        subGroupSubTotalRow: PsrLineModel,
        subTotalRow: PsrLineModel,
        totalRow: PsrLineModel
    ): PsrLineModel {
        const key: PsrLineKey = {
            id: outcomeEntries[0].parentId,
            type: subTotalRow.key.type,
            isGroupTotal: false,
            subGroupId: subGroupSubTotalRow ? subGroupSubTotalRow.key.subGroupId : null,
            isSubGroupTotal: false
        };

        const groupedWeeks: GroupedWeeks = this.groupOutcomeEntries(
            outcomeEntries,
            weeksArray,
            currencies,
            currencyRates,
            subGroupSubTotalRow,
            subTotalRow,
            totalRow
        );

        return {
            name: this.converterService.getLabelByPattern(parent, ConverterService.DEFAULT_PATTERN),
            description: parent.description,
            key: key,
            weeks: groupedWeeks.weeks,
            weeksInPeriod: groupedWeeks.weeksInPeriod,
            totals: this.processTotalColumn(groupedWeeks.weeks),
            totalsInPeriod: this.processTotalColumn(groupedWeeks.weeksInPeriod)
        };
    }

    private proccessTimesheets(
        timesheets: PsrTimesheet[],
        users: UserSimpleModel[],
        weeksArray: PsrDateColumn[],
        department: DepartmentModel,
        subGroupSubTotalRow: PsrLineModel,
        subTotalRow: PsrLineModel,
        totalRow: PsrLineModel,
        useTimesheetInternalRates: boolean
    ): PsrLineModel {
        const key: PsrLineKey = {
            id: timesheets[0].userId,
            type: PsrLineType.TIMESHEET,
            isGroupTotal: false,
            subGroupId: department.id,
            isSubGroupTotal: false
        };

        const parent: UserSimpleModel = users.find((user: UserSimpleModel) => user.id === key.id);
        const parentDescription: string = this.userService.getUserNameForView(parent);
        const groupedWeeks: GroupedWeeks = this.groupTimesheets(
            timesheets,
            weeksArray,
            subGroupSubTotalRow,
            subTotalRow,
            totalRow,
            useTimesheetInternalRates
        );

        return {
            key: key,
            name: parentDescription,
            description: parentDescription,
            totals: this.processTotalColumn(groupedWeeks.weeks),
            totalsInPeriod: this.processTotalColumn(groupedWeeks.weeksInPeriod),
            weeks: groupedWeeks.weeks,
            weeksInPeriod: groupedWeeks.weeksInPeriod
        };
    }

    private processProgressList(
        progressList: ProgressModel[],
        weeksArray: PsrDateColumn[],
        parents: ProgressParent[],
        subTotalRow: PsrLineModel,
        subGroupSubTotalRow: PsrLineModel,
        naturalResultRow: PsrLineModel,
        subGroupId: number
    ): PsrLineModel {
        const key: PsrLineKey = {
            id: progressList[0].parentId,
            type: PsrLineType.PROGRESS,
            isGroupTotal: false,
            subGroupId: subGroupId,
            isSubGroupTotal: false
        };

        const parent: ProgressParent = parents.find((_parent: ProgressParent) => _parent.id === key.id);
        const groupedWeeks: GroupedWeeks = this.groupProgressList(
            progressList,
            weeksArray,
            subTotalRow,
            subGroupSubTotalRow,
            naturalResultRow
        );

        return {
            key: key,
            name: this.converterService.getLabelByPattern(parent, ConverterService.DEFAULT_PATTERN),
            description: parent ? parent.description : '',
            weeks: groupedWeeks.weeks,
            weeksInPeriod: groupedWeeks.weeksInPeriod,
            totals: this.processProgressTotalColumn(groupedWeeks.weeks, false),
            totalsInPeriod: this.processProgressTotalColumn(groupedWeeks.weeksInPeriod, true)
        };
    }

    private generateTotalRow(type: PsrLineType, subGroupId: number = Number.MIN_SAFE_INTEGER): PsrLineModel {
        const subTotalRow: PsrLineModel = new PsrLineModel();

        let name: string = '';
        if (type === PsrLineType.TOTAL) {
            name = this.messageService.resolveMessage('ui.text.psr.projectCashFlow');
        } else {
            if (subGroupId === Number.MIN_SAFE_INTEGER) {
                switch (type) {
                    case PsrLineType.INCOME:
                        name += this.messageService.resolveMessage('ui.text.psr.income');
                        break;
                    case PsrLineType.OUTCOME:
                        name += this.messageService.resolveMessage('ui.text.psr.outcome');
                        break;
                    case PsrLineType.TIMESHEET:
                        name += this.messageService.resolveMessage('ui.text.default.timesheet');
                        break;
                    case PsrLineType.PROGRESS:
                        name = this.messageService.resolveMessage('ui.text.progress.title');
                        break;
                    default:
                        throw Error('Unexpected type');
                }
            }
        }

        subTotalRow.name = name;
        subTotalRow.key = {
            id: Number.MAX_SAFE_INTEGER,
            type: type,
            isGroupTotal: true,
            subGroupId: subGroupId,
            isSubGroupTotal: subGroupId !== Number.MIN_SAFE_INTEGER
        };

        return subTotalRow;
    }

    private findRate(
        currencyId: string,
        date: Date,
        currencies: CurrencyModel[],
        currencyRates: CurrencyRate[],
    ): number {
        let rate: number;
        const currencyRate: CurrencyRate = currencyRates.find((_currencyRate: CurrencyRate) =>
            _currencyRate.currency === currencyId
            && moment(_currencyRate.from).isBefore(date)
            && moment(_currencyRate.to).isAfter(date));

        if (currencyRate) {
            rate = currencyRate.rate;
        } else {
            rate = currencies.find((_currency: CurrencyModel) => _currency.id === currencyId).rate;
        }

        return rate;
    }

}
