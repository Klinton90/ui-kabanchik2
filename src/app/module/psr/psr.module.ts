import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppSharedModule } from '../shared/shared.module';
import { PsrPlanComponent } from './component/plan/psr-plan.component';
import { PsrService } from './service/psr.service';
import { AdminSharedModule } from '../admin/admin-shared.module';
import { TimesheetSharedModule } from '../timesheet/timesheet-shared.module';
import { OutcomeEntryListComponent } from './component/plan/outcome-entry-list.component';
import { TimesheetEntryListComponent } from './component/timesheet/timesheet-entry-list.component';
import { TimesheetSummaryListingComponent } from '../timesheet/component/summary/timesheet-summary-listing.component';
import { TimesheetEntryMultiAddModalComponent } from './component/timesheet/timesheet-entry-multi-add-modal.component';
import { TimesheetEntryMultiAdjustModalComponent } from './component/timesheet/timesheet-entry-multi-adjust-modal.component';
import { AuthGuardService } from './service/auth-guard.service';
import { PaymentEntryListComponent } from './component/plan/payment-entry-list.component';
import { ProgressModalComponent } from './component/progress/progress-modal.component';
import { DebtorListingComponent } from './component/debtor-listing/debtor-listing.component';
import { GridSharedModule } from '../grid-shared/grid-shared.module';
import { DebtorProgressRendererComponent } from './component/debtor-listing/debtor-progress-renderer.component';
import { ProjectSheetComponent } from './component/project-sheet/project-sheet.component';
import { ProgressMultiAdjustModalComponent } from './component/progress/progress-multi-adjust-modal.component';

const routes: Routes = [
    {
        path: 'plan',
        component: PsrPlanComponent,
        canActivate: [AuthGuardService],
    },
    {
        path: 'summary',
        component: TimesheetSummaryListingComponent,
        canActivate: [AuthGuardService],
        data: {
            isWorkload: true
        }
    },
    {
        path: 'debtorListing',
        component: DebtorListingComponent,
        canActivate: [AuthGuardService],
    },
    {
        path: 'projectSheet',
        component: ProjectSheetComponent,
        canActivate: [AuthGuardService],
    }
];

@NgModule({
    imports: [
        // angular
        RouterModule.forChild(routes),

        // custom
        AppSharedModule,
        AdminSharedModule,
        TimesheetSharedModule,
        GridSharedModule,
    ],
    declarations: [
        OutcomeEntryListComponent,
        TimesheetEntryListComponent,
        PaymentEntryListComponent,
        PsrPlanComponent,
        TimesheetEntryMultiAddModalComponent,
        TimesheetEntryMultiAdjustModalComponent,
        ProgressModalComponent,
        ProgressMultiAdjustModalComponent,
        DebtorListingComponent,
        DebtorProgressRendererComponent,
        ProjectSheetComponent,
    ],
    providers: [
        PsrService,
        AuthGuardService
    ],
    entryComponents: [
        OutcomeEntryListComponent,
        TimesheetEntryListComponent,
        PaymentEntryListComponent,
        DebtorProgressRendererComponent,
        TimesheetEntryMultiAddModalComponent,
        TimesheetEntryMultiAdjustModalComponent,
        ProgressModalComponent,
        ProgressMultiAdjustModalComponent
    ]
})
export class PsrModule {}
