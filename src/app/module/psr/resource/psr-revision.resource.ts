import { Injectable } from '@angular/core';
import { RestResource } from 'app/module/core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseResource } from 'app/module/core/resource/base.resource';

@Injectable({
    providedIn: 'root'
})
export class PsrRevisionResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('psrRevision');
    }

    getByTimesheetCategoryId(id: number, queryString: string, limit: number, offset: number): Observable<any> {
        let params: HttpParams = new HttpParams().append('timesheetCategoryId', id.toString());

        params = BaseResource.setOptionalPageableParams(params, queryString, limit, offset);

        return this._executeRequest({
            url: 'getByTimesheetCategoryId',
            method: 'GET',
            params: params
        });
    }

}
