import { BaseInvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { TimesheetCategory } from 'app/module/admin/component/timesheet-category/timesheet-category.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';
import { ListResponseModel } from 'app/module/core/model/list-response.model';

export class DebtorInvoiceModelId {
    invoiceId: number;
    paymentId: number;
}

export class DebtorInvoiceModel extends BaseInvoiceModel {
    id: DebtorInvoiceModelId;

    invoiceRate: number;
    invoiceCurrencyId: string;
    invoiceCurrencyName: string;

    paymentRate: number;
    paymentAmount: number;
    paymentDate: Date;
    paymentCurrencyId: string;
    paymentCurrencyName: string;

    timesheetCategory: TimesheetCategory;
}

export class DebtorStatusSum {
    status: LocalizedEnum;
    amount: number;
}

export class DebtorResult {
    statusTotals: DebtorStatusSum[];
    page: ListResponseModel;
    total: number;
    totalUnpaid: number;
}
