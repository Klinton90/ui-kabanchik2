import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, OnDestroy } from '@angular/core';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { InvoiceStatus, InvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { InvoiceResource } from 'app/module/admin/component/invoice/invoice.resource';
import { CustomCellRendererComponent } from 'app/module/grid-shared/component/custom-cell-renderer/custom-cell-renderer.component';
import { ValueFormatterParams, IGetRowsParams, GridReadyEvent, GridApi, CellClassParams, ValueGetterParams } from 'ag-grid-community';
import { ConverterService } from 'app/module/core/service/converter.service';
import { UserService } from 'app/module/core/service/user.service';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { Validators } from '@angular/forms';
import { DebtorProgressRendererComponent } from './debtor-progress-renderer.component';
import { map } from 'rxjs/operators';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { Subscription } from 'rxjs';
import { StickyService } from 'app/module/core/service/sticky.service';
import { StaticService, DEFAULT_DATE_FORMAT } from 'app/module/core/service/static.service';
import { Moment } from 'moment';
import * as moment from 'moment';
import { DebtorResult, DebtorInvoiceModel } from './debtor-listing.model';
import { MessageService } from 'app/module/core/service/message.service';

const STICKY_NAME: string = 'debtorListing';

class DebtorListingComponentSticky {
    showOnlyActiveTimesheetCategories: boolean;
    totalsAccordionExpanded: boolean;
}

@Component({
    moduleId: module.id,
    templateUrl: 'debtor-listing.component.html',
    styleUrls: ['debtor-listing.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DebtorListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    showOnlyActiveTimesheetCategories: boolean;

    totalsAccordionExpanded: boolean;

    api: GridApi;

    debtorResult: DebtorResult;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: InvoiceResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private userService: UserService,
        private serviceResource: ServiceResource,
        private stickyService: StickyService,
        private messageService: MessageService
    ) {
        this.stickyService.registerField(STICKY_NAME, (): DebtorListingComponentSticky => {
            return {
                showOnlyActiveTimesheetCategories: this.showOnlyActiveTimesheetCategories,
                totalsAccordionExpanded: this.totalsAccordionExpanded,
            };
        });

        const stickyData: DebtorListingComponentSticky = this.stickyService.getData();
        if (stickyData && stickyData[STICKY_NAME]) {
            Object.assign(this, stickyData[STICKY_NAME]);
        }
    }

    ngOnInit() {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'debtorListingGrid',
                resource: this.resource,
                newItemConstructor: () => new DebtorInvoiceModel(),
                hasAddButton: false,
                getDataMethod: (params: IGetRowsParams) => {
                    return this.resource
                        .findAllForDebtorList(params, this.api, this.showOnlyActiveTimesheetCategories)
                        .pipe(
                            map((response: RestResponseModel) => {
                                this.debtorResult = response.data;
                                this.ref.detectChanges();
                                return { data: this.debtorResult.page };
                            })
                        );
                },
                prepareRowForSave: (data: DebtorInvoiceModel) => {
                    return {
                        ...data,
                        id: data.id.invoiceId,
                    };
                },
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.debtorListing.invoiceInformation',
                    openByDefault: true,
                    children: [
                        {
                            headerName: this.messageService.resolveMessage('ui.text.invoice.title')
                                + ' '
                                + this.messageService.resolveMessage('ui.text.default.id'),
                            columnGroupShow: 'open',
                            field: 'id.invoiceId',
                            cellRendererFramework: CustomCellRendererComponent
                        },
                        {
                            headerName: this.messageService.resolveMessage('ui.text.payment.title')
                                + ' '
                                + this.messageService.resolveMessage('ui.text.default.id'),
                            columnGroupShow: 'open',
                            field: 'id.paymentId',
                            cellRendererFramework: CustomCellRendererComponent
                        },
                        {
                            headerName: 'ui.text.invoice.description',
                            field: 'description',
                            autoGenerateContext: {
                                type: ColumnObservableType.TEXT,
                                editor: false
                            }
                        },
                    ]
                },
                {
                    headerName: 'ui.text.debtorListing.projectInformation',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.timesheetCategory.title',
                            field: 'timesheetCategoryId',
                            autoGenerateContext: {
                                type: ColumnObservableType.DOMAIN2,
                                editor: false,
                                otherField: 'timesheetCategory.name',
                                infiniteProps: {
                                    name: 'timesheetCategory',
                                    orderByFields: ['isActive', 'name'],
                                    filterByFieldNames: ['name']
                                }
                            },
                        },
                        {
                            headerName: 'ui.text.timesheetCategory.managers',
                            columnGroupShow: 'open',
                            field: 'timesheetCategory',
                            sortable: false,
                            valueFormatter: this.userService.getManagersFormatterForAgGrid
                        },
                    ]
                },
                {
                    headerName: 'ui.text.invoice.title',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.invoice.amount',
                            columnGroupShow: 'open',
                            field: 'amount',
                            autoGenerateContext: {
                                type: ColumnObservableType.NUMBER,
                                editor: false
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.currency.title',
                            columnGroupShow: 'open',
                            field: 'invoiceCurrencyId',
                            autoGenerateContext: {
                                type: ColumnObservableType.DOMAIN2,
                                editor: false,
                                otherField: 'invoiceCurrencyName',
                                infiniteProps: {
                                    name: 'currency',
                                    orderByFields: ['isActive', 'name'],
                                    filterByFieldNames: ['name']
                                }
                            },
                        },
                        {
                            headerName: 'ui.text.debtorListing.baseCurrencyAmount',
                            colId: 'baseCurrencyInvoiceAmount',
                            field: 'amount',
                            valueGetter: (params: ValueGetterParams): number => {
                                const debtorInvoice: DebtorInvoiceModel = params.data;
                                return debtorInvoice && debtorInvoice.amount
                                    ? debtorInvoice.amount * debtorInvoice.invoiceRate
                                    : 0;
                            },
                            valueFormatter: (params: ValueFormatterParams): string => ConverterService.formatCurrency(params.value),
                            autoGenerateContext: {
                                type: ColumnObservableType.NUMBER,
                                editor: false,
                                filter: false,
                            }
                        },
                    ]
                },
                {
                    headerName: 'ui.text.payment.title',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.payment.amount',
                            columnGroupShow: 'open',
                            field: 'paymentAmount',
                            autoGenerateContext: {
                                type: ColumnObservableType.NUMBER,
                                editor: false
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.currency.title',
                            columnGroupShow: 'open',
                            field: 'paymentCurrencyId',
                            autoGenerateContext: {
                                type: ColumnObservableType.DOMAIN2,
                                editor: false,
                                otherField: 'paymentCurrencyName',
                                infiniteProps: {
                                    name: 'currency',
                                    orderByFields: ['isActive', 'name'],
                                    filterByFieldNames: ['name']
                                }
                            },
                        },
                        {
                            headerName: 'ui.text.debtorListing.baseCurrencyAmount',
                            colId: 'baseCurrencyPaymentAmount',
                            field: 'paymentAmount',
                            valueGetter: (params: ValueGetterParams): number => {
                                const debtorInvoice: DebtorInvoiceModel = params.data;
                                return debtorInvoice && debtorInvoice.paymentAmount
                                    ? debtorInvoice.paymentAmount * debtorInvoice.paymentRate
                                    : 0;
                            },
                            valueFormatter: (params: ValueFormatterParams): string => ConverterService.formatCurrency(params.value),
                            autoGenerateContext: {
                                type: ColumnObservableType.NUMBER,
                                editor: false,
                                filter: false,
                            }
                        },
                    ]
                },
                {
                    headerName: 'ui.text.debtorListing.datesInformation',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.invoice.datePlanned',
                            columnGroupShow: 'open',
                            field: 'datePlanned',
                            autoGenerateContext: {
                                type: ColumnObservableType.DATE,
                                editor: false
                            },
                            cellClassRules: {
                                'text-danger': (params: CellClassParams): boolean => {
                                    const debtorInvoice: DebtorInvoiceModel = params.data;
                                    return debtorInvoice
                                        && (!debtorInvoice.datePosted
                                            && ConverterService.guessMoment(debtorInvoice.datePlanned).isBefore(moment()));
                                }
                            }
                        },
                        {
                            headerName: 'ui.text.invoice.datePosted',
                            columnGroupShow: 'open',
                            field: 'datePosted',
                            autoGenerateContext: {
                                type: ColumnObservableType.DATE,
                                editor: false
                            },
                        },
                        {
                            headerName: 'ui.text.debtorListing.plannedDatePayment',
                            colId: 'plannedDatePayment',
                            // TODO: fix this...
                            sortable: false,
                            valueGetter: (params: ValueGetterParams): Moment => {
                                const debtorInvoice: DebtorInvoiceModel = params.data;
                                return debtorInvoice && debtorInvoice.datePosted
                                    ? ConverterService.guessMoment(InvoiceModel.getDate(debtorInvoice, false))
                                        .add(debtorInvoice.timesheetCategory.contractDays, 'days')
                                    : null;
                            },
                            valueFormatter: (params: ValueFormatterParams): string => {
                                return params.value
                                    ? params.value.format(DEFAULT_DATE_FORMAT)
                                    : '';
                            },
                            cellClassRules: {
                                'text-danger': (params: CellClassParams): boolean => {
                                    const debtorInvoice: DebtorInvoiceModel = params.data;
                                    const value: Moment = params.value;
                                    return debtorInvoice && !debtorInvoice.paymentAmount && value && value.isBefore(moment());
                                }
                            }
                        },
                        {
                            headerName: 'ui.text.debtorListing.actualDatePayment',
                            field: 'paymentDate',
                            autoGenerateContext: {
                                type: ColumnObservableType.DATE,
                                editor: false
                            },
                        },
                    ]
                },
                {
                    headerName: 'ui.text.debtorListing.statusInformation',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.invoice.status',
                            columnGroupShow: 'open',
                            field: 'status',
                            autoGenerateContext: {
                                type: ColumnObservableType.LOCALIZED_ENUM,
                                observable:  this.serviceResource.getLocalizedEnumOptions('kabanchik.system.invoice.domain.InvoiceStatus')
                            },
                            cellEditorParams: {
                                validators: [Validators.required]
                            },
                            cellClassRules: {
                                'text-danger': (params: CellClassParams): boolean => params.value
                                    && StaticService.resolveEnumOnEntity(params.value, InvoiceStatus) === InvoiceStatus.READY_FOR_INVOICING
                            }
                        },
                        {
                            headerName: 'ui.text.debtorListing.progress',
                            colId: 'progress',
                            field: 'status',
                            minWidth: 180,
                            width: 180,
                            resizable: false,
                            sortable: false,
                            filter: false,
                            editable: false,
                            cellRendererFramework: DebtorProgressRendererComponent
                        },
                    ]
                },
            ],
            onGridReady: (event: GridReadyEvent) => {
                this.api = event.api;
            }
        };

        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, false, false, null, false).subscribe(
            {next: (_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            }
        });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    showOnlyActiveTimesheetCategoriesChanged($event: any): void {
        this.gridOptions.api.refreshInfiniteCache();
    }

}
