import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';
import { InvoiceStatus } from 'app/module/admin/component/invoice/invoice.model';

@Component({
    templateUrl: './debtor-progress-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./debtor-progress-renderer.component.scss'],
})
export class DebtorProgressRendererComponent implements ICellRendererAngularComp {

    params: ICellRendererParams;

    value: number = 0;

    agInit(params: ICellRendererParams): void {
        this.params = params;
        this.value = this.params.getValue() ? +InvoiceStatus[this.params.getValue().value] : 0;
    }

    refresh(params: any): boolean {
        return false;
    }

}
