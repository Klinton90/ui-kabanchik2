import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef, ViewChild, OnDestroy } from '@angular/core';
import { TimesheetCategoryResource } from 'app/module/admin/component/timesheet-category/timesheet-category.resource';
import { Observable, Subscription } from 'rxjs';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { SelectItem } from 'primeng/api/selectitem';
import { ConverterService } from 'app/module/core/service/converter.service';
import { PsrRevisionResource } from '../../resource/psr-revision.resource';
import { ngSelectInfiniteDataCallback } from 'app/module/shared/component/ng-select-infinite-wrapper/ng-select-infinite-wrapper.component';
import { PsrRevision } from '../../model/psr-revision.model';
import { NgOption } from '@ng-select/ng-select';
import {
    GridApi,
    GridReadyEvent,
    ValueGetterParams,
    OriginalColumnGroup,
    ColGroupDef,
    CellClassParams,
    RowNode
} from 'ag-grid-community';
import { CustomAgGridOptionsModel, CustomColumnDef } from 'app/module/grid-shared/model/ag-grid-options.model';
import { environment } from 'environments/environment';
import { tap } from 'rxjs/operators';
import {
    MatNgSelectInfiniteWrapperComponent
} from 'app/module/shared/component/mat-ng-select-infinite-wrapper/mat-ng-select-infinite-wrapper.component';
import { UserService } from 'app/module/core/service/user.service';
import { GenericFieldEntity } from 'app/module/admin/component/generic-field/generic-field.model';
import { MessageService } from 'app/module/core/service/message.service';
import { PsrService, PsrData, DateLimitsResult } from '../../service/psr.service';
import { StaticService } from 'app/module/core/service/static.service';
import { PsrDateColumn } from 'app/module/core/model/date-column.model';
import { CurrencyResource } from 'app/module/admin/component/currency/currency.resource';
import { CurrencyModel } from 'app/module/admin/component/currency/currency.model';
import { get } from 'lodash';
import { ProjectSheetModel } from './project-sheet.model';
import * as moment from 'moment';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';

export const PROJECT_INFORMATION_GROUP_ID = 'projectInformation';
export const CONTRACT_VALUE_COL_ID_PREFIX = 'contractValue';
export const PROGRESS_FACT_COL_ID_PREFIX = 'progressFact';
export const EXPECTED_PROFIT_LOSS_COL_ID_PREFIX = 'expectedProfitLoss';
export const EXPECTED_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX = 'expectedProfitLossPercentage';
export const FORECAST_PROFIT_LOSS_COL_ID_PREFIX = 'forecastProfitLoss';
export const FORECAST_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX = 'forecastProfitLossPercentage';

export enum COL_ID_SUFFIX {
    REVISION = 'Revision',
    ACTUAL = 'Actual',
    CHANGE = 'Change'
}

@Component({
    moduleId: module.id,
    templateUrl: 'project-sheet.component.html',
    styleUrls: ['project-sheet.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectSheetComponent implements OnInit, OnDestroy {

    @ViewChild('psrRevisionSelection', {static: false}) psrRevisionSelection: MatNgSelectInfiniteWrapperComponent;

    selectedTimesheetCategories: ProjectSheetModel[] = [];
    selectedRevisions: Map<number, PsrRevision> = new Map();

    gridOptions: CustomAgGridOptionsModel;
    api: GridApi;

    currencies: CurrencyModel[] = [];

    subscriptions: Subscription[] = [];

    defaultSearch: string = !environment.production ? '0007.0-20-38P' : '';
    // defaultSearch: string = '';
    private devInit: boolean = false;
    private testRevisions: boolean = false;

    constructor(
        private timesheetCategoryResource: TimesheetCategoryResource,
        private psrRevisionResource: PsrRevisionResource,
        private converterService: ConverterService,
        private customAgGridService: CustomAgGridService,
        private userService: UserService,
        private ref: ChangeDetectorRef,
        private messageService: MessageService,
        private psrService: PsrService,
        private currencyResource: CurrencyResource,
    ) {
        this.currencyResource.list(true, {sort: [{field: 'name', order: 1}]})
            .subscribe({
                next: (response: RestResponseModel) => {
                    this.currencies = response.data;
                }
            });
    }

    ngOnInit() {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'projectSheet',
                hasPagination: false,
                hasSaveButton: false,
                hasAddButton: false,
                genericFieldEntity: GenericFieldEntity.TIMESHEET_CATEGORY,
                newItemConstructor: () => ({ id: 0 }),
                noRowsOverlayComponentParams: {
                    getMessage: () => 'ui.text.projectSheet.forceReload'
                }
            },
            floatingFilter: false,
            pagination: false,
            rowModelType: 'clientSide',
            rowData: [],
            enableSorting: false,
            rowClassRules: {
                'font-weight-bold': (params: CellClassParams) => (params.data as ProjectSheetModel).isTotalRow,
            },
            // Think if getters need to be calculated upfront instead of "on the fly" calculation
            columnDefs: [
                {
                    headerName: 'ui.text.projectSheet.projectInformation',
                    groupId: PROJECT_INFORMATION_GROUP_ID,
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.timesheetCategory.title',
                            field: 'viewName'
                        },
                        {
                            headerName: 'ui.text.timesheetCategory.description',
                            field: 'description',
                        },
                        {
                            headerName: 'ui.text.default.id',
                            field: 'id',
                            columnGroupShow: 'open'
                        },
                        {
                            headerName: 'ui.text.default.isActive',
                            field: 'isActive',
                            columnGroupShow: 'open'
                        },
                        {
                            headerName: 'ui.text.timesheetCategory.managers',
                            field: 'managers',
                            columnGroupShow: 'open',
                            valueFormatter: this.userService.getManagersFormatterForAgGrid,
                        },
                    ]
                },
                {
                    headerName: 'ui.text.psr.contractValue',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            columnGroupShow: 'open',
                            colId: `${CONTRACT_VALUE_COL_ID_PREFIX}${COL_ID_SUFFIX.REVISION}`,
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.plan.incomeSubTotalRow.totals.amount2 || 0;
                                    } else {
                                        return data.actual.plan.incomeSubTotalRow.totals.amount2 || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `${CONTRACT_VALUE_COL_ID_PREFIX}${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.plan.incomeSubTotalRow.totals.amount2',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: this.messageService.resolveMessage('ui.text.progress.title')
                        + ' '
                        + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            columnGroupShow: 'open',
                            colId: `${PROGRESS_FACT_COL_ID_PREFIX}${COL_ID_SUFFIX.REVISION}`,
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.progressFact || 0;
                                    } else {
                                        return data.actual.fact.progressSubTotalRow.totalsInPeriod.amount || 0;
                                    }
                                } else {
                                    return this.progressFactValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `${PROGRESS_FACT_COL_ID_PREFIX}${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.progressFact',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    return this.fallbackValueGetter(params);
                                } else {
                                    return this.progressFactValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            colId: `${PROGRESS_FACT_COL_ID_PREFIX}${COL_ID_SUFFIX.CHANGE}`,
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter
                        }
                    ]
                },
                {
                    headerName: 'ui.text.projectSheet.revenue',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            columnGroupShow: 'open',
                            colId: `revenue${COL_ID_SUFFIX.REVISION}`,
                            valueGetter: (params: ValueGetterParams): any => this.revenueGetter(params, true),
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `revenue${COL_ID_SUFFIX.ACTUAL}`,
                            valueGetter: this.revenueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            colId: `revenue${COL_ID_SUFFIX.CHANGE}`,
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: this.messageService.resolveMessage('ui.text.psr.spentAmount')
                        + ' '
                        + this.messageService.resolveMessage('ui.text.default.timesheet'),
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `spentAmountTimesheet${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    let result: number = 0;
                                    if (data.revision) {
                                        if (data.revision.fact.timesheetSubTotalRow?.totalsInPeriod) {
                                            result += data.revision.fact.timesheetSubTotalRow.totalsInPeriod.amount || 0;
                                        }
                                    } else {
                                        if (data.actual.fact.timesheetSubTotalRow?.totalsInPeriod) {
                                            result += data.actual.fact.timesheetSubTotalRow.totalsInPeriod.amount || 0;
                                        }
                                    }
                                    return result;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `spentAmountTimesheet${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.fact.timesheetSubTotalRow.totals.amount',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            colId: `spentAmountTimesheet${COL_ID_SUFFIX.CHANGE}`,
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: this.messageService.resolveMessage('ui.text.psr.spentAmount')
                        + ' '
                        + this.messageService.resolveMessage('ui.text.outcomeEntry.title'),
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `spentAmountOutcomeEntry${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    let result: number = 0;
                                    if (data.revision) {
                                        if (data.revision.fact.outcomeSubTotalRow?.totalsInPeriod) {
                                            result += data.revision.fact.outcomeSubTotalRow.totalsInPeriod.amount || 0;
                                        }
                                    } else {
                                        if (data.actual.fact.outcomeSubTotalRow?.totalsInPeriod) {
                                            result += data.actual.fact.outcomeSubTotalRow.totalsInPeriod.amount || 0;
                                        }
                                    }
                                    return result;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `spentAmountOutcomeEntryt${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.fact.outcomeSubTotalRow.totals.amount',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            colId: `spentAmountOutcomeEntry${COL_ID_SUFFIX.CHANGE}`,
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: this.messageService.resolveMessage('ui.text.psr.spentAmount')
                        + ' '
                        + this.messageService.resolveMessage('ui.text.default.total'),
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `spentAmount${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.spentAmount;
                                    } else {
                                        let result: number = 0;
                                        if (data.actual.fact.outcomeSubTotalRow?.totalsInPeriod) {
                                            result += data.actual.fact.outcomeSubTotalRow.totalsInPeriod.amount || 0;
                                        }
                                        if (data.actual.fact.timesheetSubTotalRow?.totalsInPeriod) {
                                            result += data.actual.fact.timesheetSubTotalRow.totalsInPeriod.amount || 0;
                                        }
                                        return result;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `spentAmount${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.spentAmount',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            colId: `spentAmount${COL_ID_SUFFIX.CHANGE}`,
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: 'ui.text.psr.estimatedToComplete',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.estimatedToComplete || 0;
                                    } else {
                                        return data.actual.legend.estimatedToCompleteInPeriod || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `estimatedToComplete${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.estimatedToComplete',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: 'ui.text.psr.invoiceAmount',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.invoiceAmount || 0;
                                    } else {
                                        return data.actual.legend.invoiceAmountInPeriod || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `invoiceAmount${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.invoiceAmount',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: 'ui.text.psr.invoicePaid',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.fact.incomeSubTotalRow.totals.amount || 0;
                                    } else {
                                        return data.actual.fact.incomeSubTotalRow.totalsInPeriod.amount || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `invoicePaid${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.fact.incomeSubTotalRow.totals.amount',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: 'ui.text.psr.budgetAmount',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `budgetAmount${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.budgetAmount || 0;
                                    } else {
                                        return data.actual.legend.budgetAmountInPeriod || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `budgetAmount${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.budgetAmount',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            colId: `budgetAmount${COL_ID_SUFFIX.CHANGE}`,
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: 'ui.text.psr.expectedProfitLoss',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `${EXPECTED_PROFIT_LOSS_COL_ID_PREFIX}${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.expectedProfitLoss || 0;
                                    } else {
                                        return data.actual.legend.expectedProfitLossInPeriod || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `${EXPECTED_PROFIT_LOSS_COL_ID_PREFIX}${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.expectedProfitLoss',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            colId: `${EXPECTED_PROFIT_LOSS_COL_ID_PREFIX}${COL_ID_SUFFIX.CHANGE}`,
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                        }
                    ]
                },
                {
                    headerName: this.messageService.resolveMessage('ui.text.psr.expectedProfitLoss')
                        + ' '
                        + this.messageService.resolveMessage('ui.text.default.percentage'),
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `${EXPECTED_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX}${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.expectedProfitLossPercentage || 0;
                                    } else {
                                        return data.actual.legend.expectedProfitLossPercentageInPeriod || 0;
                                    }
                                } else {
                                    return this.profitLossPercentageSumValueGetter(
                                        params,
                                        EXPECTED_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX,
                                        EXPECTED_PROFIT_LOSS_COL_ID_PREFIX);
                                }
                            },
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `${EXPECTED_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX}${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.expectedProfitLossPercentage',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    return this.fallbackValueGetter(params);
                                } else {
                                    return this.profitLossPercentageSumValueGetter(
                                        params,
                                        EXPECTED_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX,
                                        EXPECTED_PROFIT_LOSS_COL_ID_PREFIX);
                                }
                            },
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            colId: `${EXPECTED_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX}${COL_ID_SUFFIX.CHANGE}`,
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter
                        }
                    ]
                },
                {
                    headerName: 'ui.text.psr.forecastSpentAtCompletion',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.forecastSpentAtCompletion || 0;
                                    } else {
                                        return data.actual.legend.forecastSpentAtCompletionInPeriod || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-danger': this.forecastSpentAtCompletionCellClass.bind(this, COL_ID_SUFFIX.REVISION)
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            field: 'actual.legend.forecastSpentAtCompletion',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-danger': this.forecastSpentAtCompletionCellClass.bind(this, COL_ID_SUFFIX.ACTUAL)
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-danger': this.forecastSpentAtCompletionCellClass.bind(this, COL_ID_SUFFIX.CHANGE)
                            }
                        }
                    ]
                },
                {
                    headerName: 'ui.text.psr.forecastProfitLoss',
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `${FORECAST_PROFIT_LOSS_COL_ID_PREFIX}${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.forecastProfitLoss || 0;
                                    } else {
                                        return data.actual.legend.forecastProfitLossInPeriod || 0;
                                    }
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClass: this.forecastProfitLossCellClass.bind(this, COL_ID_SUFFIX.REVISION, false)
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `${FORECAST_PROFIT_LOSS_COL_ID_PREFIX}${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.forecastProfitLoss',
                            valueGetter: this.fallbackValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClass: this.forecastProfitLossCellClass.bind(this, COL_ID_SUFFIX.ACTUAL, false)
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            colId: `${FORECAST_PROFIT_LOSS_COL_ID_PREFIX}${COL_ID_SUFFIX.CHANGE}`,
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClass: this.forecastProfitLossCellClass.bind(this, COL_ID_SUFFIX.CHANGE, false)
                        }
                    ]
                },
                {
                    headerName: this.messageService.resolveMessage('ui.text.psr.forecastProfitLoss')
                        + ' '
                        + this.messageService.resolveMessage('ui.text.default.percentage'),
                    openByDefault: true,
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.revision',
                            colId: `${FORECAST_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX}${COL_ID_SUFFIX.REVISION}`,
                            columnGroupShow: 'open',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    if (data.revision) {
                                        return data.revision.legend.forecastProfitLossPercentage || 0;
                                    } else {
                                        return data.actual.legend.forecastProfitLossPercentageInPeriod || 0;
                                    }
                                } else {
                                    return this.profitLossPercentageSumValueGetter(
                                        params,
                                        FORECAST_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX,
                                        FORECAST_PROFIT_LOSS_COL_ID_PREFIX);
                                }
                            },
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter,
                            cellClass: this.forecastProfitLossCellClass.bind(this, COL_ID_SUFFIX.REVISION, true)
                        },
                        {
                            headerName: 'ui.text.projectSheet.actual',
                            colId: `${FORECAST_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX}${COL_ID_SUFFIX.ACTUAL}`,
                            field: 'actual.legend.forecastProfitLossPercentage',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    return this.fallbackValueGetter(params);
                                } else {
                                    return this.profitLossPercentageSumValueGetter(
                                        params,
                                        FORECAST_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX,
                                        FORECAST_PROFIT_LOSS_COL_ID_PREFIX);
                                }
                            },
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter,
                            cellClass: this.forecastProfitLossCellClass.bind(this, COL_ID_SUFFIX.ACTUAL, true)
                        },
                        {
                            headerName: 'ui.text.projectSheet.change',
                            colId: `${FORECAST_PROFIT_LOSS_PERCENTAGE_COL_ID_PREFIX}${COL_ID_SUFFIX.CHANGE}`,
                            columnGroupShow: 'open',
                            valueGetter: this.changeValueGetter,
                            valueFormatter: CustomAgGridService.getPercentageValueFormatter,
                            cellClass: this.forecastProfitLossCellClass.bind(this, COL_ID_SUFFIX.CHANGE, true)
                        }
                    ]
                },
                {
                    headerName: 'ui.text.projectSheet.result',
                    children: [
                        {
                            headerName: 'ui.text.projectSheet.resultThisYear',
                            valueGetter: (params: ValueGetterParams) => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const revenueChange: number = this.api.getValue(`revenue${COL_ID_SUFFIX.CHANGE}`, params.node) || 0;
                                    const spentAmountChange: number =
                                        this.api.getValue(`spentAmount${COL_ID_SUFFIX.CHANGE}`, params.node) || 0;
                                    return revenueChange - spentAmountChange;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.resultUntilToday',
                            colId: 'resultUntilToday',
                            valueGetter: (params: ValueGetterParams) => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const revenueActual: number = this.api.getValue(`revenue${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    const spentAmountActual: number =
                                        this.api.getValue(`spentAmount${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    return revenueActual - spentAmountActual;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.remainingContractValue',
                            colId: 'remainingContractValue',
                            valueGetter: (params: ValueGetterParams): number => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const contractValueActual: number =
                                        this.api.getValue(`${CONTRACT_VALUE_COL_ID_PREFIX}${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    const revenueActual: number = this.api.getValue(`revenue${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    return contractValueActual - revenueActual;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.forecastedFutureResult',
                            colId: 'forecastedFutureResult',
                            valueGetter: (params: ValueGetterParams) => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const remainingContractValue: number = this.api.getValue('remainingContractValue', params.node) || 0;
                                    const estimatedToCompleteActual: number =
                                        this.api.getValue(`estimatedToComplete${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    return remainingContractValue - estimatedToCompleteActual;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.totalResult',
                            valueGetter: (params: ValueGetterParams) => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const forecastedFutureResult: number = this.api.getValue('forecastedFutureResult', params.node) || 0;
                                    const resultUntilToday: number = this.api.getValue('resultUntilToday', params.node) || 0;
                                    return forecastedFutureResult + resultUntilToday;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.netWIP',
                            valueGetter: (params: ValueGetterParams) => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const revenueActual: number = this.api.getValue(`revenue${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    const invoicePaidActual: number =
                                        this.api.getValue(`invoicePaid${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    return revenueActual - invoicePaidActual;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.netOpenWIP',
                            valueGetter: (params: ValueGetterParams) => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const revenueActual: number = this.api.getValue(`revenue${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    const invoiceAmountActual: number =
                                        this.api.getValue(`invoiceAmount${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    return revenueActual - invoiceAmountActual;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        },
                        {
                            headerName: 'ui.text.projectSheet.cashFlow',
                            valueGetter: (params: ValueGetterParams) => {
                                const data: ProjectSheetModel = params.data;
                                if (!data.isTotalRow) {
                                    const invoicePaidActual = this.api.getValue(`invoicePaid${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    const spentAmountActual: number =
                                        this.api.getValue(`spentAmount${COL_ID_SUFFIX.ACTUAL}`, params.node) || 0;
                                    return invoicePaidActual - spentAmountActual;
                                } else {
                                    return this.sumValueGetter(params);
                                }
                            },
                            valueFormatter: CustomAgGridService.getCurrencyValueFormatter,
                            cellClassRules: {
                                'text-warning': this.negativeValueGridCellClass
                            }
                        }
                    ]
                }
            ],
            onGridReady: (event: GridReadyEvent) => {
                this.api = event.api;
            }
        };

        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, false, false, null, false).subscribe({
            next: (_gridOptions: CustomAgGridOptionsModel) => {
                _gridOptions.columnDefs.forEach((colGroup: ColGroupDef) => {
                    colGroup.children.forEach((col: CustomColumnDef) => {
                        col.editable = false;
                        col.filter = false;
                        col.sortable = false;
                        col.suppressMenu = true;
                    });
                });
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            }
        });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    getTimesheetCategoryLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.timesheetCategoryResource.getManagedTimesheetCategories(false, term, limit, offset)
            .pipe(tap(this.debugLoadData));
    }

    timesheetCategoryConverter = (response: RestResponseModel): SelectItem[] => {
        return this.converterService.defaultObjectConverter(response);
    }

    timesheetCategoryChanged = ($event: ProjectSheetModel[]): void => {
        this.selectedTimesheetCategories = $event;
        this.api && this.api.showNoRowsOverlay();
    }

    timesheetCategoryRemoved($event: NgOption): void {
        this.selectedRevisions.delete(($event.value as ProjectSheetModel).id);
    }

    getPsrRevisionsLoadDataCallback(timesheetCategory: ProjectSheetModel): ngSelectInfiniteDataCallback {
        return (term: string, limit: number, offset?: number): Observable<any> => {
            return this.psrRevisionResource.getByTimesheetCategoryId(timesheetCategory.id, term, limit, offset);
        };
    }

    psrRevisionChanged($event: PsrRevision, timesheetCategory: ProjectSheetModel): void {
        this.api && this.api.showNoRowsOverlay();
        if ($event) {
            this.selectedRevisions.set(timesheetCategory.id, $event);
            timesheetCategory.revision = JSON.parse($event.data);
        } else {
            this.selectedRevisions.delete(timesheetCategory.id);
            timesheetCategory.revision = null;
        }
    }

    debugLoadData = (timesheetCategoryResponse: RestResponseModel): void => {
        if (!environment.production && !this.devInit && timesheetCategoryResponse.data && timesheetCategoryResponse.data.length === 1) {
            this.devInit = true;
            this.selectedTimesheetCategories = [timesheetCategoryResponse.data[0]];
            if (this.testRevisions) {
                setTimeout(() => {
                    const subscription: Subscription = this.psrRevisionSelection.selectComponent.dataLoaded.subscribe({
                        next: (psrRevisionResponse) => {
                            this.psrRevisionSelection.selectComponent.control.setValue(psrRevisionResponse[0]);
                            this.loadData();
                        }
                    });
                    this.subscriptions.push(subscription);
                    this.psrRevisionSelection.selectComponent.typeaheadValue$.next('');
                });
            } else {
                this.loadData();
            }
        }
    }

    loadData(): void {
        let finishedCount: number = 0;
        this.selectedTimesheetCategories.forEach((timesheetCategory: ProjectSheetModel) => {
            this.psrService.fetchDataForPsr(timesheetCategory.id).subscribe({
                next: ([
                    groupedOutcomeEntries,
                    invoices,
                    groupedTimesheets,
                    groupedProgressList,
                    users,
                    departments,
                    customers,
                    suppliers,
                    subcontractors,
                    currencyRates,
                    departmentAccess
                ]: PsrData) => {
                    const dateLimits: DateLimitsResult = this.psrService.calculateDateLimitsByData(
                        invoices,
                        groupedOutcomeEntries,
                        groupedTimesheets,
                        groupedProgressList);

                    const calculatedCols: PsrDateColumn[] = <PsrDateColumn[]> StaticService.calculateWeekColsInPeriod(
                        dateLimits.minDateFrom.year(),
                        dateLimits.minDateFrom.month(),
                        dateLimits.maxDateTo.year(),
                        dateLimits.maxDateTo.month());

                    const dateTo: Date = moment().subtract(1, 'year').month(11).date(31).toDate();

                    timesheetCategory.actual = this.psrService.preparePSR(
                        invoices,
                        groupedOutcomeEntries,
                        groupedTimesheets,
                        groupedProgressList,
                        calculatedCols,
                        users,
                        customers,
                        suppliers,
                        subcontractors,
                        this.currencies,
                        departments,
                        currencyRates,
                        departmentAccess,
                        dateLimits.minDateFrom.toDate(),
                        dateTo,
                        timesheetCategory.contractDays,
                        true);

                        // TODO: dirty hack
                        if ((++finishedCount) === this.selectedTimesheetCategories.length) {
                            const rowData: ProjectSheetModel[] = [...this.selectedTimesheetCategories];

                            const totalRow: ProjectSheetModel = new ProjectSheetModel();
                            totalRow.isTotalRow = true;
                            totalRow.viewName = this.messageService.resolveMessage('ui.text.projectSheet.total');
                            rowData.push(totalRow);

                            this.api.setRowData(rowData);
                        }
                }
            });
        });
    }

    private changeValueGetter = (params: ValueGetterParams): number => {
        const parentColGroup: OriginalColumnGroup = params.column.getOriginalParent();
        const revision: number = params.api.getValue(parentColGroup.getLeafColumns()[0], params.node);
        const actual: number = params.api.getValue(parentColGroup.getLeafColumns()[1], params.node);
        return actual - revision;
    }

    private fallbackValueGetter = (params: ValueGetterParams): number => {
        const data: ProjectSheetModel = params.data;
        if (!data.isTotalRow) {
            return get(params.data, params.colDef.field) || 0;
        } else {
            return this.sumValueGetter(params);
        }
    }

    private sumValueGetter = (params: ValueGetterParams): number => {
        let result: number = 0;
        params.api.forEachNode((rowNode: RowNode) => {
            if (rowNode !== params.node) {
                result += params.api.getValue(params.column.getId(), rowNode);
            }
        });
        return result;
    }

    private revenueGetter = (params: ValueGetterParams, isRevision = false): number => {
        const data: ProjectSheetModel = params.data;
        if (!data.isTotalRow) {
            const suffix: string = isRevision ? COL_ID_SUFFIX.REVISION : COL_ID_SUFFIX.ACTUAL;
            const contractValue: number = this.api.getValue(CONTRACT_VALUE_COL_ID_PREFIX + suffix, params.node) || 0;
            const progressFact: number = this.api.getValue(PROGRESS_FACT_COL_ID_PREFIX + suffix, params.node) || 0;
            return progressFact * contractValue / 100;
        } else {
            return this.sumValueGetter(params);
        }
    }

    private profitLossPercentageSumValueGetter(params: ValueGetterParams, prefix: string, prefix2: string): number {
        const suffix: string = this.getColSuffix(params, prefix);
        const profitLoss: number = params.getValue(`${prefix2}${suffix}`);
        const contractValue: number = params.getValue(`${CONTRACT_VALUE_COL_ID_PREFIX}${suffix}`);
        return profitLoss && contractValue ? profitLoss / contractValue * 100 : 0;
    }

    private progressFactValueGetter(params: ValueGetterParams): number {
        const suffix: string = this.getColSuffix(params, PROGRESS_FACT_COL_ID_PREFIX);
        let percentageTotal: number = 0;
        let coverageAmountTotal: number = 0;
        params.api.forEachNode((rowNode: RowNode) => {
            if (rowNode !== params.node) {
                const currentCoverageAmount: number =
                    params.api.getValue(CONTRACT_VALUE_COL_ID_PREFIX + suffix, rowNode) || 0;
                percentageTotal +=
                    (params.api.getValue(params.column.getId(), rowNode) || 0) * currentCoverageAmount;
                coverageAmountTotal += currentCoverageAmount;
            }
        });
        return percentageTotal && coverageAmountTotal ? (percentageTotal / coverageAmountTotal) : 0;
    }

    private getColSuffix(params: ValueGetterParams, prefix: string): string {
        return params.column.getId().replace(prefix, '');
    }

    private negativeValueGridCellClass = (params: CellClassParams): boolean => !params.data.isTotalRow && params.value < 0;

    private forecastSpentAtCompletionCellClass = (type: COL_ID_SUFFIX, params: CellClassParams): boolean => {
        return !params.data.isTotalRow && params.value > this.api.getValue(`budgetAmount${type}`, params.node);
    }

    private forecastProfitLossCellClass = (type: COL_ID_SUFFIX, isPercentage: boolean, params: CellClassParams): string | string[] => {
        if (!params.data.isTotalRow) {
            const expectedProfitLoss: number =
                this.api.getValue(`expectedProfitLoss${isPercentage ? 'Percentage' : ''}${type}`, params.node);
            if (params.value < 0) {
                return 'text-danger';
            } else if (params.value < expectedProfitLoss) {
                return 'text-warning';
            } else {
                return 'text-success';
            }
        }
        return '';
    }

}
