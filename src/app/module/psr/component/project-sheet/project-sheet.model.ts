import { PsrCalculationResult } from '../../model/psr-line.model';
import { TimesheetCategory } from 'app/module/admin/component/timesheet-category/timesheet-category.model';

export class ProjectSheetModel extends TimesheetCategory {
    viewName: string;
    revision: PsrCalculationResult;
    actual: PsrCalculationResult;
    isTotalRow: boolean;
}
