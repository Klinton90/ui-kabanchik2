import { TimesheetResource } from 'app/module/timesheet/resource/timesheet.resource';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { UtilService } from 'app/module/core/service/util.service';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { BulkTimesheetAdjust } from './bulk-timesheet.model';
import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { UserResource } from 'app/module/admin/component/user/user.resource';
import { UserService } from 'app/module/core/service/user.service';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-entry-multi-adjust-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetEntryMultiAdjustModalComponent extends FormAwareModal<TimesheetEntryMultiAdjustModalComponent, any> {

    newFormEntityInstance: BulkTimesheetAdjust = new BulkTimesheetAdjust();

    form: FormGroup = new FormGroup({
        adjustmentWeeks: new FormControl('', [Validators.required]),
        adjustmentUserId: new FormControl('', [Validators.required]),
        adjustmentAmount: new FormControl(''),
        adjustmentFromDate: new FormControl(''),
        adjustmentToDate: new FormControl(''),
        // hidden
        timesheetCategoryId: new FormControl('', [Validators.required]),
        userId: new FormControl('', [Validators.required]),
    });

    constructor(
        public dialogRef: MatDialogRef<TimesheetEntryMultiAdjustModalComponent>,
        protected resource: TimesheetResource,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected utilService: UtilService,
        protected ref: ChangeDetectorRef,
        private userResource: UserResource,
        public userService: UserService,
        @Inject(MAT_DIALOG_DATA) data: FormAwareParams<any>
    ) {
        super();

        data.entity.adjustmentUserId = data.entity.userId;
        super.show(data);
    }

    public getUserLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.userResource.getForPSR(term, limit, offset);
    }

    // @Override
    protected getSaveAction(): string {
        return 'bulkTimesheetAdjustment';
    }

    // @Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();
        if (data.adjustmentFromDate) {
            data.adjustmentFromDate = data.adjustmentFromDate.valueOf();
        }
        if (data.adjustmentToDate) {
            data.adjustmentToDate = data.adjustmentToDate.valueOf();
        }

        return data;
    }

}
