export class BulkTimesheetAdd {
    dateFrom: Date;
    dateTo: Date;
    timesheetCategoryId: number;
    userId: number;
    amount: number;
}

export class BulkTimesheetAdjust {
    timesheetCategoryId: number;
    userId: number;
    adjustmentWeeks?: number;
    adjustmentUserId?: number;
    adjustmentAmount?: number;
    adjustmentFromDate?: Date;
    adjustmentToDate?: Date;
}
