import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { TimesheetResource } from 'app/module/timesheet/resource/timesheet.resource';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UtilService } from 'app/module/core/service/util.service';
import { BulkTimesheetAdd } from './bulk-timesheet.model';
import { Observable } from 'rxjs';
import { UserResource } from 'app/module/admin/component/user/user.resource';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { UserService } from 'app/module/core/service/user.service';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-entry-multi-add-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetEntryMultiAddModalComponent extends FormAwareModal<TimesheetEntryMultiAddModalComponent, any> {

    newFormEntityInstance: BulkTimesheetAdd = new BulkTimesheetAdd();

    form: FormGroup = new FormGroup({
        userId: new FormControl('', [Validators.required]),
        dateFrom: new FormControl('', [Validators.required]),
        dateTo: new FormControl('', []),
        amount: new FormControl('', [Validators.required]),
        // hidden
        timesheetCategoryId: new FormControl('', [Validators.required]),
    });

    constructor(
        public dialogRef: MatDialogRef<TimesheetEntryMultiAddModalComponent>,
        protected resource: TimesheetResource,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected utilService: UtilService,
        protected ref: ChangeDetectorRef,
        private userResource: UserResource,
        public userService: UserService,
        @Inject(MAT_DIALOG_DATA) data: FormAwareParams<any>
    ) {
        super();

        if (!data || !data.entity || !data.entity.timesheetCategoryId) {
            throw Error('`entity` is required.');
        }

        super.show(data);
    }

    public getUserLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.userResource.getForPSR(term, limit, offset);
    }

    // @Override
    protected successHandler(response: RestResponseModel): void {
        const dates: number[] = response.data;
        if (dates.length) {
            this.notificationService.post({
                body: 'ui.message.psr.plan.bulkTimesheetAdd.duplicates',
                type: AlertType.WARNING,
                params: [dates.length]
            });
        }
        super.successHandler(response);
    }

    // @Override
    protected getSaveAction(): string {
        return 'bulkTimesheetInsert';
    }

    // @Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();

        if (!data.dateTo) {
            data.dateTo = data.dateFrom;
        }

        data.dateFrom = data.dateFrom.day(1).valueOf();
        data.dateTo = data.dateTo.day(1).valueOf();

        return data;
    }

}
