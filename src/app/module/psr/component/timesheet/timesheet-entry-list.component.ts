import { Component, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from '@angular/core';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import {
    TimesheetReportModalComponent,
    TimesheetReportModalMode,
    TimesheetReportModalParams
} from 'app/module/timesheet/component/report/timesheet-report-modal.component';
import { BaseTimesheet, PsrTimesheet } from 'app/module/timesheet/model/timesheet.model';
import { ListAwareModalComponent, ListAwareParams } from 'app/module/shared/component/modal/list-aware-modal.component';
import { TimesheetResource } from 'app/module/timesheet/resource/timesheet.resource';
import { FormAwareModalResult } from 'app/module/shared/component/modal/success-form-aware-modal-event.model';
import { Moment } from 'moment';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export class TimesheetEntryListParams extends ListAwareParams<BaseTimesheet> {
    timesheetCategoryId: number;
    minDate: Moment;
    userId: number;
}

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-entry-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetEntryListComponent extends ListAwareModalComponent<
    TimesheetEntryListComponent,
    TimesheetReportModalComponent,
    BaseTimesheet
> {

    protected modal = TimesheetReportModalComponent;

    constructor(
        public dialogRef: MatDialogRef<TimesheetEntryListComponent>,
        protected confirmationService: ConfirmationService,
        protected resource: TimesheetResource,
        protected notificationService: AlertService,
        protected ref: ChangeDetectorRef,
        protected dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: TimesheetEntryListParams
    ) {
        super();
    }

    // Override
    protected getModalParams(): Omit<TimesheetReportModalParams, 'entity'> {
        return {
            isViewOnly: false,
            mode: TimesheetReportModalMode.PLAN,
            minDate: this.data.minDate,
        };
    }

    // Override
    protected getNewFormEntityInstance(): BaseTimesheet {
        const result: PsrTimesheet = new PsrTimesheet();
        result.timesheetCategoryId = this.data.timesheetCategoryId;
        if (this.data.userId) {
            result.userId = this.data.userId;
        }
        return result;
    }

    // Override
    // onSuccess(event: FormAwareModalResult): void {
    //     const indexToUpdate: number = this.data.rows.findIndex((row: BaseTimesheet) => row.id === event.entity.id);
    //     const _entity: BaseTimesheet = this.data.rows[indexToUpdate];
    //     this.isRefreshRequired = true;

    //     if (event.isAddMode || _entity.userId !== event.entity.userId) {
    //         this.hideAction();
    //     } else {
    //         event.entity.date.setTime(event.entity.date.getTime() - event.entity.date.getTimezoneOffset() * 60 * 1000);
    //         this.data.rows[indexToUpdate] = event.entity;
    //     }
    // }

}
