import { Component, HostListener, ChangeDetectorRef, ViewChild } from '@angular/core';
import {
    PsrLineType,
    PsrLineModel,
    PsrCalculationResult,
    PsrLineItem,
    PsrLineModelTotal,
    PsrLineTotal,
    PsrLineModelCore,
    PsrLineModelEntry
} from '../../model/psr-line.model';
import { StaticService } from 'app/module/core/service/static.service';
import { TimesheetCategory } from 'app/module/admin/component/timesheet-category/timesheet-category.model';
import { PsrTimesheet, Timesheet, TimesheetStatus } from 'app/module/timesheet/model/timesheet.model';
import { OutcomeEntryModel } from 'app/module/admin/component/outcome-entry/outcome-entry.model';
import { UserSimpleModel, ROLE } from 'app/module/admin/component/user/user.model';
import { CustomerModel } from 'app/module/admin/component/customer/customer.model';
import { SupplierModel } from 'app/module/admin/component/supplier/supplier.model';
import { SubcontractorModel } from 'app/module/admin/component/subcontractor/subcontractor.model';
import { CurrencyModel } from 'app/module/admin/component/currency/currency.model';
import { DepartmentModel } from 'app/module/admin/component/department/department.model';
import { PsrDateColumn, DateColumn } from 'app/module/core/model/date-column.model';
import { PsrService, DateLimitsResult, PsrData } from '../../service/psr.service';
import { TimesheetResource } from 'app/module/timesheet/resource/timesheet.resource';
import { OutcomeEntryResource } from 'app/module/admin/component/outcome-entry/outcome-entry.resource';
import { TimesheetCategoryResource } from 'app/module/admin/component/timesheet-category/timesheet-category.resource';
import { ConverterService } from 'app/module/core/service/converter.service';
import { CurrencyResource } from 'app/module/admin/component/currency/currency.resource';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { Observable, zip, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { OutcomeEntryListComponent, OutcomeEntryListParams } from './outcome-entry-list.component';
import { TimesheetEntryListComponent, TimesheetEntryListParams } from '../timesheet/timesheet-entry-list.component';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ExtendedInvoiceModel, InvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { PaymentEntryListComponent, PaymentEntryListParams } from './payment-entry-list.component';
import { InvoiceModalComponent, InvoiceModalParams } from 'app/module/admin/component/invoice/invoice-modal.component';
import { ProgressResource } from 'app/module/psr/component/progress/progress.resource';
import { ProgressModel, ProgressType } from 'app/module/psr/component/progress/progress.model';
import { ProgressModalComponent } from '../progress/progress-modal.component';
import { environment } from 'environments/environment';
import { cloneDeep, isArray, isNil, isFunction, omit, pickBy } from 'lodash';
import { GenericFieldEntity, GenericFieldModel } from 'app/module/admin/component/generic-field/generic-field.model';
import { UserService } from 'app/module/core/service/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GenericFieldService } from 'app/module/admin/component/generic-field/generic-field.service';
import { GenericFieldResource } from 'app/module/admin/component/generic-field/generic-field.resource';
import { GENERIC_FIELD_FORM_GROUP_NAME } from 'app/module/shared/component/generic-field-renderer/generic-field-renderer.component';
import { Moment } from 'moment';
import * as moment from 'moment';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { ChartDataSets, ChartOptions, ChartLegendLabelItem } from 'chart.js';
import { Label, Color, BaseChartDirective } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { StickyService } from 'app/module/core/service/sticky.service';
import { PaymentResource } from 'app/module/admin/component/payment/payment.resource';
import { FormAwareResource } from 'app/module/core/resource/form-aware.resource';
import { BulkUpdateModel } from 'app/module/core/model/bulk-update.model';
import { CurrencyRate } from 'app/module/admin/component/currency-rate/currency-rate.model';
import { DepartmentAccess } from 'app/module/admin/component/department-access/department-access.model';
import { PsrRevisionResource } from '../../resource/psr-revision.resource';
import { PsrRevision } from '../../model/psr-revision.model';
import {
    MatNgSelectInfiniteWrapperComponent
} from 'app/module/shared/component/mat-ng-select-infinite-wrapper/mat-ng-select-infinite-wrapper.component';
import { MessageService } from 'app/module/core/service/message.service';
import { AuthService } from 'app/service/auth.service';
import { ModalResult } from 'app/module/shared/component/modal/modal-result.model';
import { TimesheetEntryMultiAddModalComponent } from '../timesheet/timesheet-entry-multi-add-modal.component';
import { FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { TimesheetEntryMultiAdjustModalComponent } from '../timesheet/timesheet-entry-multi-adjust-modal.component';
import { ComponentType } from '@angular/cdk/portal';
import { ListAwareParams } from 'app/module/shared/component/modal/list-aware-modal.component';
import { SelectItem } from 'primeng/api/selectitem';
import { SortEvent } from 'primeng/api/sortevent';
import { ProgressMultiAdjustModalComponent } from '../progress/progress-multi-adjust-modal.component';

enum PsrMode {
    FACT, PLAN, TOTAL
}

const STICKY_NAME = 'psrPage';

@Component({
    moduleId: module.id,
    templateUrl: 'psr-plan.component.html',
    styleUrls: ['psr-plan.component.scss']
})
export class PsrPlanComponent {

    @ViewChild('psrRevisionSelection', {static: false}) psrRevisionSelection: MatNgSelectInfiniteWrapperComponent;

    // general grid section
    PSR_LINE_TYPE: typeof PsrLineType = PsrLineType;
    PSR_MODE: typeof PsrMode = PsrMode;
    CONVERTER_SERVICE: typeof ConverterService = ConverterService;
    Role: typeof ROLE = ROLE;
    gridWidth: number = 0;
    gridHeight: number = 0;
    isGridReady: boolean = true;
    // TODO: change to method
    isEditAllowed: boolean = true;

    // form section
    minDateFrom: Moment;
    dateFrom: Moment;
    dateTo: Moment;
    maxDateTo: Moment;
    isManualDates: boolean = false;
    moneyHoursReportType: boolean = false;
    mode: PsrMode = PsrMode.TOTAL;

    leftSideOpen: boolean = false;
    rightSideOpen: boolean = false;

    selectedTimesheetCategory: TimesheetCategory;
    defaultSearch: string = !environment.production ? '0007.0-20-38P' : '';
    private devInit: boolean = false;

    modes: SelectItem[] = [
        {
            label: 'ui.text.psr.mode.fact',
            value: PsrMode.FACT
        },
        {
            label: 'ui.text.psr.mode.plan',
            value: PsrMode.PLAN
        },
        {
            label: 'ui.text.psr.mode.summary',
            value: PsrMode.TOTAL
        }
    ];

    // data section
    invoices: ExtendedInvoiceModel[] = [];
    groupedOutcomeEntries: Map<boolean, OutcomeEntryModel[]> = new Map<boolean, OutcomeEntryModel[]>();
    groupedTimesheets: Map<boolean, PsrTimesheet[]> = new Map<boolean, PsrTimesheet[]>();
    groupedProgressList: Map<boolean, ProgressModel[]> = new Map<boolean, ProgressModel[]>();

    // misc data
    users: UserSimpleModel[] = [];
    customers: CustomerModel[] = [];
    suppliers: SupplierModel[] = [];
    subcontractors: SubcontractorModel[] = [];
    currencies: CurrencyModel[] = [];
    departments: DepartmentModel[] = [];
    departmentAccessList: DepartmentAccess[] = [];
    currencyRates: CurrencyRate[] = [];

    // grid data
    calculationResult: PsrCalculationResult;
    calculatedCols: PsrDateColumn[] = [];
    cols: PsrDateColumn[] = [];
    rows: PsrLineModel[] = [];
    rowsGrouping: any = {};

    // accordions
    GENERIC_FIELD_ENTITY: typeof GenericFieldEntity = GenericFieldEntity;
    timesheetCategoryForm: FormGroup = new FormGroup({
        initialBudget: new FormControl(0, [Validators.min(0.01), Validators.max(99999999999999999.99)]),
        contractDays: new FormControl(0, [Validators.required, Validators.min(1)])
    });
    genericFields: GenericFieldModel[];
    dateLimits: DateLimitsResult;

    // psrRevision
    psrRevisionForm: FormGroup = new FormGroup({
        name: new FormControl('')
    });
    selectedRevision: PsrRevision;
    cachedCalculationResult: PsrCalculationResult;

    // charts
    // financial and progress charts shared
    graphRowHeight: number = 0;
    graphHeight: number = 0;
    graphWidth: number = 0;

    ballanceChartHidden = false;
    paymentsChartHidden = false;
    outcomeChartHidden = false;
    planChartHidden = false;
    factChartHidden = false;

    weekChartLabels: Label[] = [];
    weekChartPlugins = [pluginAnnotations];
    weekChartOptions: (ChartOptions & { annotation: any }) = {
        maintainAspectRatio: false,
        // responsive: true,
        scales: {
            // We use this empty structure as a placeholder for dynamic theming.
            xAxes: [{
                id: 'weeksAxisX'
            }],
            yAxes: [
                {
                    id: 'moneyAxisY',
                    position: 'left',
                }
            ]
        },
        annotation: {
            annotations: [
                {
                    type: 'line',
                    mode: 'vertical',
                    scaleID: 'weeksAxisX',
                    value: this.getWeekGraphLabelPrefix() + StaticService.calculateWeek(new Date()),
                    borderColor: 'orange',
                    borderWidth: 2,
                    label: {
                        enabled: true,
                        fontColor: 'orange',
                        content: this.messageService.resolveMessage('ui.text.default.today')
                    }
                },
            ],
        },
        plugins: {
            datalabels: {
                display: false
            }
        }
    };

    // financial chart
    @ViewChild('financialChart', { static: false, read: BaseChartDirective }) financialChart: BaseChartDirective;
    financialChartColors: Color[] = [
        { // grey
            backgroundColor: 'rgba(148, 159, 177, 0.2)',
            borderColor: 'rgba(148, 159, 177, 1)',
            pointBackgroundColor: 'rgba(148, 159, 177, 1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148, 159, 177, 0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77, 83, 96, 0.2)',
            borderColor: 'rgba(77, 83, 96, 1)',
            pointBackgroundColor: 'rgba(77, 83, 96, 1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77, 83, 96, 1)'
        },
        { // green
            backgroundColor: 'rgba(0, 255, 0, 0.3)',
            borderColor: 'green',
            pointBackgroundColor: 'rgba(0, 255, 0, 0.3)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(0, 255, 0, 0.8)'
        },
        { // dark green
            backgroundColor: 'rgba(10, 150, 10, 0.3)',
            borderColor: 'darkgreen',
            pointBackgroundColor: 'rgba(10, 150, 10, 0.3)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(10, 150, 10, 0.8)'
        },
        { // red
            backgroundColor: 'rgba(255, 0, 0, 0.3)',
            borderColor: 'red',
            pointBackgroundColor: 'rgba(255, 0, 0, 1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(255, 0, 0, 0.8)'
        },
        { // pink
            backgroundColor: 'rgba(255, 0, 255, 0.3)',
            borderColor: 'violet',
            pointBackgroundColor: 'rgba(255, 0, 255, 1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(255, 0, 255, 0.8)'
        }
    ];
    financialChartData: ChartDataSets[] = [
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.psr.projectCashFlow')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            lineTension: 0
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.psr.projectCashFlow')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            lineTension: 0
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.psr.income')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            lineTension: 0
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.psr.income')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            lineTension: 0
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.psr.outcome')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            lineTension: 0
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.psr.outcome')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            lineTension: 0
        }
    ];

    // progress chart
    @ViewChild('progressChart', { static: false, read: BaseChartDirective }) progressChart: BaseChartDirective;
    progressChartData: ChartDataSets[] = [
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.progress.title')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            lineTension: 0
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.progress.title')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            lineTension: 0
        }
    ];
    progressChartColors: Color[] = [
        { // green
            backgroundColor: 'rgba(0, 255, 0, 0.3)',
            borderColor: 'green',
            pointBackgroundColor: 'rgba(0, 255, 0, 0.3)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(0, 255, 0, 0.8)'
        },
        { // red
            backgroundColor: 'rgba(255, 0, 0, 0.3)',
            borderColor: 'red',
            pointBackgroundColor: 'rgba(255, 0, 0, 1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(255, 0, 0, 0.8)'
        },
        // { // blue
        //     backgroundColor: 'rgba(0, 0, 255, 0.3)',
        //     borderColor: 'blue',
        //     pointBackgroundColor: 'rgba(0, 0, 255, 0.3)',
        //     pointBorderColor: '#fff',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: 'rgba(0, 0, 255, 0.8)'
        // },
        // { // violet
        //     backgroundColor: 'rgba(50, 0, 100, 0.3)',
        //     borderColor: 'darkblue',
        //     pointBackgroundColor: 'rgba(50, 0, 100, 0.3)',
        //     pointBorderColor: '#fff',
        //     pointHoverBackgroundColor: '#fff',
        //     pointHoverBorderColor: 'rgba(50, 0, 100, 0.8)'
        // }
    ];
    // preview chart
    @ViewChild('previewChart', { static: false, read: BaseChartDirective }) previewChart: BaseChartDirective;
    previewChartDataSets: ChartDataSets[] = [
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.progress.title')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.plan'),
            backgroundColor: 'rgba(0, 255, 0, 0.3)',
            borderColor: 'green',
            borderWidth: 3
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.progress.title')
                + ' '
                + this.messageService.resolveMessage('ui.text.psr.mode.fact'),
            backgroundColor: 'rgba(0, 0, 255, 0.3)',
            borderColor: 'blue',
            borderWidth: 3
        },
        {
            data: [],
            label: this.messageService.resolveMessage('ui.text.psr.budgetAmount'),
            backgroundColor: 'rgba(255, 0, 0, 0.3)',
            borderColor: 'red',
            borderWidth: 3
        }
    ];
    previewChartOptions: ChartOptions = {
        circumference: Math.PI,
        rotation: -Math.PI,
        tooltips: {
            callbacks: {
                label: (tooltip, data) => data.datasets[tooltip.datasetIndex].label
                + ' '
                + this.messageService.resolveMessage(tooltip.index ? 'ui.text.psr.toBeDone' : 'ui.text.psr.current')
                + ' '
                + data.datasets[tooltip.datasetIndex].data[tooltip.index].toString()
            }
        },
        legend: {
            onClick: (e, legendItem: ChartLegendLabelItem) => {
                this.previewChart.hideDataset(legendItem.datasetIndex, !this.previewChart.isDatasetHidden(legendItem.datasetIndex));
            },
            labels: {
                generateLabels: (chart) => {
                    return [
                        this.generatePreviewGraphLabel(0, chart),
                        this.generatePreviewGraphLabel(1, chart),
                        this.generatePreviewGraphLabel(2, chart)
                    ];
                }
            }
        },
        plugins: {
            datalabels: {
                display: false
            },
            elements: {
                text: (chart: Chart) => {
                    if (chart.data.datasets && chart.data.datasets[1] && chart.data.datasets[1].data && chart.data.datasets[1].data[0]) {
                        return chart.data.datasets[1].data[0] + '%';
                    }
                    return '';
                },
                color: '#36A2EB',       // Default black
                fontStyle: 'Helvetica', // Default Arial
                sidePadding: 12         // Default 20 (as a percentage)
            },
            dynamicColors: (chart: Chart) => {
                if (chart.data.datasets && chart.data.datasets[2] && chart.data.datasets[2].data && chart.data.datasets[2].data[0]) {
                    const percentageAmount: number = chart.data.datasets[2].data[0] as number;
                    const currentColor: string = chart.data.datasets[2].backgroundColor as string;
                    const newColor: string = percentageAmount > 100 ? 'rgba(255, 90, 0)' : 'rgba(255, 0, 0, 0.3)';
                    if (currentColor !== newColor) {
                        chart.data.datasets[2].backgroundColor = newColor;
                        chart.update();
                    }
                }
            }
        }
    };
    previewChartPlugins = [
        {
            beforeDraw: (chart: any) => {
                if (chart.config.options.plugins['elements']) {
                    // Get ctx from string
                    const ctx = (<any>chart).chart.ctx;
                    // Get options from the center object in options
                    const elementsConfig = chart.config.options.plugins['elements'];
                    const fontStyle = elementsConfig.fontStyle || 'Arial';
                    const txt = isFunction(elementsConfig.text) ? elementsConfig.text(chart) : elementsConfig.text;
                    const color = elementsConfig.color || '#000';
                    const sidePadding = elementsConfig.sidePadding || 20;
                    const sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2);
                    // Start with a base font of 30px
                    ctx.font = '30px ' + fontStyle;

                    // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                    const stringWidth = ctx.measureText(txt).width;
                    const elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                    // Find out how much the font can grow in width.
                    const widthRatio = elementWidth / stringWidth;
                    const newFontSize = Math.floor(30 * widthRatio);
                    const elementHeight = (chart.innerRadius * 2);

                    // Pick a new font size so it will not be larger than the height of label.
                    const fontSizeToUse = Math.min(newFontSize, elementHeight);

                    // Set font settings to draw it correctly.
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    const centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                    const centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 8) * 5;
                    ctx.font = fontSizeToUse + 'px ' + fontStyle;
                    ctx.fillStyle = color;

                    // Draw text in center
                    ctx.fillText(txt, centerX, centerY);
                }

                if (chart.config.options.plugins['dynamicColors']) {
                    chart.config.options.plugins['dynamicColors'](chart);
                }
            }
        }
    ];

    cellEditRow: PsrLineModel;
    cellEditCol: PsrDateColumn;
    prevCellEditAmount: number;

    editedItems: Map<PsrLineType, Set<PsrLineItem>> = new Map<PsrLineType, Set<PsrLineItem>>();

    constructor(
        private psrService: PsrService,
        private timesheetResource: TimesheetResource,
        private outcomeEntryResource: OutcomeEntryResource,
        private timesheetCategoryResource: TimesheetCategoryResource,
        private currencyResource: CurrencyResource,
        private changeDetectorRef: ChangeDetectorRef,
        private confirmationService: ConfirmationService,
        private notificationService: AlertService,
        private progressResource: ProgressResource,
        private genericFieldResource: GenericFieldResource,
        private messageService: MessageService,
        private stickyService: StickyService,
        private paymentResource: PaymentResource,
        private psrRevisionResource: PsrRevisionResource,
        public converterService: ConverterService,
        public userService: UserService,
        public authService: AuthService,
        private dialog: MatDialog,
    ) {
        this.currencyResource.list(true, {sort: [{field: 'name', order: 1}]})
            .subscribe({
                next: (response: RestResponseModel) => {
                    this.currencies = response.data;
                }
            });

        this.genericFieldResource.findAllByEntity(GenericFieldEntity.TIMESHEET_CATEGORY)
            .subscribe({
                next: (response: RestResponseModel) => {
                    this.genericFields = response.data;
                }
            });

        this.gridHeight = window.innerHeight - 150;
        this.graphRowHeight = Math.round(this.gridHeight / 8);
        this.graphHeight = this.graphRowHeight * 3;

        this.stickyService.registerField(STICKY_NAME, () => {
            return {
                leftSideOpen: this.leftSideOpen,
                rightSideOpen: this.rightSideOpen,
                rowsGrouping: pickBy(this.rowsGrouping, (value, key) => key.endsWith(Number.MIN_SAFE_INTEGER.toString())),
                mode: this.mode,
                ballanceChartHidden: this.ballanceChartHidden,
                paymentsChartHidden: this.paymentsChartHidden,
                outcomeChartHidden: this.outcomeChartHidden,
                planChartHidden: this.planChartHidden,
                factChartHidden: this.factChartHidden
            };
        });

        const stickyData: any = this.stickyService.getData();
        if (stickyData && stickyData[STICKY_NAME]) {
            Object.assign(this, stickyData[STICKY_NAME]);
        }

        if (!this.defaultSearch) {
            this.psrRevisionForm.disable();
        }

        this.onResize();
    }

    @HostListener('window:resize', ['$event'])
    onResize(event?): void {
        // 230 - sidenav width
        this.gridWidth = window.innerWidth - (this.leftSideOpen ? 230 : 0) - (this.rightSideOpen ? 230 : 0);
        this.graphWidth = this.gridWidth / 2;
    }

    getTimesheetCategoryLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.timesheetCategoryResource.getManagedTimesheetCategories(true, term, limit, offset)
            .pipe(
                tap((response: RestResponseModel) => {
                    if (!environment.production && !this.devInit && response.data && response.data.length === 1) {
                        this.devInit = true;
                        this.timesheetCategoryChanged(response.data[0]);
                    }
                })
            );
    }

    timesheetCategoryConverter = (response: RestResponseModel): SelectItem[] => {
        return this.converterService.defaultObjectConverter(response);
    }

    cellEditInit($event: any): void {
        if (this.cellEditRow && this.cellEditCol) {
            this.cellEditComplete();
        }

        const cellEditRow: PsrLineModel = $event.data;
        const cellEditCol: PsrDateColumn = $event.field;
        const item: PsrLineItem = this.getColumn(cellEditRow, cellEditCol);
        if (item && this.isCellEditAllowed(cellEditRow, item)) {
            this.cellEditRow = cellEditRow;
            this.cellEditCol = cellEditCol;
            this.prevCellEditAmount = this.getEditedItemControlValue(this.cellEditRow.key.type, item);
        }
    }

    cellEditComplete(): void {
        if (this.cellEditRow && this.cellEditCol) {
            const item: PsrLineItem = this.getColumn(this.cellEditRow, this.cellEditCol);
            if (item && this.isCellEditAllowed(this.cellEditRow, item)) {
                const controlValue: number = this.getEditedItemControlValue(this.cellEditRow.key.type, item);
                if (controlValue !== this.prevCellEditAmount) {
                    item.isDirty = true;

                    if (!item.entries || item.entries.length === 0) {
                        item.entries = [this.getEntityByRowAndCol(this.cellEditRow, this.cellEditCol)];
                    }

                    let editedItemsByType: Set<PsrLineItem> = this.editedItems.get(this.cellEditRow.key.type);
                    if (!editedItemsByType) {
                        editedItemsByType = new Set<PsrLineItem>();
                        this.editedItems.set(this.cellEditRow.key.type, editedItemsByType);
                    }
                    editedItemsByType.add(item);
                }
            }
            this.cellEditRow = null;
            this.cellEditCol = null;
            this.prevCellEditAmount = 0;
        }
    }

    cellEditCancel(): void {
        const item: PsrLineItem = this.getColumn(this.cellEditRow, this.cellEditCol);
        if (item && this.isCellEditAllowed(this.cellEditRow, item)) {
            item.amount = this.prevCellEditAmount;
        }
        this.cellEditRow = null;
        this.cellEditCol = null;
        this.prevCellEditAmount = 0;
    }

    saveEditedItems(): void {
        if (this.cellEditRow && this.cellEditCol) {
            // If cell is editable and user click "save", `cellEditComplete` is executed too late to detect changes.
            // So run additional change detection cycle.
            this.cellEditComplete();
        }

        if (this.editedItems.size) {
            let errorMessages: string[] = [];
            const updateActions: Observable<any>[] = [];
            this.editedItems.forEach((editedItemsByType: Set<PsrLineItem>, type: PsrLineType) => {
                if (editedItemsByType.size) {
                    let uiId: number = 0;
                    const data: BulkUpdateModel<number> = new BulkUpdateModel<number>();
                    editedItemsByType.forEach((item: PsrLineItem) => {
                        const entry: PsrLineModelEntry = cloneDeep(item.entries[0]);
                        (<any>entry).date = StaticService.dateToTimestamp(entry.date);
                        entry.amount = this.getEditedItemControlValue(type, item);
                        item.uiId = uiId++;

                        if (entry.amount === 0) {
                            data.idsToDelete.push({id: entry.id, uiId: item.uiId});
                        } else {
                            data.rowsForUpdate.push({data: entry, uiId: item.uiId});
                        }
                    });

                    const updateAction: Observable<any> = this.getResourceByType(type).saveAll(data)
                        .pipe(
                            tap(() => editedItemsByType.clear()),
                            catchError((response: HttpErrorResponse) => {
                                errorMessages = errorMessages.concat(this.processEditedItemsError(response, editedItemsByType, type));
                                return of(null);
                            })
                        );

                    updateActions.push(updateAction);
                }
            });

            zip(...updateActions)
                .pipe(tap(() => {
                    if (errorMessages.length) {
                        this.notificationService.post({
                            body: errorMessages,
                            type: AlertType.DANGER
                        }, 0);
                        throw null;
                    }
                }))
                .subscribe({
                    next: () => {
                        this.clearCellEditData();
                        this.notificationService.postSimple('ui.message.default.grid.save.body.edit');
                        this.refreshData();
                    },
                    error: () => {
                        this.changeDetectorRef.detectChanges();
                    }
                });
        }
    }

    isCellEditAllowed(entity: PsrLineModel, item: PsrLineItem): boolean {
        if (this.isEditAllowed && entity && entity.key && !entity.key.isGroupTotal && item
            && (this.mode === PsrMode.PLAN || entity.key.type === PsrLineType.PROGRESS)
        ) {
            if (item.entries && item.entries.length === 1) {
                return true;
            } else if ((!item.entries || item.entries.length === 0)
                && [PsrLineType.TIMESHEET, PsrLineType.PROGRESS].indexOf(entity.key.type) >= 0) {
                return true;
            }
        }
        return false;
    }

    leftSideOpenChange(): void {
        this.leftSideOpen = !this.leftSideOpen;
        this.onResize();
        if (this.financialChart) {
            this.financialChart.chart.render();
        }
        if (this.progressChart) {
            this.progressChart.chart.render();
        }
    }

    rightSideOpenChange(): void {
        this.rightSideOpen = !this.rightSideOpen;
        this.onResize();
        if (this.financialChart) {
            this.financialChart.chart.render();
        }
        if (this.progressChart) {
            this.progressChart.chart.render();
        }
    }

    refreshData(): void {
        this.clearCellEditData();

        this.psrService.fetchDataForPsr(this.selectedTimesheetCategory.id).subscribe({
            next: (
                [
                    groupedOutcomeEntries,
                    invoices,
                    groupedTimesheets,
                    groupedProgressList,
                    users,
                    departments,
                    customers,
                    suppliers,
                    subcontractors,
                    currencyRates,
                    departmentAccess
                ]: PsrData
            ) => {
                this.groupedOutcomeEntries = groupedOutcomeEntries;
                this.invoices = invoices;
                this.groupedTimesheets = groupedTimesheets;
                this.groupedProgressList = groupedProgressList;
                this.users = users;
                this.departments = departments;
                this.customers = customers;
                this.suppliers = suppliers;
                this.subcontractors = subcontractors;
                this.currencyRates = currencyRates;
                this.departmentAccessList = departmentAccess;

                this.calculateCalendarLimits();
                this.calculateCols();
                this.prepareRows();
            }
        });
    }

    fromDateChanged(value: Moment): void {
        if (value) {
            let tmpDateFrom: Moment = value.startOf('month');

            if (this.dateTo && tmpDateFrom.valueOf() > this.dateTo.valueOf()) {
                tmpDateFrom = moment([this.dateTo.year(), this.dateTo.month(), 1]);
            }

            this.dateFrom = tmpDateFrom;

            if (this.minDateFrom && tmpDateFrom.valueOf() < this.minDateFrom.valueOf()) {
                this.minDateFrom = moment(tmpDateFrom);
                this.calculateCols();
            }
            this.dateChanged();
        }
    }

    toDateChanged(value: Moment): void {
        if (value) {
            let tmpDateTo: Moment = value.endOf('month');

            if (this.dateFrom && tmpDateTo.valueOf() < this.dateFrom.valueOf()) {
                tmpDateTo = moment(this.dateFrom).endOf('month');
            }

            this.dateTo = tmpDateTo;

            if (this.maxDateTo && tmpDateTo.valueOf() > this.maxDateTo.valueOf()) {
                this.maxDateTo = moment(tmpDateTo);
                this.calculateCols();
            }
            this.dateChanged();
        }
    }

    timesheetCategoryChanged(value: TimesheetCategory): void {
        if (value) {
            this.selectedTimesheetCategory = value;

            this.isEditAllowed = this.selectedTimesheetCategory && this.selectedTimesheetCategory.isActive;
            this.resetRevisionsDropdown();
            this.selectedRevision = null;

            this.timesheetCategoryForm.get('initialBudget').setValue(this.selectedTimesheetCategory.initialBudget);
            this.timesheetCategoryForm.get('contractDays').setValue(this.selectedTimesheetCategory.contractDays);

            if (this.isEditAllowed) {
                this.timesheetCategoryForm.enable();
                this.psrRevisionForm.enable();
            } else {
                this.timesheetCategoryForm.disable();
                this.psrRevisionForm.disable();
            }
            this.psrRevisionForm.reset();

            this.isManualDates = false;
            this.refreshData();
        }
    }

    isManualDatesValueChanged($event: MatSlideToggleChange): void {
        this.isManualDates = $event.checked;
        this.updateCols();
    }

    moneyHoursReportTypeChange($event: MatSlideToggleChange): void {
        this.moneyHoursReportType = $event.checked;
    }

    modeChanged($event?: MatButtonToggleChange): void {
        if ($event) {
            this.mode = $event.value;
        }

        if (!this.selectedTimesheetCategory) {
            return;
        }

        this.isGridReady = false;
        this.changeDetectorRef.detectChanges();

        this.setRowsForMainGrid();
        this.updateCols();
        this.updateGraphVisibility();

        this.isGridReady = true;
    }

    customSort(event: SortEvent): void {
        this.psrService.customSort(event);
    }

    getRowGroupHidden(entity: PsrLineModelCore): boolean {
        return this.rowsGrouping[this.getRowGroupHeader(entity)];
    }

    toggleRowGroupHiddenState(entity: PsrLineModelCore): void {
        const groupHeader: string = this.getRowGroupHeader(entity);
        const currentValue: boolean = this.rowsGrouping[groupHeader];
        this.rowsGrouping[groupHeader] = !currentValue;
    }

    isRowHidden(entity: PsrLineModelCore): boolean {
        if (entity.key.isGroupTotal && !entity.key.isSubGroupTotal) {
            return false;
        }

        let key: string = entity.key.type.toString() + '_' + Number.MIN_SAFE_INTEGER;
        if (this.rowsGrouping[key]) {
            return true;
        }
        if (!entity.key.isSubGroupTotal) {
            key = entity.key.type.toString() + '_' + (!isNil(entity.key.subGroupId) ? entity.key.subGroupId : '');
            if (this.rowsGrouping[key]) {
                return true;
            }
        }
    }

    // if both `entity` and `col` exists, then we are in `edit` mode
    // if only `entity` exists - we are in `create with add params` mode
    // if neither exists - we are in `create` mode
    showModal(type: PsrLineType, item?: PsrLineModel, col?: PsrDateColumn, $event?: MouseEvent): void {
        if ($event) {
            $event.stopPropagation();
        }

        const params: MatDialogConfig<ListAwareParams<any>> = {
            width: 'inherit',
            maxWidth: '90vw',
            data: {
                rows: item && col ? item.weeks.get(col).entries : [],
                timesheetCategoryId: this.selectedTimesheetCategory.id
            },
        };

        switch (type) {
            case PsrLineType.INCOME: {
                (params.data as  PaymentEntryListParams).invoiceId = item && item.invoice ? item.invoice.id : null;
                (params.data as  PaymentEntryListParams).isForPlan = this.mode === PsrMode.PLAN;
                this.openModal(PaymentEntryListComponent, params);
                break;
            }
            case PsrLineType.OUTCOME: {
                if (item && !col) {
                    (params.data as OutcomeEntryListParams).parentId = item.key.id;
                    (params.data as OutcomeEntryListParams).outcomeEntryType = item.key.subGroupId;
                }
                (params.data as OutcomeEntryListParams).isForPlan = this.mode === PsrMode.PLAN;
                this.openModal(OutcomeEntryListComponent, params);
                break;
            }
            case PsrLineType.TIMESHEET: {
                if (item && !col) {
                    (params.data as TimesheetEntryListParams).userId = item.key.id;
                }
                (params.data as TimesheetEntryListParams).minDate = this.dateFrom;

                this.openModal(TimesheetEntryListComponent, params);
                break;
            }
            case PsrLineType.PROGRESS: {
                const progress: ProgressModel = item && col ? <ProgressModel>item.weeks.get(col).entries[0] : new ProgressModel();
                progress.timesheetCategoryId = this.selectedTimesheetCategory.id;
                progress.isForPlan = this.mode === PsrMode.PLAN;
                if (item) {
                    progress.parentId = item.key.id;
                    progress.progressType = ProgressType[item.key.subGroupId];
                }
                this.openModal(ProgressModalComponent, {data: {entity: progress}});
                break;
            }
            default: {
                throw new Error('Unsupported type');
            }
        }
    }

    showInvoiceModal(invoice: InvoiceModel = new InvoiceModel()): void {
        invoice.timesheetCategoryId = this.selectedTimesheetCategory.id;

        const params: InvoiceModalParams = {
            entity: invoice,
            forPsr: true
        };

        this.openModal(InvoiceModalComponent, {data: params});
    }

    showTimesheetEntryMultiAddModal(userId?: number): void {
        const params: FormAwareParams<any> = {
            entity: {
                userId,
                timesheetCategoryId: this.selectedTimesheetCategory.id,
            }
        };

        this.openModal(TimesheetEntryMultiAddModalComponent, {data: params});
    }

    showTimesheetEntryMultiAdjustModal(userId?: number): void {
        const params: FormAwareParams<any> = {
            entity: {
                userId,
                timesheetCategoryId: this.selectedTimesheetCategory.id
            }
        };

        this.openModal(TimesheetEntryMultiAdjustModalComponent, {data: params});
    }

    showProgressAdjustmentMultiModal(parentId: number, progressType: ProgressType): void {
        const params: FormAwareParams<any> = {
            entity: {
                parentId,
                progressType,
                timesheetCategoryId: this.selectedTimesheetCategory.id,
                isForPlan: this.mode === PsrMode.PLAN
            },
        };

        this.openModal(ProgressMultiAdjustModalComponent, {data: params});
    }

    bulkTimesheetRemoval(userId: number): void {
        this.confirmationService.show({
            onPrimaryClick: () => this.executeBulkTimesheetRemoval(userId),
            body: 'ui.confirmation.default.delete.body'
        });
    }

    getColumn(entity: PsrLineModel, col: PsrDateColumn): PsrLineItem {
        const weeks: Map<DateColumn, PsrLineItem> = this.isManualDates ? entity.weeksInPeriod : entity.weeks;
        return weeks != null ? weeks.get(col) : null;
    }

    hasUnits(entity: PsrLineModel): boolean {
        return !this.moneyHoursReportType && PsrLineType.TIMESHEET === entity.key.type;
    }

    isExpiredInvoice(entity: PsrLineModelTotal | PsrLineModel): boolean {
        let $total: PsrLineTotal;
        if (this.mode === PsrMode.PLAN) {
            $total = (<PsrLineModelTotal> entity).totalsPlan || (<PsrLineModel> entity).totals;
        } else {
            $total = (<PsrLineModelTotal> entity).totalsFact || (<PsrLineModel> entity).totals;
        }

        return !entity.invoice.datePosted
                && $total.amount < $total.amount2
                && new Date(entity.invoice.datePlanned).getTime() < (new Date()).getTime();
    }

    updateManagerFields(): void {
        if (this.timesheetCategoryForm.valid) {
            this.selectedTimesheetCategory.genericFieldValues = GenericFieldService.processFormValues(
                this.genericFields,
                <FormGroup> this.timesheetCategoryForm.get(GENERIC_FIELD_FORM_GROUP_NAME),
                this.selectedTimesheetCategory);
            this.selectedTimesheetCategory.initialBudget = this.timesheetCategoryForm.get('initialBudget').value;
            this.selectedTimesheetCategory.contractDays = this.timesheetCategoryForm.get('contractDays').value;

            this.timesheetCategoryResource.updateManagerFields(this.selectedTimesheetCategory)
                .subscribe({
                    next: (response: RestResponseModel) => {
                        this.notificationService.postSimple('ui.message.default.grid.save.body.edit');
                        this.selectedTimesheetCategory.genericFieldValues = response.data.genericFieldValues
                            .map((genericFieldValue) => omit(genericFieldValue, '$id'));
                    },
                    error: (error: HttpErrorResponse) => this.notificationService.processBackendValidation(error)
                });
        }
    }

    toggleBallance(): void {
        this.ballanceChartHidden = !this.ballanceChartHidden;

        const is0Hidden = this.financialChart.isDatasetHidden(0);
        if (is0Hidden !== this.ballanceChartHidden || this.planChartHidden) {
            this.financialChart.hideDataset(0, this.ballanceChartHidden || this.planChartHidden);
        }

        const is1Hidden = this.financialChart.isDatasetHidden(1);
        if (is1Hidden !== this.ballanceChartHidden || this.factChartHidden) {
            this.financialChart.hideDataset(1, this.ballanceChartHidden || this.factChartHidden);
        }
    }

    togglePayments(): void {
        this.paymentsChartHidden = !this.paymentsChartHidden;

        const is2Hidden = this.financialChart.isDatasetHidden(2);
        if (is2Hidden !== this.paymentsChartHidden || this.planChartHidden) {
            this.financialChart.hideDataset(2, this.paymentsChartHidden || this.planChartHidden);
        }

        const is3Hidden = this.financialChart.isDatasetHidden(3);
        if (is3Hidden !== this.paymentsChartHidden || this.factChartHidden) {
            this.financialChart.hideDataset(3, this.paymentsChartHidden || this.factChartHidden);
        }
    }

    toggleOutcome(): void {
        this.outcomeChartHidden = !this.outcomeChartHidden;

        const is4Hidden = this.financialChart.isDatasetHidden(4);
        if (is4Hidden !== this.outcomeChartHidden || this.planChartHidden) {
            this.financialChart.hideDataset(4, this.outcomeChartHidden || this.planChartHidden);
        }

        const is5Hidden = this.financialChart.isDatasetHidden(5);
        if (is5Hidden !== this.outcomeChartHidden || this.factChartHidden) {
            this.financialChart.hideDataset(5, this.outcomeChartHidden || this.factChartHidden);
        }
    }

    togglePlan(): void {
        this.planChartHidden = !this.planChartHidden;

        const is0Hidden = this.financialChart.isDatasetHidden(0);
        if (is0Hidden !== this.ballanceChartHidden || this.planChartHidden) {
            this.financialChart.hideDataset(0, this.ballanceChartHidden || this.planChartHidden);
        }

        const is2Hidden = this.financialChart.isDatasetHidden(2);
        if (is2Hidden !== this.paymentsChartHidden || this.planChartHidden) {
            this.financialChart.hideDataset(2, this.paymentsChartHidden || this.planChartHidden);
        }

        const is4Hidden = this.financialChart.isDatasetHidden(4);
        if (is4Hidden !== this.outcomeChartHidden || this.planChartHidden) {
            this.financialChart.hideDataset(4, this.outcomeChartHidden || this.planChartHidden);
        }

        this.progressChart.hideDataset(0, this.planChartHidden);
    }

    toggleFact(): void {
        this.factChartHidden = !this.factChartHidden;

        const is1Hidden = this.financialChart.isDatasetHidden(1);
        if (is1Hidden !== this.ballanceChartHidden || this.factChartHidden) {
            this.financialChart.hideDataset(1, this.ballanceChartHidden || this.factChartHidden);
        }

        const is3Hidden = this.financialChart.isDatasetHidden(3);
        if (is3Hidden !== this.paymentsChartHidden || this.factChartHidden) {
            this.financialChart.hideDataset(3, this.paymentsChartHidden || this.factChartHidden);
        }

        const is5Hidden = this.financialChart.isDatasetHidden(5);
        if (is5Hidden !== this.outcomeChartHidden || this.factChartHidden) {
            this.financialChart.hideDataset(5, this.outcomeChartHidden || this.factChartHidden);
        }

        this.progressChart.hideDataset(1, this.factChartHidden);
    }

    exportData(): void {
        this.psrService.exportData(
            this.calculationResult,
            this.selectedTimesheetCategory,
            this.dateLimits,
            this.genericFields);
    }

    saveRevision(): void {
        const data: string = JSON.stringify(this.calculationResult, (key: any, value: any) => {
            if (value instanceof Map) {
                return Array.from(value.entries());
            }
            return value;
        });
        const psrRevision: PsrRevision = {
            id: 0,
            name: this.psrRevisionForm.value.name,
            timesheetCategoryId: this.selectedTimesheetCategory.id,
            data: data
        };
        this.psrRevisionResource.insert(psrRevision)
            .subscribe({
                next: () => {
                    this.notificationService.postSimple('ui.message.default.grid.save.body.edit');
                    this.psrRevisionForm.reset();
                    this.resetRevisionsDropdown();
                },
                error: (error: HttpErrorResponse) => this.notificationService.processBackendValidation(error)
            });
    }

    getPsrRevisionsLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.psrRevisionResource.getByTimesheetCategoryId(this.selectedTimesheetCategory.id, term, limit, offset);
    }

    psrRevisionChanged(psrRevision: PsrRevision): void {
        this.selectedRevision = psrRevision;
        this.isManualDates = false;
        if (psrRevision) {
            this.isEditAllowed = false;
            this.updateCols();
            if (!this.cachedCalculationResult) {
                this.cachedCalculationResult = this.calculationResult;
            }

            this.psrRevisionForm.disable();
            this.psrRevisionForm.reset();

            this.calculationResult = JSON.parse(psrRevision.data, (key: any, value: any) => {
                if (['weeks', 'weeksInPeriod'].indexOf(key) > -1) {
                    const result: Map<PsrDateColumn, PsrLineItem> = new Map();
                    value.forEach((entry: any[]) => {
                        const _key: PsrDateColumn = entry[0];
                        const _value: PsrLineItem = entry[1];
                        const _col: PsrDateColumn = this.cols.find((col: PsrDateColumn) => {
                            return col.week === _key.week && col.year === _key.year;
                        });

                        if (!_col) {
                            this.cachedCalculationResult = null;
                            this.notificationService.post({
                                type: AlertType.WARNING,
                                body: 'Something went wrong while loading Psr Revision. Please make screenshot and send it to support.'
                            });
                        }

                        result.set(_col, _value);
                    });
                    return result;
                }
                return value;
            });
        } else {
            this.calculationResult = this.cachedCalculationResult;
            this.cachedCalculationResult = null;
            this.isEditAllowed = this.selectedTimesheetCategory && this.selectedTimesheetCategory.isActive;

            this.psrRevisionForm.enable();
        }

        this.prepareGraphs();
    }

    columnTrackBy(index: number, col: PsrDateColumn): number {
        return col.weekStart.getTime() + col.weekEnd.getTime();
    }

    private openModal(component: ComponentType<any>, params: MatDialogConfig): void {
        this.dialog.open(component, params)
        .afterClosed()
        .subscribe({
            next: (result: ModalResult) => {
                if (result && result.isRefreshRequired) {
                    this.refreshData();
                }
            }
        });
    }

    private getRowGroupHeader(entity: PsrLineModelCore): string {
        return entity.key.type.toString() + '_' + (!isNil(entity.key.subGroupId) ? entity.key.subGroupId : '');
    }

    private getEditedItemControlValue(type: PsrLineType, item: PsrLineItem): number {
        return type === PsrLineType.TIMESHEET ? item.units : item.amount;
    }

    private clearCellEditData(): void {
        this.cellEditRow = null;
        this.cellEditCol = null;
        this.prevCellEditAmount = 0;
        this.editedItems = new Map<PsrLineType, Set<PsrLineItem>>();
    }

    private processEditedItemsError(response: HttpErrorResponse, editedItemsByType: Set<PsrLineItem>, type: PsrLineType): string[] {
        const messages: string[] = [];
        if (response.status === 422) {
            const json: any = response.error;

            if (isArray(json)) {
                json.forEach((error) => {
                    if (!isNil(error.id)) {
                        const matchItem: PsrLineItem = Array.from(editedItemsByType)
                            .find((item: PsrLineItem) => item.uiId === parseInt(error.id));
                        if (matchItem) {
                            const errorMessage: string = this.messageService.resolveMessage(error.message);
                            matchItem.errorMessage = errorMessage;

                            const week: number = StaticService.calculateWeek(matchItem.entries[0].date);
                            const $type: string = this.messageService.resolveMessage(this.getEntryNameByType(type));
                            messages.push(`'${$type}' Entry at week '${week}' returned error: '${errorMessage}'`);
                        }
                    }
                });
            } else {
                this.notificationService.processBackendValidation(response);
            }
        }

        return messages;
    }

    private getEntityByRowAndCol(rowModel: PsrLineModel, col: PsrDateColumn): PsrLineModelEntry {
        switch (rowModel.key.type) {
            case PsrLineType.TIMESHEET: {
                const timesheet = new Timesheet();
                timesheet.date = col.weekStart;
                timesheet.status = TimesheetStatus.PLANNED;
                timesheet.timesheetCategoryId = this.selectedTimesheetCategory.id;
                timesheet.userId = rowModel.key.id;
                return timesheet;
            }
            case PsrLineType.PROGRESS: {
                const progress = new ProgressModel();
                progress.date = col.weekStart;
                progress.timesheetCategoryId = this.selectedTimesheetCategory.id;
                progress.progressType = ProgressType[rowModel.key.subGroupId];
                progress.parentId = rowModel.key.id;
                progress.isForPlan = this.mode === PsrMode.PLAN;
                return progress;
            }
            default: {
                throw Error('Unexpecter row type.');
            }
        }
    }

    private getEntryNameByType(type: PsrLineType): string {
        switch (type) {
            case PsrLineType.INCOME: {
                return 'ui.text.payment.title';
            }
            case PsrLineType.OUTCOME: {
                return 'ui.text.outcomeEntry.title';
            }
            case PsrLineType.TIMESHEET: {
                return 'ui.text.default.timesheet';
            }
            case PsrLineType.PROGRESS: {
                return 'ui.text.progress.title';
            }
            default: {
                throw Error('Unexpecter row type.');
            }
        }
    }

    private getResourceByType(type: PsrLineType): FormAwareResource {
        switch (type) {
            case PsrLineType.INCOME: {
                return this.paymentResource;
            }
            case PsrLineType.OUTCOME: {
                return this.outcomeEntryResource;
            }
            case PsrLineType.TIMESHEET: {
                return this.timesheetResource;
            }
            case PsrLineType.PROGRESS: {
                return this.progressResource;
            }
            default: {
                throw Error('Unexpecter row type.');
            }
        }
    }

    private generatePreviewGraphLabel(datasetIndex: number, chart: Chart): ChartLegendLabelItem {
        return {
            datasetIndex: datasetIndex,
            text: chart.config.data.datasets[datasetIndex].label + ' ' + chart.config.data.datasets[datasetIndex].data[0] + '%',
            fillStyle: <string>chart.config.data.datasets[datasetIndex].backgroundColor
        };
    }

    private updateGraphVisibility(): void {
        this.financialChartData[0].hidden = this.ballanceChartHidden || this.planChartHidden;
        this.financialChartData[1].hidden = this.ballanceChartHidden || this.factChartHidden;
        this.financialChartData[2].hidden = this.paymentsChartHidden || this.planChartHidden;
        this.financialChartData[3].hidden = this.paymentsChartHidden || this.factChartHidden;
        this.financialChartData[4].hidden = this.outcomeChartHidden || this.planChartHidden;
        this.financialChartData[5].hidden = this.outcomeChartHidden || this.factChartHidden;
    }

    private dateChanged(): void {
        if (this.selectedTimesheetCategory) {
            this.isManualDates = true;
            this.prepareRows();
        }
    }

    private calculateCols(): void {
        this.calculatedCols = <PsrDateColumn[]> StaticService.calculateWeekColsInPeriod(
            this.minDateFrom.year(),
            this.minDateFrom.month(),
            this.maxDateTo.year(),
            this.maxDateTo.month());
    }

    private updateCols(): void {
        if (this.isManualDates) {
            if (this.rows && this.rows.length > 0) {
                this.cols = Array.from(this.rows[0].weeksInPeriod.keys());
            } else {
                // Fallback in case when project doesn't have data yet at all.
                // In all other cases cols should come from calculationResult.
                this.cols = <PsrDateColumn[]> StaticService.calculateWeekColsInPeriod(
                    this.dateFrom.year(),
                    this.dateFrom.month(),
                    this.dateTo.year(),
                    this.dateTo.month()
                );
            }
        } else {
            this.cols = this.calculatedCols;
        }

        this.weekChartLabels = this.cols.map((col: PsrDateColumn) => this.getWeekGraphLabelPrefix() + col.week);
    }

    private prepareRows(): void {
        this.calculationResult = this.psrService.preparePSR(
            this.invoices,
            this.groupedOutcomeEntries,
            this.groupedTimesheets,
            this.groupedProgressList,
            this.calculatedCols,
            this.users,
            this.customers,
            this.suppliers,
            this.subcontractors,
            this.currencies,
            this.departments,
            this.currencyRates,
            this.departmentAccessList,
            this.dateFrom.toDate(),
            this.dateTo.toDate(),
            this.selectedTimesheetCategory.contractDays,
            false);

        this.prepareGraphs();
    }

    private prepareGraphs(): void {
        if (this.calculationResult) {
            this.formatPreviewGraphSource(this.calculationResult.legend.progressPlan, <number[]>this.previewChartDataSets[0].data);
            this.formatPreviewGraphSource(this.calculationResult.legend.progressFact, <number[]>this.previewChartDataSets[1].data);
            this.formatPreviewGraphSource(this.calculationResult.legend.budgetSpent, <number[]>this.previewChartDataSets[2].data);

            this.formatTotalRowGraphSource(this.calculationResult.plan.totalRow, <number[]>this.financialChartData[0].data);
            this.formatTotalRowGraphSource(this.calculationResult.fact.totalRow, <number[]>this.financialChartData[1].data);

            this.formatPaymentGraphSource(this.calculationResult.plan.incomeSubTotalRow, <number[]>this.financialChartData[2].data);
            this.formatPaymentGraphSource(this.calculationResult.fact.incomeSubTotalRow, <number[]>this.financialChartData[3].data);

            this.formatTotalRowGraphSource(this.calculationResult.plan.outcomeTotalRow, <number[]>this.financialChartData[4].data, true);
            this.formatTotalRowGraphSource(this.calculationResult.fact.outcomeTotalRow, <number[]>this.financialChartData[5].data, true);

            this.formatTotalRowGraphSource(this.calculationResult.plan.progressSubTotalRow, <number[]>this.progressChartData[0].data);
            this.formatTotalRowGraphSource(this.calculationResult.fact.progressSubTotalRow, <number[]>this.progressChartData[1].data);

            this.modeChanged();
        }
    }

    private getWeekGraphLabelPrefix(): string {
        return this.messageService.resolveMessage('ui.text.default.week') + ' ';
    }

    private formatPreviewGraphSource(amount: number, target: number[]): void {
        target.length = 0;
        const percentage: number = amount ? Math.round(amount * 100) / 100 : 0;
        target.push(percentage);
        target.push(100 - percentage);
    }

    private formatTotalRowGraphSource(source: PsrLineModel, target: number[], negate: boolean = false): void {
        target.length = 0;
        if (source && source.weeks) {
            Array.from(source.weeks.values()).forEach((item: PsrLineItem, i: number) => {
                target.push((negate ? -1 : 1) * item.amount);
            });
        }
    }

    private formatPaymentGraphSource(source: PsrLineModel, target: number[]): void {
        target.length = 0;
        if (source && source.weeks) {
            Array.from(source.weeks.values()).forEach((item: PsrLineItem, i: number) => {
                target.push(item.amount + (target[i - 1] || 0));
            });
        }
    }

    private executeBulkTimesheetRemoval(userId: number): void {
        this.timesheetResource.bulkTimesheetRemoval(userId, this.selectedTimesheetCategory.id)
                .subscribe({
                    next: (response: RestResponseModel) => {
                        this.refreshData();
                        this.notificationService.post({
                            body: 'ui.message.default.grid.delete.body',
                            type: AlertType.SUCCESS
                        });
                    },
                    error: (response: HttpErrorResponse) => {
                        this.notificationService.processBackendValidation(response);
                    }
            });
    }

    private calculateCalendarLimits(): void {
        this.dateLimits = this.psrService.calculateDateLimitsByData(
            this.invoices,
            this.groupedOutcomeEntries,
            this.groupedTimesheets,
            this.groupedProgressList);

        this.minDateFrom = moment(this.dateLimits.minDateFrom);
        this.maxDateTo = moment(this.dateLimits.maxDateTo);

        if (!this.isManualDates) {
            this.dateFrom = moment(this.minDateFrom);
            this.dateTo = moment(this.maxDateTo);
        }
    }

    private resetRevisionsDropdown(): void {
        if (this.psrRevisionSelection) {
            this.psrRevisionSelection.selectComponent.reset();
        }
    }

    private setRowsForMainGrid(): void {
        this.rows = this.mode === PsrMode.PLAN ? this.calculationResult.plan.rows : this.calculationResult.fact.rows;
    }

}
