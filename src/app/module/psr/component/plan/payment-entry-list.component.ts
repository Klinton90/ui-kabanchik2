import { Component, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from '@angular/core';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { ListAwareModalComponent, ListAwareParams } from 'app/module/shared/component/modal/list-aware-modal.component';
import { FormAwareModalResult } from 'app/module/shared/component/modal/success-form-aware-modal-event.model';
import { PaymentModel } from 'app/module/admin/component/payment/payment.model';
import { PaymentModalComponent, PaymentModalParams } from 'app/module/admin/component/payment/payment-modal.component';
import { PaymentResource } from 'app/module/admin/component/payment/payment.resource';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export class PaymentEntryListParams extends ListAwareParams<PaymentModel> {
    invoiceId: number;
    isForPlan: boolean;
}

@Component({
    moduleId: module.id,
    templateUrl: 'payment-entry-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentEntryListComponent extends ListAwareModalComponent<
    PaymentEntryListComponent,
    PaymentModalComponent,
    PaymentModel
> {

    modal = PaymentModalComponent;

    protected getModalParams(): Omit<PaymentModalParams, 'entity'> {
        return {
            forPsr: true
        };
    }

    constructor(
        public dialogRef: MatDialogRef<PaymentEntryListComponent>,
        protected confirmationService: ConfirmationService,
        protected resource: PaymentResource,
        protected notificationService: AlertService,
        protected ref: ChangeDetectorRef,
        protected dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: PaymentEntryListParams
    ) {
        super();
    }

    protected getNewFormEntityInstance(): PaymentModel {
        const entity: PaymentModel = new PaymentModel();
        entity.invoiceId = this.data.invoiceId;
        entity.isForPlan = this.data.isForPlan;
        return entity;
    }

    // Override
    // onSuccess(event: FormAwareModalResult): void {
    //     const indexToUpdate: number = this.data.rows.findIndex((row: PaymentModel) => row.id === event.entity.id);
    //     const _entity: PaymentModel = this.data.rows[indexToUpdate];
    //     this.isRefreshRequired = true;

    //     event.entity.date.setTime(event.entity.date.getTime() - event.entity.date.getTimezoneOffset() * 60 * 1000);
    //     this.data.rows[indexToUpdate] = event.entity;

    //     if (event.isAddMode || _entity.date.getTime() !== event.entity.date.getTime()) {
    //         this.hideAction();
    //     }
    // }

}
