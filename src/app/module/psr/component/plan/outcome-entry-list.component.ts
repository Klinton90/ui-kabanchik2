import { Component, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from '@angular/core';
import { OutcomeEntryModel, OutcomeEntryType } from 'app/module/admin/component/outcome-entry/outcome-entry.model';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { OutcomeEntryResource } from 'app/module/admin/component/outcome-entry/outcome-entry.resource';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import {
    OutcomeEntryModalComponent,
    OutcomeEntryModalParams
} from 'app/module/admin/component/outcome-entry/outcome-entry-modal.component';
import { ListAwareModalComponent, ListAwareParams } from 'app/module/shared/component/modal/list-aware-modal.component';
import { FormAwareModalResult } from 'app/module/shared/component/modal/success-form-aware-modal-event.model';
import { isNil } from 'lodash';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export class OutcomeEntryListParams extends ListAwareParams<OutcomeEntryModel> {
    timesheetCategoryId: number;
    parentId: number;
    outcomeEntryType: OutcomeEntryType;
    isForPlan: boolean;
}

@Component({
    moduleId: module.id,
    templateUrl: 'outcome-entry-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutcomeEntryListComponent extends ListAwareModalComponent<
    OutcomeEntryListComponent,
    OutcomeEntryModalComponent,
    OutcomeEntryModel
> {

    modal = OutcomeEntryModalComponent;

    constructor(
        public dialogRef: MatDialogRef<OutcomeEntryListComponent>,
        protected confirmationService: ConfirmationService,
        protected resource: OutcomeEntryResource,
        protected notificationService: AlertService,
        protected ref: ChangeDetectorRef,
        protected dialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: OutcomeEntryListParams
    ) {
        super();
    }

    // Override
    protected getModalParams(): Omit<OutcomeEntryModalParams, 'entity'> {
        return {
            forPsr: true
        };
    }

    // Override
    protected getNewFormEntityInstance(): OutcomeEntryModel {
        const entity: OutcomeEntryModel = new OutcomeEntryModel();
        entity.timesheetCategoryId = this.data.timesheetCategoryId;
        entity.isForPlan = this.data.isForPlan;
        if (this.data.parentId && !isNil(this.data.outcomeEntryType)) {
            entity.parentId = this.data.parentId;
            entity.outcomeEntryType = OutcomeEntryType[this.data.outcomeEntryType];
        }
        return entity;
    }

    // Override
    // onSuccess(event: FormAwareModalResult): void {
    //     const indexToUpdate: number = this.data.rows.findIndex((row: OutcomeEntryModel) => row.id === event.entity.id);
    //     const _entity: OutcomeEntryModel = this.data.rows[indexToUpdate];
    //     this.isRefreshRequired = true;

    //     event.entity.date.setTime(event.entity.date.getTime() - event.entity.date.getTimezoneOffset() * 60 * 1000);
    //     this.data.rows[indexToUpdate] = event.entity;

    //     if (event.isAddMode || _entity.parentId !== event.entity.parentId || _entity.date.getTime() !== event.entity.date.getTime()) {
    //         this.hideAction();
    //     }
    // }

}
