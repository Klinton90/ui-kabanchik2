import { Component, ChangeDetectionStrategy, ChangeDetectorRef, Inject } from '@angular/core';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { BulkProgressAdjustment } from './progress.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProgressResource } from './progress.resource';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { UtilService } from 'app/module/core/service/util.service';

@Component({
    moduleId: module.id,
    templateUrl: 'progress-multi-adjust-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressMultiAdjustModalComponent extends FormAwareModal<ProgressMultiAdjustModalComponent, any> {

    newFormEntityInstance: BulkProgressAdjustment = new BulkProgressAdjustment();

    form: FormGroup = new FormGroup({
        adjustmentWeeks: new FormControl('', [Validators.required]),
        adjustmentFromDate: new FormControl(''),
        adjustmentToDate: new FormControl(''),
        // hidden
        timesheetCategoryId: new FormControl('', [Validators.required]),
        progressType: new FormControl('', [Validators.required]),
        parentId: new FormControl('', [Validators.required]),
        isForPlan: new FormControl('', [Validators.required])
    });

    constructor(
        public dialogRef: MatDialogRef<ProgressMultiAdjustModalComponent>,
        protected resource: ProgressResource,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected utilService: UtilService,
        protected ref: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) data: FormAwareParams<any>
    ) {
        super();
        super.show(data);
    }

    // @Override
    protected getSaveAction(): string {
        return 'bulkProgressAdjustment';
    }

    // @Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();
        if (data.adjustmentFromDate) {
            data.adjustmentFromDate = data.adjustmentFromDate.valueOf();
        }
        if (data.adjustmentToDate) {
            data.adjustmentToDate = data.adjustmentToDate.valueOf();
        }

        return data;
    }
}
