import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { ProgressModel, ProgressType } from 'app/module/psr/component/progress/progress.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProgressResource } from 'app/module/psr/component/progress/progress.resource';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { UtilService } from 'app/module/core/service/util.service';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { SubcontractorResource } from 'app/module/admin/component/subcontractor/subcontractor.resource';
import { ConverterService } from 'app/module/core/service/converter.service';
import { DepartmentResource } from 'app/module/admin/component/department/department.resource';
import { Observable } from 'rxjs';
import { SelectItem } from 'primeng/api/selectitem';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { Moment } from 'moment';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { ngSelectInfiniteDataCallback } from 'app/module/shared/component/ng-select-infinite-wrapper/ng-select-infinite-wrapper.component';

@Component({
    moduleId: module.id,
    templateUrl: 'progress-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProgressModalComponent extends FormAwareModal<ProgressModalComponent, ProgressModel> {

    newFormEntityInstance: ProgressModel = new ProgressModel();

    progressTypeOptions: SelectItem[] = [];

    parentLoadDataCallback: ngSelectInfiniteDataCallback;

    form: FormGroup = new FormGroup({
        id: new FormControl(''),
        date: new FormControl('', [Validators.required]),
        amount: new FormControl('', [Validators.required, Validators.min(1), Validators.max(100)]),
        description: new FormControl('', [Validators.maxLength(255)]),
        progressType: new FormControl('', [Validators.required]),
        parentId: new FormControl('', [Validators.required]),
        // hidden
        timesheetCategoryId: new FormControl('', [Validators.required]),
        isForPlan: new FormControl()
    });

    constructor(
        public dialogRef: MatDialogRef<ProgressModalComponent>,
        protected resource: ProgressResource,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected utilService: UtilService,
        protected serviceResource: ServiceResource,
        private subcontractorResource: SubcontractorResource,
        private departmentResource: DepartmentResource,
        public converterService: ConverterService,
        protected ref: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) data: FormAwareParams<ProgressModel>
    ) {
        super();

        this.form.controls['isForPlan'].disable();

        super.show(data);

        this.calcParentLoadDataCallback();

        this.serviceResource.getLocalizedEnumOptions('kabanchik.system.progress.domain.ProgressType')
            .subscribe({
                next: (response: RestResponseModel) => {
                    this.progressTypeOptions = response.data;
                }
            });
    }

    getDepartmentLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        const timesheetCategoryId: number = this.form.controls['timesheetCategoryId'].value;
        const isForPlan: boolean = this.form.controls['isForPlan'].value;
        return this.departmentResource.getForProgressModal(timesheetCategoryId, isForPlan, term, limit, offset);
    }

    getSubcontractorLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        const timesheetCategoryId: number = this.form.controls['timesheetCategoryId'].value;
        return this.subcontractorResource.getForProgressModal(timesheetCategoryId, term, limit, offset);
    }

    typeChanged(): void {
        this.calcParentLoadDataCallback();
        this.form.get('parentId').setValue(null);
    }

    deleteAction(): void {
        this.resource.delete(this.form.get('id').value)
            .subscribe((response: RestResponseModel) => {
                this.successHandler(response, {
                    body: 'ui.message.default.grid.delete.body',
                    type: AlertType.SUCCESS
                });
            });
    }

    onDateChange(newValue: Moment): void {
        if (newValue && newValue.day && newValue.day() !== 1) {
            this.form.controls['date'].setValue(newValue.day(1));
        }
    }

    // Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();
        data.date = data.date.day(1).valueOf();
        return data;
    }

    private calcParentLoadDataCallback(): void {
        const type: ProgressType | LocalizedEnum | string = this.form.getRawValue().progressType;
        if (type === ProgressType.SUB_CONTRACTOR) {
            this.parentLoadDataCallback = this.getSubcontractorLoadDataCallback;
        } else if (type === ProgressType.DEPARTMENT) {
            this.parentLoadDataCallback = this.getDepartmentLoadDataCallback;
        } else if (type === 'SUB_CONTRACTOR') {
            this.parentLoadDataCallback = this.getSubcontractorLoadDataCallback;
        } else if (type === 'DEPARTMENT') {
            this.parentLoadDataCallback = this.getDepartmentLoadDataCallback;
        } else if (type instanceof LocalizedEnum) {
            if (type.value === 'SUB_CONTRACTOR') {
                this.parentLoadDataCallback = this.getSubcontractorLoadDataCallback;
            } else if (type.value === 'DEPARTMENT') {
                this.parentLoadDataCallback = this.getDepartmentLoadDataCallback;
            }
        } else {
            throw Error('Unexpected type');
        }
    }

}
