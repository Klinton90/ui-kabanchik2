import { HttpClient, HttpParams } from '@angular/common/http';
import { RestResource } from '../../../core/resource/rest.resource';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { map } from 'rxjs/operators';
import { isNil } from 'lodash';
import { BulkProgressAdjustment } from './progress.model';

@Injectable({
    providedIn: 'root'
})
export class ProgressResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('progress');
    }

    // @Deprecated
    findAllByTimesheetCategoryid(timesheetCategoryId: number, forPlan?: boolean): Observable<any> {
        let params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());

        if (!isNil(forPlan)) {
            params = params.append('forPlan', forPlan.toString());
        }

        return this._executeRequest({
            method: 'GET',
            url: '/findAllByTimesheetCategoryid',
            params: params
        }).pipe(
            map((response: RestResponseModel) => this.responseConverter(response.data))
        );
    }

    getForPSR(timesheetCategoryId: number): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/getForPSR',
            params: new HttpParams().append('timesheetCategoryId', timesheetCategoryId.toString())
        }).pipe(
            map((response: RestResponseModel) => this.responseConverter(response.data))
        );
    }

    bulkProgressAdjustment(data: BulkProgressAdjustment): Observable<any> {
        return this._executeRequest({
            method: 'POST',
            url: '/bulkProgressAdjustment',
        }, data);
    }

    private responseConverter(response: any[]): any[] {
        response.forEach((row) => {
            const date: Date = new Date(row.date);
            row.date = date;
        });

        return response;
    }

}
