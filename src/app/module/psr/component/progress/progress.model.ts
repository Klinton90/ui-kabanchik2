import { IDomainModel } from '../../../core/model/i-domain.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';

export class ProgressModel implements IDomainModel<number> {
    id: number;
    date: Date;
    timesheetCategoryId: number = null;
    amount: number = 0;
    description: string = '';
    progressType: ProgressType | LocalizedEnum | string = 'SUB_CONTRACTOR';
    parentId: number = null;
    isForPlan: boolean = false;
}

export enum ProgressType {
    SUB_CONTRACTOR,
    DEPARTMENT
}

export class BulkProgressAdjustment {
    timesheetCategoryId: number;
    progressType: ProgressType;
    parentId: number = null;
    isForPlan: boolean = false;
    adjustmentWeeks: number;
    adjustmentFromDate?: Date;
    adjustmentToDate?: Date;
}
