import { NgModule } from '@angular/core';
import { LoaderComponent } from './component/loader/loader.component';
import { AppSharedModule } from '../shared/shared.module';
import { ConfirmationComponent } from './component/confirmation/confirmation.component';
import { AlertComponent } from './component/alert/alert.component';

@NgModule({
    imports: [
        // angular

        // custom
        AppSharedModule
    ],
    declarations: [
        AlertComponent,
        LoaderComponent,
        ConfirmationComponent
    ],
    exports: [
        LoaderComponent,
    ],
    entryComponents: [
        ConfirmationComponent,
        AlertComponent
    ]
})
export class CoreModule {}
