import 'reflect-metadata';
import { forOwn, isNil } from 'lodash';

export function mandatoryPropertyValidator(target: any, requiredProps: string[]) {
    for (const prop of requiredProps) {
        if (prop.indexOf('||') > -1) {
            const propParts: string[] = prop.split('||').map(propPart => propPart.trim());
            for (const propPart of propParts) {
                if (!target[propPart]) {
                    throw Error('One of these properties \''
                        + propParts.join('\', \'')
                        + '\' should be assigned to \''
                        + target.constructor.toString().match(/\w+/g)[1]
                        + '\'!');
                }
            }
        } else if (prop.indexOf('|') > -1) {
            let propFound: boolean = false;
            const propParts: string[] = prop.split('|').map(propPart => propPart.trim());
            for (const propPart of propParts) {
                if (target[propPart]) {
                    if (!propFound) {
                        propFound = true;
                    } else {
                        throw Error('Only SINGLE property of \''
                            + propParts.join('\', \'')
                            + '\' should be assigned to \''
                            + target.constructor.toString().match(/\w+/g)[1]
                            + '\'!');
                    }
                }
            }
            if (!propFound) {
                throw Error('One of these properties \''
                    + propParts.join('\', \'')
                    + '\' should be assigned to \''
                    + target.constructor.toString().match(/\w+/g)[1]
                    + '\'!');
            }
        } else {
            if (!target[prop]) {
                throw Error('Property \''
                    + prop
                    + '\' is required for \''
                    + target.constructor.toString().match(/\w+/g)[1]
                    + '\'!');
            }
        }
    }
}

const mandatoryMetadataKey = Symbol('mandatory');

// Always define default value!!!
// @Mandatory() @Input() title: string = null;
export function Mandatory() {
    return (target, property) => {
        Reflect.defineMetadata(mandatoryMetadataKey, true, target, property);
    };
}

export function mandatoryPropertyValidator2(target: any) {
    forOwn(target, function(value: any, key: string) {
        if (Reflect.getMetadata(mandatoryMetadataKey, target, key) === true && isNil(value)) {
            throw Error('Property \''
                + key
                + '\' is required for \''
                + target.constructor.toString().match(/\w+/g)[1]
                + '\'!');
        }
    });
}
