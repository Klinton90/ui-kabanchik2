import {InjectionToken} from '@angular/core';
import { SelectItem } from 'primeng/api/selectitem';

export const YES_TEXT_TOKEN = new InjectionToken<string>('YES_TEXT_TOKEN');
export const YES_TEXT_DEFAULT = 'ui.text.default.yes';

export const NO_TEXT_TOKEN = new InjectionToken<string>('NO_TEXT_TOKEN');
export const NO_TEXT_DEFAULT = 'ui.text.default.no';

export const OK_TEXT_TOKEN = new InjectionToken<string>('OK_TEXT_TOKEN');
export const OK_TEXT_DEFAULT = 'ui.text.default.ok';

export const CANCEL_TEXT_TOKEN = new InjectionToken<string>('CANCEL_TEXT_TOKEN');
export const CANCEL_TEXT_DEFAULT = 'ui.text.default.cancel';

export const CLOSE_TEXT_TOKEN = new InjectionToken<string>('CLOSE_TEXT_TOKEN');
export const CLOSE_TEXT_DEFAULT = 'ui.text.default.close';

export const DONE_TEXT_TOKEN = new InjectionToken<string>('DONE_TEXT_TOKEN');
export const DONE_TEXT_DEFAULT = 'ui.text.default.done';

export const CLEAR_TEXT_TOKEN = new InjectionToken<string>('CLEAR_TEXT_TOKEN');
export const CLEAR_TEXT_DEFAULT = 'ui.text.default.clear';

export const TODAY_TEXT_TOKEN = new InjectionToken<string>('TODAY_TEXT_TOKEN');
export const TODAY_TEXT_DEFAULT = 'ui.text.default.today';

export const NOW_TEXT_TOKEN = new InjectionToken<string>('NOW_TEXT_TOKEN');
export const NOW_TEXT_DEFAULT = 'ui.text.default.now';

export const CONFIRMATION_TITLE_TOKEN = new InjectionToken<string>('CONFIRMATION_TITLE_TOKEN');
export const CONFIRMATION_TITLE_DEFAULT = 'ui.confirmation.default.title';

export const CONFIRMATION_BODY_ICON_TOKEN = new InjectionToken<string>('CONFIRMATION_BODY_ICON_TOKEN');
export const CONFIRMATION_BODY_ICON_DEFAULT = 'fas fa-question-circle';

export const CONFIRMATION_PRIMARY_ICON_TOKEN = new InjectionToken<string>('CONFIRMATION_PRIMARY_ICON_TOKEN');
export const CONFIRMATION_PRIMARY_ICON_DEFAULT = 'fas fa-check';

export const CONFIRMATION_SECONDARY_ICON_TOKEN = new InjectionToken<string>('CONFIRMATION_SECONDARY_ICON_TOKEN');
export const CONFIRMATION_SECONDARY_ICON_DEFAULT = 'fas fa-times';

export const MESSAGE_TITLE_DANGER_TOKEN = new InjectionToken<string>('MESSAGE_TITLE_DANGER_TOKEN');
export const MESSAGE_TITLE_DANGER_DEFAULT = 'ui.message.default.error.title';

export const MESSAGE_TITLE_SUCCESS_TOKEN = new InjectionToken<string>('MESSAGE_TITLE_SUCCESS_TOKEN');
export const MESSAGE_TITLE_SUCCESS_DEFAULT = 'ui.message.default.success.title';

export const MESSAGE_TITLE_WARNING_TOKEN = new InjectionToken<string>('MESSAGE_TITLE_WARNING_TOKEN');
export const MESSAGE_TITLE_WARNING_DEFAULT = 'ui.message.default.warning.title';

export const MESSAGE_TITLE_INFO_TOKEN = new InjectionToken<string>('MESSAGE_TITLE_INFO_TOKEN');
export const MESSAGE_TITLE_INFO_DEFAULT = 'ui.message.default.info.title';

export const NOTIFICATION_TIMEOUT_TOKEN = new InjectionToken<string>('NOTIFICATION_TIMEOUT_TOKEN');
export const NOTIFICATION_TIMEOUT_DEFAULT: number = 3000;

export const MONTHS_ARRAY: SelectItem[] = [
    {
        label: 'ui.text.default.month.january',
        value: 0
    },
    {
        label: 'ui.text.default.month.february',
        value: 1
    },
    {
        label: 'ui.text.default.month.march',
        value: 2
    },
    {
        label: 'ui.text.default.month.april',
        value: 3
    },
    {
        label: 'ui.text.default.month.may',
        value: 4
    },
    {
        label: 'ui.text.default.month.june',
        value: 5
    },
    {
        label: 'ui.text.default.month.july',
        value: 6
    },
    {
        label: 'ui.text.default.month.august',
        value: 7
    },
    {
        label: 'ui.text.default.month.september',
        value: 8
    },
    {
        label: 'ui.text.default.month.october',
        value: 9
    },
    {
        label: 'ui.text.default.month.november',
        value: 10
    },
    {
        label: 'ui.text.default.month.december',
        value: 11
    },
];
