import { isUndefined, keys, pick, assignInWith } from 'lodash';

export function componentDefaultsCustomizer(objValue, srcValue){
    return isUndefined(objValue) || objValue === '' ? srcValue : objValue;
}

export function componentDefaults(target, source){
    assignInWith(target, pick(source, keys(target)), componentDefaultsCustomizer);
}