export class JobExecution {
    id: number;
    name: string;
    createdTime: Date;
    status: JobStatus;
    errorsCount: number;
    isRunning: boolean;

    constructor (json: any) {
        const date: Date = new Date(json.createdTime);
        // date.setMinutes(date.getMinutes() + date.getTimezoneOffset());

        this.id = json.id;
        this.name = json.name;
        this.createdTime = date;
        this.status = <JobStatus>JobStatus[json.status.toString()];
        this.errorsCount = json.errorsCount;
        this.isRunning = json.isRunning;
    }

}

export enum JobStatus {
    COMPLETED = 'COMPLETED',
    STARTING = 'STARTING',
    STARTED = 'STARTED',
    STOPPING = 'STOPPING',
    STOPPED = 'STOPPED',
    FAILED = 'FAILED',
    ABANDONED = 'ABANDONED',
    UNKNOWN = 'UNKNOWN'
}
