export class DateColumn {
    year: number;
    week: number;
    weekStart: Date;
    weekEnd: Date;
}

export class TimesheetSummaryDateColumn extends DateColumn {
    maxHours?: number;
}

export class PsrDateColumn extends DateColumn {
    isInPeriod: boolean;
}
