
import { SortOrder } from './sort-order.model';
import { SortMeta } from 'primeng/api/sortmeta';

export class Pageable {
    sort?: SortMeta[];
    size?: number;
    page?: number;
}

export class AgSortModel {
    colId: string;
    sort: SortOrder;
}
