export interface IDomainModel<ID> {
    id: ID;
}

export interface ActivatableDomain {
    isActive: boolean;
}

export interface Domain<ID> extends IDomainModel<ID>, ActivatableDomain {

}
