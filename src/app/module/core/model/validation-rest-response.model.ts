import {ErrorRestResponseModel} from './error-rest-response.model';

export interface ValidationRestResponseModel extends ErrorRestResponseModel {
    data: any;
    field: string;
    id: number;
}
