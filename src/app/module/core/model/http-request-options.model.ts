import {HttpHeaders, HttpParams} from '@angular/common/http';
// import {HttpObserve} from "@angular/common/http/src/client";

export declare type HttpObserve = 'body' | 'events' | 'response';
export type HttpResponseType = 'arraybuffer' | 'blob' | 'json' | 'text';
export type HttpRequestMethod = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH' | 'OPTIONS' | 'HEAD';

export class HttpRequestOptions {
    url: string;
    method: HttpRequestMethod;
    headers?: HttpHeaders = new HttpHeaders();
    observe?: HttpObserve = 'body';
    params?: HttpParams = new HttpParams();
    reportProgress?: boolean = false;
    responseType?: HttpResponseType = 'json';
    withCredentials?: boolean = false;
}
