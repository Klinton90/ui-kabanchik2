export interface ListResponseModel {
    content: Array<any>;
    first: boolean;
    last: boolean;
    // is it current page number???
    number: number;
    // elements in current page
    numberOfElements: number;
    // pagination size
    size: number;
    sort: string;
    // total elements in table
    totalElements: number;
    totalPages: number;
}
