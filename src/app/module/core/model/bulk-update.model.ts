import { IDomainModel } from './i-domain.model';

export abstract class BulkSaveAbstractModel {
    uiId: number;
}

export class BulkSaveInsertUpdateModel<ID> extends BulkSaveAbstractModel {
    data: IDomainModel<ID>;
}

export class BulkSaveDeleteDTO<ID> extends BulkSaveAbstractModel {
    id: ID;
}

export class BulkUpdateModel<ID> {
    rowsForUpdate: BulkSaveInsertUpdateModel<ID>[] = [];
    idsToDelete: BulkSaveDeleteDTO<ID>[] = [];
}
