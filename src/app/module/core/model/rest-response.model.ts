import {BaseRestResponseModel} from './base-rest-response.model';

export interface RestResponseModel extends BaseRestResponseModel {
    id: number;
    field: string;
    data: any;
}
