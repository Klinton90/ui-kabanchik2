export interface BaseRestResponseModel {
    type: MessageType;
}

export enum MessageType {
    SUCCESS = <any>'SUCCESS',
    INFO = <any>'INFO',
    WARNING = <any>'WARNING',
    ERROR = <any>'ERROR',
    VALIDATION = <any>'VALIDATION'
}
