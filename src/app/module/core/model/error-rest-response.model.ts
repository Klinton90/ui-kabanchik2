import {BaseRestResponseModel} from './base-rest-response.model';

export interface ErrorRestResponseModel extends BaseRestResponseModel {
    message: string;
    messageCode: string;
}
