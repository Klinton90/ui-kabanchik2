import { HttpRequestOptions } from '../model/http-request-options.model';
import { Observable } from 'rxjs';
import { isObject } from 'lodash';
import { BaseResource } from './base.resource';
import { BulkUpdateModel, BulkSaveInsertUpdateModel } from '../model/bulk-update.model';

export abstract class FormAwareResource extends BaseResource {

    public insertAction: HttpRequestOptions = {
        url: '/create',
        method: 'POST'
    };

    public updateAction: HttpRequestOptions = {
        url: '/update/:id',
        method: 'PUT'
    };

    public deleteAction: HttpRequestOptions = {
        url: '/delete/:id',
        method: 'DELETE'
    };

    public insert(data: object): Observable<any> {
        if (!isObject(data)) {
            throw new Error('Data must be Object');
        }
        return this._executeRequest(this.insertAction, this.transformRequest(data));
    }

    public update(data: object): Observable<any> {
        if (!isObject(data)) {
            throw new Error('Data must be Object');
        }
        return this._executeRequest(this.updateAction, this.transformRequest(data));
    }

    public save(data: object): Observable<any> {
        if (this.isUpdateAction(data)) {
            return this.update(data);
        } else {
            return this.insert(data);
        }
    }

    public saveAll(data: BulkUpdateModel<any>): Observable<any> {
        return this._executeRequest({
            url: 'saveAll',
            method: 'POST'
        }, this.transformBulkUpdateModel(data));
    }

    public delete(data: any): Observable<any> {
        const obj = isObject(data) ? data : {id: data};
        return this._executeRequest(this.deleteAction, obj);
    }

    protected isUpdateAction(data: object): boolean {
        let isUpdate: boolean = true;
        const parts: string[] = this.updateAction.url.split('/');
        for (let i: number = 0; i < parts.length; i++) {
            const part: string = parts[i];
            if (part.startsWith(':')) {
                const value: any = this._findValueByMap(data, part);
                if (!value || value.toString().length === 0 || value.toString() === '0') {
                    isUpdate = false;
                    break;
                }
            }
        }

        return isUpdate;
    }

    private transformBulkUpdateModel(data: BulkUpdateModel<any>): BulkUpdateModel<any> {
        if (data && data.rowsForUpdate && data.rowsForUpdate.length) {
            data.rowsForUpdate = data.rowsForUpdate.map((row: BulkSaveInsertUpdateModel<any>) => {
                row.data = this.transformRequest(row.data);
                return row;
            });
        }
        return data;
    }

    public transformRequest(item: any): any {
        return item;
    }

}
