import { HttpParams } from '@angular/common/http';
import { isObject, isNil, isDate, isArray } from 'lodash';
import { Observable } from 'rxjs';
import { Pageable, AgSortModel } from '../model/pageable.model';
import { SortOrder } from '../model/sort-order.model';
import { HttpRequestOptions } from '../model/http-request-options.model';
import { MatchMode } from '../../shared/model/match-mode.model';
import { FormAwareResource } from './form-aware.resource';
import { IGetRowsParams, GridApi, ColDef } from 'ag-grid-community';
import { AgFilterMetadata } from 'app/module/grid-shared/model/ag-grid-options.model';
import { RestResponseModel } from '../model/rest-response.model';
import { map } from 'rxjs/operators';
import { FilterMetadata } from 'primeng/api/filtermetadata';

export const SEARCH_TYPE_SUFFIX = 'MatchMode';

// Default error handler and default rest endpoint implementations
export abstract class RestResource extends FormAwareResource {

    public listAction: HttpRequestOptions = {
        url: '/list',
        method: 'GET'
    };

    public getAction: HttpRequestOptions = {
        url: '/get/:id',
        method: 'GET'
    };

    public findByGenericFiltersAction: HttpRequestOptions = {
        url: 'findByGenericFilters',
        method: 'GET'
    };

    public findLikeAction: HttpRequestOptions = {
        url: 'findLike',
        method: 'GET'
    };

    public getUniqueValuesAction: HttpRequestOptions = {
        url: 'getUniqueValues',
        method: 'GET'
    };

    public list(showActive?: boolean, pageable?: Pageable): Observable<any> {
        const newRequest: HttpRequestOptions = Object.assign({}, this.listAction);

        if (showActive || pageable) {
            let params: HttpParams = new HttpParams();
            if (showActive) {
                params = params.set('showActive', showActive.toString());
            }
            if (pageable) {
                params = this.setPageable(params, pageable);
            }
            newRequest.params = params;
        }

        return this._executeRequest(newRequest, null).pipe(
            map((response: RestResponseModel) => this.transformRestResponseModel(response))
        );
    }

    public get(data: any): Observable<any> {
        const obj = isObject(data) ? data : {id: data};
        return this._executeRequest(this.getAction, obj).pipe(
            map((response: RestResponseModel) => this.transformRestResponseModel(response))
        );
    }

    public findByGenericFilters(showActive: boolean, pageable: Pageable, filters: {[s: string]: FilterMetadata}): Observable<any> {
        const newRequest: HttpRequestOptions = Object.assign({}, this.findByGenericFiltersAction);

        let params: HttpParams = new HttpParams().append('showActive', showActive.toString());

        Object.keys(filters).forEach(key => {
            const filter: FilterMetadata = filters[key];
            const matchMode: string = !isNil(filter.matchMode) ? filter.matchMode : MatchMode.EQUALS.toString();
            const value = isDate(filter.value) ? filter.value.getTime() : filter.value;

            params = params.append(key, value).append(key + SEARCH_TYPE_SUFFIX, matchMode);
        });

        newRequest.params = this.setPageable(params, pageable);

        return this._executeRequest(newRequest).pipe(
            map((response: RestResponseModel) => this.transformRestResponseModel(response))
        );
    }

    public findAgByGenericFilters(
        requestParams: IGetRowsParams,
        api: GridApi,
        showActive?: boolean,
        additionalParams?: Map<string, string>
    ): Observable<any> {
        const newRequest: HttpRequestOptions = Object.assign({}, this.findByGenericFiltersAction);
        newRequest.params = this.getAgFullPageableParams(requestParams, api, showActive);

        if (additionalParams) {
            additionalParams.forEach((value: string, key: string) => {
                newRequest.params = newRequest.params.append(key, value);
            });
        }

        return this._executeRequest(newRequest).pipe(
            map((response: RestResponseModel) => this.transformRestResponseModel(response))
        );
    }

    public findLike(showActive: boolean, pageable: Pageable, likeValue: string): Observable<any> {
        const newRequest: HttpRequestOptions = Object.assign({}, this.findLikeAction);

        const params: HttpParams = new HttpParams()
            .append('showActive', showActive.toString())
            .append('likeValue', likeValue);

        newRequest.params = this.setPageable(params, pageable);

        return this._executeRequest(newRequest).pipe(
            map((response: RestResponseModel) => this.transformRestResponseModel(response))
        );
    }

    public getUniqueValues(
        field: string,
        orderByFields?: string[],
        queryString?: string,
        filterByFieldNames?: string[],
        limit?: number,
        offset?: number
    ): Observable<any> {
        const newRequest: HttpRequestOptions = Object.assign({}, this.getUniqueValuesAction);

        newRequest.params = new HttpParams()
            .append('field', field);

        if (orderByFields && orderByFields.length) {
            orderByFields.forEach(orderByField =>
                newRequest.params = newRequest.params.append('orderByFieldNames', orderByField));
        }

        if (queryString && filterByFieldNames && filterByFieldNames.length) {
            newRequest.params = newRequest.params.append('queryString', queryString);

            filterByFieldNames.forEach(filterByFieldName =>
                newRequest.params = newRequest.params.append('filterByFieldNames', filterByFieldName));
        }

        if (limit && limit > 0) {
            newRequest.params = newRequest.params.append('limit', limit.toString());
        }

        if (offset && offset > 0) {
            newRequest.params = newRequest.params.append('offset', offset.toString());
        }

        return this._executeRequest(newRequest);
    }

    protected setPageable(params: HttpParams, pageable: Pageable): HttpParams {
        let result: HttpParams = params;

        if (pageable.page) {
            result = result.append('page', pageable.page.toString());
        }
        if (pageable.size) {
            result = result.append('size', pageable.size.toString());
        }

        if (pageable.sort) {
            pageable.sort.forEach((meta: {field: string, order: number | SortOrder}) => {
                let sortOrder: SortOrder;
                if (typeof meta.order === 'number') {
                    sortOrder = meta.order > 0 ? SortOrder.ASC : SortOrder.DESC;
                } else {
                    sortOrder = meta.order;
                }
                result = result.append('sort', `${meta.field},${sortOrder.toString()}`);
            });
        }

        return result;
    }

    protected setAgPageable(params: HttpParams, requestParams: IGetRowsParams): HttpParams {
        const size: number = requestParams.endRow - requestParams.startRow;
        const page: number = Math.floor(requestParams.startRow / size);
        params = params.append('size', size.toString()).append('page', page.toString());

        requestParams.sortModel.forEach((sortModel: AgSortModel) => {
            params = params.append('sort', `${sortModel.colId},${sortModel.sort}`);
        });

        return params;
    }

    protected getAgDefaultListingParams(requestParams?: IGetRowsParams, showActive?: boolean, params?: HttpParams): HttpParams {
        if (!params) {
            params = new HttpParams();
        }

        if (showActive) {
            params.set('showActive', showActive.toString());
        }
        if (requestParams) {
            params = this.setAgPageable(params, requestParams);
        }

        return params;
    }

    protected getAgFullPageableParams(requestParams: IGetRowsParams, api: GridApi, showActive?: boolean): HttpParams {
        let params: HttpParams = this.getAgDefaultListingParams(requestParams, showActive);

        const filterNames: string[] = Object.keys(requestParams.filterModel);
        if (filterNames && filterNames.length) {
            filterNames.forEach((key: string) => {
                const filter: AgFilterMetadata = requestParams.filterModel[key];
                const matchMode: MatchMode = !isNil(filter.type) ? filter.type : MatchMode.EQUALS;
                const column: ColDef = api.getColumnDef(key);
                const field = column && column.field ? column.field : key;

                params = params.append(key + SEARCH_TYPE_SUFFIX, matchMode.toString());

                if (matchMode === MatchMode.IN && isArray(filter.filter)) {
                    filter.filter.forEach((_value: any) => {
                        params = params.append(field, _value.toString());
                    });
                } else {
                    const value: string = isDate(filter.filter) ? filter.filter.getTime().toString() : filter.filter.toString();
                    params = params.append(field, value);

                    if ((matchMode === MatchMode.IN_RANGE || matchMode === MatchMode.DATE) && filter.filterTo) {
                        params = params.append(field, filter.filterTo.toString());
                    }
                }
            });
        }

        return params;
    }

    protected transformRestResponseModel(response: RestResponseModel): RestResponseModel {
        if (response && response.data && response.data.content && response.data.content.length) {
            response.data.content = response.data.content.map(this.transformResponse);
        }
        return response;
    }

    public transformResponse(item: any): any {
        return item;
    }

}
