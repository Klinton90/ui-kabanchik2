import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { isNil } from 'lodash';
import { Observable } from 'rxjs';
import { HttpRequestOptions } from '../model/http-request-options.model';

// Base implementation of Resource service
export abstract class BaseResource {

    constructor(basePath: string) {
        this.basePath = basePath.replace('//', '/');
    }

    protected abstract http: HttpClient;

    protected basePath: string;

    public headers: HttpHeaders = new HttpHeaders();

    public propertyMapping: Map<string, string> = new Map<string, string>();

    public static mergeHeaders(from: HttpHeaders, to: HttpHeaders): void {
        from.keys().forEach((key: string) => {
            to.set(key, from.get(key));
        });
    }

    public static setOptionalPageableParams(params?: HttpParams, queryString?: string, limit?: number, offset?: number) {
        if (!params) {
            params = new HttpParams();
        }

        if (queryString) {
            params = params.append('queryString', queryString);
        }

        if (limit) {
            params = params.append('limit', limit.toString());
        }

        if (offset) {
            params = params.append('offset', offset.toString());
        }

        return params;
    }

    protected _executeRequest(request: HttpRequestOptions, obj?: object): Observable<any> {
        if (isNil(request.method)) {
            throw Error('Request parameter `method` is required.');
        }

        const requestCopy: HttpRequestOptions = Object.assign({}, request);
        requestCopy.url = this._preparePath(request.url, obj);
        BaseResource.mergeHeaders(this.headers, requestCopy.headers);

        const o: Observable<any> = this.http.request(requestCopy.method, requestCopy.url, {
            body: obj && ['PATCH', 'POST', 'PUT'].indexOf(request.method) >= 0 ? obj : null,
            headers: requestCopy.headers,
            observe: requestCopy.observe,
            params: requestCopy.params,
            responseType: requestCopy.responseType,
            reportProgress: requestCopy.reportProgress,
            withCredentials: requestCopy.withCredentials
        });

        return o;
    }

    protected _preparePath(url: string, obj?: object): string {
        url.replace('\\', '/');

        let path: string =
            (!this.basePath.startsWith('/') && !this.basePath.startsWith('http') ? '/' : '')
            + this.basePath
            + (!this.basePath.endsWith('/') && !url.startsWith('/') ? '/' : '')
            + url;

        path = path.replace('//', '/');

        const parts: string[] = path.split('/');
        parts.forEach((part: string) => {
            if (part.startsWith(':')) {
                const value: any = this._findValueByMap(obj, part);
                if (!isNil(value)) {
                    path = path.replace(part, value.toString());
                } else {
                    throw new Error('Cannot resolve parameter \'' + part + '\' in request \'' + path + '\'');
                }
            }
        });

        return path;
    }

    protected _findValueByMap(obj: object, part: string): any {
        let result: any;
        if (obj) {
            const key: string = part.substring(1, part.length);
            const newKey: string = this.propertyMapping[key];
            result = newKey ? obj[newKey] : obj[key];
        }
        return result;
    }
}
