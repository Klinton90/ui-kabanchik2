import { BaseResource } from './base.resource';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient } from '@angular/common/http';
import { RestResponseModel } from '../model/rest-response.model';
import { JobExecution } from '../model/job-execution.model';
import { map } from 'rxjs/operators';
import { StaticService } from '../service/static.service';

@Injectable({
    providedIn: 'root'
})
export class BatchJobMonitorResource extends BaseResource {

    constructor(
        protected http: HttpClient
    ) {
        super('batchJobMonitor');
    }

    public getDownloadPath(id: number): string {
        const params: HttpParams = new HttpParams().append('id', id.toString());
        return StaticService.backendHref + this._preparePath('/report.csv?') + params.toString();
    }

    public getAllActiveJobExecutions(): Observable<any> {
        return this._executeRequest({
            url: '/list',
            method: 'GET'
        }).pipe(
            map((response) => response.data.content.forEach(this.responseConverter))
        );
    }

    public getActiveJobExecutionsByName(jobName: string): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('jobName', jobName);

        return this._executeRequest({
            url: '/getByName',
            method: 'GET',
            params: params
        }).pipe(
            map((response: RestResponseModel) => response.data.map((json) => this.responseConverter(json)))
        );
    }

    public getJobExecutionById(id: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('id', id.toString());

        return this._executeRequest({
            url: '/get',
            method: 'GET',
            params: params
        }).pipe(
            map((response: RestResponseModel) => this.responseConverter(response.data))
        );
    }

    private responseConverter(json: any): any {
        return new JobExecution(json);
    }

}
