import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseResource } from './base.resource';

@Injectable({
    providedIn: 'root'
})
export class ServiceResource extends BaseResource {

    constructor(
        protected http: HttpClient
    ) {
        super('');
    }

    getLocalizedEnumOptions(enumName: string): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('name', enumName);

        return this._executeRequest(
            {
                url: '/enum/localized',
                method: 'GET',
                params: params
            }
        );
    }
}
