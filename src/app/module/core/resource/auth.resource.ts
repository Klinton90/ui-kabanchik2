import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BaseResource} from './base.resource';
import { CUSTOM_HTTP_PARAMS, CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER } from '../../../config/cleanup.interceptor';

// Loader is shown for this Resource
@Injectable({
    providedIn: 'root'
})
export class AuthResource extends BaseResource {

    constructor(
        protected http: HttpClient
    ) {
        super('');
    }

    public logout(): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append(CUSTOM_HTTP_PARAMS, CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER);

        return this._executeRequest({
            url: 'logout',
            method: 'POST',
            responseType: 'text',
            params: params
        });
    }

    public auth(): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append(CUSTOM_HTTP_PARAMS, CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER);

        return this._executeRequest({
            url: 'auth',
            method: 'GET',
            params: params
        });
    }

    // TODO: encrypt password
    public login(login: string, password: string): Observable<any> {
        const body: FormData = new FormData();
        body.append('email', login);
        body.append('password', password);

        const params: HttpParams = new HttpParams()
            .append(CUSTOM_HTTP_PARAMS, CUSTOM_HTTP_PARAMS_NO_ERROR_HANDLER);

        return this._executeRequest({
            url: 'login',
            method: 'POST',
            params: params
        }, body);
    }

}
