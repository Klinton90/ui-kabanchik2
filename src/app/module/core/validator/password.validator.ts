import {AbstractControl, FormGroup, ValidatorFn} from '@angular/forms';

export function passwordSizeValidator(min: number, max: number): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const length: number = control && control.value ? control.value.toString().length : 0;

        if (length > 0 && (length < min || length > max)) {
            return {'ui.validation.user.passwordSize': {
                min: min,
                max: max,
                actualLength: length
            }};
        } else {
            return null;
        }
    };
}

export function passwordConfirmValidator(originalFieldName: string, confirmFieldName: string): ValidatorFn {
    return (group: FormGroup): {[key: string]: any} => {
        const password = group.controls[originalFieldName];
        const confirmPassword = group.controls[confirmFieldName];
        if (password && confirmPassword && password.value !== confirmPassword.value) {
            return {'ui.validation.user.passwordCompare': true};
        }
        return null;
    };
}
