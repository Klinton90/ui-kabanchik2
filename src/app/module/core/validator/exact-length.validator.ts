import { AbstractControl, ValidatorFn } from '@angular/forms';

export function exactLengthValidator(sizes: number[]): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        const length: number = control && control.value ? control.value.toString().length : 0;

        if (sizes.indexOf(length) < 0) {
            return {'ui.validation.default.exactLength': {
                    requiredLength: sizes.join(', '),
                    actualLength: length
                }
            };
        } else {
            return null;
        }
    };
}
