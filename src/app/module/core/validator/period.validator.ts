import { ValidatorFn, AbstractControl } from '@angular/forms';
import { Moment } from 'moment';

export interface DateAwareModel {
    from: number;
    to: number;
}

export interface DateAwareModel2 {
    from: Moment;
    to: Moment;
}

export function periodValidator(data: DateAwareModel, isFrom?: boolean): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
        if (isFrom) {
            if (!control.value || (control.value as Moment).isAfter(data.to)) {
                return {
                    'ui.validation.default.wrongDates': true
                };
            }
        } else {
            if (!control.value || (control.value as Moment).isBefore(data.from)) {
                return {
                    'ui.validation.default.wrongDates': true
                };
            }
        }
        return null;
    };
}

export const periodFormValidator: ValidatorFn = (control: AbstractControl): {[key: string]: any} => {
    const data: DateAwareModel2 = control.value;

    // Not required
    if (!data.from && !data.to) {
        return null;
    } else if (!data.from || !data.to || data.from.isAfter(data.to)) {
        return {
            'ui.validation.default.wrongDates': true
        };
    }

    return null;
};
