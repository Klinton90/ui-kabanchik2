import {ChangeDetectorRef, Component, OnDestroy} from "@angular/core";
import {Subscription} from "rxjs";
import {LoaderService} from "../../service/loader.service";

@Component({
    moduleId: module.id,
    selector: "loader",
    styleUrls: ["loader.component.css"],
    templateUrl: "loader.component.html",
})
export class LoaderComponent implements OnDestroy{

    visibleCount: number = 0;
    private subscription: Subscription;

    constructor(private ls: LoaderService, private ref: ChangeDetectorRef){
        this.subscription = ls.$loaderPosted.subscribe((visible?: boolean) => {
            if(visible){
                this.visibleCount++;
            }else if(visible === false && this.visibleCount > 0){
                this.visibleCount--;
            }else{
                this.visibleCount = 0;
            }
            this.ref.detectChanges();
        });
    }

    ngOnDestroy(): void{
        this.subscription.unsubscribe();
    }

}