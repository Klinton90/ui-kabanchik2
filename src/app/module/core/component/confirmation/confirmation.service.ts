import { Injectable } from '@angular/core';
import { ConfirmationMessageModel } from './confirmation-message.model';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationComponent } from './confirmation.component';

@Injectable({providedIn: 'root'})
export class ConfirmationService {

    constructor(private dialog: MatDialog) {}

    show(params: ConfirmationMessageModel): void {
        this.dialog.open(ConfirmationComponent, {data: params});
    }

}
