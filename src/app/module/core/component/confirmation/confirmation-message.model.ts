export interface ConfirmationMessageModel {
    title?: string;
    body: string;
    bodyIcon?: string;
    primaryText?: string;
    primaryIcon?: string;
    secondaryText?: string;
    secondaryIcon?: string;
    hasX?: boolean;
    onPrimaryClick?: () => void;
    onSecondaryClick?: () => void;
    onXClick?: () => void;
    onEscapePress?: () => void;
    onBackdropClick?: () => void;
}