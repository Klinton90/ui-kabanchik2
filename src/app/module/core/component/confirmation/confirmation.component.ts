import { Component, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationMessageModel } from './confirmation-message.model';

@Component({
    moduleId: module.id,
    selector: 'appConfirmation',
    templateUrl: 'confirmation.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmationComponent {

    data: ConfirmationMessageModel;

    constructor(
        public dialogRef: MatDialogRef<ConfirmationComponent>,
        @Inject(MAT_DIALOG_DATA) private modalData: ConfirmationMessageModel
    ) {
        this.data = {
            body: modalData.body,
            title: modalData.title || 'ui.confirmation.default.title',
            bodyIcon: modalData.bodyIcon || 'fa-question-circle',
            primaryText: modalData.primaryText || undefined,
            primaryIcon: modalData.primaryIcon || 'fa-check',
            secondaryText: modalData.secondaryText || 'ui.text.default.no',
            secondaryIcon: modalData.secondaryIcon || 'fa-times',
            hasX: modalData.hasX || false
        };
    }

    onPrimaryClick(): void {
        this.dialogRef.close();
        if (this.modalData.onPrimaryClick) {
            this.modalData.onPrimaryClick();
        }
    }

    onSecondaryClick(): void {
        this.dialogRef.close();
        if (this.modalData.onSecondaryClick) {
            this.modalData.onSecondaryClick();
        }
    }

    onXClick(): void {
        this.dialogRef.close();
        if (this.modalData.onXClick) {
            this.modalData.onXClick();
        }
    }

    onBackdropClick(): void {
        this.dialogRef.close();
        if (this.modalData.onBackdropClick) {
            this.modalData.onBackdropClick();
        }
    }

    onEscapePress(): void {
        this.dialogRef.close();
        if (this.modalData.onEscapePress) {
            this.modalData.onEscapePress();
        }
    }

}
