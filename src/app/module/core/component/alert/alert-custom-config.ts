export enum AlertType {
    SUCCESS = 'success',
    INFO = 'info',
    WARNING = 'warning',
    DANGER = 'danger'
}
export class AlertLink {
    link?: string;
    linkText?: string;
    linkIcon?: string;
    linkClick?: () => void;
    linkCloseOnClick?: boolean;
}

export class AlertCustomConfig {
    type: AlertType;
    title?: string;
    body: string[] | string;
    dismissible?: boolean = true;
    close?: () => void;

    params?: any[];
    links?: AlertLink[];
}
