import {AfterViewInit, ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';
import {AlertCustomConfig, AlertLink} from './alert-custom-config';
import { isArray } from 'lodash';

@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlertComponent implements AfterViewInit {

    ngAfterViewInit(): void {
    }

    constructor(
        public snackBarRef: MatSnackBarRef<AlertComponent>,
        @Inject(MAT_SNACK_BAR_DATA) public data: AlertCustomConfig
    ) {}

    hide(): void {
        if (this.data.close) {
            this.data.close();
        }
        this.snackBarRef.dismiss();
    }

    public onLinkClick(link: AlertLink): void {
        if (link.linkClick) {
            link.linkClick();
        }
        if (link.linkCloseOnClick) {
            this.hide();
        }
    }

    isArray(val: any): boolean {
        return isArray(val);
    }

}
