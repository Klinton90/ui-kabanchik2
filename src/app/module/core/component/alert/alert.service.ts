import { ValidationRestResponseModel } from '../../model/validation-rest-response.model';
import { AbstractControl, FormGroup } from '@angular/forms';
import { isNil } from 'lodash';
import { BACKEND_VALIDATION_KEY, MessageService } from '../../service/message.service';
import { Inject, Optional, Injectable } from '@angular/core';
import {
    MESSAGE_TITLE_DANGER_DEFAULT, MESSAGE_TITLE_DANGER_TOKEN,
    MESSAGE_TITLE_INFO_DEFAULT, MESSAGE_TITLE_INFO_TOKEN,
    MESSAGE_TITLE_SUCCESS_DEFAULT, MESSAGE_TITLE_SUCCESS_TOKEN,
    MESSAGE_TITLE_WARNING_DEFAULT, MESSAGE_TITLE_WARNING_TOKEN,
    NOTIFICATION_TIMEOUT_DEFAULT, NOTIFICATION_TIMEOUT_TOKEN, CLOSE_TEXT_TOKEN, CLOSE_TEXT_DEFAULT
} from '../../common/constants-and-tokens';
import { HttpErrorResponse } from '@angular/common/http';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { AlertType, AlertCustomConfig } from './alert-custom-config';
import { AlertComponent } from './alert.component';

@Injectable({providedIn: 'root'})
export class AlertService {

    private timeout: number;
    private infoTitle: string;
    private successTitle: string;
    private dangerTitle: string;
    private warningTitle: string;

    private closeTitle: string;

    constructor(
        @Optional() @Inject(NOTIFICATION_TIMEOUT_TOKEN) timeout: number,
        @Optional() @Inject(MESSAGE_TITLE_INFO_TOKEN) infoTitle: string,
        @Optional() @Inject(MESSAGE_TITLE_SUCCESS_TOKEN) successTitle: string,
        @Optional() @Inject(MESSAGE_TITLE_DANGER_TOKEN) dangerTitle: string,
        @Optional() @Inject(MESSAGE_TITLE_WARNING_TOKEN) warningTitle: string,
        @Optional() @Inject(CLOSE_TEXT_TOKEN) closeTitle: string,
        private matSnackBar: MatSnackBar,
        private messageService: MessageService,
    ) {
        this.timeout = timeout || NOTIFICATION_TIMEOUT_DEFAULT;
        this.infoTitle = infoTitle || MESSAGE_TITLE_INFO_DEFAULT;
        this.successTitle = successTitle || MESSAGE_TITLE_SUCCESS_DEFAULT;
        this.dangerTitle = dangerTitle || MESSAGE_TITLE_DANGER_DEFAULT;
        this.warningTitle = warningTitle || MESSAGE_TITLE_WARNING_DEFAULT;

        this.closeTitle = closeTitle || CLOSE_TEXT_DEFAULT;
    }

    post(message: AlertCustomConfig, duration?: number): void {
        const config: MatSnackBarConfig = {
            data: message
        };

        if (isNil(message.dismissible)) {
            message.dismissible = true;
        }

        if (message.type == null) {
            message.type = AlertType.INFO;
        }

        if (isNil(duration) && message.type === AlertType.SUCCESS) {
            config.duration = this.timeout;
        }

        if (message.title == null) {
            switch (message.type) {
                case AlertType.INFO:
                    message.title = this.infoTitle;
                    break;
                case AlertType.DANGER:
                    message.title = this.dangerTitle;
                    break;
                case AlertType.WARNING:
                    message.title = this.warningTitle;
                    break;
                case AlertType.SUCCESS:
                    message.title = this.successTitle;
                    break;
                default:
                    message.title = '';
                    break;
            }
        }

        this.matSnackBar.openFromComponent(AlertComponent, config);
    }

    processFormFieldBackendValidation(response: HttpErrorResponse, form: FormGroup): void {
        if (response.status === 422) {
            const json: ValidationRestResponseModel = response.error;

            if (!isNil(json.field)) {
                const control: AbstractControl = form.get(json.field);
                if (control) {
                    control.setErrors({[BACKEND_VALIDATION_KEY]: json});
                }
            }

            this.post({
                body: json.message || json.messageCode || 'true',
                title: 'ui.message.default.validation.title',
                type: AlertType.DANGER
            });
        }
    }

    processBackendValidation = (response: HttpErrorResponse): void => {
        if (response.status === 422) {
            const json: ValidationRestResponseModel = response.error;
            this.post({
                body: json.message || json.messageCode || 'true',
                title: 'ui.message.default.validation.title',
                type: AlertType.DANGER
            });
        }
    }

    postSimple(message: string): void {
        this.matSnackBar.open(this.messageService.resolveMessage(message), null, {
            duration: this.timeout
        });
    }

}
