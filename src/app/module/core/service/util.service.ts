import { FormGroup, AbstractControl } from '@angular/forms';
import {Injectable, Inject} from '@angular/core';
import {MessageService} from './message.service';
import { SelectItem } from 'primeng/api/selectitem';
import { DOCUMENT } from '@angular/common';
import { AlertService } from '../component/alert/alert.service';
import { Router } from '@angular/router';
import { StaticService } from './static.service';
import { AlertType } from '../component/alert/alert-custom-config';
import { values } from 'lodash';

@Injectable({providedIn: 'root'})
export class UtilService {

    public booleanOptions: SelectItem[];

    constructor(
        private router: Router,
        private notificationService: AlertService,
        protected messageService: MessageService,
        @Inject(DOCUMENT) private document: any
    ) {
        this.booleanOptions = this.addDefaultOption([
            {
                label: this.messageService.resolveMessage('ui.text.default.yes'),
                value: 'true'
            },
            {
                label: this.messageService.resolveMessage('ui.text.default.no'),
                value: 'false'
            }
        ]);
    }

    public addDefaultOption(options: SelectItem[], defaultValue: any = ''): SelectItem[] {
        const defaultOption: SelectItem = options.find((val: SelectItem) => {
            return val.value === '' || val.value === 0 || val.value === '0';
        });
        if (defaultOption == null) {
            options.unshift({
                label: this.messageService.resolveMessage('ui.text.default.selectPlaceholder'),
                value: defaultValue
            });
        }
        return options;
    }

    public removeFocus() {
        this.document.activeElement.blur();
    }

    public submitFormWithBlur(form: FormGroup, submitCallback: () => any) {
        this.removeFocus();
        form.markAsDirty();
        values(form.controls).forEach((control: AbstractControl) => {
            control.markAsTouched();
            control.updateValueAndValidity({emitEvent: true});
        });
        if (form.valid) {
            submitCallback();
        }
    }

    public processRouteNotAllowed(): void {
        this.notificationService.post({
            body: 'ui.message.default.error.body.403',
            type: AlertType.DANGER
        }, 3000);
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            this.router.navigateByUrl(this.router.url === '/' ? '/home' : this.router.url);
        });
    }

    setFormGroupValues(form: FormGroup, entity: any): void {
        Object.keys(form.controls).forEach((key: string) => {
            const control: AbstractControl = form.controls[key];
            let value: any = entity ? entity[key] : null;

            if (control instanceof FormGroup) {
                this.setFormGroupValues(control, value);
            } else {
                if (StaticService.isInstanceOfLocalizedEnum(value)) {
                    value = value.value;
                }
                control.setValue(value);
            }
        });
    }
}
