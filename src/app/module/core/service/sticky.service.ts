import { Injectable } from '@angular/core';
import { StaticService } from './static.service';
import { Router, Event, NavigationStart } from '@angular/router';

@Injectable({providedIn: 'root'})
export class StickyService {

    dataMap: Map<string, () => any> = new Map();

    constructor(private router: Router) {
        router.events.subscribe((event: Event) => {
            if (event instanceof NavigationStart) {
                this.dataMap.clear();
            }
        });
    }

    registerField(fieldName: string, data: () => any): void {
        this.dataMap.set(fieldName, data);
    }

    saveData(): void {
        if (this.dataMap.size > 0) {
            const data: Object = {};
            this.dataMap.forEach((value: any, key: string) => data[key] = StaticService.resolveFunctor(value));
            localStorage.setItem(this.router.url, JSON.stringify(data));
        }
    }

    getData(): any {
        const data: string | null = localStorage.getItem(this.router.url);
        if (data) {
            return JSON.parse(data);
        }
    }

}
