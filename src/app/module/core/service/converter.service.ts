import { IDomainModel } from '../model/i-domain.model';
import { UtilService } from './util.service';
import { get, isString, isNumber, isNaN, isDate, upperFirst } from 'lodash';
import { Injectable } from '@angular/core';
import { SelectItem } from 'primeng/api/selectitem';
import { Moment } from 'moment';
import * as moment from 'moment';
import { RestResponseModel } from '../model/rest-response.model';
import { formatNumber } from '@angular/common';
import { formatDate, getLocaleDateFormat, FormatWidth } from '@angular/common';
import { PsrLineModelCore, PsrLineType } from 'app/module/psr/model/psr-line.model';
import { MessageService } from './message.service';

const IS_ACTIVE_PROP_NAME = ':isActive:';
const PERCENTAGE_SIGN = '%';

type LabelTransformFunction = (obj: any) => string;
type ValueTransformFunction = (obj: any) => any;

export type LabelTransform = true | string | LabelTransformFunction;
export type ValueTransform = string | ValueTransformFunction;

@Injectable({providedIn: 'root'})
export class ConverterService {

    public static DEFAULT_PATTERN = `\${name}\${${IS_ACTIVE_PROP_NAME}}`;

    constructor(
        private utilService: UtilService,
        private messageService: MessageService
    ) {}

    public static roundCurrency(data: number): number {
        return Math.round(data * 100) / 100;
    }

    public static formatCurrency(data: number = 0): string {
        return formatNumber(data, 'en-en', '1.2-2');
    }

    public static formatPercentage(data: number = 0): string {
        return ConverterService.formatCurrency(data) + PERCENTAGE_SIGN;
    }

    static getPercentagePrefix(entity: PsrLineModelCore): string {
        return entity.key.type === PsrLineType.PROGRESS ? PERCENTAGE_SIGN : '';
    }

    public static formatDate(data: Date): string {
        return formatDate(data, getLocaleDateFormat('en-en', FormatWidth.Short), 'en-en');
    }

    public static guessMoment(_value: string | number | Date | Moment): Moment {
        if (_value) {
            const value: number | Date | Moment = isString(_value) ? Number.parseInt(_value) : _value;

            if (isNumber(value) && !isNaN(value)) {
                if (value.toString().length === 13) {
                    return moment(value);
                }
                return moment.unix(value);
            } else if (isDate(value)) {
                return moment(value);
            } else if ((value as Moment).unix) {
                return value as Moment;
            } else {
                throw Error('Unexpected value for date formatter.');
            }
        }
    }

    public static convertListToSelectItems(options: any[]): SelectItem[] {
        const result: SelectItem[] = [];

        options.forEach((option: any) => {
            result.push({
                label: option,
                value: option,
            });
        });

        return result;
    }

    public convertEnumsToSelectItems(data: any, valueAsNumber: boolean = false, addDefaultOption: boolean = false): SelectItem[] {
        const result: SelectItem[] = [];
        for (const enumMember in data) {
            if (parseInt(enumMember, 10) >= 0) {
                result.push({
                    label: upperFirst(data[enumMember].toString().toLowerCase()).replace('_', ' '),
                    value: valueAsNumber ? enumMember : data[enumMember].toString()
                });
            }
        }
        return addDefaultOption ? this.utilService.addDefaultOption(result) : result;
    }

    public convertObjectListToSelectItems(
        options: Object[],
        addDefaullt: boolean,
        labelProperty: LabelTransform,
        valueProperty?: ValueTransform,
        defaultValue?: any
    ): SelectItem[] {
        const result: SelectItem[] = [];

        options.forEach((option: any) => {
            result.push(this.convertObjectToSelectItem(option, labelProperty, valueProperty));
        });

        if (addDefaullt) {
            return this.utilService.addDefaultOption(result, defaultValue);
        } else {
            return result;
        }
    }

    public defaultSelectItemsConverter = (response: RestResponseModel): SelectItem[] => {
        return this.convertObjectListToSelectItems(response.data.content || response.data, false, ConverterService.DEFAULT_PATTERN, 'id');
    }

    public defaultObjectConverter(response: RestResponseModel, viewNameProp: string = 'viewName'): any[] {
        const items = response.data.content || response.data;
        items.forEach((item: any) => {
            item[viewNameProp] = this.getLabelByPattern(item, ConverterService.DEFAULT_PATTERN);
        });
        return items;
    }

    public convertObjectToSelectItem(option: any, labelProperty: LabelTransform, valueProperty?: ValueTransform): SelectItem {
        const label: any = this.getLabelByPattern(option, labelProperty);

        if (label) {
            if (typeof(label) === 'string') {
                let value: any = option;

                if (valueProperty) {
                    if (typeof valueProperty === 'string') {
                        value = get(option, valueProperty);
                        if (typeof value === 'function') {
                            value = value();
                        }
                    } else {
                        value = valueProperty(option);
                    }
                }

                if (value) {
                    return {
                        label: label,
                        value: value
                    };
                } else {
                    throw Error('Option [' + JSON.stringify(option) + '] must have value property'
                        + (valueProperty ? ' "' + valueProperty + '"' : '') + '.');
                }
            } else {
                throw Error('Option [' + JSON.stringify(option) + '] must have label property \'' + labelProperty + '" type of "String".');
            }
        } else {
            throw Error('Option [' + JSON.stringify(option) + '] doesn\'t have label property \'' + labelProperty + '\'.');
        }
    }

    public getLabelByPattern(option: any, labelProperty: LabelTransform): string {
        if (option) {
            let label: any = '';

            if (labelProperty === true) {
                label = this.getLabelByPattern(option, ConverterService.DEFAULT_PATTERN);
            } else if (typeof labelProperty === 'string') {
                if (labelProperty.indexOf('$') > -1) {
                    let match: string[];
                    label = labelProperty;
                    const rg: RegExp = /\${(.[^{}]+)}/g;
                    while (match = rg.exec(labelProperty)) {
                        const variableName = match[0];
                        const propName = match[1];
                        if (propName === IS_ACTIVE_PROP_NAME) {
                            const isActiveText = get(option, propName.substring(1, propName.length - 1))
                                ? ''
                                : ' (' + this.messageService.resolveMessage('ui.text.default.inactive') + ')';
                            label = label.replace(variableName, isActiveText);
                        } else {
                            label = label.replace(variableName, get(option, propName));
                        }
                    }
                } else {
                    label = get(option, labelProperty);
                    if (typeof label === 'function') {
                        label = label();
                    }
                }
            } else {
                label = labelProperty(option);
            }

            return label;
        } else {
            return '';
        }
    }

    public getLabelByPatternAndId<ID> (list: IDomainModel<ID>[], id: ID, labelProperty: LabelTransform): string {
        return list ? this.getLabelByPattern(list.find(item => item.id === id), labelProperty) : '';
    }

}
