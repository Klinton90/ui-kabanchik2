import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class LoaderService {

    private loaderPoster: Subject<boolean> = new Subject<boolean>();
    public $loaderPosted: Observable<boolean> = this.loaderPoster.asObservable();

    constructor() {}

    public show(): void {
        this.loaderPoster.next(true);
    }

    public hide(): void {
        this.loaderPoster.next(false);
    }

    public forceHide(): void {
        this.loaderPoster.next(null);
    }

}
