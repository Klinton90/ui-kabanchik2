import { ConfigurationModel } from '../../admin/component/configuration/configuration.model';
import { Domain } from '../model/i-domain.model';
import { DateColumn } from '../model/date-column.model';
import { isFunction } from 'lodash';
import { SelectItem } from 'primeng/api/selectitem';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';
import { environment } from 'environments/environment';
import { Pageable } from '../model/pageable.model';
import { SortOrder } from '../model/sort-order.model';

export const DEFAULT_DATE_FORMAT: string = 'MM/DD/YYYY';

export class StaticService {

    public static cachedConfiguration: ConfigurationModel;

    public static backendHref: string = `${location.protocol}//${location.hostname}:${environment.rest_port}${environment.rest_href}`;
    // public static backendHref: string = 'http://46.101.177.78:18000/kabanchik';

    public static dateToTimestamp(date: Date): number {
        return date.getTime() - date.getTimezoneOffset() * 60 * 1000;
    }

    public static isInstanceOfSelectItem(object: any): object is SelectItem {
        return object !== null && typeof object === 'object' && 'value' in object && 'label' in object;
    }

    public static isInstanceOfLocalizedEnum(object: any): object is LocalizedEnum {
        return StaticService.isInstanceOfSelectItem(object) && 'messageCode' in object;
    }

    public static resolveFunctor<T>(functor: any | ((t?: T) => any), ...param: T[]): any {
        return isFunction(functor) ? functor(...param) : functor;
    }

    public static resolveEnumOnEntity<T>(enumValue: any, enumType: T): any {
        if ((<LocalizedEnum>enumValue).value) {
            return enumType[(<LocalizedEnum>enumValue).value];
        } else {
            return enumValue;
        }
    }

    public static createPageable(limit: number, offset?: number, fields: string[] = ['name']): Pageable {
        return {
            size: limit,
            page: Math.floor(offset / limit),
            sort: fields.map((field) => ({field, order: SortOrder.ASC}))
        };
    }

    /**
     * Get week from provided {@link Date}.
     * Body of this function works based on ISO standards.
     * I.e. it calculated week 53 for date based on how many days of week belongs to first/last week of the year.
     */
    public static calculateWeekISO(date: Date): number {
        const d: Date = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()));
        const dayNum: number = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        const yearStart: Date = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        return Math.ceil((((d.getTime() - yearStart.getTime()) / 86400000) + 1) / 7);
    }

    /**
     * At the moment, I always split year periods by first/last day of the year.
     * This function hacks ISO standard.
     */
    public static calculateWeek(date: Date): number {
        const week: number = StaticService.calculateWeekISO(date);

        if (week === 53 && date.getMonth() === 1) {
            // 2019-01-01
            return 1;
        } else if (week === 1 && date.getMonth() === 11) {
            // 2019-12-31
            return 53;
        } else {
            return week;
        }
    }

    /**
     * Get {@link Date} of provided day of the year (0-365)
     */
    public static dateFromDay(year, day) {
        const date = new Date(Date.UTC(year, 0, 1));
        return new Date(date.setUTCDate(day));
    }

    /**
     * Get {@link Date} equal to Monday of the week where {@param date} belongs to
     */
    public static getUTCMonday(date: Date): Date {
        const d: Date = new Date(date);
        const day: number = d.getUTCDay();
        const diff: number = d.getUTCDate() - day + (day === 0 ? -6 : 1);
        return new Date(d.setUTCDate(diff));
    }

    public static getMonday(date: Date): Date {
        const d: Date = new Date(date);
        const day: number = d.getDay();
        const diff: number = d.getDate() - day + (day === 0 ? -6 : 1);
        return new Date(d.setDate(diff));
    }

    public static getMondayOrFirstDayOfMonth(date: Date): Date {
        const monday: Date = StaticService.getMonday(date);
        if (monday.getMonth() < date.getMonth()) {
            return new Date(date.getFullYear(), date.getMonth(), 1);
        } else {
            return monday;
        }
    }

    public static convertStringToEnumInt(_enum: any, val: string, prefix: string | number = 0, suffix: string | number = 0): number {
        const prefixLength: number = typeof prefix === 'string' ? prefix.length : prefix;
        const suffixLength: number = typeof suffix === 'string' ? suffix.length : suffix;
        const _val: string = val.substring(prefixLength, val.length - suffixLength);
        return parseInt(_enum[_val], 10);
    }

    public static filterDomainsByActiveAndId<ID>(list: Domain<ID>[], forceId: ID) {
        return list.filter(item => item.isActive || item.id === forceId);
    }

    public static filterDomainsByActiveAndIds<ID>(list: Domain<ID>[], forceIds: ID[]) {
        return list.filter(item => item.isActive || forceIds.indexOf(item.id) >= 0);
    }

    public static calculateWeekColsInPeriod(yearFrom: number, monthFrom: number, yearTo?: number, monthTo?: number): DateColumn[] {
        if (!yearTo) {
            yearTo = yearFrom;
            monthTo = monthFrom;
        }

        if (yearFrom > yearTo) {
            throw Error('Wrong period range');
        }

        const firstDayOfPeriodMonth: Date = new Date(Date.UTC(yearFrom, monthFrom, 1));
        if (firstDayOfPeriodMonth.getUTCDay() === 0) {
            firstDayOfPeriodMonth.setUTCDate(firstDayOfPeriodMonth.getUTCDate() + 1);
        } else if (firstDayOfPeriodMonth.getUTCDay() === 6) {
            firstDayOfPeriodMonth.setUTCDate(firstDayOfPeriodMonth.getUTCDate() + 2);
        }

        const lastDayOfPeriodMonth: Date = new Date(Date.UTC(yearTo, monthTo + 1, 0));

        return StaticService.getWeeksBetweenDates(firstDayOfPeriodMonth, lastDayOfPeriodMonth);
    }

    public static getWeeksBetweenDates(firstDayOfPeriodMonth: Date, lastDayOfPeriodMonth: Date): DateColumn[] {
        const result: DateColumn[] = [];

        const yearFrom: number = firstDayOfPeriodMonth.getUTCFullYear();
        const yearTo: number = lastDayOfPeriodMonth.getUTCFullYear();

        const firstWeekOfPeriod: number = StaticService.calculateWeek(firstDayOfPeriodMonth);
        const lastWeekOfPeriod: number = StaticService.calculateWeek(lastDayOfPeriodMonth);

        for (let j: number = yearFrom; j <= yearTo; j++) {
            const yearStart: Date = new Date(Date.UTC(j, 0, 1));
            const yearEnd: Date = new Date(Date.UTC(j, 11, 31));

            const firstWeekOfYear: number = StaticService.calculateWeek(yearStart);

            let firstWeek: number = j === yearFrom ? firstWeekOfPeriod : 1;
            if (firstWeek === 53) {
                // for multi year period
                firstWeek = 1;
            }

            let lastWeek: number = j === yearTo ? lastWeekOfPeriod : StaticService.calculateWeek(yearEnd);
            if (firstWeekOfYear !== 1) {
                // for monthly periods in 2021 since it "starts" with week 53 I need to shift everything by 1 week ahead
                if (firstWeek !== 1) {
                    firstWeek += 1;
                }
                lastWeek += 1;
            }

            let weekStart: Date = j === yearFrom ? StaticService.getUTCMonday(firstDayOfPeriodMonth) : new Date(yearStart);

            if (StaticService.cachedConfiguration.timesheetMode && weekStart.getTime() < firstDayOfPeriodMonth.getTime()) {
                weekStart = new Date(firstDayOfPeriodMonth);
            }

            for (let i: number = firstWeek; i <= lastWeek; i++) {
                let weekEnd: Date = new Date(weekStart);
                if (weekStart.getUTCDay() !== 0) {
                    // make weekEnd to be Sunday
                    weekEnd.setUTCDate(weekStart.getUTCDate() + 6 - (weekStart.getUTCDay() - 1));
                    weekEnd.setUTCHours(23, 59, 59, 999);
                }

                if (StaticService.cachedConfiguration.timesheetMode) {
                    if (weekStart.getUTCFullYear() !== weekEnd.getUTCFullYear()) {
                        // for multi year period - weekEnd on lastWeekOfYear should be yearEnd
                        weekEnd = new Date(yearEnd);
                    } else if (weekEnd.getTime() > lastDayOfPeriodMonth.getTime()) {
                        // for multi month period - weekEnd on lastWeek should be lastDayOfMonth
                        weekEnd = new Date(lastDayOfPeriodMonth);
                    }
                }

                if ((StaticService.cachedConfiguration.timesheetMode
                    || !(i === 1 && weekStart.getUTCDay() > 1))
                    && weekStart.getFullYear() <= j
                ) {
                    result.push({
                        year: j,
                        week: i,
                        weekStart: new Date(weekStart),
                        weekEnd: weekEnd
                    });
                }

                weekStart = new Date(weekEnd);
                weekStart.setUTCDate(weekStart.getUTCDate() + 1);
                weekStart.setUTCHours(0, 0, 0, 0);
            }
        }

        return result;
    }

    public static groupBy(collection: any[], key: (entry: any) => any | any): Map<any, any> {
        const mapper: (entry: any) => any =
            isFunction(key) ?
            (currentItem) => key(currentItem) :
            (currentItem) => currentItem[key];

        return collection.reduce(function(returnValue: Map<any, any>, currentItem: any) {
            const _key: any = mapper(currentItem);
            const entrySet: any[] = returnValue.get(_key) || [];

            entrySet.push(currentItem);
            returnValue.set(_key, entrySet);

            return returnValue;
        }, new Map<any, any>());
    }

    public static compareVars(value1: any, value2: any): number {
        if (value1 == null && value2 != null) {
            return -1;
        } else if (value1 != null && value2 == null) {
            return 1;
        } else if (value1 == null && value2 == null) {
            return 0;
        } else if (typeof value1 === 'string' && typeof value2 === 'string') {
            return value1.localeCompare(value2);
        } else {
            return (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
        }
    }

}
