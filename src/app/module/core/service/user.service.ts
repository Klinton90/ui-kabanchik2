import { ConverterService } from './converter.service';
import { Injectable } from '@angular/core';
import { UserSimpleModel } from '../../admin/component/user/user.model';
import { isArray } from 'lodash';
import { ValueFormatterParams } from 'ag-grid-community';
import { MessageService } from './message.service';
import { RestResponseModel } from '../model/rest-response.model';

@Injectable({providedIn: 'root'})
export class UserService {

    constructor(
        private messageService: MessageService,
        private converterService: ConverterService
    ) {}

    getUserNameForView(user: UserSimpleModel): string {
        if (user) {
            let result: string = (user.firstName + ' ' + user.lastName).trim();
            if (!result) {
                result = user.email;
            }
            if (!user.isActive) {
                result += ' (' + this.messageService.resolveMessage('ui.text.default.inactive') + ')';
            }
            return result;
        } else {
            return '';
        }
    }

    getUserNameById(users: UserSimpleModel[], id: number): string {
        return this.converterService.getLabelByPatternAndId(users, id, user => this.getUserNameForView(user));
    }

    joinUserNames(users: UserSimpleModel[]): string {
        return users && isArray(users)
            ? users.map((user: UserSimpleModel) => this.getUserNameForView(user)).join()
            : '';
    }

    getManagersFormatterForAgGrid = (params: ValueFormatterParams): string => {
        return params.value ? this.joinUserNames(params.value.managers || params.value) : '';
    }

    defaultUserConverter = (response: RestResponseModel): any[] => {
        const options: any[] = (response.data.content || response.data);
        options.forEach(user => {
            (<any>user).viewName = this.getUserNameForView(user);
        });
        return options;
    }

}
