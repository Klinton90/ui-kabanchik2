import {InjectionToken} from '@angular/core';
import {Observable} from 'rxjs';

export const LANGUAGE_SERVICE_TOKEN = new InjectionToken<ILanguageService>('ILanguageService');

export interface ILanguageService {
    reloading$: Observable<boolean>;
    getMessage(key: string): string;
    initMessages(isAuthorized: boolean, localizationId?: string): Observable<any>;
    refreshMessages(forceBackend: boolean): Observable<any>;
}
