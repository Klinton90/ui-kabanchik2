import { ValidationRestResponseModel } from '../model/validation-rest-response.model';
import { isObject, isArray, isNil, toNumber } from 'lodash';
import { Inject, Injectable } from '@angular/core';
import { ILanguageService, LANGUAGE_SERVICE_TOKEN } from './i-language.service';

type ILanguageServiceAlias = ILanguageService;

export const BACKEND_VALIDATION_KEY = 'backend';

@Injectable({providedIn: 'root'})
export class MessageService {

    constructor(
        @Inject(LANGUAGE_SERVICE_TOKEN) protected languageService: ILanguageServiceAlias,
    ) {}

    public static propPattern: RegExp = /\${(\w+)}/;
    public static arrayPattern: RegExp = /{([0-9]+)}/;

    public static getMessageVars(message: string, pattern: RegExp): string[] {
        return message.match(pattern);
    }

    public static replacePropVars(message: string, variables: string[], source: object): string {
        if (!isObject(source)) {
            throw new Error('`source` must be an Object.');
        }

        for (const variable of variables) {
            message.replace(variable, source[variable.substring(2, variable.length - 1)]);
        }

        return message;
    }

    public static replaceArrayVars(message: string, variables: string[], source: Array<any> | object): string {
        if (isArray(source)) {
            for (let i = Math.max(variables.length, source.length); i > 0; i--) {
                message.replace(variables[i], source[i].toString());
            }
        } else if (isObject(source)) {
            const keys = Object.keys(source);
            for (let i = Math.max(variables.length, keys.length); i > 0; i--) {
                message.replace(variables[i], source[keys[i]].toString());
            }
        } else {
            throw new Error('`source` must be an Array or Object.');
        }

        return message;
    }

    public static replacePropVars2(message: string, pattern: RegExp, source: object): string {
        if (!isObject(source)) {
            throw new Error('`source` must be an Object.');
        }

        let match: RegExpExecArray;
        while (match = pattern.exec(message)) {
            const variable: string = source[match[1]] || '';
            message = message.replace(match[0], variable);
        }

        return message;
    }

    public static replaceArrayVars2(message: string, pattern: RegExp, source: Array<any> | object): string {
        if (!isObject(source)) {
            throw new Error('`source` must be an Array or Object.');
        }

        let match: RegExpExecArray;
        const sourceKeys: string[] = Object.keys(source);
        while (match = pattern.exec(message)) {
            const index: number = toNumber(match[1]);
            let variable: any = isArray(source) ? source[sourceKeys[index]] : source[index];
            variable = !isNil(variable) ? variable : '';
            message = message.replace(match[0], variable.toString());
        }

        return message;
    }

    public resolveMessage(key: string, params?: object): string {
        let message: string = '';

        if (key) {
            if (params) {
                if (key === BACKEND_VALIDATION_KEY) {
                    const _validationParams: ValidationRestResponseModel = <ValidationRestResponseModel> params;
                    message = _validationParams.message || this.processMessage(_validationParams.messageCode, _validationParams.data);
                } else {
                    message = this.processMessage(key, params);
                }
            } else {
                message = this.languageService.getMessage(key);
            }
        }

        return message;
    }

    private processMessage(messageCode: string, source: object): string {
        let message = this.languageService.getMessage(messageCode);

        if (message && message.length > 0 && source && isObject(source)) {
            if (!isArray(source)) {
                message = MessageService.replacePropVars2(message, MessageService.propPattern, source);
            } else {
                message = MessageService.replaceArrayVars2(message, MessageService.arrayPattern, source);
            }
        }

        return message;
    }

}
