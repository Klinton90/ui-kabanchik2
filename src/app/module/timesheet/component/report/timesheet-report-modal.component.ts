import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { TimesheetStatus, BaseTimesheet } from '../../model/timesheet.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { AlertService } from '../../../core/component/alert/alert.service';
import { TimesheetResource } from '../../resource/timesheet.resource';
import { SelectItem } from 'primeng/api/selectitem';
import { UtilService } from '../../../core/service/util.service';
import { UserResource } from 'app/module/admin/component/user/user.resource';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { UserService } from 'app/module/core/service/user.service';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { Moment } from 'moment';
import { Observable } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';

export enum TimesheetReportModalMode {
    USER, REVIEWER, PLAN
}

export class TimesheetReportModalParams implements FormAwareParams<BaseTimesheet> {
    entity: BaseTimesheet;
    mode: TimesheetReportModalMode;
    minDate?: Moment;
    isViewOnly: boolean;
}

@Component({
    moduleId: module.id,
    selector: 'timesheetReportModal',
    templateUrl: 'timesheet-report-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetReportModalComponent extends FormAwareModal<TimesheetReportModalComponent, BaseTimesheet> {

    params: TimesheetReportModalParams;
    TIMESHEET_REPORT_MODAL_MODE = TimesheetReportModalMode;
    availableStatuses: SelectItem[];

    newFormEntityInstance: BaseTimesheet = null;

    form: FormGroup = new FormGroup({
        id: new FormControl(''),
        amount: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(99.99)]),
        comment: new FormControl(''),
        wbsId: new FormControl(''),
        // manager fields
        newComment: new FormControl('', [Validators.maxLength(65025255)]),
        status: new FormControl(''),
        // hidden fields
        timesheetCategoryId: new FormControl(''),
        date: new FormControl(''),
        userId: new FormControl('')
    });

    constructor (
        public dialogRef: MatDialogRef<TimesheetReportModalComponent>,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected resource: TimesheetResource,
        protected utilService: UtilService,
        protected ref: ChangeDetectorRef,
        private userResource: UserResource,
        public userService: UserService,
        private serviceResource: ServiceResource,
        @Inject(MAT_DIALOG_DATA) data: TimesheetReportModalParams
    ) {
        super();

        if (data.mode === TimesheetReportModalMode.PLAN) {
            data.entity.status = TimesheetStatus.PLANNED;

            this.form.controls['status'].setValidators([]);
            this.form.controls['userId'].setValidators(Validators.compose([Validators.required]));
            this.form.controls['date'].setValidators(Validators.compose([Validators.required]));
            this.form.controls['wbsId'].setValidators([]);
        } else {
            this.form.controls['userId'].setValidators([]);
            this.form.controls['date'].setValidators([]);
            this.form.controls['wbsId'].setValidators([Validators.required]);

            this.form.controls['wbsId'].disable();
            this.form.controls['date'].disable();

            if (data.mode === TimesheetReportModalMode.REVIEWER) {
                this.form.controls['status'].setValidators(Validators.compose([Validators.required]));
            } else {
                this.form.controls['status'].setValidators([]);
            }

            if (data.isViewOnly) {
                this.form.controls['status'].disable();
            } else {
                this.form.controls['status'].enable();
            }

            if (!data.entity.status) {
                data.entity.status = null;
            }
        }

        super.show(data);
        // TODO: should this be converted to `null`?
        this.isAddMode = !data.entity.id;

        this.serviceResource.getLocalizedEnumOptions('kabanchik.system.timesheet.domain.TimesheetStatus')
            .subscribe({
                next: (response: RestResponseModel) => {
                    const statuses: SelectItem[] = response.data;
                    if (data.mode !== TimesheetReportModalMode.PLAN) {
                        if (data.mode === TimesheetReportModalMode.REVIEWER) {
                            this.availableStatuses = statuses.filter((item: SelectItem) => item.value !== 'PLANNED');
                        } else {
                            if (data.isViewOnly) {
                                this.availableStatuses = statuses.filter((item: SelectItem) => item.value !== 'PLANNED');
                            } else {
                                this.availableStatuses =
                                    statuses.filter((item: SelectItem) => ['PLANNED', 'APPROVED'].indexOf(item.value) < 0);
                            }
                        }
                    }
                }
            });
    }

    public getUserLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.userResource.getForPSR(term, limit, offset);
    }

    onDateChange(newValue: Moment): void {
        if (newValue && newValue.day && newValue.day() !== 1) {
            this.form.controls['date'].setValue(newValue.day(1));
        }
    }

    // Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();
        data.comment = data.newComment || '';
        data.newComment = undefined;

        data.date = data.date.valueOf();

        return data;
    }

    // Override
    protected getSaveAction(): string {
        return this.isAddMode ? (this.params.mode === TimesheetReportModalMode.PLAN ? 'createDoubleTimesheet' : 'insert') : 'update';
    }

}
