import { UserResource } from '../../../admin/component/user/user.resource';
import { SelectItem } from 'primeng/api/selectitem';
import { TimesheetResource } from '../../resource/timesheet.resource';
import { OnInit, ChangeDetectorRef, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { Component } from '@angular/core';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import {
    Timesheet,
    TimesheetStatus,
    TimesheetRowType,
    TimesheetCalculationSubResult,
    TimesheetRowWbsAware
} from '../../model/timesheet.model';
import { TimesheetCategory } from '../../../admin/component/timesheet-category/timesheet-category.model';
import { TimesheetCategoryResource } from '../../../admin/component/timesheet-category/timesheet-category.resource';
import { LocalizedEnum } from '../../../shared/model/localized-enum.model';
import { TimesheetReportModalComponent, TimesheetReportModalMode, TimesheetReportModalParams } from './timesheet-report-modal.component';
import { ConverterService } from '../../../core/service/converter.service';
import { TimesheetService } from '../../service/timesheet.service';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { AuthService } from '../../../../service/auth.service';
import { ROLE } from '../../../admin/component/user/user.model';
import { UserService } from '../../../core/service/user.service';
import { StaticService } from '../../../core/service/static.service';
import { Observable, zip } from 'rxjs';
import { DateColumn } from 'app/module/core/model/date-column.model';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ModalResult } from 'app/module/shared/component/modal/modal-result.model';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-report-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetReportListingComponent implements OnInit, AfterViewInit {

    TIMESHEET_ROW_TYPE: typeof TimesheetRowType = TimesheetRowType;

    cols: DateColumn[];
    rows: TimesheetRowWbsAware[] = [];
    currentUserTimesheetCategories: TimesheetCategory[] = [];
    currentUserTimesheetCategoryItems: SelectItem[] = [];
    managedTimesheetCategories: TimesheetCategory[] = [];
    managedTimesheetCategoryItems: SelectItem[] = [];
    availableTimesheetCategoryItems: SelectItem[] = [];

    userId: number;
    date: Moment;

    isDirty: boolean = false;
    isReviewerMode: boolean = false;
    isOwnTimesheet: boolean = false;

    private preventDataLoad: boolean = true;

    constructor(
        private timesheetResource: TimesheetResource,
        public timesheetService: TimesheetService,
        private timesheetCategoryResource: TimesheetCategoryResource,
        public converterService: ConverterService,
        private confirmationService: ConfirmationService,
        private authService: AuthService,
        private userResource: UserResource,
        public userService: UserService,
        private route: ActivatedRoute,
        private ref: ChangeDetectorRef,
        private dialog: MatDialog,
    ) {}

    ngOnInit(): void {
        this.date = moment(new Date()).startOf('month');

        if (this.authService.roles.indexOf(ROLE.EXECUTOR) === -1) {
            this.userId = this.authService.user.id;
            this.isReviewerMode = false;
            this.isOwnTimesheet = true;
            this.cols = StaticService.calculateWeekColsInPeriod(this.date.year(), this.date.month());
            this.updateTimesheetCategories();
            this.ref.detectChanges();
        } else {
            this.isReviewerMode = true;
            this.cols = StaticService.calculateWeekColsInPeriod(this.date.year(), this.date.month());

            if (this.route.snapshot.paramMap.has('userId')) {
                this.userId = Number.parseInt(this.route.snapshot.paramMap.get('userId'));
                this.userChanged();
            } else if (this.authService.roles.indexOf(ROLE.ADMIN) === -1) {
                this.userId = this.authService.user.id;
                this.userChanged();
            }

            this.ref.detectChanges();
        }
    }

    ngAfterViewInit(): void {
        this.preventDataLoad = false;
    }

    getUserLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.userResource.getReportingUsers(term, limit, offset);
    }

    userChanged(): void {
        this.isOwnTimesheet = this.userId === this.authService.user.id;
        this.updateTimesheetCategories();
    }

    dateChanged(value: Moment): void {
        if (value && !this.preventDataLoad) {
            let revertCallback = null;
            const prevValue: Moment = this.date;
            this.date = value.startOf('month');
            revertCallback = () => {
                this.date = prevValue;
            };

            if (!this.isDirty) {
                this.cols = StaticService.calculateWeekColsInPeriod(this.date.year(), this.date.month());
                if (this.userId) {
                    this.updateTimesheetCategories();
                }
            } else {
                this.showConfirmation(revertCallback);
            }
        }
    }

    refreshAction(): void {
        if (this.userId) {
            if (!this.isDirty) {
                this.getData();
            } else {
                this.showConfirmation(() => this.getData());
            }
        }
    }

    addRow(): void {
        const row: TimesheetRowWbsAware = new TimesheetRowWbsAware();
        for (let i: number = 0; i < this.cols.length; i++) {
            const col: DateColumn = this.cols[i];
            row.weeks[col.week] = Timesheet.forModal(col.weekStart, this.userId);
        }
        this.rows.push(row);
    }

    submitAction(timesheet: Timesheet): void {
        this.timesheetResource.submit(timesheet.id).subscribe((response: RestResponseModel) => {
            this.refreshAction();
        });
    }

    openModal(col: DateColumn, timesheetRow: TimesheetRowWbsAware): void {
        const timesheet: Timesheet = timesheetRow.weeks.get(col) || Timesheet.forModal(col.weekStart, this.userId);
        timesheet.timesheetCategoryId = timesheetRow.timesheetCategoryId;
        timesheet.wbsId = timesheetRow.wbsId;
        const params: TimesheetReportModalParams = {
            entity: timesheet,
            isViewOnly: !this.isEditableRow(timesheet),
            mode: this.isManagedTimesheetCategory(timesheet.timesheetCategoryId)
                ? TimesheetReportModalMode.REVIEWER
                : TimesheetReportModalMode.USER
        };
        this.dialog.open(TimesheetReportModalComponent, {data: params})
            .afterClosed()
            .subscribe({
                next: (result: ModalResult) => {
                    if (result && result.isRefreshRequired) {
                        this.isDirty = false;
                        this.refreshAction();
                    }
                }
            });
    }

    isManagedTimesheetCategory(targetTimesheetCategoryId: number): boolean {
        return this.managedTimesheetCategories.find((timesheetCategory: TimesheetCategory) => {
            return timesheetCategory.id === targetTimesheetCategoryId;
        }) != null;
    }

    isSharableRow(timesheet: Timesheet): boolean {
        return timesheet
            && timesheet.status
            && (<LocalizedEnum>timesheet.status).value === 'EDITABLE';
    }

    isApprovableRow(timesheet: Timesheet): boolean {
        return this.isReviewerMode
            && timesheet
            && timesheet.status
            && (<LocalizedEnum>timesheet.status).value === 'POSTED'
            && this.isManagedTimesheetCategory(timesheet.timesheetCategoryId);
    }

    isEditableRow(timesheet?: Timesheet): boolean {
        return this.authService.roles.indexOf(ROLE.ADMIN) >= 0
            || (this.isOwnTimesheet
                && (!timesheet
                    || !timesheet.status
                    || (timesheet.status as any) === TimesheetStatus.EDITABLE
                    || timesheet.status === TimesheetStatus.POSTED
                    || timesheet.status === TimesheetStatus.DECLINED
                    || (<LocalizedEnum>timesheet.status).value === 'EDITABLE'
                    || (<LocalizedEnum>timesheet.status).value === 'POSTED'
                    || (<LocalizedEnum>timesheet.status).value === 'DECLINED'
                )
            )
            || (this.isReviewerMode
                && (!timesheet
                    || (timesheet.status
                        && timesheet.status !== TimesheetStatus.APPROVED
                        && (<LocalizedEnum>timesheet.status).value !== 'APPROVED'
                        && timesheet.status !== TimesheetStatus.PLANNED
                        && (<LocalizedEnum>timesheet.status).value !== 'PLANNED'
                        && this.isManagedTimesheetCategory(timesheet.timesheetCategoryId)
                    )
                )
            );
    }

    editInit($event): void {
        this.availableTimesheetCategoryItems =
            this.isOwnTimesheet
            ? this.currentUserTimesheetCategoryItems
            : this.managedTimesheetCategoryItems;
    }

    timesheetSelected(value: number, timesheetRow: TimesheetRowWbsAware): void {
        this.isDirty = true;
        timesheetRow.timesheetCategoryId = value;
    }

    showConfirmation(revertCallback?: () => void): void {
        this.confirmationService.show(
            {
                body: 'ui.confirmation.grid.dirty.body',
                onPrimaryClick: () => {
                    this.isDirty = false;
                    this.refreshAction();
                },
                onSecondaryClick: () => {
                    if (revertCallback) {
                        revertCallback();
                    }
                }
            }
        );
    }

    approveAction(timesheet: Timesheet): void {
        this.timesheetResource.approve(timesheet.id).subscribe((response: RestResponseModel) => {
            this.refreshAction();
        });
    }

    getTimesheetCategories() {
        return this.isOwnTimesheet ? this.currentUserTimesheetCategories : this.managedTimesheetCategories;
    }

    getTimesheetCategoryName(row: TimesheetRowWbsAware): string {
        return this.converterService.getLabelByPatternAndId(
            this.getTimesheetCategories(),
            row.timesheetCategoryId,
            ConverterService.DEFAULT_PATTERN);
    }

    private getData(): void {
        const request: Observable<any> =
            !this.isOwnTimesheet
            ? this.timesheetResource.findAllManagedTimesheetsByUserId(this.userId, this.date.year(), this.date.month())
            : this.timesheetResource.getTimesheetForLoggedUser(this.date.year(), this.date.month());

        request.subscribe((timesheets: Timesheet[]) => {
            const calculationResult: TimesheetCalculationSubResult =
                this.timesheetService.convertTimesheetToTimesheetCategoryAndWbsIdBreakdownRows(
                    timesheets,
                    this.cols,
                    this.getTimesheetCategories(),
                    true);

            this.rows = <TimesheetRowWbsAware[]>calculationResult.rows;
            this.ref.detectChanges();
        });
    }

    private updateTimesheetCategories() {
        const oTimesheetCategories: Observable<any>[] = [];
        if (this.isOwnTimesheet) {
            const oCurrentUserTimesheetCategories: Observable<any> = this.timesheetCategoryResource
                .findAllByCurrentUserAndPeriod(this.date.year(), this.date.month(), this.date.year(), this.date.month())
                .pipe(
                    tap((response: RestResponseModel) => {
                        this.currentUserTimesheetCategories = response.data;
                        this.currentUserTimesheetCategoryItems = this.converterService.convertObjectListToSelectItems(
                            response.data,
                            false,
                            ConverterService.DEFAULT_PATTERN,
                            'id',
                            0);
                    })
                );
            oTimesheetCategories.push(oCurrentUserTimesheetCategories);
        }
        if (this.isReviewerMode) {
            const oManagedTimesheetCategories: Observable<any> = this.timesheetCategoryResource
                .findAllManagedTimesheetCategoriesByUserIdAndPeriod(
                    this.userId,
                    this.date.year(),
                    this.date.month(),
                    this.date.year(),
                    this.date.month()
                )
                    .pipe(
                        tap((response: RestResponseModel) => {
                            this.managedTimesheetCategories = response.data;
                            this.managedTimesheetCategoryItems = this.converterService.convertObjectListToSelectItems(
                                response.data,
                                false,
                                ConverterService.DEFAULT_PATTERN,
                                'id',
                                0);
                        })
                    );
            oTimesheetCategories.push(oManagedTimesheetCategories);
        }

        zip(...oTimesheetCategories).subscribe(() => this.refreshAction());
    }

}
