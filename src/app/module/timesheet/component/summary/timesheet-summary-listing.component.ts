import {
    TimesheetSummary,
    TimesheetRow,
    TimesheetCategoryBreakdownRow,
    TimesheetStatus,
    TimesheetWeeklyItem,
    TimesheetRowType,
    TimesheetCalculationSubResult
} from '../../model/timesheet.model';
import { TimesheetCategory } from '../../../admin/component/timesheet-category/timesheet-category.model';
import { Component, OnInit, HostListener, ChangeDetectorRef, AfterViewInit  } from '@angular/core';
import { TimesheetService } from '../../service/timesheet.service';
import { TimesheetResource } from '../../resource/timesheet.resource';
import { TimesheetCategoryResource } from '../../../admin/component/timesheet-category/timesheet-category.resource';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { ConverterService } from '../../../core/service/converter.service';
import { UserService } from '../../../core/service/user.service';
import { UserResource } from '../../../admin/component/user/user.resource';
import { UserSimpleModel, ROLE } from '../../../admin/component/user/user.model';
import { DepartmentResource } from '../../../admin/component/department/department.resource';
import { DepartmentModel } from '../../../admin/component/department/department.model';
import { StaticService } from '../../../core/service/static.service';
import { SortOrder } from '../../../core/model/sort-order.model';
import { Observable, zip } from 'rxjs';
import { TimesheetSummaryDateColumn } from 'app/module/core/model/date-column.model';
import { tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { HolidayResource } from 'app/module/admin/component/holiday/holiday.resource';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';
import { Label } from 'ng2-charts';
import { ChartOptions, ChartDataSets } from 'chart.js';
import * as pluginColoschemes from 'chartjs-plugin-colorschemes';
import { StickyService } from 'app/module/core/service/sticky.service';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Context } from 'chartjs-plugin-datalabels';
import { MessageService } from 'app/module/core/service/message.service';
import { Moment } from 'moment';
import * as moment from 'moment';
import { SelectItem } from 'primeng/api/selectitem';
import { SortEvent } from 'primeng/api/sortevent';

const STICKY_NAME = 'timesheetSummaryPage';

const specialRowsTypes: TimesheetRowType[] = [
    TimesheetRowType.AVAILABLE_HOURS,
    TimesheetRowType.OCCUPATION_PERCENTAGE,
    TimesheetRowType.TOTAL
];

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-summary-listing.component.html',
    styleUrls: ['timesheet-summary-listing.component.scss']
})
export class TimesheetSummaryListingComponent implements OnInit, AfterViewInit {

    TIMESHEET_ROW_TYPE: typeof TimesheetRowType = TimesheetRowType;

    exportCols: {
        header: string,
        field: string
    }[];

    cols: TimesheetSummaryDateColumn[];
    rows1: TimesheetRow[] = [];
    rows2: TimesheetCategoryBreakdownRow[] = [];
    rows3: TimesheetRow[] = [];
    rows4: TimesheetCategoryBreakdownRow[] = [];
    userRowsGrouping: any = {};

    timesheets: TimesheetSummary[];

    timesheetCategoryItems: SelectItem[] = [];
    userItems: SelectItem[] = [];

    timesheetCategories: TimesheetCategory[] = [];
    users: UserSimpleModel[] = [];
    departments: DepartmentModel[] = [];

    timesheetCategoryId: number;
    userId: number;
    departmentId: number;

    dateFrom: Moment = moment(new Date()).startOf('month');
    dateTo: Moment = moment(new Date()).endOf('month');

    showPreliminary: boolean;
    reportType: boolean = true;

    gridWidth: number = 0;
    activeTab: number = 0;

    isWorkload: boolean;

    // chart
    weekChartLabels: Label[] = [];
    barChartData: ChartDataSets[] = [];
    barChartPlugins = [
        pluginColoschemes,
        pluginDataLabels
    ];
    barChartOptions: ChartOptions = {
        maintainAspectRatio: false,
        // responsive: false,
        // We use these empty structures as placeholders for dynamic theming.
        scales: {
            xAxes: [{
                stacked: true,
            }],
            yAxes: [{
                stacked: true,
            }]
        },
        plugins: {
            datalabels: {
                // anchor: 'end',
                // align: 'start',
                // offset: 40
                display: (context: Context) => {
                    return !!context.dataset.data[context.dataIndex];
                }
            },
            colorschemes: {
                scheme: 'brewer.Paired12'
            }
        }
    };

    private preventDataLoad: boolean = true;

    constructor(
        private timesheetResource: TimesheetResource,
        private timesheetService: TimesheetService,
        private timesheetCategoryResource: TimesheetCategoryResource,
        public converterService: ConverterService,
        private userResource: UserResource,
        private userService: UserService,
        private messageService: MessageService,
        private departmentResource: DepartmentResource,
        private route: ActivatedRoute,
        private holidayResource: HolidayResource,
        private router: Router,
        private ref: ChangeDetectorRef,
        stickyService: StickyService
    ) {
        this.onResize();

        stickyService.registerField(STICKY_NAME, () => {
            return {
                activeTab: this.activeTab,
                userRowsGrouping: this.userRowsGrouping
            };
        });

        const stickyData: any = stickyService.getData();
        if (stickyData && stickyData[STICKY_NAME]) {
            Object.assign(this, stickyData[STICKY_NAME]);
        }
    }

    ngOnInit(): void {
        this.isWorkload = this.route.snapshot.data.isWorkload;
        this.showPreliminary = this.isWorkload;
        this.departmentResource.list(false, {sort: [{field: 'name', order: SortOrder.ASC}]})
            .subscribe({
                next: (response: RestResponseModel) => {
                    this.departments = response.data;
                }
            });
        this.calculateGridCols();
    }

    ngAfterViewInit(): void {
        this.preventDataLoad = false;
    }

    @HostListener('window:resize', ['$event'])
    onResize(event?): void {
        this.gridWidth = window.innerWidth - (15 + 16 + 5) * 2;
    }

    refreshData(): void {
        const userListAction: Observable<any> = this.isWorkload
                ? this.userResource.getUsersForWorkload()
                : this.userResource.getReportingUsersByPeriod(this.dateFrom, this.dateTo);
        const oUserList: Observable<any> = userListAction.pipe(
            tap((response: RestResponseModel) => {
                this.users = response.data;
                this.userItems = this.converterService.convertObjectListToSelectItems(
                    response.data,
                    false,
                    (user: UserSimpleModel) => this.userService.getUserNameForView(user),
                    'id');
            })
        );

        const timesheetCategoryAction: Observable<any> = this.isWorkload
                ? this.timesheetCategoryResource.getTimesheetCategoriesForPsrSummaryByPeriod(
                    this.dateFrom,
                    this.dateTo,
                    this.departmentId)
                : this.timesheetCategoryResource.getManagedTimesheetCategoriesForSummaryByPeriod(
                    this.dateFrom,
                    this.dateTo,
                    this.departmentId);
        const oTimesheetCategoryList: Observable<any> = timesheetCategoryAction.pipe(
            tap((response: RestResponseModel) => {
                this.timesheetCategories = response.data;
                this.timesheetCategoryItems = this.converterService.convertObjectListToSelectItems(
                    response.data,
                    false,
                    ConverterService.DEFAULT_PATTERN,
                    'id');
            })
        );

        const timesheetAction: Observable<any> = this.isWorkload
                ? this.timesheetResource.findAllTimesheetsForPsrByLoggedUser(
                    this.dateFrom,
                    this.dateTo,
                    this.departmentId)
                : this.timesheetResource.findAllManagedTimesheetsForLoggedUser(
                    this.dateFrom,
                    this.dateTo,
                    this.departmentId);
        const oTimesheetList: Observable<any> = timesheetAction.pipe(
            tap((timesheets: TimesheetSummary[]) => {
                this.timesheets = timesheets;
            })
        );

        const weeks: Date[] = this.cols.map((col: TimesheetSummaryDateColumn) => col.weekStart);
        const oWeeklyRestrictions: Observable<any> = this.holidayResource.getWeeklyMaxList(weeks).pipe(
            tap((response: RestResponseModel) => {
                this.cols.forEach((col: TimesheetSummaryDateColumn) => {
                    col.maxHours = response.data[col.weekStart.getTime()];
                });
            })
        );

        zip(oUserList, oTimesheetCategoryList, oTimesheetList, oWeeklyRestrictions)
            .subscribe(() => this.prepareRows());
    }

    prepareRows(showPreliminary: boolean = this.showPreliminary): void {
        this.showPreliminary = showPreliminary;
        this.prepareMainCategoryBreakdownRows();
        this.prepareMainUserBreakdownRows();
        this.prepareUserBreakdownRows();
        this.prepareCategoryBreakdownRows();
        this.ref.detectChanges();
    }

    prepareUserBreakdownRows(): void {
        if (this.timesheetCategoryId) {
            const categoryFilteredTimesheets: TimesheetSummary[] = this.timesheets.filter((row: TimesheetSummary) => {
                return row.timesheetCategoryId === this.timesheetCategoryId;
            });

            const calculationResult: TimesheetCalculationSubResult = this.timesheetService.convertTimesheetToUserBreakdownRows(
                categoryFilteredTimesheets,
                this.cols,
                this.users,
                this.departments,
                this.showPreliminary,
                this.isWorkload);

            this.rows2 = <TimesheetCategoryBreakdownRow[]>calculationResult.rows;
        }
    }

    prepareCategoryBreakdownRows(): void {
        if (this.userId) {
            const userFilteredTimesheets: TimesheetSummary[] = this.timesheets.filter((row: TimesheetSummary) => {
                return row.userId === this.userId;
            });

            const calculationResult: TimesheetCalculationSubResult = this.timesheetService.convertTimesheetToTimesheetCategoryBreakdownRows(
                userFilteredTimesheets,
                this.cols,
                this.timesheetCategories,
                this.showPreliminary,
                this.isWorkload);

            this.rows3 = <TimesheetRow[]>calculationResult.rows;
        }
    }

    calculateGridCols(): void {
        this.cols = StaticService.calculateWeekColsInPeriod(
            this.dateFrom.year(),
            this.dateFrom.month(),
            this.dateTo.year(),
            this.dateTo.month());

        if (this.isWorkload) {
            this.weekChartLabels = this.cols.map((col: TimesheetSummaryDateColumn) => this.getWeekGraphLabelPrefix() + col.week);
            this.weekChartLabels.unshift(this.messageService.resolveMessage('ui.text.summary.availableResources'));
        }

        this.refreshData();
    }

    exportCSV2(): void {
        let csv = '\ufeff';

        // header
        csv +=  '"' +
                String(this.messageService.resolveMessage('ui.text.user.title')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.department.title')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.timesheetCategory.title')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.timesheetCategory.description')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.report.wbsId')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.default.year')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.default.week')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.report.amount')).replace(/"/g, '""') +
                '","' +
                String(this.messageService.resolveMessage('ui.text.summary.money')).replace(/"/g, '""') +
                '"';

        // body
        this.timesheets.forEach((timesheet: TimesheetSummary) => {
            const isApprovedStatus: boolean = TimesheetStatus[(<LocalizedEnum>timesheet.status).value] === TimesheetStatus.APPROVED;
            if (this.showPreliminary || isApprovedStatus) {
                const timesheetCategory: TimesheetCategory = this.timesheetCategories
                    .find(item => item.id === timesheet.timesheetCategoryId);
                csv +=  '\n"' +
                        String(this.userService.getUserNameById(this.users, timesheet.userId)).replace(/"/g, '""') +
                        '","' +
                        String(this.departments.find((department: DepartmentModel) =>
                            department.id === timesheet.departmentId).name).replace(/"/g, '""') +
                        '","' +
                        String(timesheetCategory.name).replace(/"/g, '""') +
                        '","' +
                        String(timesheetCategory.description).replace(/"/g, '""') +
                        '","' +
                        String(timesheet.wbsId).replace(/"/g, '""') +
                        '","' +
                        timesheet.date.getUTCFullYear() +
                        '","' +
                        StaticService.calculateWeek(timesheet.date) +
                        '","' +
                        timesheet.amount +
                        '","' +
                        timesheet.amount * timesheet.rate +
                        '"';
            }
        });

        // download
        this.download(csv);
    }

    customCategorySort(event: SortEvent): void {
        event.data.sort((data1, data2) => {
            const typeResult: number = StaticService.compareVars(data1.type, data2.type);
            if (typeResult !== 0) {
                return typeResult;
            }

            const result: number = StaticService.compareVars(data1[event.field], data2[event.field]);
            return (event.order * result);
        });
    }

    customUserSort(event: SortEvent): void {
        event.data.sort((data1: TimesheetCategoryBreakdownRow, data2: TimesheetCategoryBreakdownRow) => {
            if (specialRowsTypes.indexOf(data1.type) < 0 && specialRowsTypes.indexOf(data2.type) < 0) {
                const departmentResult: number = StaticService.compareVars(data1.departmentId, data2.departmentId);
                if (departmentResult !== 0) {
                    return event.order * departmentResult;
                }
            }

            const typeResult: number = StaticService.compareVars(data1.type, data2.type);
            if (typeResult !== 0) {
                return typeResult;
            }

            const result: number = StaticService.compareVars(data1[event.field], data2[event.field]);
            return event.order * result;
        });
    }

    openUserBreakdownTab(timesheetCategoryId: number): void {
        this.activeTab = 1;
        this.timesheetCategoryId = timesheetCategoryId;
        this.prepareUserBreakdownRows();
    }

    openCategoryBreakdownTab(userId: number): void {
        this.activeTab = 2;
        this.userId = userId;
        this.prepareCategoryBreakdownRows();
    }

    openUserApproval(userId: number): void {
        this.router.navigateByUrl(`/timesheet/report/${userId}`);
    }

    getHoursClass(entity: TimesheetCategoryBreakdownRow, col: TimesheetSummaryDateColumn): string {
        if (entity.type !== TimesheetRowType.SIMPLE || !entity.weeks.get(col)) {
            return '';
        }

        const role: ROLE = StaticService.resolveEnumOnEntity(entity.user.role, ROLE);
        const workloadMax: number = !entity.user || [ROLE.USER, ROLE.INTERNAL].includes(role)
                ? StaticService.cachedConfiguration.userWeeklyWorkloadMax
                : StaticService.cachedConfiguration.executorWeeklyWorkloadMax;

        const amount: number = entity.weeks.get(col).amount;
        if (amount < col.maxHours * workloadMax / 100) {
            return 'bg-warning';
        } else if (amount >= col.maxHours * workloadMax / 100 && amount <= col.maxHours) {
            return '';
        } else {
            return 'bg-danger';
        }
    }

    toggleRowGroupHiddenState(departmentId: number): void {
        const currentValue: boolean = this.userRowsGrouping[departmentId];
        this.userRowsGrouping[departmentId] = !currentValue;
    }

    fromDateChanged(value: Moment): void {
        if (value && !this.preventDataLoad) {
            this.dateFrom = value.startOf('month');

            if (this.dateFrom.valueOf() > this.dateTo.valueOf()) {
                this.dateFrom = moment(this.dateTo);
            }

            this.calculateGridCols();
        }
    }

    toDateChanged(value: Moment): void {
        if (value && !this.preventDataLoad) {
            this.dateTo = value.endOf('month');

            if (this.dateTo.valueOf() < this.dateFrom.valueOf()) {
                this.dateTo = moment(this.dateFrom);
            }

            this.calculateGridCols();
        }
    }

    getDepartmentLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.departmentResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    departmentConverter = (response: RestResponseModel): SelectItem[] => {
        return this.converterService.defaultObjectConverter(response);
    }

    private prepareMainCategoryBreakdownRows(): void {
        const calculationResult: TimesheetCalculationSubResult = this.timesheetService.convertTimesheetToTimesheetCategoryBreakdownRows(
            this.timesheets,
            this.cols,
            this.timesheetCategories,
            this.showPreliminary,
            this.isWorkload);

        if (this.isWorkload) {
            calculationResult.rows = calculationResult.rows.concat(this.timesheetService.getTimesheetCategoryBreakdownWorkloadRows(
                this.cols, calculationResult.totalRow, this.users, this.departmentId));
        }

        this.rows1 = <TimesheetRow[]>calculationResult.rows;
    }

    private prepareMainUserBreakdownRows(): void {
        const calculationResult: TimesheetCalculationSubResult = this.timesheetService.convertTimesheetToUserBreakdownRows(
            this.timesheets,
            this.cols,
            this.users,
            this.departments,
            this.showPreliminary,
            this.isWorkload);

        if (this.isWorkload) {
            calculationResult.rows = calculationResult.rows.concat(this.timesheetService.getUserBreakdownWorloadRows(
                this.cols, calculationResult.totalRow, this.users, this.departmentId));
        }

        this.rows4 = <TimesheetCategoryBreakdownRow[]>calculationResult.rows;

        if (this.isWorkload) {
            this.barChartData.length = 0;

            const weeksFillerData: number[] = new Array(this.cols.length).fill(0);
            this.departments.forEach((department: DepartmentModel) => {
                if (department.forWorkload) {
                    let departmentData: number[] = [this.timesheetService.getAvailableUsersForDepartment(this.users, department.id)];

                    const departmentDataRow: TimesheetCategoryBreakdownRow = this.rows4.find((row: TimesheetCategoryBreakdownRow) =>
                        row.departmentId === department.id && row.type === TimesheetRowType.GROUP_TOTAL);

                    if (departmentDataRow) {
                        departmentDataRow.weeks.forEach((value: TimesheetWeeklyItem, col: TimesheetSummaryDateColumn) => {
                            departmentData.push(
                                Math.ceil(value.amount / (StaticService.cachedConfiguration.userWeeklyWorkloadMax / 100) / col.maxHours));
                        });
                    } else {
                        departmentData = departmentData.concat(weeksFillerData);
                    }

                    this.barChartData.push({
                        data: departmentData,
                        label: department.name
                    });
                }
            });
        }
    }

    private download(csv: string): void {
        const blob = new Blob([csv], {
            type: 'text/csv;charset=utf-8;'
        });

        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveOrOpenBlob(blob, 'download.csv');
        } else {
            const link = document.createElement('a');
            link.style.display = 'none';
            document.body.appendChild(link);
            if (link.download !== undefined) {
                link.setAttribute('href', URL.createObjectURL(blob));
                link.setAttribute('download', 'download.csv');
                link.click();
            } else {
                csv = 'data:text/csv;charset=utf-8,' + csv;
                window.open(encodeURI(csv));
            }
            document.body.removeChild(link);
        }
    }

    private getWeekGraphLabelPrefix(): string {
        return this.messageService.resolveMessage('ui.text.default.week') + ' ';
    }

}
