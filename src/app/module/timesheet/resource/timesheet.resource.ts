import { TimesheetSummary } from '../model/timesheet.model';
import { HttpRequestOptions } from '../../core/model/http-request-options.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormAwareResource } from '../../core/resource/form-aware.resource';
import { Timesheet, RawTimesheet } from '../model/timesheet.model';
import { RestResponseModel } from '../../core/model/rest-response.model';
import { map } from 'rxjs/operators';
import { BulkTimesheetAdjust } from 'app/module/psr/component/timesheet/bulk-timesheet.model';
import { Moment } from 'moment';

@Injectable({
    providedIn: 'root'
})
export class TimesheetResource extends FormAwareResource {

    constructor(
        protected http: HttpClient
    ) {
        super('timesheet');
    }

    public getTimesheetForLoggedUser(year: number, month: number): Observable<Timesheet[]> {
        const params: HttpParams = new HttpParams()
            .append('year', year.toString())
            .append('month', month.toString());

        return this._executeRequest({
            url: '/getTimesheetForLoggedUser',
            method: 'GET',
            params: params
        }).pipe(
            map(this.convertResponseToTimesheet)
        );
    }

    public findAllManagedTimesheetsByUserId(userId: number, year: number, month: number): Observable<Timesheet[]> {
        const params: HttpParams = new HttpParams()
            .append('userId', userId.toString())
            .append('year', year.toString())
            .append('month', month.toString());

        return this._executeRequest({
            url: '/findAllManagedTimesheetsByUserId',
            method: 'GET',
            params: params
        }).pipe(
            map(this.convertResponseToTimesheet)
        );
    }

    public findAllManagedTimesheetsForLoggedUser(
        dateFrom: Moment,
        dateTo: Moment,
        departmentId?: number
    ): Observable<TimesheetSummary[]> {
        let params: HttpParams = new HttpParams()
            .append('yearFrom', dateFrom.year().toString())
            .append('monthFrom', dateFrom.month().toString())
            .append('yearTo', dateTo.year().toString())
            .append('monthTo', dateTo.month().toString());

        if (departmentId) {
            params = params.append('departmentId', departmentId.toString());
        }

        return this._executeRequest({
            url: '/findAllManagedTimesheetsForLoggedUser',
            method: 'GET',
            params: params
        }).pipe(
            map(this.convertResponseToTimesheetSummary)
        );
    }

    public findAllTimesheetsForPsrByLoggedUser(
        dateFrom: Moment,
        dateTo: Moment,
        departmentId?: number
    ): Observable<TimesheetSummary[]> {
        let params: HttpParams = new HttpParams()
            .append('yearFrom', dateFrom.year().toString())
            .append('monthFrom', dateFrom.month().toString())
            .append('yearTo', dateTo.year().toString())
            .append('monthTo', dateTo.month().toString());

        if (departmentId) {
            params = params.append('departmentId', departmentId.toString());
        }

        return this._executeRequest({
            url: '/findAllTimesheetsForPsrByLoggedUser',
            method: 'GET',
            params: params
        }).pipe(
            map(this.convertResponseToTimesheetSummary)
        );
    }

    public findAllForPsr(timesheetCategoryId: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());

        return this._executeRequest({
            url: '/findAllForPsr',
            method: 'GET',
            params: params
        }).pipe(
            map(this.convertResponseToPsrTimesheet)
        );
    }

    public submit(timesheetId: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetId', timesheetId.toString());

        return this._executeRequest({
            url: '/submit',
            method: 'PUT',
            params: params
        });
    }

    public approve(timesheetId: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetId', timesheetId.toString());

        return this._executeRequest({
            url: '/approve',
            method: 'PUT',
            params: params
        });
    }

    public decline(timesheetId: number, comment: string): Observable<any> {
        return this._executeRequest({
            url: '/decline',
            method: 'PUT'
        }, {
            id: timesheetId,
            comment: comment
        });
    }

    // Deprecated
    public saveAndSubmit(data: any): Observable<any> {
        const requestOptions: HttpRequestOptions = Object.assign({}, this.isUpdateAction(data) ? this.updateAction : this.insertAction);
        requestOptions.params = new HttpParams()
            .append('submit', true.toString());

        return this._executeRequest(requestOptions, data);
    }

    bulkTimesheetInsert(data: any): Observable<any> {
        return this._executeRequest({
            method: 'POST',
            url: '/bulkTimesheetInsert'
        }, data);
    }

    bulkTimesheetAdjustment(data: BulkTimesheetAdjust): Observable<any> {
        return this._executeRequest({
            method: 'POST',
            url: '/bulkTimesheetAdjustment'
        }, data);
    }

    bulkTimesheetRemoval(userId: number, timesheetCategoryId: number): Observable<any> {
        return this._executeRequest({
            method: 'DELETE',
            url: '/bulkTimesheetRemoval',
            params: new HttpParams()
                    .append('userId', userId.toString())
                    .append('timesheetCategoryId', timesheetCategoryId.toString())
        });
    }

    createDoubleTimesheet(data: any): Observable<any> {
        return this._executeRequest({
            method: 'POST',
            url: '/createDoubleTimesheet'
        }, data);
    }

    private convertResponseToTimesheet(response: RestResponseModel): Timesheet[] {
        const result: Timesheet[] = [];
        response.data.forEach((raw: RawTimesheet) => {
            result.push(Timesheet.fromRawTimesheet(raw));
        });
        return result;
    }

    private convertResponseToTimesheetSummary(response: RestResponseModel): TimesheetSummary[] {
        const result: TimesheetSummary[] = [];
        response.data.forEach((raw: RawTimesheet) => {
            result.push(TimesheetSummary.fromRawTimesheet(raw));
        });
        return result;
    }

    private convertResponseToPsrTimesheet(response: RestResponseModel): RestResponseModel {
        response.data.forEach((row) => {
            const date: Date = new Date(row.date);
            row.date = date;
        });

        return response.data;
    }

}
