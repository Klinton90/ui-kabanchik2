import { UtilService } from '../../core/service/util.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlSegment } from '@angular/router';
import { AuthService } from '../../../service/auth.service';
import { Observable } from 'rxjs';
import { ROLE } from '../../admin/component/user/user.model';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(
        private authService: AuthService,
        private utilService: UtilService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        let allowed: boolean = true;
        for (let i = 0; i < route.url.length; i++) {
            const urlSegment: UrlSegment = route.url[i];
            if (['summary'].indexOf(urlSegment.path) >= 0) {
                allowed = this.authService.roles.indexOf(ROLE.EXECUTOR) >= 0;
            }
        }

        if (!allowed) {
            this.utilService.processRouteNotAllowed();
        }
        return allowed;
    }
}
