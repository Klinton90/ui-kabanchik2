import { ConverterService } from './../../core/service/converter.service';
import { LocalizedEnum } from '../../shared/model/localized-enum.model';
import { TimesheetSummary,
         Timesheet,
         TimesheetTotals,
         ITimesheetRow,
         TimesheetStatus,
         TimesheetWeeklyItem,
         TimesheetRowType,
         TimesheetCalculationSubResult,
         TimesheetRowWbsAware} from '../model/timesheet.model';
import { SelectItem } from 'primeng/api/selectitem';
import { Injectable } from '@angular/core';
import { TimesheetRow, TimesheetCategoryBreakdownRow } from '../model/timesheet.model';
import { cloneDeep, Dictionary, groupBy, keys } from 'lodash';
import { TimesheetCategory } from '../../admin/component/timesheet-category/timesheet-category.model';
import { UserSimpleModel, ROLE } from '../../admin/component/user/user.model';
import { UserService } from '../../core/service/user.service';
import { TimesheetSummaryDateColumn } from 'app/module/core/model/date-column.model';
import { StaticService } from 'app/module/core/service/static.service';
import { DepartmentModel } from 'app/module/admin/component/department/department.model';
import { MessageService } from 'app/module/core/service/message.service';

class GroupedTimesheetsSubResult {
    hasPreliminaryRows: boolean = false;
    weeks: Map<TimesheetSummaryDateColumn, TimesheetWeeklyItem> = new Map<TimesheetSummaryDateColumn, TimesheetWeeklyItem>();
}

@Injectable()
export class TimesheetService {

    constructor(
        private messageService: MessageService,
        private userService: UserService,
        private converterService: ConverterService
    ) {}

    calculateYears(year: number): SelectItem[] {
        const result: SelectItem[] = [];
        for (let i = year - 2; i <= year + 2; i++) {
            result.push({
                label: i.toString(),
                value: i
            });
        }
        return result;
    }

    convertTimesheetToTimesheetCategoryAndWbsIdBreakdownRows(
        timesheets: Timesheet[],
        weeksArray: TimesheetSummaryDateColumn[],
        timesheetCategories: TimesheetCategory[],
        showPreliminary: boolean,
    ): TimesheetCalculationSubResult {
        const result: TimesheetCalculationSubResult = new TimesheetCalculationSubResult();

        const totalRow: TimesheetRow = new TimesheetRow();
        totalRow.timesheetCategoryName = totalRow.timesheetCategoryName = this.messageService.resolveMessage('ui.text.summary.total');
        totalRow.isNew = false;
        totalRow.type = TimesheetRowType.TOTAL;
        totalRow.timesheetCategoryId = Number.MAX_SAFE_INTEGER;
        totalRow.totals = new TimesheetTotals();

        result.rows.push(totalRow);
        result.totalRow = totalRow;

        if (timesheets.length) {
            const timesheetGroups: Dictionary<Timesheet[]> = <Dictionary<Timesheet[]>>groupBy(
                timesheets,
                (timesheet: Timesheet) => timesheet.timesheetCategoryId);

            Object.entries(timesheetGroups).forEach(([key, timesheetGroup]) => {
                const timesheetCategoryId: number = parseInt(key);
                const timesheetCategory: TimesheetCategory = timesheetCategories.find(item => item.id === timesheetCategoryId);
                const wbsIdGroups: Dictionary<Timesheet[]> = <Dictionary<Timesheet[]>>groupBy(
                    timesheetGroup,
                    (timesheet: Timesheet) => timesheet.wbsId);
                Object.entries(wbsIdGroups).forEach(([wbsIdKey, wbsIdGroup]) => {
                    const groupedSubResult: GroupedTimesheetsSubResult = this.groupTimesheets(
                        wbsIdGroup,
                        weeksArray,
                        totalRow,
                        null,
                        showPreliminary);

                    const row: TimesheetRowWbsAware = {
                        timesheetCategoryDescription: timesheetCategory.description,
                        timesheetCategoryName: this.converterService.getLabelByPattern(timesheetCategory, ConverterService.DEFAULT_PATTERN),
                        timesheetCategoryId: timesheetCategoryId,
                        weeks: groupedSubResult.weeks,
                        hasPreliminaryRows: groupedSubResult.hasPreliminaryRows,
                        wbsId: wbsIdKey,
                        isNew: false,
                        type: TimesheetRowType.SIMPLE,
                        totals: this.getTotalsOfTimesheetGroup(groupedSubResult.weeks)
                    };

                    result.rows.push(row);
                });
            });

            totalRow.totals = this.getTotalsOfTimesheetGroup(totalRow.weeks);
        }
        return result;
    }

    convertTimesheetToTimesheetCategoryBreakdownRows(
        timesheets: Timesheet[],
        weeksArray: TimesheetSummaryDateColumn[],
        timesheetCategories: TimesheetCategory[],
        showPreliminary: boolean,
        isWorkload: boolean = false
    ): TimesheetCalculationSubResult {
        const result: TimesheetCalculationSubResult = new TimesheetCalculationSubResult();

        const totalRow: TimesheetRow = new TimesheetRow();
        totalRow.timesheetCategoryName = totalRow.timesheetCategoryName = this.messageService.resolveMessage(
            isWorkload
            ? 'ui.text.summary.plannedHours'
            : 'ui.text.summary.total');
        totalRow.isNew = false;
        totalRow.type = TimesheetRowType.TOTAL;
        totalRow.timesheetCategoryId = Number.MAX_SAFE_INTEGER;
        totalRow.totals = new TimesheetTotals();

        result.rows.push(totalRow);
        result.totalRow = totalRow;

        if (timesheets.length) {
            const timesheetGroups: Dictionary<Timesheet[]> = <Dictionary<Timesheet[]>>groupBy(
                timesheets,
                (timesheet: Timesheet) => timesheet.timesheetCategoryId);

            keys(timesheetGroups).forEach((key) => {
                const timesheetCategoryId: number = parseInt(key);
                const timesheetCategory: TimesheetCategory = timesheetCategories.find(item => item.id === timesheetCategoryId);
                const groupedSubResult: GroupedTimesheetsSubResult = this.groupTimesheets(
                    timesheetGroups[key],
                    weeksArray,
                    totalRow,
                    null,
                    showPreliminary);

                const row: TimesheetRow = {
                    timesheetCategoryDescription: timesheetCategory.description,
                    timesheetCategoryName: this.converterService.getLabelByPattern(timesheetCategory, ConverterService.DEFAULT_PATTERN),
                    timesheetCategoryId: timesheetCategoryId,
                    weeks: groupedSubResult.weeks,
                    hasPreliminaryRows: groupedSubResult.hasPreliminaryRows,
                    isNew: false,
                    type: TimesheetRowType.SIMPLE,
                    totals: this.getTotalsOfTimesheetGroup(groupedSubResult.weeks)
                };

                result.rows.push(row);
            });

            totalRow.totals = this.getTotalsOfTimesheetGroup(totalRow.weeks);
        }

        return result;
    }

    convertTimesheetToUserBreakdownRows(
        timesheets: TimesheetSummary[],
        weeksArray: TimesheetSummaryDateColumn[],
        users: UserSimpleModel[],
        departments: DepartmentModel[],
        showPreliminary: boolean,
        isWorkload: boolean
    ): TimesheetCalculationSubResult {
        const result: TimesheetCalculationSubResult = new TimesheetCalculationSubResult();

        const totalRow: TimesheetCategoryBreakdownRow = new TimesheetCategoryBreakdownRow();
        totalRow.type = TimesheetRowType.TOTAL;
        totalRow.userName = this.messageService.resolveMessage(
            isWorkload
            ? 'ui.text.summary.plannedHours'
            : 'ui.text.summary.total');
        totalRow.userId = Number.MAX_SAFE_INTEGER;
        totalRow.departmentId = Number.MAX_SAFE_INTEGER;
        totalRow.totals = new TimesheetTotals();

        result.rows.push(totalRow);
        result.totalRow = totalRow;

        if (timesheets.length) {
            const timesheetDepartmentGroups: Map<number, TimesheetSummary[]> = StaticService.groupBy(timesheets,
                (timesheet: TimesheetSummary): number => {
                    return timesheet.departmentId;
                });

            Array.from(timesheetDepartmentGroups.values()).forEach((_departmentTimesheets:  TimesheetSummary[]) => {
                const departmentId: number = _departmentTimesheets[0].departmentId;
                const department: DepartmentModel = departments
                    .find(_department => _department.id === departmentId);

                if (!isWorkload || department.forWorkload) {
                    const departmentSubTotalRow: TimesheetCategoryBreakdownRow = new TimesheetCategoryBreakdownRow();
                    departmentSubTotalRow.type = TimesheetRowType.GROUP_TOTAL;
                    departmentSubTotalRow.userName = department.name;
                    departmentSubTotalRow.userId = Number.MAX_SAFE_INTEGER;
                    departmentSubTotalRow.departmentId = department.id;
                    departmentSubTotalRow.totals = new TimesheetTotals();

                    result.rows.push(departmentSubTotalRow);

                    const timesheetUserGroups: Map<number, TimesheetSummary[]> = StaticService.groupBy(_departmentTimesheets,
                        (timesheet: TimesheetSummary): number => {
                            return timesheet.userId;
                        });

                    Array.from(timesheetUserGroups.values()).forEach((_userTimesheets:  TimesheetSummary[]) => {
                        const userId: number = _userTimesheets[0].userId;
                        const user: UserSimpleModel = users.find(_user => _user.id === userId);

                        const groupedSubResult: GroupedTimesheetsSubResult = this.groupTimesheets(
                            _userTimesheets,
                            weeksArray,
                            totalRow,
                            departmentSubTotalRow,
                            showPreliminary);

                        const row: TimesheetCategoryBreakdownRow = {
                            user: user,
                            userName: this.userService.getUserNameForView(user),
                            userId: userId,
                            weeks: groupedSubResult.weeks,
                            hasPreliminaryRows: groupedSubResult.hasPreliminaryRows,
                            type: TimesheetRowType.SIMPLE,
                            totals: this.getTotalsOfTimesheetGroup(groupedSubResult.weeks),
                            departmentId: departmentId
                        };

                        result.rows.push(row);
                    });

                    departmentSubTotalRow.totals = this.getTotalsOfTimesheetGroup(departmentSubTotalRow.weeks);
                }
            });

            totalRow.totals = this.getTotalsOfTimesheetGroup(totalRow.weeks);
        }

        return result;
    }

    getTimesheetCategoryBreakdownWorkloadRows(
        weeksArray: TimesheetSummaryDateColumn[],
        totalRow: ITimesheetRow,
        users: UserSimpleModel[],
        departmentId: number
    ): TimesheetRow[] {
        const results: TimesheetRow[] = [];

        const availableHoursRow: TimesheetRow = new TimesheetRow();
        availableHoursRow.type = TimesheetRowType.AVAILABLE_HOURS;
        availableHoursRow.timesheetCategoryName = this.messageService.resolveMessage('ui.text.summary.availableHours');
        availableHoursRow.timesheetCategoryDescription = this.messageService.resolveMessage('ui.text.summary.availableHours');
        availableHoursRow.timesheetCategoryId = Number.MAX_SAFE_INTEGER;
        this.createDataForAvailableHoursRow(availableHoursRow, weeksArray, this.getNumberOfUsersForAvailableHours(users, departmentId));
        results.push(availableHoursRow);

        const occupationRow: TimesheetRow = new TimesheetRow();
        occupationRow.type = TimesheetRowType.OCCUPATION_PERCENTAGE;
        occupationRow.timesheetCategoryName = this.messageService.resolveMessage('ui.text.summary.occupationPercent');
        occupationRow.timesheetCategoryDescription = this.messageService.resolveMessage('ui.text.summary.occupationPercent');
        occupationRow.timesheetCategoryId = Number.MAX_SAFE_INTEGER;
        occupationRow.totals = new TimesheetTotals();
        this.createDataForOccupationRow(occupationRow, totalRow, availableHoursRow);
        results.push(occupationRow);

        return results;
    }

    getUserBreakdownWorloadRows(
        weeksArray: TimesheetSummaryDateColumn[],
        totalRow: ITimesheetRow,
        users: UserSimpleModel[],
        departmentId: number
    ): TimesheetCategoryBreakdownRow[] {
        const results: TimesheetCategoryBreakdownRow[] = [];

        const availableHoursRow: TimesheetCategoryBreakdownRow = new TimesheetCategoryBreakdownRow();
        availableHoursRow.type = TimesheetRowType.AVAILABLE_HOURS;
        availableHoursRow.userName = this.messageService.resolveMessage('ui.text.summary.availableHours');
        availableHoursRow.userId = Number.MAX_SAFE_INTEGER;
        availableHoursRow.departmentId = Number.MAX_SAFE_INTEGER;
        this.createDataForAvailableHoursRow(availableHoursRow, weeksArray, this.getNumberOfUsersForAvailableHours(users, departmentId));
        results.push(availableHoursRow);

        const occupationRow: TimesheetCategoryBreakdownRow = new TimesheetCategoryBreakdownRow();
        occupationRow.type = TimesheetRowType.OCCUPATION_PERCENTAGE;
        occupationRow.userName = this.messageService.resolveMessage('ui.text.summary.occupationPercent');
        occupationRow.userId = Number.MAX_SAFE_INTEGER;
        occupationRow.departmentId = Number.MAX_SAFE_INTEGER;
        occupationRow.totals = new TimesheetTotals();
        this.createDataForOccupationRow(occupationRow, totalRow, availableHoursRow);
        results.push(occupationRow);

        return results;
    }

    getAvailableUsersForDepartment(users: UserSimpleModel[], departmentId: number): number {
        return users.filter((user: UserSimpleModel) =>
            user.isActive
            && user.departmentId === departmentId
            && StaticService.resolveEnumOnEntity(user.role, ROLE) !== ROLE.INTERNAL).length;
    }

    private groupTimesheets(
        timesheets: Timesheet[],
        weeks: TimesheetSummaryDateColumn[],
        totalRow: ITimesheetRow,
        subTotalRow: ITimesheetRow,
        showPreliminary: boolean
    ): GroupedTimesheetsSubResult {
        const result: GroupedTimesheetsSubResult = new GroupedTimesheetsSubResult();

        weeks.forEach((currentWeek: TimesheetSummaryDateColumn) => {
            let resultTimesheet: TimesheetWeeklyItem;

            timesheets.forEach((timesheet: Timesheet) => {
                if (timesheet.date.getTime() >= currentWeek.weekStart.getTime()
                    && timesheet.date.getTime() <= currentWeek.weekEnd.getTime()
                ) {
                    const isApprovedStatus: boolean = TimesheetStatus[(<LocalizedEnum>timesheet.status).value] === TimesheetStatus.APPROVED;
                    if (!isApprovedStatus) {
                        result.hasPreliminaryRows = true;
                    }
                    if (showPreliminary || isApprovedStatus) {
                        if (!resultTimesheet) {
                            resultTimesheet = TimesheetWeeklyItem.cloneTimesheet(timesheet);
                        } else {
                            // This happens only on Summary page
                            resultTimesheet.amount += timesheet.amount;
                            resultTimesheet.amount2 += timesheet.amount * timesheet.rate;
                        }
                    }
                }
            });

            if (!resultTimesheet) {
                // Create placeholder record - that is required for Report page only.
                resultTimesheet = TimesheetWeeklyItem.forModal(
                    currentWeek.weekStart,
                    timesheets[0].userId,
                    timesheets[0].timesheetCategoryId);
            }

            this.processTotalRow(resultTimesheet, totalRow, currentWeek);
            if (subTotalRow) {
                this.processTotalRow(resultTimesheet, subTotalRow, currentWeek);
            }

            result.weeks.set(currentWeek, resultTimesheet);
        });

        return result;
    }

    private processTotalRow(
        timesheetItem: TimesheetWeeklyItem,
        totalRow: ITimesheetRow,
        dateColumn: TimesheetSummaryDateColumn
    ): void {
        if (TimesheetStatus[(<LocalizedEnum>timesheetItem.status).value] !== TimesheetStatus.DECLINED) {
            let totalTimesheet: TimesheetWeeklyItem = totalRow.weeks.get(dateColumn);
            if (!totalTimesheet) {
                totalTimesheet = cloneDeep(timesheetItem);
                totalRow.weeks.set(dateColumn, totalTimesheet);
            } else {
                totalTimesheet.amount += timesheetItem.amount;
                totalTimesheet.amount2 += timesheetItem.amount2;
            }
        }
    }

    private getTotalsOfTimesheetGroup(weeks: Map<TimesheetSummaryDateColumn, TimesheetWeeklyItem>): TimesheetTotals {
        let totalHours: number = 0;
        let totalMoney: number = 0;

        for (const week of Array.from(weeks.values())) {
            if (TimesheetStatus[(<LocalizedEnum>week.status).value] !== TimesheetStatus.DECLINED) {
                totalHours += week.amount;
                totalMoney += week.amount2;
            }
        }

        return {
            totalHours: totalHours,
            totalMoney: totalMoney
        };
    }

    private createDataForAvailableHoursRow(
        availableHoursRow: ITimesheetRow,
        weeks: TimesheetSummaryDateColumn[],
        usersAmount: number
    ): void {
        weeks.forEach((currentWeek: TimesheetSummaryDateColumn) => {
            const item: TimesheetWeeklyItem = TimesheetWeeklyItem.forSummary(currentWeek.weekStart);
            item.amount = item.amount2 = usersAmount * currentWeek.maxHours;
            availableHoursRow.weeks.set(currentWeek, item);
        });

        availableHoursRow.totals = this.getTotalsOfTimesheetGroup(availableHoursRow.weeks);
    }

    private createDataForOccupationRow(
        occupationRow: ITimesheetRow,
        totalRow: ITimesheetRow,
        availableHoursRow: ITimesheetRow
    ): void {
        availableHoursRow.weeks.forEach((availableRowItem: TimesheetWeeklyItem, currentWeek: TimesheetSummaryDateColumn) => {
            const totalRowItem: TimesheetWeeklyItem = totalRow.weeks.get(currentWeek);
            const item: TimesheetWeeklyItem = TimesheetWeeklyItem.forSummary(currentWeek.weekStart);
            item.amount = item.amount2 = totalRowItem && totalRowItem.amount && availableRowItem && availableRowItem.amount
                ? Math.round(totalRowItem.amount / availableRowItem.amount * 100 * 100) / 100
                : 0;
            occupationRow.weeks.set(currentWeek, item);
        });
    }

    private getNumberOfUsersForAvailableHours(users: UserSimpleModel[], departmentId: number): number {
        return departmentId ? this.getAvailableUsersForDepartment(users, departmentId) : users.length;
    }

}
