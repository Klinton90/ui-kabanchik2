import { AppSharedModule } from '../shared/shared.module';
import { NgModule } from '@angular/core';
import { TimesheetReportModalComponent } from './component/report/timesheet-report-modal.component';
import { TimesheetSummaryListingComponent } from './component/summary/timesheet-summary-listing.component';
import { TimesheetService } from './service/timesheet.service';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
    imports: [
        // material
        MatTabsModule,

        // custom
        AppSharedModule,
    ],
    declarations: [
        TimesheetReportModalComponent,
        TimesheetSummaryListingComponent
    ],
    exports: [
        TimesheetReportModalComponent,
        TimesheetSummaryListingComponent
    ],
    providers: [
        TimesheetService
    ],
    entryComponents: [
        TimesheetReportModalComponent
    ]
})
export class TimesheetSharedModule {}
