import { LocalizedEnum } from '../../shared/model/localized-enum.model';
import { DateColumn } from 'app/module/core/model/date-column.model';
import { UserSimpleModel } from '../../admin/component/user/user.model';
import { cloneDeep } from 'lodash';

export enum TimesheetStatus {
    EDITABLE, POSTED, APPROVED, DECLINED, PLANNED
}

export class RawTimesheet {
    id: number = 0;
    amount: number = 0;
    date: number = 0;
    status: TimesheetStatus | LocalizedEnum = TimesheetStatus.EDITABLE;
    comment: string = '';
    wbsId: string = '';
    timesheetCategoryId: number = 0;
    userId: number = 0;
}

export class BaseTimesheet {
    id: number = 0;
    amount: number = 0;
    date: Date;
    status: TimesheetStatus | LocalizedEnum = TimesheetStatus.EDITABLE;
    comment: string = '';
    wbsId: string = '';
    timesheetCategoryId: number = 0;
    userId: number;
    rate: number = 0;
    internalRate: number = 0;
}

export class PsrTimesheet extends BaseTimesheet {
    departmentId: number;
}

export class Timesheet extends BaseTimesheet {

    public static forModal(date: Date, userId: number): Timesheet {
        const result: Timesheet = new Timesheet();
        result.userId = userId;
        result.date = date;
        return result;
    }

    public static fromRawTimesheet(raw: RawTimesheet): Timesheet {
        const result: Timesheet = new Timesheet();
        Object.assign(result, raw);
        result.date = new Date(raw.date);
        return result;
    }

}

export class TimesheetSummary extends Timesheet {
    departmentId: number;

    public static fromRawTimesheet(raw: RawTimesheet): TimesheetSummary {
        const result: TimesheetSummary = new TimesheetSummary();
        Object.assign(result, raw);
        result.date = new Date(raw.date);
        return result;
    }

}

export class TimesheetWeeklyItem extends BaseTimesheet {
    amount2: number = 0;

    public static forModal(date: Date, userId: number, timesheetCategoryId: number): TimesheetWeeklyItem {
        const result: TimesheetWeeklyItem = new TimesheetWeeklyItem();
        result.userId = userId;
        result.date = date;
        result.timesheetCategoryId = timesheetCategoryId;
        return result;
    }

    public static forSummary(date: Date): TimesheetWeeklyItem {
        const result: TimesheetWeeklyItem = new TimesheetWeeklyItem();
        result.date = date;
        return result;
    }

    public static cloneTimesheet(timesheet: Timesheet): TimesheetWeeklyItem {
        const result: TimesheetWeeklyItem = <any>cloneDeep(timesheet);
        result.amount2 = result.amount * result.rate;
        return result;
    }

}

export class TimesheetTotals {
    totalHours: number = 0;
    totalMoney: number = 0;
}

export enum TimesheetRowType {
    AVAILABLE_HOURS,
    OCCUPATION_PERCENTAGE,
    TOTAL,
    GROUP_TOTAL,
    SIMPLE
}

export class ITimesheetRow {
    weeks: Map<DateColumn, TimesheetWeeklyItem> = new Map();
    totals: TimesheetTotals;
    type: TimesheetRowType;
    hasPreliminaryRows: boolean;
}

export class TimesheetRow extends ITimesheetRow {
    timesheetCategoryDescription: string = '';
    timesheetCategoryName: string = '';
    timesheetCategoryId: number = 0;
    isNew: boolean = true;
}

export class TimesheetRowWbsAware extends TimesheetRow {
    wbsId: string = '';
}

export class TimesheetCategoryBreakdownRow extends ITimesheetRow {
    user: UserSimpleModel;
    userId: number = 0;
    userName: string = '';
    departmentId: number = 0;
}

export class TimesheetCalculationSubResult {
    rows: ITimesheetRow[] = [];
    totalRow: ITimesheetRow;
}

export function isTimesheetOrBreakdownRow(object: any): object is TimesheetRow {
    return 'timesheetCategoryId' in object;
}
