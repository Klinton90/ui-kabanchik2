import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppSharedModule } from '../shared/shared.module';
import { TimesheetReportListingComponent } from './component/report/timesheet-report-listing.component';
import { AuthGuardService } from './service/auth-guard.service';
import { TimesheetSummaryListingComponent } from './component/summary/timesheet-summary-listing.component';
import { TimesheetSharedModule } from './timesheet-shared.module';

const routes: Routes = [
    {
        path: 'report',
        component: TimesheetReportListingComponent
    },
    {
        path: 'report/:userId',
        component: TimesheetReportListingComponent
    },
    {
        path: 'summary',
        component: TimesheetSummaryListingComponent,
        canActivate: [AuthGuardService],
        data: {
            isWorkload: false
        }
    }
];

@NgModule({
    imports: [
        // angular
        RouterModule.forChild(routes),

        // custom
        AppSharedModule,
        TimesheetSharedModule
    ],
    declarations: [
        TimesheetReportListingComponent
    ],
    providers: [
        AuthGuardService
    ]
})
export class TimesheetModule {}
