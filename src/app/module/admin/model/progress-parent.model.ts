import { IDomainModel, ActivatableDomain } from '../../core/model/i-domain.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';

export interface ProgressParent extends IDomainModel<number>, ActivatableDomain {
    name: string;
    description: string;
    progressType: LocalizedEnum;
}
