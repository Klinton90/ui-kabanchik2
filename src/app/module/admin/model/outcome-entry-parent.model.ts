import { IDomainModel, ActivatableDomain } from "../../core/model/i-domain.model";
import { LocalizedEnum } from "app/module/shared/model/localized-enum.model";

export interface OutcomeEntryParent extends IDomainModel<number>, ActivatableDomain {
    name: string;
    governmentId: string;
    description: string;
    outcomeEntryType: LocalizedEnum;
}