import { IDomainModel } from "app/module/core/model/i-domain.model";

export class GenericFieldValue implements IDomainModel<number> {
    id: number;
    value: string;
    parentId: number;
    genericFieldId: number;
}

export interface IGenericFieldAwareDomain<ID> extends IDomainModel<any> {
    genericFieldValues: GenericFieldValue[];
}