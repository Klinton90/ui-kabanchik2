import { Validators, FormControl, FormGroup } from '@angular/forms';
import { UserResource } from './user.resource';
import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { AlertService } from '../../../core/component/alert/alert.service';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { UtilService } from '../../../core/service/util.service';
import { UserSimpleModel } from './user.model';
import { Subscription } from 'rxjs';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { HttpErrorResponse } from '@angular/common/http';
import { AlertType, AlertCustomConfig } from 'app/module/core/component/alert/alert-custom-config';
import * as moment from 'moment';
import { Moment } from 'moment';
import { FormAwareModal } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

class ReminderModalParams {
    isManagerReminder: boolean;
    userEntity: UserSimpleModel;
}

class ReminderModalModel {
    date: Moment = moment(new Date());
}

@Component({
    moduleId: module.id,
    templateUrl: 'reminder-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReminderModalComponent extends FormAwareModal<ReminderModalComponent, any> {

    newFormEntityInstance: ReminderModalModel = new ReminderModalModel();

    form: FormGroup = new FormGroup({
        date: new FormControl('', [Validators.required])
    });

    constructor(
        public dialogRef: MatDialogRef<ReminderModalComponent>,
        protected resource: UserResource,
        protected utilService: UtilService,
        protected notificationService: AlertService,
        protected confirmationService: ConfirmationService,
        protected ref: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) private data: ReminderModalParams
    ) {
        super();
    }

    saveAction(): Subscription {
        const date: Moment = this.form.getRawValue().date;
        if (this.data.isManagerReminder) {
            return this.resource.sendManagerReminder(this.data.userEntity.id, date.toDate()).subscribe(
                (response: RestResponseModel) => {
                    const message: AlertCustomConfig = {
                        body: response.data ? 'ui.message.user.sendManagerReminder.success' : 'ui.message.user.sendManagerReminder.reject',
                        type: response.data ? AlertType.SUCCESS : AlertType.INFO
                    };

                    this.successHandler(response, message);
                },
                (response: HttpErrorResponse) => {
                    this.errorHandler(response);
                }
            );
        } else {
            return this.resource.sendUserReminder(this.data.userEntity.id, date.toDate()).subscribe(
                (response: RestResponseModel) => {
                    const message: AlertCustomConfig = {
                        body: response.data ? 'ui.message.user.sendUserReminder.success' : 'ui.message.user.sendUserReminder.reject',
                        type: response.data ? AlertType.SUCCESS : AlertType.INFO
                    };

                    this.successHandler(response, message);
                },
                (response: HttpErrorResponse) => {
                    this.errorHandler(response);
                }
            );
        }
    }

}
