import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { RegisterUserModel } from './user.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RestResource } from '../../../core/resource/rest.resource';
import { UserProfie } from './user-profile.model';
import { Pageable } from 'app/module/core/model/pageable.model';
import { BaseResource } from 'app/module/core/resource/base.resource';
import { Moment } from 'moment';

@Injectable({
    providedIn: 'root'
})
export class UserResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('user');
    }

    public register(user: RegisterUserModel): Observable<any> {
        return this._executeRequest({
            url: '/register',
            method: 'POST'
        }, user);
    }

    public forgetPassword(email: string): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('email', email);

        return this._executeRequest({
            url: '/forgetPassword',
            method: 'GET',
            params: params
        });
    }

    public activateAccount(email: string, code: string): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('email', email)
            .append('securityCode', code);

        return this._executeRequest({
            url: '/activateBySecurityCode',
            method: 'GET',
            params: params
        });
    }

    // TODO: encrypt password
    public updatePassword(code: string, password: string): Observable<any> {
        return this._executeRequest(
            {
                url: '/updatePassword',
                method: 'PUT'
            },
            {
                password: password,
                securityCode: code
            }
        );
    }

    // TODO: encrypt password
    public updateOwnPassword(password: string): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('password', password);

        return this._executeRequest({
            url: '/updateOwnPassword',
            method: 'PUT',
            params: params
        });
    }

    public resetActivation(id: number): Observable<any> {
        return this._executeRequest(
            {
                url: '/resetActivation/:id',
                method: 'PUT'
            },
            {id: id}
        );
    }

    public getReportingUsers(queryString?: string, limit?: number, offset?: number): Observable<any> {
        return this._executeRequest({
            url: '/getReportingUsers',
            method: 'GET',
            params: BaseResource.setOptionalPageableParams(null, queryString, limit, offset)
        });
    }

    public getReportingUsersByPeriod(
        dateFrom: Moment,
        dateTo: Moment,
    ): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('yearFrom', dateFrom.year().toString())
            .append('monthFrom', dateFrom.month().toString())
            .append('yearTo', dateTo.year().toString())
            .append('monthTo', dateTo.month().toString());

        return this._executeRequest({
            url: '/getReportingUsersByPeriod',
            method: 'GET',
            params: params
        });
    }

    public getUsersForWorkload(): Observable<any> {
        return this._executeRequest({
            url: '/getUsersForWorkload',
            method: 'GET'
        });
    }

    public getManagers(queryString?: string, limit?: number, offset?: number): Observable<any> {
        return this._executeRequest({
            url: '/getManagers',
            method: 'GET',
            params: BaseResource.setOptionalPageableParams(null, queryString, limit, offset)
        });
    }

    public sendUserReminder(userId: number, date: Date): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('userId', userId.toString())
            .append('timestamp', date.getTime().toString());

        return this._executeRequest({
            url: '/sendUserReminder',
            method: 'PUT',
            params: params
        });
    }

    public sendManagerReminder(managerId: number, date: Date): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('managerId', managerId.toString())
            .append('timestamp', date.getTime().toString());

        return this._executeRequest({
            url: '/sendManagerReminder',
            method: 'PUT',
            params: params
        });
    }

    public getForPSR(queryString?: string, limit?: number, offset?: number): Observable<any> {
        return this._executeRequest({
            url: '/getForPSR',
            method: 'GET',
            params: BaseResource.setOptionalPageableParams(null, queryString, limit, offset)
        });
    }

    public getUsersByTimesheetCategoryId(timesheetCategoryId: number, withIntenal: boolean): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString())
            .append('withInternal', withIntenal.toString());

        return this._executeRequest({
            url: '/getUsersByTimesheetCategoryId',
            method: 'GET',
            params: params
        });
    }

    public getUsersForTimesheetAccessBulkUserAdd(queryString?: string, limit?: number, offset?: number): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/getUsersForTimesheetAccessBulkUserAdd',
            params: BaseResource.setOptionalPageableParams(null, queryString, limit, offset)
        });
    }

    public updateProfile(userProfile: UserProfie): Observable<any> {
        return this._executeRequest(
            {
                url: '/updateProfile',
                method: 'PUT'
            },
            userProfile
        );
    }

    listAllSimple(showActive?: boolean, pageable?: Pageable): Observable<any> {
        let params: HttpParams = new HttpParams();

        if (showActive) {
            params = params.set('showActive', showActive.toString());
        }
        if (pageable) {
            params = this.setPageable(params, pageable);
        }

        return this._executeRequest({
            url: 'listAllSimple',
            method: 'GET',
            params: params
        });
    }

}
