import { Domain } from '../../../core/model/i-domain.model';
import {LocalizedEnum} from '../../../shared/model/localized-enum.model';
import { LocalizationDomainModel } from '../localization/localization-domain.model';
import { GenericFieldValue, IGenericFieldAwareDomain } from '../../model/generic-field-value.model';

export interface RegisterUserModel {
    email?: string;
    role?: ROLE | string;
}

export class UserSimpleModel implements Domain<number> {
    id: number = 0;
    email: string = '';
    isActive: boolean = true;
    firstName: string = '';
    lastName: string = '';
    role: ROLE | LocalizedEnum = null;

    password: string = '';
    localizationId: string = '';
    baseRate: number = 0;
    internalBaseRate: number = 0;
    departmentId: number = null;
    receiveReminder: boolean = true;
}

export interface UserPrincipalModel extends UserSimpleModel {
    localization: LocalizationDomainModel;
}

export class UserModel extends UserSimpleModel implements IGenericFieldAwareDomain<number> {
    genericFieldValues: GenericFieldValue[] = [];
}

export enum ROLE {
    USER,
    EXECUTOR,
    ADMIN,
    INTERNAL,
    GUEST
}
