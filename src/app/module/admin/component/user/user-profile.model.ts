import {LocalizationDomainModel} from "../localization/localization-domain.model";

export class UserProfie{
    firstName: string;
    lastName: string;
    localization: LocalizationDomainModel;
    receiveReminder: boolean;
}