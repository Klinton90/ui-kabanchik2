import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, OnDestroy } from '@angular/core';
import { UserResource } from './user.resource';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { UserModel, ROLE } from './user.model';
import { GenericFieldEntity } from '../generic-field/generic-field.model';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { DepartmentResource } from '../department/department.resource';
import { LocalizationResource } from '../localization/localization.resource';
import { SortOrder } from 'app/module/core/model/sort-order.model';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { Observable, Subscription } from 'rxjs';
import { shareReplay, tap } from 'rxjs/operators';
import { ConverterService } from 'app/module/core/service/converter.service';
import { Validators } from '@angular/forms';
import { passwordSizeValidator } from 'app/module/core/validator/password.validator';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';
import { ICellRendererParams } from 'ag-grid-community';
import { ReminderModalComponent } from './reminder-modal.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
    moduleId: module.id,
    templateUrl: 'user-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListingComponent implements OnInit, OnDestroy {

    private $departments: Observable<any>;
    private $localizations: Observable<any>;

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: UserResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private serviceResource: ServiceResource,
        private departmentResource: DepartmentResource,
        private localizationResource: LocalizationResource,
        private converterService: ConverterService,
        private alertService: AlertService,
        private dialog: MatDialog,
    ) {
        this.$localizations = this.localizationResource
            .list(false, {sort: [{field: 'name', order: SortOrder.ASC}]})
            .pipe(
                shareReplay(1),
                tap((response: RestResponseModel) => {
                    this.converterService.convertObjectListToSelectItems(response.data, false, ConverterService.DEFAULT_PATTERN, 'id');
                })
            );
        this.$departments = this.departmentResource
            .list(false, {sort: [{field: 'name', order: SortOrder.ASC}]})
            .pipe(
                shareReplay(1),
                tap((response: RestResponseModel) => {
                    this.converterService.convertObjectListToSelectItems(response.data, false, ConverterService.DEFAULT_PATTERN, 'id');
                })
            );
    }

    ngOnInit() {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'userListingGrid',
                resource: this.resource,
                newItemConstructor: () => new UserModel(),
                genericFieldEntity: GenericFieldEntity.USER,
                actionsColumnContext: {
                    minWidth: 110,
                    actionColumnButtons: [
                        {
                            title: 'ui.text.user.activate',
                            classList: 'btn btn-xs btn-primary fas fa-plug',
                            click: this.resetActivationAction.bind(this)
                        },
                        {
                            title: 'ui.text.user.sendUserReminder',
                            classList: 'btn btn-xs btn-primary fas fa-bell',
                            click: this.showReminderModal.bind(this, false),
                            hidden: (context: ICellRendererParams) =>
                                context.data && context.data.role && (<LocalizedEnum>context.data.role).value === ROLE[ROLE.ADMIN],
                            disabled: (context: ICellRendererParams) => context.data && !context.data.receiveReminder
                        },
                        {
                            title: 'ui.text.user.sendManagerReminder',
                            classList: 'btn btn-xs btn-primary fas fa-bolt',
                            click: this.showReminderModal.bind(this, true),
                            hidden: (context: ICellRendererParams) =>
                                context.data && context.data.role && (<LocalizedEnum>context.data.role).value !== ROLE[ROLE.EXECUTOR],
                            disabled: (context: ICellRendererParams) => context.data && !context.data.receiveReminder
                        }
                    ]
                }
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.default.email',
                    field: 'email',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.minLength(8), Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.user.password',
                    field: 'password',
                    valueFormatter: () => '********',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        type: 'password',
                        validators: (data: UserModel) => {
                            if (data && data.id) {
                                return Validators.compose([passwordSizeValidator(8, 255)]);
                            } else {
                                return Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(255)]);
                            }
                        }
                    }
                },
                {
                    headerName: 'ui.text.user.profile.firstName',
                    field: 'firstName',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.user.profile.lastName',
                    field: 'lastName',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.user.role',
                    field: 'role',
                    autoGenerateContext: {
                        type: ColumnObservableType.LOCALIZED_ENUM,
                        observable:  this.serviceResource.getLocalizedEnumOptions('kabanchik.system.user.domain.UserRole')
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.user.profile.receiveReminder',
                    field: 'receiveReminder',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN,
                        filter: false,
                        floatingFilter: false
                    }
                },
                {
                    headerName: 'ui.text.user.profile.baseRate',
                    field: 'baseRate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(999999.99)]
                    },
                    valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                },
                {
                    headerName: 'ui.text.user.profile.internalBaseRate',
                    field: 'internalBaseRate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(999999.99)]
                    },
                    valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                },
                {
                    headerName: 'ui.text.localization.title',
                    field: 'localizationId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN,
                        observable: this.$localizations,
                        bindLabel: 'name'
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.department.title',
                    field: 'departmentId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN,
                        observable: this.$departments,
                        bindLabel: 'name'
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                }
            ]
        };

        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    resetActivationAction(context: ICellRendererParams): void {
        this.resource.resetActivation(context.data.id).subscribe(() => {
            this.alertService.post({
                body: 'ui.message.user.resetActivation.body',
                type: AlertType.SUCCESS
            });
        });
    }

    showReminderModal(isManagerReminder: boolean, context: ICellRendererParams): void {
        this.dialog.open(ReminderModalComponent, {data: {
            isManagerReminder: isManagerReminder,
            userEntity: context.data
        }});
    }

}
