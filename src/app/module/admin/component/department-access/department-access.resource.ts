import { Injectable } from '@angular/core';
import { RestResource } from 'app/module/core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BulkDepartmentAccessByDepartment, BulkDepartmentAccessByTimesheetCategory } from './department-access.model';

@Injectable({
    providedIn: 'root'
})
export class DepartmentAccessResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('departmentAccess');
    }

    public bulkDepartmentInsert(data: BulkDepartmentAccessByTimesheetCategory): Observable<any> {
        return this._executeRequest({
            url: '/bulkDepartmentInsert',
            method: 'POST'
        }, data);
    }

    public bulkCategoriesInsert(data: BulkDepartmentAccessByDepartment): Observable<any> {
        return this._executeRequest({
            url: '/bulkCategoriesInsert',
            method: 'POST'
        }, data);
    }

    public findByTimesheetCategoryId(timesheetCategoryId: number): Observable<any> {
        return this._executeRequest({
            url: '/findByTimesheetCategoryId',
            method: 'GET',
            params: new HttpParams().append('timesheetCategoryId', timesheetCategoryId.toString())
        });
    }

}
