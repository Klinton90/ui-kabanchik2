import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { DepartmentAccessResource } from './department-access.resource';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { DepartmentAccess } from './department-access.model';
import { MatDialog } from '@angular/material/dialog';
import {
    DepartmentAccessBulkCategoriesAddModalComponent
} from './department-access-bulk-categories-add-modal.component';
import { ComponentType } from '@angular/cdk/portal';
import { ModalResult } from 'app/module/shared/component/modal/modal-result.model';
import { DepartmentAccessBulkDepartmentsddModalComponent } from './department-access-bulk-departments-add-modal.component';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { DepartmentResource } from '../department/department.resource';
import { Subscription } from 'rxjs';
import { Validators } from '@angular/forms';

@Component({
    moduleId: module.id,
    templateUrl: 'department-access-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DepartmentAccessListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: DepartmentAccessResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private dialog: MatDialog,
        private timesheetCategoryResource: TimesheetCategoryResource,
        private departmentResource: DepartmentResource
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'departmentAccessListingGrid',
                resource: this.resource,
                newItemConstructor: () => new DepartmentAccess(),
                buttons: [
                    {
                        title: 'ui.text.departmentAccess.bulkDepartmentsAdd',
                        classList: 'btn btn-sm btn-primary fa fa-users',
                        click: this.openBulkModal.bind(this, DepartmentAccessBulkDepartmentsddModalComponent)
                    },
                    {
                        title: 'ui.text.departmentAccess.bulkCategoriesAdd',
                        classList: 'btn btn-sm btn-primary fa fa-user-plus',
                        click: this.openBulkModal.bind(this, DepartmentAccessBulkCategoriesAddModalComponent)
                    },
                ]
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.timesheetCategory.title',
                    field: 'timesheetCategoryId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'timesheetCategoryName',
                        infiniteProps: {
                            name: 'timesheetCategory',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['name'],
                            editorOptionsResource: this.timesheetCategoryResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.department.title',
                    field: 'departmentId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'departmentName',
                        infiniteProps: {
                            name: 'department',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['name'],
                            editorOptionsResource: this.departmentResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    private openBulkModal(modal: ComponentType<any>): void {
        this.dialog.open(modal)
            .afterClosed()
            .subscribe({
                next: (result: ModalResult) => {
                    if (result && result.isRefreshRequired) {
                        this.gridOptions.api.refreshInfiniteCache();
                    }
                }
            });
    }

}
