import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormAwareModal } from 'app/module/shared/component/modal/form-aware-modal.component';
import { DepartmentAccess } from './department-access.model';
import { MatDialogRef } from '@angular/material/dialog';
import { DepartmentAccessResource } from './department-access.resource';
import { UtilService } from 'app/module/core/service/util.service';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DepartmentResource } from '../department/department.resource';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { ConverterService } from 'app/module/core/service/converter.service';
import { AlertType, AlertCustomConfig } from 'app/module/core/component/alert/alert-custom-config';
import { Observable } from 'rxjs';
import { StaticService } from 'app/module/core/service/static.service';

@Component({
    moduleId: module.id,
    templateUrl: 'department-access-bulk-categories-add-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DepartmentAccessBulkCategoriesAddModalComponent
extends FormAwareModal<DepartmentAccessBulkCategoriesAddModalComponent, DepartmentAccess> {

    protected newFormEntityInstance: DepartmentAccess = new DepartmentAccess();
    form: FormGroup = new FormGroup({
        departmentId: new FormControl('', [Validators.required]),
        timesheetCategoryIds: new FormControl('', [Validators.required]),
    });

    constructor(
        public dialogRef: MatDialogRef<DepartmentAccessBulkCategoriesAddModalComponent>,
        protected resource: DepartmentAccessResource,
        protected utilService: UtilService,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        public ref: ChangeDetectorRef,
        private departmentResource: DepartmentResource,
        private timesheetCategoryResource: TimesheetCategoryResource,
        public converterService: ConverterService,
    ) {
        super();
    }

    getDepartmentLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.departmentResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    getTimesheetCategoryLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.timesheetCategoryResource.getManagedTimesheetCategories(false, term, limit, offset);
    }

    // @Override
    protected successHandler(response: RestResponseModel): void {
        let message: AlertCustomConfig;
        if (response.data.length) {
            message = {
                body: 'ui.message.departmentAccess.bulkTimesheetCategoriesAdd.duplicates',
                type: AlertType.WARNING,
                params: [response.data.join(', ')]
            };
        }
        super.successHandler(response, message);
    }

    // @Override
    protected getSaveAction(): string {
        return 'bulkCategoriesInsert';
    }

}
