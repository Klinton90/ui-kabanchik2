import { Domain } from '../../../core/model/i-domain.model';

export class DepartmentAccess implements Domain<number> {

    id: number = 0;
    isActive: boolean = true;
    timesheetCategoryId: number = null;
    departmentId: number = null;

    timesheetCategoryName: string = null;
    departmentName: string = null;

}

export class BulkDepartmentAccessByTimesheetCategory {
    timesheetCategoryId: number = null;
    departmentIds: number[] = [];
}

export class BulkDepartmentAccessByDepartment {
    deprtmentId: number = null;
    timesheetCategoryIds: number[] = [];
}
