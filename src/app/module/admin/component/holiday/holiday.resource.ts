import { RestResource } from "../../../core/resource/rest.resource";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class HolidayResource extends RestResource {

    constructor(
        protected http: HttpClient
    ){
        super("holiday");
    }

    getWeeklyMaxList(weeks: Date[]): Observable<any> {
        const _weeks: number[] = weeks.map((week: Date) => week.getTime());
        return this._executeRequest({
            method: 'POST',
            url: '/getWeeklyMaxList'
        }, _weeks);
    }

}