import { Component, ChangeDetectorRef, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { HolidayResource } from './holiday.resource';
import { HolidayModel } from './holiday.model';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { Validators } from '@angular/forms';

@Component({
    moduleId: module.id,
    templateUrl: 'holiday-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HolidayListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: HolidayResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'holidayListingGrid',
                resource: this.resource,
                newItemConstructor: () => new HolidayModel(),
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.holiday.name',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(50)]
                    }
                },
                {
                    headerName: 'ui.text.holiday.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.holiday.date',
                    field: 'date',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
