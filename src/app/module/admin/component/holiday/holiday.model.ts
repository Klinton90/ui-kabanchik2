import { Domain } from '../../../core/model/i-domain.model';

export class HolidayModel implements Domain<number> {
    id: number = null;
    name: string = '';
    description: string = '';
    date: Date | number = '' as any;
    isActive: boolean = true;
}
