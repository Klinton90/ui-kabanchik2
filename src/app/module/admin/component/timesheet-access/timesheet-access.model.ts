import { Domain } from '../../../core/model/i-domain.model';
import { DateAwareModel } from 'app/module/core/validator/period.validator';

export class TimesheetAccess implements Domain<number> {
    id: number = 0;
    rate: number = 0;
    internalRate: number = 0;
    isActive: boolean = true;
    userName: string = '';
    timesheetCategoryName: string = '';
    userId: number = null;
    timesheetCategoryId: number = null;
}

export class BulkTimesheetAccess implements DateAwareModel {
    timesheetCategoryId: number = null;
    userIds: number[] = [];
    from: number = null;
    to: number = null;
}

export class BulkUserRate {
    userId: number = null;
    timesheetCategoryIds: number[] = [];
}
