import { Component, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { TimesheetAccessResource } from './timesheet-access.resource';
import { UserSimpleModel, ROLE } from '../user/user.model';
import { UserService } from '../../../core/service/user.service';
import { UserResource } from '../user/user.resource';
import { AuthService } from 'app/service/auth.service';
import {
    CustomAgGridOptionsModel,
    ColumnObservableType,
    CustomGridReadyEvent,
    CustomApi
} from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { TimesheetAccess } from './timesheet-access.model';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { Validators } from '@angular/forms';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { TimesheetAccessBulkCategoriesAddModalComponent } from './timesheet-access-bulk-categories-add-modal.component';
import { ComponentType } from '@angular/cdk/portal';
import { MatDialog } from '@angular/material/dialog';
import { ModalResult } from 'app/module/shared/component/modal/modal-result.model';
import { TimesheetAccessBulkUsersAddModalComponent } from './timesheet-access-bulk-users-add-modal.component';
import { ICellRendererParams } from 'ag-grid-community';
import { TimesheetRateListingComponent } from '../timesheet-rate/timesheet-rate-listing.component';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-access-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetAccessListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    private subscriptions: Subscription[] = [];

    private isAdmin: boolean = this.authService.roles.indexOf(ROLE.ADMIN) >= 0;

    private customApi: CustomApi;

    constructor(
        public resource: TimesheetAccessResource,
        protected ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private dialog: MatDialog,
        private timesheetCategoryResource: TimesheetCategoryResource,
        private userResource: UserResource,
        private userService: UserService,
        private authService: AuthService,
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'timesheetAccessListingGrid',
                resource: this.resource,
                newItemConstructor: () => new TimesheetAccess(),
                actionsColumnContext: {
                    actionColumnButtons: [
                        {
                            title: 'ui.text.timesheetRate.title',
                            classList: 'btn btn-xs btn-primary fa fa-money-check-alt',
                            click: this.showRates,
                        },
                    ]
                },
                buttons: [
                    {
                        title: 'ui.text.timesheetAccess.bulkUsersAdd',
                        classList: 'btn btn-sm btn-primary fa fa-users',
                        click: this.openBulkModal.bind(this, TimesheetAccessBulkUsersAddModalComponent)
                    },
                    {
                        title: 'ui.text.timesheetAccess.bulkTimesheetCategoriesAdd',
                        classList: 'btn btn-sm btn-primary fa fa-user-plus',
                        click: this.openBulkModal.bind(this, TimesheetAccessBulkCategoriesAddModalComponent)
                    },
                    {
                        title: 'ui.text.timesheetAccess.disableRates',
                        classList: 'btn btn-sm btn-primary fa fa-dumpster',
                        click: this.toggleRates,
                        disabled: this.hasSelectedRows,
                    },
                    {
                        title: 'ui.text.timesheetAccess.enableRates',
                        classList: 'btn btn-sm btn-primary fa fa-recycle',
                        click: this.toggleRates.bind(this, true),
                        disabled: this.hasSelectedRows,
                    },
                    {
                        title: 'ui.text.timesheetAccess.resetRates',
                        classList: 'btn btn-sm btn-primary fa fa-bolt',
                        click: this.resetRates,
                        hidden: !this.isAdmin,
                        disabled: this.hasSelectedRows,
                    },
                ]
            },
            editType: '',
            rowSelection: 'multiple',
            rowMultiSelectWithClick: true,
            onGridReady: (event: CustomGridReadyEvent) => this.customApi = event.customApi,
            onSelectionChanged: () => {
                this.gridOptions.context.buttonsContext = this.gridOptions.api.getSelectedRows().length <= 0;
                this.customApi.refreshUi();
            },
            columnDefs: [
                {
                    ...CustomAgGridService.idColumn,
                    checkboxSelection: true,
                },
                {
                    headerName: 'ui.text.timesheetAccess.rate',
                    field: 'rate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(999999.99)]
                    },
                    hide: !this.isAdmin
                },
                {
                    headerName: 'ui.text.timesheetAccess.internalRate',
                    field: 'internalRate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(999999.99)]
                    },
                    hide: !this.isAdmin
                },
                {
                    headerName: 'ui.text.timesheetCategory.title',
                    field: 'timesheetCategoryId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'timesheetCategoryName',
                        infiniteProps: {
                            name: 'timesheetCategory',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['name'],
                            editorOptionsResource: this.timesheetCategoryResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required],
                        disabled: (data: UserSimpleModel) => !!data.id
                    }
                },
                {
                    headerName: 'ui.text.user.title',
                    field: 'userId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        bindLabel: 'viewName',
                        bindValue: 'id',
                        otherField: 'userName',
                        infiniteProps: {
                            name: 'user',
                            orderByFields: ['firstName', 'lastName'],
                            filterByFieldNames: ['firstName', 'lastName'],
                            editorOptionsResource: this.userResource,
                            converter: this.userService.defaultUserConverter
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required],
                        disabled: (data: UserSimpleModel) => !!data.id
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    private hasSelectedRows = (): boolean => {
        return !this.gridOptions || !this.gridOptions.api || this.gridOptions.api.getSelectedRows().length <= 0;
    }

    private resetRates = (): void => {
        const timesheetAccessIds: number[] = this.gridOptions.api.getSelectedRows().map((user) => user.id);
        this.resource.resetRates(timesheetAccessIds).subscribe(() => {
            this.gridOptions.api.deselectAll();
            this.gridOptions.api.refreshInfiniteCache();
        });
    }

    private toggleRates = (isActive: boolean): void => {
        const timesheetAccessIds: number[] = this.gridOptions.api.getSelectedRows().map((user) => user.id);
        this.resource.toggleRates(timesheetAccessIds, isActive).subscribe(() => {
            this.gridOptions.api.deselectAll();
            this.gridOptions.api.refreshInfiniteCache();
        });
    }

    private openBulkModal(modal: ComponentType<any>): void {
        this.dialog.open(modal)
            .afterClosed()
            .subscribe({
                next: (result: ModalResult) => {
                    if (result && result.isRefreshRequired) {
                        this.gridOptions.api.refreshInfiniteCache();
                    }
                }
            });
    }

    private showRates = (context: ICellRendererParams): void => {
        this.dialog.open(TimesheetRateListingComponent, {
            width: 'inherit',
            maxWidth: '90vw',
            data: {timesheetAccess: context.data}
        });
    }

}
