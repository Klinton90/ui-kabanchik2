import { Observable, Subscription } from 'rxjs';
import { Component, ChangeDetectorRef, ChangeDetectionStrategy, OnDestroy, OnInit } from '@angular/core';
import { BulkTimesheetAccess } from './timesheet-access.model';
import { FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { TimesheetAccessResource } from './timesheet-access.resource';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { AlertService } from '../../../core/component/alert/alert.service';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { UserResource } from '../user/user.resource';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { ConverterService } from '../../../core/service/converter.service';
import { UserService } from '../../../core/service/user.service';
import { UtilService } from '../../../core/service/util.service';
import { AlertType, AlertCustomConfig } from 'app/module/core/component/alert/alert-custom-config';
import { FormAwareModal } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef } from '@angular/material/dialog';
import { periodFormValidator } from 'app/module/core/validator/period.validator';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-access-bulk-users-add-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetAccessBulkUsersAddModalComponent
extends FormAwareModal<TimesheetAccessBulkUsersAddModalComponent, any>
implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = [];
    protected newFormEntityInstance: BulkTimesheetAccess = new BulkTimesheetAccess();
    form: FormGroup = new FormGroup({
        timesheetCategoryId: new FormControl('', [Validators.required]),
        userIds: new FormControl('', [Validators.required]),
        from: new FormControl(null),
        to: new FormControl(null),
    }, [periodFormValidator]);

    constructor(
        public dialogRef: MatDialogRef<TimesheetAccessBulkUsersAddModalComponent>,
        protected resource: TimesheetAccessResource,
        protected utilService: UtilService,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected ref: ChangeDetectorRef,
        private timesheetCategoryResource: TimesheetCategoryResource,
        private userResource: UserResource,
        public converterService: ConverterService,
        public userService: UserService,
    ) {
        super();
    }
    ngOnInit(): void {
        this.subscriptions.push(this.form.valueChanges.subscribe(() => {
            const control: AbstractControl = this.form.controls['to'];
            control.setErrors(this.form.errors);
            control.markAsTouched();
        }));
    }

    getTimesheetCategoryLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.timesheetCategoryResource.getManagedTimesheetCategories(false, term, limit, offset);
    }

    getUserLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.userResource.getUsersForTimesheetAccessBulkUserAdd(term, limit, offset);
    }

    // @Override
    protected successHandler(response: RestResponseModel): void {
        let message: AlertCustomConfig;
        if (response.data.length) {
            message = {
                body: 'ui.message.timesheetAccess.bulkUsersAdd.duplicates',
                type: AlertType.WARNING,
                params: [response.data.join(', ')]
            };
        }
        super.successHandler(response, message);
    }

    // @Override
    getSaveAction(): string {
        return 'bulkUsersInsert';
    }

    // @Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();

        if (data.from) {
            data.from = data.from.valueOf();
        } else {
            data.from = null;
        }

        if (data.to) {
            data.to = data.to.valueOf();
        } else {
            data.to = null;
        }

        return data;
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
