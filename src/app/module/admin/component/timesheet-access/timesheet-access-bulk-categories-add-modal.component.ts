import { Observable } from 'rxjs';
import { Component, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TimesheetAccessResource } from './timesheet-access.resource';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { AlertService } from '../../../core/component/alert/alert.service';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { UserResource } from '../user/user.resource';
import { ConverterService } from '../../../core/service/converter.service';
import { UserService } from '../../../core/service/user.service';
import { BulkUserRate } from './timesheet-access.model';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { UtilService } from '../../../core/service/util.service';
import { AlertType, AlertCustomConfig } from 'app/module/core/component/alert/alert-custom-config';
import { FormAwareModal } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-access-bulk-categories-add-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetAccessBulkCategoriesAddModalComponent
extends FormAwareModal<TimesheetAccessBulkCategoriesAddModalComponent, any> {

    protected newFormEntityInstance: BulkUserRate;
    form: FormGroup = new FormGroup({
        userId: new FormControl('', [Validators.required]),
        timesheetCategoryIds: new FormControl('', [Validators.required]),
        // dateFrom: new FormControl(''),
        // dateTo: new FormControl(''),
    });

    constructor(
        public dialogRef: MatDialogRef<TimesheetAccessBulkCategoriesAddModalComponent>,
        protected resource: TimesheetAccessResource,
        protected utilService: UtilService,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected ref: ChangeDetectorRef,
        private timesheetCategoryResource: TimesheetCategoryResource,
        private userResource: UserResource,
        public converterService: ConverterService,
        public userService: UserService,
    ) {
        super();
    }

    getTimesheetCategoryLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.timesheetCategoryResource.getManagedTimesheetCategories(false, term, limit, offset);
    }

    getUserLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.userResource.getUsersForTimesheetAccessBulkUserAdd(term, limit, offset);
    }

    // @Override
    protected successHandler(response: RestResponseModel): void {
        let message: AlertCustomConfig;
        if (response.data.length) {
            message = {
                body: 'ui.message.timesheetAccess.bulkTimesheetCategoriesAdd.duplicates',
                type: AlertType.WARNING,
                params: [response.data.join(', ')]
            };
        }
        super.successHandler(response, message);
    }

    // @Override
    getSaveAction(): string {
        return 'bulkCategoriesInsert';
    }

    // @Override
    // protected getDataForSave(): any {
    //     const data: any = super.getDataForSave();

    //     if (data.dateFrom) {
    //         data.dateFrom = data.dateFrom.valueOf();
    //     } else {
    //         data.dateFrom = null;
    //     }

    //     if (data.dateTo) {
    //         data.dateTo = data.dateTo.valueOf();
    //     } else {
    //         data.dateTo = null;
    //     }

    //     return data;
    // }

}
