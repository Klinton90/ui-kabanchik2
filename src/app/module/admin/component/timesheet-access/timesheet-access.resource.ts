import { BulkTimesheetAccess, BulkUserRate } from './timesheet-access.model';
import { Injectable } from '@angular/core';
import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TimesheetAccessResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('timesheetAccess');
    }

    public bulkUsersInsert(data: BulkTimesheetAccess): Observable<any> {
        return this._executeRequest({
            url: '/bulkUsersInsert',
            method: 'POST'
        }, data);
    }

    public bulkCategoriesInsert(data: BulkUserRate): Observable<any> {
        return this._executeRequest({
            url: '/bulkCategoriesInsert',
            method: 'POST'
        }, data);
    }

    public resetRates(timesheetAccessIds: number[]): Observable<any> {
        return this._executeRequest({
            url: '/resetRates',
            method: 'POST'
        }, timesheetAccessIds);
    }

    public toggleRates(timesheetAccessIds: number[], isActive: boolean): Observable<any> {
        return this._executeRequest({
            url: '/toggleRates',
            method: 'POST'
        }, {
            timesheetAccessIds: timesheetAccessIds,
            isActive: isActive
        });
    }

}
