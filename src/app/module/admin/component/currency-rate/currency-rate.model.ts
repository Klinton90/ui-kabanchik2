import { IDomainModel } from 'app/module/core/model/i-domain.model';
import { DateAwareModel } from 'app/module/core/validator/period.validator';

export class CurrencyRate implements IDomainModel<number>, DateAwareModel {

    id: number = 0;
    currency: string = null;
    rate: number = null;
    from: number;
    to: number;

}
