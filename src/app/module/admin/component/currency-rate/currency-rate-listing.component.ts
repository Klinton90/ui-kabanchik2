import { OnInit, Component, ChangeDetectionStrategy, ChangeDetectorRef, Inject, OnDestroy } from '@angular/core';
import { CustomAgGridOptionsModel, ColumnObservableType, AgFilterMetadata } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Observable, Subscription } from 'rxjs';
import { CurrencyRateResource } from './currency-rate.resource';
import { CurrencyResource } from '../currency/currency.resource';
import { shareReplay, tap } from 'rxjs/operators';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { CurrencyRate } from './currency-rate.model';
import { Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CurrencyModel } from '../currency/currency.model';
import { MatchMode } from 'app/module/shared/model/match-mode.model';
import { SortOrder } from 'app/module/core/model/sort-order.model';
import { ConverterService } from 'app/module/core/service/converter.service';
import { periodValidator } from 'app/module/core/validator/period.validator';

interface CurrencyRateListingComponentData {
    currency: CurrencyModel;
}

@Component({
    moduleId: module.id,
    templateUrl: 'currency-rate-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CurrencyRateListingComponent implements OnInit, OnDestroy {

    private $currencies: Observable<RestResponseModel>;

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        public dialogRef: MatDialogRef<CurrencyRateListingComponent>,
        private resource: CurrencyRateResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private currencyResource: CurrencyResource,
        private converterService: ConverterService,
        @Inject(MAT_DIALOG_DATA) public data: CurrencyRateListingComponentData
    ) {
        this.$currencies = this.currencyResource
            .list(false, {sort: [{field: 'name', order: SortOrder.ASC}]})
            .pipe(
                shareReplay(1),
                tap((response: RestResponseModel) => {
                    this.converterService.convertObjectListToSelectItems(response.data, false, ConverterService.DEFAULT_PATTERN, 'id');
                })
            );
    }

    ngOnInit(): void {
        const extraFilter: Map<string, AgFilterMetadata> = new Map<string, AgFilterMetadata>();
        extraFilter.set('currency', {
            filter: this.data.currency.id,
            type: MatchMode.EQUALS,
            filterType: 'id'
        });
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'currencyRateListingGrid',
                resource: this.resource,
                extraFilter: extraFilter,
                newItemConstructor: () => {
                    const result: any = new CurrencyRate();
                    result.from = '';
                    result.to = '';
                    result.currency = this.data.currency.id;
                    return result;
                },
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.department.title',
                    field: 'currency',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN,
                        observable: this.$currencies,
                        bindLabel: 'name',
                        editor: false,
                    }
                },
                {
                    headerName: 'ui.text.currencyRate.rate',
                    field: 'rate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.min(0.01), Validators.max(1000.00)]
                    },
                    valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                },
                {
                    headerName: 'ui.text.currencyRate.from',
                    field: 'from',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: (data: any) => [
                            Validators.required,
                            periodValidator(data, true)
                        ]
                    }
                },
                {
                    headerName: 'ui.text.currencyRate.to',
                    field: 'to',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: (data: any) => [
                            Validators.required,
                            periodValidator(data, false)
                        ]
                    }
                }
            ]
        };

        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, false)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
