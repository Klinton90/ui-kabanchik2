import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { StaticContent } from '../../../shared/model/static-content-type.model';
import { BaseResource } from '../../../core/resource/base.resource';

@Injectable({
    providedIn: 'root'
})
export class StaticContentResource extends BaseResource {

    constructor(
        protected http: HttpClient
    ){
        super("staticContent");
    }

    public get(type: string, localizationId: string): Observable<any>{
        let params = new HttpParams()
            .append("localizationId", localizationId)
            .append("type", type.toString());

        return this._executeRequest({
            url: "/get",
            method: "GET",
            params: params
        });
    }

    public save(data: StaticContent): Observable<any>{
        return this._executeRequest({
            url: "/save",
            method: "PUT"
        }, data);
    }

}