import { StaticContent } from '../../../shared/model/static-content-type.model';
import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { LocalizationResource } from '../localization/localization.resource';
import { AuthService } from '../../../../service/auth.service';
import { SelectItem } from 'primeng/api/selectitem';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { SortOrder } from '../../../core/model/sort-order.model';
import { ConverterService } from '../../../core/service/converter.service';
import { AlertService } from '../../../core/component/alert/alert.service';
import { StaticContentResource } from './static-content.resource';
import { ServiceResource } from '../../../core/resource/service.resource';
import { UtilService } from '../../../core/service/util.service';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { tap } from 'rxjs/operators';
import { Observable, zip } from 'rxjs';

@Component({
    moduleId: module.id,
    templateUrl: 'static-content.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StaticContentComponent implements OnInit {

    text: string;

    type: string;
    types: SelectItem[] = [];

    localizationId: string;
    localizations: SelectItem[] = [];

    constructor(
        private authService: AuthService,
        private notificationService: AlertService,
        private staticContentResource: StaticContentResource,
        private localizationResource: LocalizationResource,
        private converterService: ConverterService,
        private serviceResource: ServiceResource,
        private utilService: UtilService,
        private ref: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        this.localizationId = this.authService.localizationId;

        const oTypes: Observable<any> = this.serviceResource
            .getLocalizedEnumOptions('kabanchik.system.staticcontent.domain.StaticContentType')
            .pipe(tap((response: RestResponseModel) => {
                this.types = this.utilService.addDefaultOption(response.data);
            }));

        const oLocalizations: Observable<any> = this.localizationResource
            .listAllPublic(true, {sort: [{field: 'name', order: SortOrder.ASC}]})
            .pipe(tap((response: RestResponseModel) => {
                this.localizations = this.converterService.convertObjectListToSelectItems(response.data, false, 'name', 'id');
            }));

        zip(oTypes, oLocalizations).subscribe(() => this.ref.detectChanges());
    }

    loadContent(): void {
        if (this.type) {
            this.staticContentResource.get(this.type, this.localizationId).subscribe((data: RestResponseModel) => {
                this.text = data.data;
                this.ref.detectChanges();
            });
        } else {
            this.text = '';
        }
    }

    save(): void {
        const data: StaticContent = {
            localizationId: this.localizationId,
            type: this.type,
            text: this.text
        };

        this.staticContentResource.save(data).subscribe(
            (response: RestResponseModel) => {
                this.notificationService.post({
                    type: AlertType.SUCCESS,
                    body: 'ui.message.default.grid.save.body.edit'
                });
            }
        );
    }
}
