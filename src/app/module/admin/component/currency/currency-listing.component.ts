import { Component, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { CurrencyResource } from './currency.resource';
import { CurrencyModel, UAH } from './currency.model';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CurrencyRateListingComponent } from '../currency-rate/currency-rate-listing.component';
import { ICellRendererParams, ValueFormatterParams } from 'ag-grid-community';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { AlertService } from 'app/module/core/component/alert/alert.service';

@Component({
    moduleId: module.id,
    templateUrl: 'currency-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CurrencyListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: CurrencyResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private dialog: MatDialog,
        private alertService: AlertService
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'currencyListingGrid',
                resource: this.resource,
                newItemConstructor: () => new CurrencyModel(),
                actionsColumnContext: {
                    actionColumnButtons: [
                        {
                            title: 'ui.text.currency.updateBankRates',
                            classList: 'btn btn-xs btn-primary fa fa-download',
                            click: this.updateBankRate,
                            hidden: this.isUAH
                        },
                        {
                            title: 'ui.text.currencyRate.title',
                            classList: 'btn btn-xs btn-primary fa fa-money-check-alt',
                            click: this.showRates,
                            hidden: this.isUAH
                        },
                    ]
                },
                buttons: [
                    {
                        title: 'ui.text.currency.updateBankRates',
                        classList: 'btn btn-sm btn-primary fa fa-download',
                        click: this.updateBankRates
                    },
                ]
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.default.id',
                    field: 'newId',
                    sortable: false,
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT,
                        filter: false
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(3)],
                        disabled: (data) => !!data.id
                    },
                    valueFormatter: (params: ValueFormatterParams) => {
                        return (params.data && params.data.id) || params.value;
                    },
                },
                {
                    headerName: 'ui.text.currency.name',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(50)]
                    }
                },
                {
                    headerName: 'ui.text.currency.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.currency.rate',
                    field: 'rate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01)]
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, true, true, null, false)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    private updateBankRates = (): void => {
        this.resource.updateBankRates().subscribe({
            next: () => {
                this.alertService.post({
                    body: 'ui.message.currency.bankRateUpdate.body',
                    type: AlertType.SUCCESS
                });
                this.gridOptions.api.refreshInfiniteCache();
            },
            error: this.alertService.processBackendValidation
        });
    }

    private updateBankRate = (context: ICellRendererParams): void => {
        this.resource.updateBankRate(context.data.id).subscribe({
            next: () => {
                this.alertService.post({
                    body: 'ui.message.currency.bankRateUpdate.body',
                    type: AlertType.SUCCESS
                });
                this.gridOptions.api.refreshInfiniteCache();
            },
            error: this.alertService.processBackendValidation
        });
    }

    private showRates = (context: ICellRendererParams): void => {
        this.dialog.open(CurrencyRateListingComponent, {
            width: 'inherit',
            maxWidth: '90vw',
            data: {currency: context.data}
        });
    }

    private isUAH = (context: ICellRendererParams): boolean => {
        return !context.data.id || context.data.id === UAH;
    }

}
