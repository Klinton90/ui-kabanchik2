import { Domain } from '../../../core/model/i-domain.model';

export const UAH = 'UAH';

export class CurrencyModel implements Domain<string> {
    newId: string = '';

    id: string = '';
    name: string = '';
    description: string = '';
    rate: number = 0.01;
    isActive: boolean = true;
}
