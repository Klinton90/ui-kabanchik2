import { Observable } from 'rxjs';
import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CurrencyResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('currency');
    }

    public updateBankRates(): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/updateBankRates'
        });
    }

    updateBankRate(id: string): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/updateBankRate/' + id,
        });
    }

}
