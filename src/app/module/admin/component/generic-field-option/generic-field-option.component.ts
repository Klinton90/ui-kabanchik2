import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { GenericFieldOptionResource } from './generic-field-option.resource';
import { GenericFieldResource } from '../generic-field/generic-field.resource';
import { GenericFieldOptionModel } from './generic-field-option.model';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
    templateUrl: './generic-field-option.component.html',
    styleUrls: ['./generic-field-option.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenericFieldOptionComponent implements OnInit, OnDestroy {

    public gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: GenericFieldOptionResource,
        private genericFieldResource: GenericFieldResource,
        private ref: ChangeDetectorRef,
        private atAgGridService: CustomAgGridService
    ) { }

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'genericFieldOptionGrid',
                resource: this.resource,
                newItemConstructor: () => new GenericFieldOptionModel()
            },
            columnDefs: [
                {
                    headerName: 'ui.text.genericFieldOption.value',
                    field: 'value',
                    filter: 'agTextColumnFilter',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(1), Validators.max(255)]
                    }
                },
                {
                    headerName: 'ui.text.genericFieldOption.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.max(255)]
                    }
                },
                {
                    headerName: 'ui.text.genericField.title',
                    field: 'genericFieldId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN,
                        observable: this.genericFieldResource.findAllOfSelectType(),
                        bindLabel: 'name'
                    },
                }
            ]
        };

        const subscription: Subscription = this.atAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
