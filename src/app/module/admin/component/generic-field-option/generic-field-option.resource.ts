import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GenericFieldOptionResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('genericFieldOption');
    }

}
