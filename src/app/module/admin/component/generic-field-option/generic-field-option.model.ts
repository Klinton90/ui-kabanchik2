import { Domain } from '../../../core/model/i-domain.model';

export class GenericFieldOptionModel implements Domain<number> {
    id: number;
    value: string = '';
    description: string = '';
    genericFieldId: number;
    isActive: boolean = true;
}
