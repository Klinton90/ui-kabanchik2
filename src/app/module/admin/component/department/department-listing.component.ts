import { Component, ChangeDetectionStrategy, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { DepartmentModel } from './department.model';
import { DepartmentResource } from './department.resource';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { Validators } from '@angular/forms';

@Component({
    moduleId: module.id,
    templateUrl: 'department-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DepartmentListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: DepartmentResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'departmentListingGrid',
                resource: this.resource,
                newItemConstructor: () => new DepartmentModel(),
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.department.name',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(50)]
                    }
                },
                {
                    headerName: 'ui.text.department.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.department.forWorkload',
                    field: 'forWorkload',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN
                    },
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }
}
