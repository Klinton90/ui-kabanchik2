import { Injectable } from '@angular/core';
import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { isNil } from 'lodash';
import { BaseResource } from 'app/module/core/resource/base.resource';

@Injectable({
    providedIn: 'root'
})
export class DepartmentResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('department');
    }

    public getForPSR(timesheetCategoryId: number): Observable<any> {
        return this._executeRequest({
            url: '/getForPSR',
            method: 'GET',
            params: new HttpParams().append('timesheetCategoryId', timesheetCategoryId.toString())
        });
    }

    public getForProgressModal(
        timesheetCategoryId: number,
        isForPlan?: boolean,
        queryString?: string,
        limit?: number,
        offset?: number
    ): Observable<any> {
        let params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());

        if (!isNil(isForPlan)) {
            params = params.append('isForPlan', isForPlan.toString());
        }

        return this._executeRequest({
            url: '/getForProgressModal',
            method: 'GET',
            params: BaseResource.setOptionalPageableParams(params, queryString, limit, offset)
        });
    }

}
