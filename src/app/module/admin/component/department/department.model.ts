import { Domain } from '../../../core/model/i-domain.model';
import { ProgressParent } from '../../model/progress-parent.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';

export class DepartmentModel implements Domain<number>, ProgressParent {
    id: number = null;
    name: string = '';
    description: string = '';
    isActive: boolean = true;
    forWorkload: boolean = true;
    progressType: LocalizedEnum = null;
}
