import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class SupplierResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('supplier');
    }

    public getForPSR(timesheetCategoryId: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());

        return this._executeRequest({
            url: '/getForPSR',
            method: 'GET',
            params: params
        });
    }

}
