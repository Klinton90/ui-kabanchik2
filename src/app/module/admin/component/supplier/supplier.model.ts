import { OutcomeEntryParent } from '../../model/outcome-entry-parent.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';

export class SupplierModel implements OutcomeEntryParent {
    id: number = null;
    name: string = '';
    description: string = '';
    isActive: boolean = true;
    governmentId: string = '';
    outcomeEntryType: LocalizedEnum;
}
