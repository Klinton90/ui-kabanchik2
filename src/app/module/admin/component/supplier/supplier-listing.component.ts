import { Component, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { SupplierResource } from './supplier.resource';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { SupplierModel } from './supplier.model';
import { Validators } from '@angular/forms';
import { exactLengthValidator } from 'app/module/core/validator/exact-length.validator';

@Component({
    moduleId: module.id,
    templateUrl: 'supplier-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SupplierListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: SupplierResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'supplierListingGrid',
                resource: this.resource,
                newItemConstructor: () => new SupplierModel(),
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.supplier.name',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(50)]
                    }
                },
                {
                    headerName: 'ui.text.supplier.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.supplier.governmentId',
                    field: 'governmentId',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, exactLengthValidator([8, 10])]
                    }
                }
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }
}
