import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GenericFieldEntity } from './generic-field.model';

@Injectable({
    providedIn: 'root'
})
export class GenericFieldResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('genericField');
    }

    findAllOfSelectType(): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/findAllOfSelectType'
        });
    }

    findAllByEntity(entity: GenericFieldEntity): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/findAllByEntity',
            params: new HttpParams().append('entity', GenericFieldEntity[entity])
        });
    }

}
