import { Domain } from '../../../core/model/i-domain.model';
import { GenericFieldOptionModel } from '../generic-field-option/generic-field-option.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';

export class GenericFieldModel implements Domain<number> {
    id: number;
    required: boolean = false;
    entity: GenericFieldEntity;
    type: GenericFieldType | LocalizedEnum;
    name: string = '';
    isActive: boolean = true;
}

export class GenericFieldWithOptionModel extends GenericFieldModel {
    genericFieldOptions?: GenericFieldOptionModel[];
}

export enum GenericFieldEntity {
    TIMESHEET_CATEGORY,
    USER
}

export enum GenericFieldType {
    TEXT,
    NUMBER,
    SELECT,
    DATE,
    BOOLEAN
}
