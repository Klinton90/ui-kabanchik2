import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { GenericFieldResource } from 'app/module/admin/component/generic-field/generic-field.resource';
import { GenericFieldModel } from './generic-field.model';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
    templateUrl: './generic-field.component.html',
    styleUrls: ['./generic-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenericFieldComponent implements OnInit, OnDestroy {

    public gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: GenericFieldResource,
        private serviceResource: ServiceResource,
        private ref: ChangeDetectorRef,
        private atAgGridService: CustomAgGridService
    ) { }

    ngOnInit() {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'genericFieldGrid',
                resource: this.resource,
                newItemConstructor: () => new GenericFieldModel()
            },
            columnDefs: [
                {
                    headerName: 'ui.text.genericField.name',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(1), Validators.max(255)]
                    }
                },
                {
                    headerName: 'ui.text.genericField.entity',
                    field: 'entity',
                    autoGenerateContext: {
                        type: ColumnObservableType.LOCALIZED_ENUM,
                        observable:  this.serviceResource.getLocalizedEnumOptions('kabanchik.system.genericfield.domain.GenericFieldEntity')
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.genericField.type',
                    field: 'type',
                    autoGenerateContext: {
                        type: ColumnObservableType.LOCALIZED_ENUM,
                        observable: this.serviceResource.getLocalizedEnumOptions('kabanchik.system.genericfield.domain.GenericFieldType')
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.genericField.required',
                    field: 'required',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN,
                        filter: false,
                        floatingFilter: false
                    },
                }
            ]
        };

        const subscription: Subscription = this.atAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
