import { GenericFieldModel, GenericFieldType } from './generic-field.model';
import { FormGroup } from '@angular/forms';
import { StaticService } from 'app/module/core/service/static.service';
import { GenericFieldValue, IGenericFieldAwareDomain } from '../../model/generic-field-value.model';
import { isNil } from 'lodash';

export const GENERIC_FIELD_PREFIX = 'GenericField';

export class GenericFieldService {

    static getFormFieldName(genericField: GenericFieldModel): string {
        return GENERIC_FIELD_PREFIX + genericField.id;
    }

    static processFormValues(
        genericFields: GenericFieldModel[],
        form: FormGroup,
        entity: IGenericFieldAwareDomain<any>
    ): GenericFieldValue[] {
        const result: GenericFieldValue[] = [];

        genericFields.forEach((genericField: GenericFieldModel) => {
            let value: any = form.get(GenericFieldService.getFormFieldName(genericField)).value;
            if (isNil(value)) {
                value = '';
            } else if (value && StaticService.resolveEnumOnEntity(genericField.type, GenericFieldType) === GenericFieldType.DATE) {
                value = value.valueOf();
            }

            let genericFieldValue: GenericFieldValue = entity.genericFieldValues
                    .find((_genericFieldValue: GenericFieldValue) => _genericFieldValue.genericFieldId === genericField.id);

            if (genericFieldValue) {
                genericFieldValue.value = value;
            } else {
                genericFieldValue = {
                    id: 0,
                    value: value,
                    genericFieldId: genericField.id,
                    parentId: entity.id
                };
            }

            result.push(genericFieldValue);
        });

        return result;
    }

}
