import { Component, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { TimesheetCategoryResource } from './timesheet-category.resource';
import {
    CustomAgGridOptionsModel,
    ColumnObservableType,
    CustomAgGridModalContext
} from 'app/module/grid-shared/model/ag-grid-options.model';
import { TimesheetCategory } from './timesheet-category.model';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { GenericFieldEntity } from '../generic-field/generic-field.model';
import { ICellRendererParams, ValueGetterParams } from 'ag-grid-community';
import { TimesheetCategoryModalComponent } from './timesheet-category-modal.component';
import { UserResource } from '../user/user.resource';
import { Validators } from '@angular/forms';
import { UserService } from 'app/module/core/service/user.service';
import { FormAndGenericFieldAwareParams } from 'app/module/shared/component/modal/form-and-generic-field-aware-modal.component';
import { Subscription } from 'rxjs';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-category-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetCategoryListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: TimesheetCategoryResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private userResource: UserResource,
        private userService: UserService
    ) {}

    ngOnInit() {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'timesheetCategoryGrid',
                resource: this.resource,
                newItemConstructor: () => new TimesheetCategory(),
                genericFieldEntity: GenericFieldEntity.TIMESHEET_CATEGORY
            },
            columnDefs: [
                {
                    headerName: 'ui.text.timesheetCategory.title',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50)]
                    }
                },
                {
                    headerName: 'ui.text.timesheetCategory.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.timesheetCategory.initialBudget',
                    field: 'initialBudget',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.min(0.01), Validators.max(99999999999999999.99)]
                    },
                    valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                },
                {
                    headerName: 'ui.text.timesheetCategory.contractDays',
                    field: 'contractDays',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(1)]
                    }
                },
                {
                    headerName: 'ui.text.timesheetCategory.requireApproval',
                    field: 'requireApproval',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN,
                        filter: false,
                        floatingFilter: false
                    }
                },
                {
                    headerName: 'ui.text.timesheetCategory.allowForPsr',
                    field: 'allowForPsr',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN
                    }
                },
                {
                    headerName: 'ui.text.timesheetCategory.managers',
                    field: 'managers',
                    sortable: false,
                    valueGetter: (params: ValueGetterParams) => {
                        if (params && params.data) {
                            if (params.data.managers && params.data.managers.length && !params.data.managers[0].viewName) {
                                params.data.managers.forEach(user => user.viewName = this.userService.getUserNameForView(user));
                            }
                            return params.data.managers;
                        } else {
                            return null;
                        }
                    },
                    valueFormatter: this.userService.getManagersFormatterForAgGrid,
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN_MULTIPLE2,
                        bindLabel: 'viewName',
                        bindValue: 'id',
                        infiniteProps: {
                            name: 'timesheetCategory',
                            editorOptionsMethod: this.getManagers.bind(this),
                            filterMethod: this.getManagers.bind(this),
                            converter: this.userService.defaultUserConverter
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.minLength(1)],
                        bindValue: ''
                    }
                }
            ]
        };

        const modalParams: CustomAgGridModalContext<TimesheetCategoryModalComponent, TimesheetCategory> = {
            component: TimesheetCategoryModalComponent,
            modalDataBuilder: this.modalDataBuilder
        };

        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, true, true, modalParams)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    modalDataBuilder(context: ICellRendererParams): FormAndGenericFieldAwareParams<TimesheetCategory> {
        return {
            entity: context ? context.data : new TimesheetCategory()
        };
    }

    private getManagers(queryString?: string, limit?: number, offset?: number) {
        return this.userResource.getManagers(queryString, limit, offset);
    }

}
