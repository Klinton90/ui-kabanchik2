import { UserSimpleModel } from '../user/user.model';
import { Domain } from '../../../core/model/i-domain.model';
import { GenericFieldValue } from '../../model/generic-field-value.model';
import { IGenericFieldAwareDomain } from 'app/module/admin/model/generic-field-value.model';

export class TimesheetCategorySimple implements Domain<number> {
    id: number = 0;
    name: string = '';
    description: string = '';
    isActive: boolean = true;
    requireApproval: boolean = true;
    allowForPsr: boolean = true;
    initialBudget: number = 0;
    contractDays: number = 0;
}

export class TimesheetCategory extends TimesheetCategorySimple implements IGenericFieldAwareDomain<number> {
    managers: UserSimpleModel[] = [];
    genericFieldValues: GenericFieldValue[] = [];
}
