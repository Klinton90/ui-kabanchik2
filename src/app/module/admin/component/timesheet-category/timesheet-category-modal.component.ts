import { ChangeDetectionStrategy, Component, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserSimpleModel } from '../user/user.model';
import { UserService } from 'app/module/core/service/user.service';
import { TimesheetCategory } from './timesheet-category.model';
import { UtilService } from 'app/module/core/service/util.service';
import { TimesheetCategoryResource } from './timesheet-category.resource';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import {
    FormAndGenericFieldAwareModal,
    FormAndGenericFieldAwareParams
} from 'app/module/shared/component/modal/form-and-generic-field-aware-modal.component';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { UserResource } from '../user/user.resource';
import { Observable } from 'rxjs';

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-category-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetCategoryModalComponent extends FormAndGenericFieldAwareModal<TimesheetCategoryModalComponent, TimesheetCategory> {

    // Override
    protected newFormEntityInstance: TimesheetCategory = new TimesheetCategory();
    form: FormGroup = new FormGroup({
        id: new FormControl(''),
        name: new FormControl('', [Validators.required, Validators.maxLength(50)]),
        description: new FormControl('', [Validators.maxLength(255)]),
        isActive: new FormControl(''),
        requireApproval: new FormControl(''),
        allowForPsr: new FormControl(''),
        initialBudget: new FormControl(0, [Validators.min(0.01), Validators.max(99999999999999999.99)]),
        contractDays: new FormControl(0, [Validators.required, Validators.min(1)]),
        managers: new FormControl([], [Validators.required])
    });

    constructor(
        public dialogRef: MatDialogRef<TimesheetCategoryModalComponent>,
        protected resource: TimesheetCategoryResource,
        protected utilService: UtilService,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        public userService: UserService,
        public ref: ChangeDetectorRef,
        private userResource: UserResource,
        @Inject(MAT_DIALOG_DATA) public data: FormAndGenericFieldAwareParams<TimesheetCategory>
    ) {
        super();

        this.show(data);
    }

    getUserLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.userResource.getManagers(term, limit, offset);
    }

    userConverter = (response: RestResponseModel): UserSimpleModel[] => {
        response.data.forEach(user => (<any>user).viewName = this.userService.getUserNameForView(user));
        return response.data;
    }

}
