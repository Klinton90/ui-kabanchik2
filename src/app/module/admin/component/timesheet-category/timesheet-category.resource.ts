import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pageable } from 'app/module/core/model/pageable.model';
import { TimesheetCategory } from './timesheet-category.model';
import { BaseResource } from 'app/module/core/resource/base.resource';
import { Moment } from 'moment';

@Injectable({
    providedIn: 'root'
})
export class TimesheetCategoryResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('timesheetCategory');
    }

    // DEPRECATED
    getListByCurrentUser(): Observable<any> {
        return this._executeRequest({
            url: 'getListByCurrentUser',
            method: 'GET'
        });
    }

    // DEPRECATED
    getListManagedTimesheetCategoriesByUser(userId: number): Observable<any> {
        const params: HttpParams = new HttpParams().append('userId', userId.toString());
        return this._executeRequest({
            url: 'getListManagedTimesheetCategoriesByUser',
            method: 'GET',
            params: params
        });
    }

    getManagedTimesheetCategories(forPsr: boolean, queryString?: string, limit?: number, offset?: number): Observable<any> {
        let params: HttpParams = new HttpParams().append('forPsr', forPsr.toString());

        params = BaseResource.setOptionalPageableParams(params, queryString, limit, offset);

        return this._executeRequest({
            url: 'getManagedTimesheetCategories',
            method: 'GET',
            params: params
        });
    }

    getManagedTimesheetCategoriesForSummaryByPeriod(
        dateFrom: Moment,
        dateTo: Moment,
        departmentId?: number
    ): Observable<any> {
        let params: HttpParams = new HttpParams()
            .append('yearFrom', dateFrom.year().toString())
            .append('monthFrom', dateFrom.month().toString())
            .append('yearTo', dateTo.year().toString())
            .append('monthTo', dateTo.month().toString());

        if (departmentId) {
            params = params.append('departmentId', departmentId.toString());
        }

        return this._executeRequest({
            url: 'getManagedTimesheetCategoriesForSummaryByPeriod',
            method: 'GET',
            params: params
        });
    }

    getTimesheetCategoriesForPsrSummaryByPeriod(
        dateFrom: Moment,
        dateTo: Moment,
        departmentId?: number
    ): Observable<any> {
        let params: HttpParams = new HttpParams()
            .append('yearFrom', dateFrom.year().toString())
            .append('monthFrom', dateFrom.month().toString())
            .append('yearTo', dateTo.year().toString())
            .append('monthTo', dateTo.month().toString());

        if (departmentId) {
            params = params.append('departmentId', departmentId.toString());
        }

        return this._executeRequest({
            url: 'getTimesheetCategoriesForPsrSummaryByPeriod',
            method: 'GET',
            params: params
        });
    }

    findAllByCurrentUserAndPeriod(yearFrom: number, monthFrom: number, yearTo: number, monthTo: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('yearFrom', yearFrom.toString())
            .append('monthFrom', monthFrom.toString())
            .append('yearTo', yearTo.toString())
            .append('monthTo', monthTo.toString());

        return this._executeRequest({
            url: 'findAllByCurrentUserAndPeriod',
            method: 'GET',
            params: params
        });
    }

    findAllManagedTimesheetCategoriesByUserIdAndPeriod(
        userId: number,
        yearFrom: number,
        monthFrom: number,
        yearTo: number,
        monthTo: number
    ) {
        const params: HttpParams = new HttpParams()
            .append('userId', userId.toString())
            .append('yearFrom', yearFrom.toString())
            .append('monthFrom', monthFrom.toString())
            .append('yearTo', yearTo.toString())
            .append('monthTo', monthTo.toString());

        return this._executeRequest({
            url: 'findAllManagedTimesheetCategoriesByUserIdAndPeriod',
            method: 'GET',
            params: params
        });
    }

    // DEPRECATED
    getForPsr(): Observable<any> {
        return this._executeRequest({
            url: 'getForPsr',
            method: 'GET'
        });
    }

    listAllSimple(showActive?: boolean, pageable?: Pageable): Observable<any> {
        let params: HttpParams = new HttpParams();

        if (showActive) {
            params = params.set('showActive', showActive.toString());
        }
        if (pageable) {
            params = this.setPageable(params, pageable);
        }

        return this._executeRequest({
            url: 'listAllSimple',
            method: 'GET',
            params: params
        });
    }

    // deprecated
    updateGenericFieldValues(timesheetCategory: TimesheetCategory): Observable<any> {
        return this._executeRequest({
            url: 'updateGenericFieldValues',
            method: 'PUT'
        }, timesheetCategory);
    }

    updateManagerFields(timesheetCategory: TimesheetCategory): Observable<any> {
        return this._executeRequest({
            url: 'updateManagerFields',
            method: 'PUT'
        }, timesheetCategory);
    }

}
