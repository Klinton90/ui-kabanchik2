import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { LocalizationDomainModel } from './localization-domain.model';
import { AlertService } from '../../../core/component/alert/alert.service';
import { LocalizationResource } from './localization.resource';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { UtilService } from '../../../core/service/util.service';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    moduleId: module.id,
    templateUrl: 'localization-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocalizationModalComponent extends FormAwareModal<LocalizationModalComponent, LocalizationDomainModel> {
    newFormEntityInstance: LocalizationDomainModel = new LocalizationDomainModel();

    form: FormGroup = new FormGroup({
        id: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]),
        name: new FormControl('', [Validators.required, Validators.maxLength(255)]),
        isActive: new FormControl('', [])
    });

    constructor(
        public dialogRef: MatDialogRef<LocalizationModalComponent>,
        protected resource: LocalizationResource,
        protected notificationService: AlertService,
        protected confirmationService: ConfirmationService,
        protected utilService: UtilService,
        protected ref: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) data: FormAwareParams<LocalizationDomainModel>
    ) {
        super();

        super.show(data);
    }

}
