import {Domain} from "../../../core/model/i-domain.model";

export const PRIMARY_LOCALIZATION_CODE = "EN";

export class LocalizationDomainModel implements Domain<string> {
    id: string;
    name: string;
    isActive: boolean = true;
}