import { Observable } from 'rxjs';
import { RestResource } from '../../../core/resource/rest.resource';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Pageable } from '../../../core/model/pageable.model';

@Injectable({
    providedIn: 'root'
})
export class LocalizationResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('/localization');
    }

    public listAllPublic(showActive?: boolean, pageable?: Pageable): Observable<any> {
        let params: HttpParams = new HttpParams();
        if (showActive || pageable) {
            if (showActive) {
                params = params.set('showActive', showActive.toString());
            }
            if (pageable) {
                params = this.setPageable(params, pageable);
            }
        }

        return this._executeRequest({
            method: 'GET',
            url: '/listAllPublic',
            params: params
        });
    }

    public getMessagesBySession(): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/getMessagesBySession',
        });
    }

    public getMessagesByLocalizationId(localizationId: string): Observable<any> {
        const params = new HttpParams()
            .append('localizationId', localizationId);

        return this._executeRequest({
            method: 'GET',
            url: '/getMessagesByLocalizationId',
            params: params
        });
    }

    public updateMessages(localizationId: string, data: Object): Observable<any> {
        return this._executeRequest({
            method: 'PUT',
            url: '/updateMessages/' + localizationId
        }, data);
    }

    public refreshLocalization(): Observable<any> {
        return this._executeRequest({
            method: 'GET',
            url: '/refreshLocalization'
        });
    }
}
