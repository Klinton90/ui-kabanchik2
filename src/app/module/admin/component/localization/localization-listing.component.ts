import { MessageModel } from '../../model/message.model';
import { ConverterService } from '../../../core/service/converter.service';
import { Component, OnInit, ViewChild, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AlertService } from '../../../core/component/alert/alert.service';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { LocalizationResource } from './localization.resource';
import { MatchMode } from '../../../shared/model/match-mode.model';
import { SelectItem } from 'primeng/api/selectitem';
import { Table } from 'primeng/table';
import { forOwn, sortBy } from 'lodash';
import { Observable } from 'rxjs';
import { LocalizationDomainModel, PRIMARY_LOCALIZATION_CODE } from './localization-domain.model';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { FormAwareModalResult } from '../../../shared/component/modal/success-form-aware-modal-event.model';
import { SortOrder } from '../../../core/model/sort-order.model';
import { HttpErrorResponse } from '@angular/common/http';
import { LANGUAGE_SERVICE_TOKEN, ILanguageService } from '../../../core/service/i-language.service';
import { finalize, tap } from 'rxjs/operators';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { MatDialog } from '@angular/material/dialog';
import { LocalizationModalComponent } from './localization-modal.component';
import { AuthService } from 'app/service/auth.service';

type ILanguageServiceAlias = ILanguageService;

@Component({
    moduleId: module.id,
    templateUrl: 'localization-listing.component.html',
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocalizationListingComponent implements OnInit {

    @ViewChild(Table, {static: true}) dataTable: Table;
    MATCH_MODE: typeof MatchMode = MatchMode;

    localizations: SelectItem[] = [];
    selectedLocalization: LocalizationDomainModel;

    dataList: MessageModel[] = [];
    editedList: string[] = [];

    constructor(
        protected resource: LocalizationResource,
        protected notificationService: AlertService,
        protected confirmationService: ConfirmationService,
        protected converterService: ConverterService,
        private ref: ChangeDetectorRef,
        private dialog: MatDialog,
        private authService: AuthService,
        @Inject(LANGUAGE_SERVICE_TOKEN) private languageService: ILanguageServiceAlias
    ) {}

    ngOnInit(): void {
        this.dataTable.rows = 5;
        this.dataTable.first = 0;
        this.dataTable.pageLinks = 5;
        this.dataTable.rowsPerPageOptions = [5, 10, 25, 50];
        this.dataTable.sortField = 'key';

        this.selectedLocalization = this.authService.user.localization;

        this.listLocalizationsAction().subscribe(() => {
            this.listLangVarsAction();
        });
    }

    showModal(localization?: LocalizationDomainModel): void {
        this.dialog.open(LocalizationModalComponent, {data: {entity: localization}});
    }

    listLocalizationsAction(): Observable<any> {
        return this.resource.list(null, {sort: [{field: 'name', order: SortOrder.ASC}]})
            .pipe(tap((response: RestResponseModel) => {
                this.localizations = this.converterService.convertObjectListToSelectItems(
                    response.data, false, ConverterService.DEFAULT_PATTERN);
            }));
    }

    selectLocalization(value: LocalizationDomainModel): void {
        this.selectedLocalization = value;
        this.listLangVarsAction();
    }

    deleteLocalizationAction(entity: LocalizationDomainModel): void {
        this.confirmationService.show({
            body: 'ui.confirmation.default.delete.body',
            onPrimaryClick: () => this.executeDeleteAction(entity)
        });
    }

    executeDeleteAction(entity: LocalizationDomainModel): void {
        this.resource.delete(entity.id).subscribe((response: RestResponseModel) => {
            this.localizations.splice(this.findLocalizationIndex(entity), 1);
            this.selectLocalization(this.localizations[0].value);
            this.notificationService.post({
                body: 'ui.message.default.grid.delete.body',
                type: AlertType.SUCCESS
            });
        }, (response: HttpErrorResponse) => {
            this.notificationService.processBackendValidation(response);
        });
    }

    updateLocalizationList(event: FormAwareModalResult): void {
        const newItem: SelectItem = this.converterService.convertObjectToSelectItem(event.response.data, ConverterService.DEFAULT_PATTERN);
        if (event.isAddMode) {
            this.localizations.push(newItem);
            this.selectLocalization(newItem.value);
        } else {
            this.localizations[this.findLocalizationIndex(event.response.data)] = newItem;
        }
        this.selectedLocalization = newItem.value;
        this.localizations = sortBy(this.localizations, ['label']);
    }

    listLangVarsAction(): void {
        if (this.editedList.length) {
            this.confirmationService.show({
                body: 'ui.confirmation.localization.refreshLocalization.body',
                onPrimaryClick: () => {
                    this.editedList.length = 0;
                    this._listLangVarsAction();
                }
            });
        } else {
            this._listLangVarsAction();
        }
    }

    updateLangVarsAction(): void {
        const updateMap: Object = {};

        this.editedList.forEach((key: string) => {
            const matchModel: MessageModel = this.dataList.find((model: MessageModel) => {
                return model.key === key;
            });

            if (matchModel) {
                updateMap[key] = matchModel.value;
            }
        });

        if (Object.keys(updateMap).length > 0) {
            this.resource.updateMessages(this.selectedLocalization.id, updateMap)
                .pipe(
                    finalize(() => this.editedList = [])
                ).subscribe((response: any) => {
                    this.fillDataList(response);
                    this.notificationService.post({
                        body: 'ui.message.default.grid.save.body.edit',
                        type: AlertType.SUCCESS
                    });
                    this.notificationService.post({
                        body: 'ui.message.localization.refreshMessages.body',
                        type: AlertType.INFO
                    });
                });
        }
    }

    rowEdited($event: any): void {
        if (this.editedList.indexOf($event.data.key) === -1) {
            this.editedList.push($event.data.key);
        }
    }

    deleteAllowed(localization: LocalizationDomainModel) {
        return localization && localization.id !== PRIMARY_LOCALIZATION_CODE;
    }

    refreshLocalization(event: MouseEvent): void {
        event.stopPropagation();
        this.confirmationService.show({
            body: 'ui.confirmation.localization.refreshLocalization.body',
            onPrimaryClick: () => {
                this.languageService.refreshMessages(true);
            }
        });
    }

    private _listLangVarsAction(): void {
        this.resource.getMessagesByLocalizationId(this.selectedLocalization.id).subscribe((data: any) => {
            this.fillDataList(data);
        });
    }

    private findLocalizationIndex(localization: LocalizationDomainModel): number {
        return this.localizations.findIndex((item: SelectItem) => {
            return item.value.id === localization.id;
        });
    }

    private fillDataList(data): void {
        this.dataList = [];
        forOwn(data, (value: string, key: string) => {
            this.dataList.push({
                key: key,
                value: value
            });
        });
        this.ref.detectChanges();
    }

}
