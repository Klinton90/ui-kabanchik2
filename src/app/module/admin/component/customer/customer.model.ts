export class CustomerModel {
    id: number = null;
    name: string = '';
    description: string = '';
    isActive: boolean = true;
    governmentId: string = '';
}
