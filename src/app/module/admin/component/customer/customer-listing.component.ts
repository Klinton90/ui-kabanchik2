import { Component, ChangeDetectorRef, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { CustomerResource } from './customer.resource';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { CustomerModel } from './customer.model';
import { Validators } from '@angular/forms';
import { exactLengthValidator } from 'app/module/core/validator/exact-length.validator';

@Component({
    moduleId: module.id,
    templateUrl: 'customer-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomerListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: CustomerResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'customerListingGrid',
                resource: this.resource,
                newItemConstructor: () => new CustomerModel(),
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.customer.name',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(50)]
                    }
                },
                {
                    headerName: 'ui.text.customer.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.customer.governmentId',
                    field: 'governmentId',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, exactLengthValidator([8, 10])]
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
