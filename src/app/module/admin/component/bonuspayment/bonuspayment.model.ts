import { IDomainModel } from '../../../core/model/i-domain.model';

export class Bonuspayment implements IDomainModel<number> {
    id: number = 0;
    amount: number = 0;
    comment: string = '';
    userId: number;
    timesheetCategoryId: number;
    date: Date;
}
