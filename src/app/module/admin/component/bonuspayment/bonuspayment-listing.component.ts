import { UserResource } from '../user/user.resource';
import { Component, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { ConverterService } from '../../../core/service/converter.service';
import { BonuspaymentResource } from './bonuspayment.resource';
import { UserService } from '../../../core/service/user.service';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { Bonuspayment } from './bonuspayment.model';
import { Validators } from '@angular/forms';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';

@Component({
    moduleId: module.id,
    templateUrl: 'bonuspayment-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BonuspaymentListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    private subscriptions: Subscription[] = [];

    constructor(
        public resource: BonuspaymentResource,
        protected ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        protected converterService: ConverterService,
        private timesheetCategoryResource: TimesheetCategoryResource,
        protected userResource: UserResource,
        public userService: UserService,
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'bonuspaymentListingGrid',
                resource: this.resource,
                newItemConstructor: () => new Bonuspayment(),
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.bonuspayment.amount',
                    field: 'amount',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(999999.99)]
                    },
                },
                {
                    headerName: 'ui.text.bonuspayment.comment',
                    field: 'comment',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(65025)]
                    },
                },
                {
                    headerName: 'ui.text.bonuspayment.date',
                    field: 'date',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    },
                },
                {
                    headerName: 'ui.text.timesheetCategory.title',
                    field: 'timesheetCategoryId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'timesheetCategoryName',
                        infiniteProps: {
                            name: 'timesheetCategory',
                            filterByFieldNames: ['name'],
                            editorOptionsResource: this.timesheetCategoryResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required],
                    }
                },
                {
                    headerName: 'ui.text.user.title',
                    field: 'userId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        bindLabel: 'viewName',
                        bindValue: 'id',
                        otherField: 'userName',
                        infiniteProps: {
                            name: 'user',
                            orderByFields: ['firstName', 'lastName'],
                            filterByFieldNames: ['firstName', 'lastName'],
                            editorOptionsResource: this.userResource,
                            converter: this.userService.defaultUserConverter
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required],
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
