import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestResource } from '../../../core/resource/rest.resource';
import { Pageable } from '../../../core/model/pageable.model';
import { Observable } from 'rxjs';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { map } from 'rxjs/operators';
import { FilterMetadata } from 'primeng/api/filtermetadata';

@Injectable({
    providedIn: 'root'
})
export class BonuspaymentResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('bonuspayment');
    }

    public findByGenericFilters(showActive: boolean, pageable: Pageable, filters: {[s: string]: FilterMetadata}): Observable<any> {
        return super.findByGenericFilters(showActive, pageable, filters).pipe(
            map(this.responseConverter)
        );
    }

    public findLike(showActive: boolean, pageable: Pageable, likeValue: string): Observable<any> {
        return super.findLike(showActive, pageable, likeValue).pipe(
            map(this.responseConverter)
        );
    }

    public findAllManagedBonuspaymentsForLoggedUser(
        yearFrom: number,
        monthFrom: number,
        yearTo: number,
        monthTo: number,
        departmentId?: number
    ): Observable<any[]> {
        let params: HttpParams = new HttpParams()
            .append('yearFrom', yearFrom.toString())
            .append('monthFrom', monthFrom.toString())
            .append('yearTo', yearTo.toString())
            .append('monthTo', monthTo.toString());

        if (departmentId) {
            params = params.append('departmentId', departmentId.toString());
        }

        return this._executeRequest({
            url: '/findAllManagedBonuspaymentsForLoggedUser',
            method: 'GET',
            params: params
        });
    }

    private responseConverter(response: RestResponseModel): RestResponseModel {
        response.data.content.forEach((row) => {
            const date: Date = new Date(row.date);
            row.date = date;
        });

        return response;
    }

}
