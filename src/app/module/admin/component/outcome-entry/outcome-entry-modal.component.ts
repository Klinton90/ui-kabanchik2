import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { LocalizedEnum } from '../../../shared/model/localized-enum.model';
import { SupplierResource } from '../supplier/supplier.resource';
import { SubcontractorResource } from '../subcontractor/subcontractor.resource';
import { CurrencyResource } from '../currency/currency.resource';
import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { AlertService } from '../../../core/component/alert/alert.service';
import { UtilService } from '../../../core/service/util.service';
import { OutcomeEntryModel, OutcomeEntryType } from './outcome-entry.model';
import { OutcomeEntryResource } from './outcome-entry.resource';
import { SelectItem } from 'primeng/api/selectitem';
import { Observable } from 'rxjs';
import { ServiceResource } from '../../../core/resource/service.resource';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { ConverterService } from '../../../core/service/converter.service';
import { StaticService } from '../../../core/service/static.service';
import { isNil } from 'lodash';
import { FormAwareParams, FormAwareModal } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ngSelectInfiniteDataCallback } from 'app/module/shared/component/ng-select-infinite-wrapper/ng-select-infinite-wrapper.component';

export class OutcomeEntryModalParams implements FormAwareParams<OutcomeEntryModel> {
    entity: OutcomeEntryModel;
    forPsr?: boolean;
}

@Component({
    moduleId: module.id,
    templateUrl: 'outcome-entry-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutcomeEntryModalComponent extends FormAwareModal<OutcomeEntryModalComponent, OutcomeEntryModel> {

    newFormEntityInstance: OutcomeEntryModel = new OutcomeEntryModel();

    outcomeEntryTypeOptions: SelectItem[] = [];

    parentLoadDataCallback: ngSelectInfiniteDataCallback;

    params: OutcomeEntryModalParams = new OutcomeEntryModalParams();

    form: FormGroup = new FormGroup({
        id: new FormControl(''),
        date: new FormControl('', [Validators.required]),
        timesheetCategoryId: new FormControl('', [Validators.required]),
        amount: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(9999999.99)]),
        currencyId: new FormControl('', [Validators.required]),
        outcomeEntryType: new FormControl('', [Validators.required]),
        parentId: new FormControl('', [Validators.required]),
        description: new FormControl('', [Validators.maxLength(255)]),
        isForPlan: new FormControl('')
    });

    constructor(
        public dialogRef: MatDialogRef<OutcomeEntryModalComponent>,
        protected resource: OutcomeEntryResource,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected utilService: UtilService,
        protected ref: ChangeDetectorRef,
        protected serviceResource: ServiceResource,
        protected currencyResource: CurrencyResource,
        protected subcontractorResource: SubcontractorResource,
        protected supplierResource: SupplierResource,
        protected timesheetCategoryResource: TimesheetCategoryResource,
        public converterService: ConverterService,
        @Inject(MAT_DIALOG_DATA) data: OutcomeEntryModalParams
    ) {
        super();

        if (data && data.forPsr) {
            this.form.controls['isForPlan'].disable();
            this.form.controls['timesheetCategoryId'].disable();
            if (!isNil(data.entity.id)) {
                this.form.controls['outcomeEntryType'].disable();
            } else {
                this.form.controls['outcomeEntryType'].enable();
            }
        } else {
            this.form.controls['isForPlan'].enable();
            this.form.controls['timesheetCategoryId'].enable();
            this.form.controls['outcomeEntryType'].enable();
        }

        super.show(data);

        this.calcParentLoadDataCallback();

        this.serviceResource.getLocalizedEnumOptions('kabanchik.system.outcomeentry.domain.OutcomeEntryType')
            .subscribe({
                next: (response: RestResponseModel) => {
                    this.outcomeEntryTypeOptions = response.data;
                }
            });
    }

    getTimesheetCategoryLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.timesheetCategoryResource.getManagedTimesheetCategories(false, term, limit, offset);
    }

    getCurrencyLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.currencyResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    getSubcontractorLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.subcontractorResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    getSupplierLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.supplierResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    typeChanged(): void {
        this.calcParentLoadDataCallback();
        this.form.get('parentId').setValue(null);
    }

    // @Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();
        data.date = data.date.valueOf();
        return data;
    }

    private calcParentLoadDataCallback(): void {
        const type: OutcomeEntryType | LocalizedEnum | string = this.form.getRawValue().outcomeEntryType;
        if (type === OutcomeEntryType.SUB_CONTRACTOR) {
            this.parentLoadDataCallback = this.getSubcontractorLoadDataCallback;
        } else if (type === OutcomeEntryType.SUPPLIER) {
            this.parentLoadDataCallback = this.getSupplierLoadDataCallback;
        } else if (type === 'SUB_CONTRACTOR') {
            this.parentLoadDataCallback = this.getSubcontractorLoadDataCallback;
        } else if (type === 'SUPPLIER') {
            this.parentLoadDataCallback = this.getSupplierLoadDataCallback;
        } else if (type instanceof LocalizedEnum) {
            if (type.value === 'SUB_CONTRACTOR') {
                this.parentLoadDataCallback = this.getSubcontractorLoadDataCallback;
            } else if (type.value === 'SUPPLIER') {
                this.parentLoadDataCallback = this.getSupplierLoadDataCallback;
            }
        } else {
            throw Error('Unexpected type');
        }
    }

}
