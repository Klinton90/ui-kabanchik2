import { Component, ChangeDetectorRef, ViewChild, ChangeDetectionStrategy, OnDestroy, OnInit } from '@angular/core';
import { OutcomeEntryResource } from './outcome-entry.resource';
import { Subscription, timer } from 'rxjs';
import { OutcomeEntryModel } from './outcome-entry.model';
import { BatchJobMonitorResource } from 'app/module/core/resource/batch-job-monitor.resource';
import { JobExecution, JobStatus } from 'app/module/core/model/job-execution.model';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { finalize } from 'rxjs/operators';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import {
    CustomAgGridOptionsModel,
    ColumnObservableType,
    CustomGridReadyEvent,
    CustomApi
} from 'app/module/grid-shared/model/ag-grid-options.model';
import { Validators } from '@angular/forms';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { CurrencyResource } from '../currency/currency.resource';
import { FileUpload } from 'primeng/fileupload';

@Component({
    moduleId: module.id,
    templateUrl: 'outcome-entry-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutcomeEntryListingComponent implements OnInit, OnDestroy {

    @ViewChild(FileUpload, {static: false}) fileUpload: FileUpload;

    intervalSubscriptions: Map<number, Subscription> = new Map<number, Subscription>();

    gridOptions: CustomAgGridOptionsModel;

    private subscriptions: Subscription[] = [];

    private customApi: CustomApi;

    constructor(
        private resource: OutcomeEntryResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private batchJobMonitorResource: BatchJobMonitorResource,
        private alertService: AlertService,
        private serviceResource: ServiceResource,
        private timesheetCategoryResource: TimesheetCategoryResource,
        private currencyResource: CurrencyResource,
    ) {
        this.batchJobMonitorResource.getActiveJobExecutionsByName('csvImportJob')
            .subscribe((jobExecutions: JobExecution[]) => {
                if (jobExecutions.length) {
                    this.alertService.post({
                        body: 'ui.message.default.upload.activeJobsExist',
                        type: AlertType.WARNING,
                        params: [jobExecutions.length]
                    });
                    jobExecutions.forEach((jobExecution: JobExecution) => this.refreshJobStatus(jobExecution));
                }
            });
    }

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'outcomeEntryListingGrid',
                resource: this.resource,
                newItemConstructor: () => new OutcomeEntryModel(),
                buttons: [
                    {
                        title: 'ui.text.upload.syncUpload',
                        classList: 'btn btn-sm btn-primary fa fa-redo',
                        click: () => this.refreshJobsStatus,
                        hidden: () => !this.intervalSubscriptions.size
                    },
                    {
                        title: 'ui.text.upload.downloadExample',
                        classList: 'btn btn-sm btn-primary fa fa-download',
                        click: () => window.open(this.resource.getDownloadExamplePath(), '_blank')
                    }
                ]
            },
            editType: '',
            onGridReady: (event: CustomGridReadyEvent) => this.customApi = event.customApi,
            columnDefs: [
                {
                    headerName: 'ui.text.outcomeEntry.amount',
                    field: 'amount',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(9999999.99)]
                    }
                },
                {
                    headerName: 'ui.text.outcomeEntry.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.outcomeEntry.date',
                    field: 'date',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.outcomeEntry.outcomeEntryType',
                    field: 'outcomeEntryType',
                    autoGenerateContext: {
                        type: ColumnObservableType.LOCALIZED_ENUM,
                        observable:  this.serviceResource.getLocalizedEnumOptions('kabanchik.system.outcomeentry.domain.OutcomeEntryType')
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.timesheetCategory.title',
                    field: 'timesheetCategoryId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'timesheetCategoryName',
                        infiniteProps: {
                            name: 'timesheetCategory',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['name'],
                            editorOptionsResource: this.timesheetCategoryResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.currency.title',
                    field: 'currencyId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'currencyName',
                        infiniteProps: {
                            name: 'currency',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['id', 'name'],
                            editorOptionsResource: this.currencyResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.outcomeEntry.parent',
                    field: 'parentId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'parentName',
                        infiniteProps: {
                            name: 'parent',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['name'],
                            filterMethod: this.resource.getUniqueParents,
                            editorOptionsMethod: this.resource.getParents
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.outcomeEntry.isForPlan',
                    field: 'isForPlan',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, false)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    csvImport(file: File): void {
        this.resource.csvImport(file)
            .pipe(
                finalize(() => this.fileUpload.clear())
            )
            .subscribe((response: RestResponseModel) => {
                this.alertService.post({
                    body: 'ui.message.default.upload.started',
                    type: AlertType.SUCCESS
                });
                this.refreshJobStatus(new JobExecution(response.data));
            }
        );
    }

    private refreshJobsStatus = (): void => {
        Array.from(this.intervalSubscriptions.keys()).forEach((id: number) => {
            this.intervalUnsubscribe(id);
            this.batchJobMonitorResource.getJobExecutionById(id)
                    .subscribe((_jobExecution: JobExecution) => this.refreshJobStatus(_jobExecution));
        });
    }

    private refreshJobStatus(jobExecution: JobExecution): void {
        if (jobExecution.isRunning) {
            this.intervalUnsubscribe(jobExecution.id);
            const intervalSubscription = timer(10000, 10000)
                    .subscribe(() => this.batchJobMonitorResource.getJobExecutionById(jobExecution.id)
                    .subscribe((_jobExecution: JobExecution) => this.refreshJobStatus(_jobExecution)));
            this.intervalSubscriptions.set(jobExecution.id, intervalSubscription);
            this.customApi.refreshUi();
        } else {
            if ([
                JobStatus.STARTED,
                JobStatus.STARTING,
                JobStatus.ABANDONED,
                JobStatus.FAILED,
                JobStatus.UNKNOWN,
                JobStatus.STOPPED
            ].indexOf(jobExecution.status) > -1) {
                this.alertService.post({
                    body: 'ui.message.default.upload.finished.error.title',
                    type: AlertType.DANGER
                });
            } else {
                if (jobExecution.errorsCount) {
                    this.alertService.post({
                        title: 'ui.message.default.upload.finished.success.title',
                        body: 'ui.message.default.upload.finished.success.bodyWithErrors',
                        params: [jobExecution.errorsCount],
                        type: AlertType.INFO,
                        links: [{
                            link: this.batchJobMonitorResource.getDownloadPath(jobExecution.id),
                            linkIcon: 'fa-download',
                            linkCloseOnClick: true
                        }]
                    });
                } else {
                    this.alertService.post({
                        title: 'ui.message.default.upload.finished.success.title',
                        body: 'ui.message.default.upload.finished.success.bodyNoErrors',
                        type: AlertType.INFO
                    });
                }
            }

            this. intervalUnsubscribe(jobExecution.id);
        }
    }

    private intervalUnsubscribe(id: number): void {
        if (this.intervalSubscriptions.has(id)) {
            this.intervalSubscriptions.get(id).unsubscribe();
            this.intervalSubscriptions.delete(id);
            this.customApi.refreshUi();
        }
    }

}
