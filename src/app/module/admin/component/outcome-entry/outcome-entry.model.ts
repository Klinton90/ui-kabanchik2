import { LocalizedEnum } from '../../../shared/model/localized-enum.model';
import { IDomainModel } from '../../../core/model/i-domain.model';

export class OutcomeEntrySimpleModel implements IDomainModel<number> {
    id: number = null;
    date: Date = '' as any;
    timesheetCategoryId: number = null;
    amount: number = 0;
    description: string = '';
    currencyId: string = null;
    outcomeEntryType: OutcomeEntryType | LocalizedEnum | string = 'SUB_CONTRACTOR';
    parentId: number = null;
    isForPlan: boolean = false;
}

export class OutcomeEntryModel extends OutcomeEntrySimpleModel {
    timesheetCategoryName: string = '';
    currencyName: string = '';
    parentName: string = '';
}

export enum OutcomeEntryType {
    SUB_CONTRACTOR,
    SUPPLIER
}
