import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { Pageable } from '../../../core/model/pageable.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { isNil } from 'lodash';
import { StaticService } from 'app/module/core/service/static.service';
import { FilterMetadata } from 'primeng/api/filtermetadata';

@Injectable({
    providedIn: 'root'
})
export class OutcomeEntryResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('outcomeEntry');
    }

    getDownloadExamplePath() {
        return StaticService.backendHref + this._preparePath('/example.csv');
    }

    findByGenericFilters(showActive: boolean, pageable: Pageable, filters: {[s: string]: FilterMetadata}): Observable<any> {
        return super.findByGenericFilters(showActive, pageable, filters).pipe(
            map((response: RestResponseModel) => {
                response.data.content = this.responseConverter(response.data.content);
                return response;
            })
        );
    }

    findLike(showActive: boolean, pageable: Pageable, likeValue: string): Observable<any> {
        return super.findLike(showActive, pageable, likeValue).pipe(
            map((response: RestResponseModel) => {
                response.data.content = this.responseConverter(response.data.content);
                return response;
            })
        );
    }

    findAllByTimesheetCategoryId(timesheetCategoryId: number, forPlan?: boolean): Observable<any> {
        let params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());

        if (!isNil(forPlan)) {
            params = params.append('forPlan', forPlan.toString());
        }

        return this._executeRequest({
            url: '/getAllByTimesheetCategoryId',
            method: 'GET',
            params: params
        }).pipe(
            map((response: RestResponseModel) => this.responseConverter(response.data))
        );
    }

    getUniqueParents = (queryString: string, limit: number, offset?: number): Observable<any> => {
        let params: HttpParams = new HttpParams();

        if (queryString) {
            params = params.append('queryString', queryString);
        }

        if (limit && limit > 0) {
            params = params.append('limit', limit.toString());
        }

        if (offset && offset > 0) {
            params = params.append('offset', offset.toString());
        }

        return this._executeRequest({
            url: '/getUniqueParents',
            method: 'GET',
            params: params
        });
    }

    getParents = (queryString: string, limit: number, offset?: number): Observable<any> => {
        let params: HttpParams = new HttpParams();

        if (queryString) {
            params = params.append('queryString', queryString);
        }

        if (limit && limit > 0) {
            params = params.append('limit', limit.toString());
        }

        if (offset && offset > 0) {
            params = params.append('offset', offset.toString());
        }

        return this._executeRequest({
            url: '/getParents',
            method: 'GET',
            params: params
        });
    }

    csvImport(file: File): Observable<any> {
        const formData: FormData = new FormData();
        formData.append('file', file);

        return this._executeRequest({
            url: '/csvImport',
            method: 'POST'
        }, formData);
    }

    private responseConverter(response: any[]): any[] {
        response.forEach((row) => {
            const date: Date = new Date(row.date);
            row.date = date;
        });

        return response;
    }

}
