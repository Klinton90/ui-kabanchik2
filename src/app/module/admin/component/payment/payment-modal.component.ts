import { Component, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { PaymentModel } from 'app/module/admin/component/payment/payment.model';
import { Observable } from 'rxjs';
import { CurrencyModel } from '../currency/currency.model';
import { SelectItem } from 'primeng/api/selectitem';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PaymentResource } from 'app/module/admin/component/payment/payment.resource';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { UtilService } from 'app/module/core/service/util.service';
import { CurrencyResource } from 'app/module/admin/component/currency/currency.resource';
import { ConverterService } from 'app/module/core/service/converter.service';
import { StaticService } from 'app/module/core/service/static.service';
import { InvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export class PaymentModalParams implements FormAwareParams<PaymentModel> {
    entity: PaymentModel;
    forPsr?: boolean;
}

@Component({
    moduleId: module.id,
    templateUrl: 'payment-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentModalComponent extends FormAwareModal<PaymentModalComponent, PaymentModel> {

    modalInit: Observable<any>;
    currencies: CurrencyModel[] = [];
    currencyOptions: SelectItem[] = [];
    invoices: InvoiceModel[] = [];
    invoiceOptions: SelectItem[] = [];

    newFormEntityInstance: PaymentModel = new PaymentModel();

    form: FormGroup = new FormGroup({
        id: new FormControl(''),
        date: new FormControl('', [Validators.required]),
        amount: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(999999999.99)]),
        currencyId: new FormControl('', [Validators.required]),
        invoiceId: new FormControl('', [Validators.required]),
        description: new FormControl('', [Validators.maxLength(255)]),
        isForPlan: new FormControl()
    });

    constructor(
        public dialogRef: MatDialogRef<PaymentModalComponent>,
        protected resource: PaymentResource,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected utilService: UtilService,
        protected currencyResource: CurrencyResource,
        public converterService: ConverterService,
        protected ref: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) data: PaymentModalParams
    ) {
        super();

        if (!data.entity.invoiceId) {
            throw new Error('`invoiceId` is required');
        }

        if (data.forPsr) {
            this.form.controls['isForPlan'].disable();
        } else {
            this.form.controls['isForPlan'].enable();
        }

        super.show(data);
    }

    getCurrencyLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.currencyResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    // Overwrite
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();
        data.date = data.date.valueOf();
        return data;
    }

}
