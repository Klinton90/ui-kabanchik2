import { IDomainModel } from '../../../core/model/i-domain.model';

export class PaymentModel implements IDomainModel<number> {

    id: number = null;
    date: Date = '' as any;
    amount: number = 0;
    description: string = '';
    isForPlan: boolean = true;
    currencyId: string = null;
    invoiceId: number = 0;

}
