import { Injectable } from '@angular/core';
import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient } from '@angular/common/http';
import { Pageable } from '../../../core/model/pageable.model';
import { Observable } from 'rxjs';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { map } from 'rxjs/operators';
import { FilterMetadata } from 'primeng/api/filtermetadata';

@Injectable({
    providedIn: 'root'
})
export class PaymentResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('payment');
    }

    public findByGenericFilters(showActive: boolean, pageable: Pageable, filters: {[s: string]: FilterMetadata}): Observable<any> {
        return super.findByGenericFilters(showActive, pageable, filters).pipe(
            map((response: RestResponseModel) => {
                response.data.content = this.responseConverter(response.data.content);
                return response;
            })
        );
    }

    public findLike(showActive: boolean, pageable: Pageable, likeValue: string): Observable<any> {
        return super.findLike(showActive, pageable, likeValue).pipe(
            map((response: RestResponseModel) => {
                response.data.content = this.responseConverter(response.data.content);
                return response;
            })
        );
    }

    private responseConverter(response: any[]): any[] {
        response.forEach((row) => {
            const date: Date = new Date(row.date);
            row.date = date;
        });

        return response;
    }

}
