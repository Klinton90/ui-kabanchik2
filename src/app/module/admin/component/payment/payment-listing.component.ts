import { Component, ChangeDetectorRef, OnInit, ChangeDetectionStrategy, OnDestroy, Inject } from '@angular/core';
import { PaymentResource } from 'app/module/admin/component/payment/payment.resource';
import { PaymentModel } from 'app/module/admin/component/payment/payment.model';
import { Subscription } from 'rxjs';
import { CustomAgGridOptionsModel, ColumnObservableType, AgFilterMetadata } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Validators } from '@angular/forms';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { CurrencyResource } from '../currency/currency.resource';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatchMode } from 'app/module/shared/model/match-mode.model';

export class PaymentListingModalParams {
    invoiceId: number;
}

@Component({
    moduleId: module.id,
    templateUrl: 'payment-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PaymentListingComponent implements OnInit, OnDestroy {

    private data: PaymentListingModalParams;

    private subscriptions: Subscription[] = [];

    gridOptions: CustomAgGridOptionsModel;

    constructor(
        public dialogRef: MatDialogRef<PaymentListingComponent>,
        private resource: PaymentResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private currencyResource: CurrencyResource,
        @Inject(MAT_DIALOG_DATA) data: PaymentListingModalParams
    ) {
        this.data = data;
    }

    ngOnInit(): void {
        const extraFilter: Map<string, AgFilterMetadata> = new Map<string, AgFilterMetadata>();
        extraFilter.set('invoiceId', {
            filter: this.data.invoiceId,
            type: MatchMode.EQUALS,
            filterType: 'id'
        });
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'invoiceListingGrid',
                resource: this.resource,
                extraFilter: extraFilter,
                newItemConstructor: () => {
                    const result: PaymentModel = new PaymentModel()
                    result.invoiceId = this.data.invoiceId;
                    return result;
                },
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.payment.amount',
                    field: 'amount',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(999999999.99)]
                    }
                },
                {
                    headerName: 'ui.text.invoice.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.payment.date',
                    field: 'date',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.currency.title',
                    field: 'currencyId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'currencyName',
                        infiniteProps: {
                            name: 'currency',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['id', 'name'],
                            editorOptionsResource: this.currencyResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.payment.isForPlan',
                    field: 'isForPlan',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, false)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

}
