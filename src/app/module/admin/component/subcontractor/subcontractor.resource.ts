import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseResource } from 'app/module/core/resource/base.resource';

@Injectable({
    providedIn: 'root'
})
export class SubcontractorResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('subcontractor');
    }

    public getForPSR(timesheetCategoryId: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());

        return this._executeRequest({
            url: '/getForPSR',
            method: 'GET',
            params: params
        });
    }

    public getForProgressModal(
        timesheetCategoryId: number,
        queryString?: string,
        limit?: number,
        offset?: number
    ): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());
        return this._executeRequest({
            url: '/getForProgressModal',
            method: 'GET',
            params: BaseResource.setOptionalPageableParams(params, queryString, limit, offset)
        });
    }

}
