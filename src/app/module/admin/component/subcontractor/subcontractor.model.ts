import { OutcomeEntryParent } from '../../model/outcome-entry-parent.model';
import { ProgressParent } from '../../model/progress-parent.model';
import { LocalizedEnum } from 'app/module/shared/model/localized-enum.model';

export class SubcontractorModel implements OutcomeEntryParent, ProgressParent {
    id: number = null;
    name: string = '';
    description: string = '';
    isActive: boolean = true;
    governmentId: string = '';
    progressReportable: boolean = true;
    progressType: LocalizedEnum = null;
    outcomeEntryType: LocalizedEnum = null;
}
