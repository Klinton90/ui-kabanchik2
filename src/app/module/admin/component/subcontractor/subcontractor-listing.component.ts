import { Component, ChangeDetectorRef, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { SubcontractorResource } from './subcontractor.resource';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { SubcontractorModel } from './subcontractor.model';
import { Validators } from '@angular/forms';
import { exactLengthValidator } from 'app/module/core/validator/exact-length.validator';

@Component({
    moduleId: module.id,
    templateUrl: 'subcontractor-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubcontractorListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        private resource: SubcontractorResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'subcontractorListingGrid',
                resource: this.resource,
                newItemConstructor: () => new SubcontractorModel(),
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.subcontractor.name',
                    field: 'name',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.maxLength(50)]
                    }
                },
                {
                    headerName: 'ui.text.subcontractor.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.subcontractor.governmentId',
                    field: 'governmentId',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.required, exactLengthValidator([8, 10])]
                    }
                },
                {
                    headerName: 'ui.text.subcontractor.progressReportable',
                    field: 'progressReportable',
                    autoGenerateContext: {
                        type: ColumnObservableType.BOOLEAN
                    },
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }
}
