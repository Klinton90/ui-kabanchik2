import { HttpClient } from '@angular/common/http';
import { RestResource } from 'app/module/core/resource/rest.resource';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TimesheetRateResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('timesheetRate');
    }

}
