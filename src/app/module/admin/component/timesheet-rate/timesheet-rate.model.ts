import { Domain } from '../../../core/model/i-domain.model';

export class TimesheetRate implements Domain<number> {
    id: number = 0;
    rate: number = 0;
    internalRate: number = 0;
    isActive: boolean = true;
    timesheetRateId: number = null;
    from: number;
    to: number;
}
