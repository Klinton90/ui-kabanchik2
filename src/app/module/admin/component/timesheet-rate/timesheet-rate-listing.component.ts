import { Component, ChangeDetectionStrategy, OnInit, OnDestroy, ChangeDetectorRef, Inject } from '@angular/core';
import { CustomAgGridOptionsModel, AgFilterMetadata, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TimesheetRateResource } from './timesheet-rate.resource';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
// import { ConverterService } from 'app/module/core/service/converter.service';
import { MatchMode } from 'app/module/shared/model/match-mode.model';
import { TimesheetAccess } from '../timesheet-access/timesheet-access.model';
import { TimesheetRate } from './timesheet-rate.model';
import { Validators } from '@angular/forms';
import { periodValidator } from 'app/module/core/validator/period.validator';

interface TimesheetRateListingComponentData {
    timesheetAccess: TimesheetAccess;
}

@Component({
    moduleId: module.id,
    templateUrl: 'timesheet-rate-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimesheetRateListingComponent implements OnInit, OnDestroy {

    gridOptions: CustomAgGridOptionsModel;

    subscriptions: Subscription[] = [];

    constructor(
        public dialogRef: MatDialogRef<any>,
        private resource: TimesheetRateResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        // private converterService: ConverterService,
        @Inject(MAT_DIALOG_DATA) public data: TimesheetRateListingComponentData
    ) {
    }

    ngOnInit(): void {
        const extraFilter: Map<string, AgFilterMetadata> = new Map<string, AgFilterMetadata>();
        extraFilter.set('timesheetAccessId', {
            filter: this.data.timesheetAccess.id,
            type: MatchMode.EQUALS,
            filterType: 'id'
        });
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'timesheetRateListingGrid',
                resource: this.resource,
                extraFilter: extraFilter,
                newItemConstructor: () => {
                    const result: any = new TimesheetRate();
                    result.from = '';
                    result.to = '';
                    result.timesheetAccessId = this.data.timesheetAccess.id;
                    return result;
                },
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.timesheetRate.rate',
                    field: 'rate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.min(0.01), Validators.max(1000.00)]
                    },
                    valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                },
                {
                    headerName: 'ui.text.timesheetRate.internalRate',
                    field: 'internalRate',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.min(0.01), Validators.max(1000.00)]
                    },
                    valueFormatter: CustomAgGridService.getCurrencyValueFormatter
                },
                {
                    headerName: 'ui.text.timesheetRate.from',
                    field: 'from',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: (data: any) => [
                            Validators.required,
                            periodValidator(data, true)
                        ]
                    }
                },
                {
                    headerName: 'ui.text.timesheetRate.to',
                    field: 'to',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: (data: any) => [
                            Validators.required,
                            periodValidator(data, false)
                        ]
                    }
                }
            ]
        };

        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, false)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }
}
