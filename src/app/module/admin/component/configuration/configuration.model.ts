export class ConfigurationModel {
    timesheetMode: boolean = false;
    allowedAuthFailureCount: number = 5;
    supportEmail: string = "";
    userCron1: string = "";
    userCron2: string = "";
    managerCron1: string = "";
    managerCron2: string = "";
    bankCurrencyCron: string = "";
    userWeeklyWorkloadMax: number = 85;
    executorWeeklyWorkloadMax: number = 65;
}