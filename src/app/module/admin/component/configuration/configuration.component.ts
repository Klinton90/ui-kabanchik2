import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../../core/component/alert/alert.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UtilService } from '../../../core/service/util.service';
import { ConfigurationModel } from './configuration.model';
import { ConfigurationResource } from './configuration.resource';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';

@Component({
    moduleId: module.id,
    templateUrl: 'configuration.component.html'
})
export class ConfigurationComponent implements OnInit {

    public form: FormGroup = this.fb.group({
        timesheetMode: [''],
        allowedAuthFailureCount: ['', [Validators.required, Validators.min(0), Validators.max(99)]],
        supportEmail: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(255), Validators.email]],
        userCron1: ['', [Validators.maxLength(255)]],
        userCron2: ['', [Validators.maxLength(255)]],
        managerCron1: ['', [Validators.maxLength(255)]],
        managerCron2: ['', [Validators.maxLength(255)]],
        bankCurrencyCron: ['', [Validators.maxLength(255)]],
        userWeeklyWorkloadMax: ['', [Validators.min(0), Validators.max(100)]],
        executorWeeklyWorkloadMax: ['', [Validators.min(0), Validators.max(100)]],
    });

    constructor(
        private fb: FormBuilder,
        private configurationResource: ConfigurationResource,
        private notificationService: AlertService,
        private utilService: UtilService
    ) {}

    ngOnInit(): void {
        this.configurationResource.get().subscribe((config: ConfigurationModel) => {
            this.utilService.setFormGroupValues(this.form, config);
        });
    }

    save(): void {
        this.configurationResource.save(this.form.value).subscribe(
            () => {
                this.notificationService.post({
                    type: AlertType.SUCCESS,
                    body: 'ui.message.default.grid.save.body.edit'
                });
            }
        );
    }

}
