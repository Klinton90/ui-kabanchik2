import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from 'rxjs';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { StaticService } from "../../../core/service/static.service";
import { ConfigurationModel } from "./configuration.model";
import { BaseResource } from "../../../core/resource/base.resource";
import { map } from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class ConfigurationResource extends BaseResource {

    constructor(
        protected http: HttpClient
    ){
        super("configuration");
    }

    public get(): Observable<ConfigurationModel> {
        if (StaticService.cachedConfiguration) {
            return of(StaticService.cachedConfiguration);
        } else {
            return this._executeRequest({
                url: "/get",
                method: "GET"
            }).pipe(
                map((response: RestResponseModel) => {
                    StaticService.cachedConfiguration = response.data;
                    return response.data;
                })
            );
        }
    }

    public save(data: ConfigurationModel): Observable<any> {
        const o: Observable<any> = this._executeRequest({
            url: "/save",
            method: "PUT"
        }, data);

        o.subscribe(() => {
            StaticService.cachedConfiguration = data;
        });

        return o;
    }

}