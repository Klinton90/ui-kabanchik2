import { IDomainModel } from '../../../core/model/i-domain.model';
import { PaymentModel } from '../payment/payment.model';

export enum InvoiceStatus {
    PENDING,
    DELIVERABLES_READY,
    READY_FOR_INVOICING,
    INVOICE_ISSUED,
    CLIENT_CONFIRMED,
    WAITING_PAYMENT_FROM_END_CLIENT,
    WAITING_FOR_PAYMENT,
    PAID
}

export class BaseInvoiceModel {
    datePlanned: Date = '' as any;
    datePosted: Date = '' as any;
    amount: number = 0;
    description: string = '';
    status: InvoiceStatus = null;
}

export class InvoiceModel extends BaseInvoiceModel implements IDomainModel<number> {

    id: number = null;

    timesheetCategoryId: number = null;
    currencyId: string = null;
    customerId: number = null;

    public static isAssignableFrom(data: any): data is InvoiceModel {
        return !!data.datePlanned;
    }

    public static getDate(invoice: BaseInvoiceModel, isForPlan: boolean): Date {
        if (isForPlan) {
            return invoice.datePlanned;
        } else {
            return invoice.datePosted || invoice.datePlanned;
        }
    }

    public static getMinDate(invoice: BaseInvoiceModel) {
        if (invoice.datePosted) {
            return invoice.datePosted.getTime() < invoice.datePlanned.getTime() ? invoice.datePosted : invoice.datePlanned;
        } else {
            return invoice.datePlanned;
        }
    }

    public static getMaxDate(invoice: BaseInvoiceModel) {
        if (invoice.datePosted) {
            return invoice.datePosted.getTime() > invoice.datePlanned.getTime() ? invoice.datePosted : invoice.datePlanned;
        } else {
            return invoice.datePlanned;
        }
    }

}

export class ExtendedInvoiceModel extends InvoiceModel {
    paymentDTOS: PaymentModel[] = [];
}
