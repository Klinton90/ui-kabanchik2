import { Component, ChangeDetectorRef, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { InvoiceResource } from 'app/module/admin/component/invoice/invoice.resource';
import { InvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { PaymentListingComponent } from '../payment/payment-listing.component';
import { Subscription } from 'rxjs';
import { CustomAgGridOptionsModel, ColumnObservableType } from 'app/module/grid-shared/model/ag-grid-options.model';
import { Validators } from '@angular/forms';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { TimesheetCategoryResource } from '../timesheet-category/timesheet-category.resource';
import { CurrencyResource } from '../currency/currency.resource';
import { CustomAgGridService } from 'app/module/grid-shared/service/custom-ag-grid.service';
import { CustomerResource } from '../customer/customer.resource';
import { ICellRendererParams } from 'ag-grid-community';
import { MatDialog } from '@angular/material/dialog';

@Component({
    moduleId: module.id,
    templateUrl: 'invoice-listing.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoiceListingComponent implements OnInit, OnDestroy {

    private subscriptions: Subscription[] = [];

    gridOptions: CustomAgGridOptionsModel;

    constructor(
        public resource: InvoiceResource,
        private ref: ChangeDetectorRef,
        private customAgGridService: CustomAgGridService,
        private dialog: MatDialog,
        private serviceResource: ServiceResource,
        private timesheetCategoryResource: TimesheetCategoryResource,
        private currencyResource: CurrencyResource,
        private customerResource: CustomerResource
    ) {}

    ngOnInit(): void {
        const gridOptions: CustomAgGridOptionsModel = {
            context: {
                stickyName: 'invoiceListingGrid',
                resource: this.resource,
                newItemConstructor: () => new InvoiceModel(),
                actionsColumnContext: {
                    actionColumnButtons: [
                        {
                            title: 'ui.text.payment.title',
                            classList: 'btn btn-xs btn-primary fa fa-money-check-alt',
                            click: this.showPayments
                        }
                    ]
                }
            },
            editType: '',
            columnDefs: [
                {
                    headerName: 'ui.text.invoice.amount',
                    field: 'amount',
                    autoGenerateContext: {
                        type: ColumnObservableType.NUMBER
                    },
                    cellEditorParams: {
                        validators: [Validators.required, Validators.min(0.01), Validators.max(9999999.99)]
                    }
                },
                {
                    headerName: 'ui.text.invoice.description',
                    field: 'description',
                    autoGenerateContext: {
                        type: ColumnObservableType.TEXT
                    },
                    cellEditorParams: {
                        validators: [Validators.maxLength(255)]
                    }
                },
                {
                    headerName: 'ui.text.invoice.status',
                    field: 'status',
                    autoGenerateContext: {
                        type: ColumnObservableType.LOCALIZED_ENUM,
                        observable:  this.serviceResource.getLocalizedEnumOptions('kabanchik.system.invoice.domain.InvoiceStatus')
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.invoice.datePlanned',
                    field: 'datePlanned',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.invoice.datePosted',
                    field: 'datePosted',
                    autoGenerateContext: {
                        type: ColumnObservableType.DATE
                    },
                },
                {
                    headerName: 'ui.text.timesheetCategory.title',
                    field: 'timesheetCategoryId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'timesheetCategoryName',
                        infiniteProps: {
                            name: 'timesheetCategory',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['name'],
                            editorOptionsResource: this.timesheetCategoryResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.currency.title',
                    field: 'currencyId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'currencyName',
                        infiniteProps: {
                            name: 'currency',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['id', 'name'],
                            editorOptionsResource: this.currencyResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
                {
                    headerName: 'ui.text.customer.title',
                    field: 'customerId',
                    autoGenerateContext: {
                        type: ColumnObservableType.DOMAIN2,
                        otherField: 'customerName',
                        infiniteProps: {
                            name: 'customer',
                            orderByFields: ['isActive', 'name'],
                            filterByFieldNames: ['name'],
                            editorOptionsResource: this.customerResource,
                        }
                    },
                    cellEditorParams: {
                        validators: [Validators.required]
                    }
                },
            ]
        };
        const subscription: Subscription = this.customAgGridService.prepareGrid(gridOptions, false)
            .subscribe((_gridOptions: CustomAgGridOptionsModel) => {
                this.gridOptions = _gridOptions;
                this.ref.detectChanges();
            });
        this.subscriptions.push(subscription);
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe);
    }

    showPayments = (context: ICellRendererParams): void => {
        const invoice: InvoiceModel = context.data;
        this.dialog.open(PaymentListingComponent, {
            width: 'inherit',
            maxWidth: '90vw',
            data: {invoiceId: invoice.id}
        });
    }

}
