import { Injectable } from '@angular/core';
import { RestResource } from '../../../core/resource/rest.resource';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RestResponseModel } from '../../../core/model/rest-response.model';
import { PaymentModel } from '../payment/payment.model';
import { IGetRowsParams, GridApi } from 'ag-grid-community';
import { InvoiceModel } from './invoice.model';
import { isDate } from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class InvoiceResource extends RestResource {

    constructor(
        protected http: HttpClient
    ) {
        super('invoice');
    }

    public findAllByTimesheetCategoryId(timesheetCategoryId: number): Observable<any> {
        const params: HttpParams = new HttpParams()
            .append('timesheetCategoryId', timesheetCategoryId.toString());

        return this._executeRequest({
            url: '/getAllByTimesheetCategoryId',
            method: 'GET',
            params: params
        }).pipe(
            map((response: RestResponseModel) => this.responseConverter(response.data))
        );
    }

    public findAllForDebtorList(
        requestParams: IGetRowsParams,
        api: GridApi,
        showOnlyActiveTimesheetCategories?: boolean
    ): Observable<any> {
        let params: HttpParams = this.getAgFullPageableParams(requestParams, api, false);

        if (showOnlyActiveTimesheetCategories) {
            params = params.append(
                'showOnlyActiveTimesheetCategories',
                showOnlyActiveTimesheetCategories && showOnlyActiveTimesheetCategories.toString());
        }

        return this._executeRequest({
            url: '/findAllForDebtorList',
            method: 'GET',
            params: params
        }).pipe(
            map((response: RestResponseModel) => this.transformRestResponseModel(response))
        );
    }

    public transformResponse(item: any): any {
        const datePlanned: Date = new Date(item.datePlanned);
        item.datePlanned = datePlanned;
        if (item.datePosted) {
            const datePosted: Date = new Date(item.datePosted);
            item.datePosted = datePosted;
        }
        if (item.paymentDTOS) {
            item.paymentDTOS.forEach((payment: PaymentModel) => {
                const paymentDate: Date = new Date(payment.date);
                payment.date = paymentDate;
            });
        }
        if (item.paymentDate) {
            item.paymentDate = new Date(item.paymentDate);
        }

        return item;
    }

    private responseConverter(response: any[]): any[] {
        response.forEach((row) => {
            this.transformResponse(row);
        });

        return response;
    }

    public transformRequest(data: InvoiceModel): any {
        if (isDate(data.datePlanned)) {
            (<any>data).datePlanned = data.datePlanned.getTime() - data.datePlanned.getTimezoneOffset() * 60 * 1000;
        }

        if (isDate(data.datePosted)) {
            if (data.datePosted) {
                (<any>data).datePosted = data.datePosted.getTime() - data.datePosted.getTimezoneOffset() * 60 * 1000;
            } else {
                data.datePosted = null;
            }
        }

        return data;
    }

}
