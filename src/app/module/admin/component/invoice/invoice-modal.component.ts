import { Component, ChangeDetectorRef, Inject, ChangeDetectionStrategy } from '@angular/core';
import { InvoiceModel } from 'app/module/admin/component/invoice/invoice.model';
import { InvoiceResource } from 'app/module/admin/component/invoice/invoice.resource';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { UtilService } from 'app/module/core/service/util.service';
import { ConverterService } from 'app/module/core/service/converter.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { CurrencyResource } from 'app/module/admin/component/currency/currency.resource';
import { TimesheetCategoryResource } from 'app/module/admin/component/timesheet-category/timesheet-category.resource';
import { CustomerResource } from 'app/module/admin/component/customer/customer.resource';
import { Observable } from 'rxjs';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { SelectItem } from 'primeng/api/selectitem';
import { StaticService } from 'app/module/core/service/static.service';
import { ServiceResource } from 'app/module/core/resource/service.resource';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';

export class InvoiceModalParams implements FormAwareParams<InvoiceModel> {
    entity: InvoiceModel;
    forPsr?: boolean;
}

@Component({
    moduleId: module.id,
    templateUrl: 'invoice-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InvoiceModalComponent extends FormAwareModal<InvoiceModalComponent, InvoiceModel> {

    statuses: SelectItem[];

    newFormEntityInstance: InvoiceModel = new InvoiceModel();

    form: FormGroup = new FormGroup({
        id: new FormControl(''),
        datePlanned: new FormControl('', [Validators.required]),
        datePosted: new FormControl(''),
        amount: new FormControl('', [Validators.required, Validators.min(0.01), Validators.max(999999999.99)]),
        description: new FormControl('', [Validators.maxLength(255)]),
        status: new FormControl('', [Validators.required]),
        timesheetCategoryId: new FormControl('', [Validators.required]),
        currencyId: new FormControl('', [Validators.required]),
        customerId: new FormControl('', [Validators.required]),
    });

    constructor(
        public dialogRef: MatDialogRef<InvoiceModalComponent>,
        protected resource: InvoiceResource,
        protected confirmationService: ConfirmationService,
        protected notificationService: AlertService,
        protected utilService: UtilService,
        protected currencyResource: CurrencyResource,
        public converterService: ConverterService,
        protected timesheetCategoryResource: TimesheetCategoryResource,
        protected customerResource: CustomerResource,
        protected ref: ChangeDetectorRef,
        protected serviceResource: ServiceResource,
        @Inject(MAT_DIALOG_DATA) data: InvoiceModalParams
    ) {
        super();

        this.show(data);

        this.serviceResource.getLocalizedEnumOptions('kabanchik.system.invoice.domain.InvoiceStatus')
            .subscribe({
                next: (response: RestResponseModel) => {
                    this.statuses = response.data;
                }
            });
    }

    // @Override
    show(params?: InvoiceModalParams): void {
        if (params && params.forPsr) {
            this.form.controls['timesheetCategoryId'].disable();
        } else {
            this.form.controls['timesheetCategoryId'].enable();
        }

        super.show(params);
    }

    getTimesheetCategoryLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.timesheetCategoryResource.getManagedTimesheetCategories(false, term, limit, offset);
    }

    getCurrencyLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.currencyResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    getCustomerLoadDataCallback = (term: string, limit: number, offset?: number): Observable<any> => {
        return this.customerResource.findLike(false, StaticService.createPageable(limit, offset), term);
    }

    // @Override
    protected getDataForSave(): any {
        const data: any = super.getDataForSave();

        data.datePlanned = data.datePlanned.valueOf();

        if (data.datePosted) {
            data.datePosted = data.datePosted.valueOf();
        } else {
            data.datePosted = null;
        }

        return data;
    }

}
