import { NgModule } from '@angular/core';
import { OutcomeEntryModalComponent } from './component/outcome-entry/outcome-entry-modal.component';
import { AppSharedModule } from '../shared/shared.module';
import { PaymentModalComponent } from './component/payment/payment-modal.component';
import { InvoiceModalComponent } from './component/invoice/invoice-modal.component';

@NgModule({
    imports: [
        // custom
        AppSharedModule
    ],
    declarations: [
        OutcomeEntryModalComponent,
        PaymentModalComponent,
        InvoiceModalComponent
    ],
    exports: [
        OutcomeEntryModalComponent,
        PaymentModalComponent,
        InvoiceModalComponent
    ],
    entryComponents: [
        InvoiceModalComponent,
        OutcomeEntryModalComponent,
        PaymentModalComponent,
    ]
})
export class AdminSharedModule {}
