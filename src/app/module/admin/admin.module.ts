import { StaticContentComponent } from './component/static-content/static-content.component';
import { NgModule, ComponentFactoryResolver } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppSharedModule } from '../shared/shared.module';
import { LocalizationListingComponent } from './component/localization/localization-listing.component';
import { LocalizationModalComponent } from './component/localization/localization-modal.component';
import { TimesheetCategoryListingComponent } from './component/timesheet-category/timesheet-category-listing.component';
import { TimesheetCategoryModalComponent } from './component/timesheet-category/timesheet-category-modal.component';
import { TimesheetAccessListingComponent } from './component/timesheet-access/timesheet-access-listing.component';
import { TimesheetAccessResource } from './component/timesheet-access/timesheet-access.resource';
import { FileUploadModule } from 'primeng/fileupload';
import { DepartmentListingComponent } from './component/department/department-listing.component';
import { TimesheetAccessBulkUsersAddModalComponent } from './component/timesheet-access/timesheet-access-bulk-users-add-modal.component';
import {
    TimesheetAccessBulkCategoriesAddModalComponent
} from './component/timesheet-access/timesheet-access-bulk-categories-add-modal.component';
import { ConfigurationComponent } from './component/configuration/configuration.component';
import { BonuspaymentListingComponent } from './component/bonuspayment/bonuspayment-listing.component';
import { HolidayListingComponent } from './component/holiday/holiday-listing.component';
import { ReminderModalComponent } from './component/user/reminder-modal.component';
import { CustomerListingComponent } from './component/customer/customer-listing.component';
import { SubcontractorListingComponent } from './component/subcontractor/subcontractor-listing.component';
import { SupplierListingComponent } from './component/supplier/supplier-listing.component';
import { CurrencyListingComponent } from './component/currency/currency-listing.component';
import { OutcomeEntryListingComponent } from './component/outcome-entry/outcome-entry-listing.component';
import { InvoiceListingComponent } from './component/invoice/invoice-listing.component';
import { PaymentListingComponent } from './component/payment/payment-listing.component';
import { GridSharedModule } from '../grid-shared/grid-shared.module';
import { GenericFieldComponent } from './component/generic-field/generic-field.component';
import { GenericFieldOptionComponent } from './component/generic-field-option/generic-field-option.component';
import { CoalescingComponentFactoryResolver } from 'app/service/coalescing-component-factory-resolver.service';
import { UserListingComponent } from './component/user/user-listing.component';
import { AuthGuardService } from './service/auth-guard.service';
import { CurrencyRateListingComponent } from './component/currency-rate/currency-rate-listing.component';
import { DepartmentAccessListingComponent } from './component/department-access/department-access-listing.component';
import {
    DepartmentAccessBulkCategoriesAddModalComponent
} from './component/department-access/department-access-bulk-categories-add-modal.component';
import {
    DepartmentAccessBulkDepartmentsddModalComponent
} from './component/department-access/department-access-bulk-departments-add-modal.component';
import { TimesheetRateListingComponent } from './component/timesheet-rate/timesheet-rate-listing.component';

const routes: Routes = [
    {
        path: 'user',
        component: UserListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'localization',
        component: LocalizationListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'timesheetCategory',
        component: TimesheetCategoryListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'timesheetAccess',
        component: TimesheetAccessListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'staticContent',
        component: StaticContentComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'department',
        component: DepartmentListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'configuration',
        component: ConfigurationComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'bonuspayment',
        component: BonuspaymentListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'holiday',
        component: HolidayListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'customer',
        component: CustomerListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'subcontractor',
        component: SubcontractorListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'supplier',
        component: SupplierListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'currency',
        component: CurrencyListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'outcomeEntry',
        component: OutcomeEntryListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'invoice',
        component: InvoiceListingComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'genericField',
        component: GenericFieldComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'genericFieldOption',
        component: GenericFieldOptionComponent,
        canActivate: [AuthGuardService]
    },
    {
        path: 'departmentAccess',
        component: DepartmentAccessListingComponent,
        canActivate: [AuthGuardService]
    }
];

@NgModule({
    imports: [
        // angular
        RouterModule.forChild(routes),

        // primeng
        FileUploadModule,

        // custom
        AppSharedModule,
        GridSharedModule,
    ],
    declarations: [
        LocalizationListingComponent,
        LocalizationModalComponent,
        TimesheetCategoryListingComponent,
        TimesheetCategoryModalComponent,
        TimesheetAccessListingComponent,
        TimesheetAccessBulkUsersAddModalComponent,
        TimesheetAccessBulkCategoriesAddModalComponent,
        StaticContentComponent,
        DepartmentListingComponent,
        ConfigurationComponent,
        BonuspaymentListingComponent,
        HolidayListingComponent,
        ReminderModalComponent,
        CustomerListingComponent,
        SubcontractorListingComponent,
        SupplierListingComponent,
        CurrencyListingComponent,
        OutcomeEntryListingComponent,
        InvoiceListingComponent,
        PaymentListingComponent,
        GenericFieldComponent,
        GenericFieldOptionComponent,
        UserListingComponent,
        CurrencyRateListingComponent,
        DepartmentAccessListingComponent,
        DepartmentAccessBulkCategoriesAddModalComponent,
        DepartmentAccessBulkDepartmentsddModalComponent,
        TimesheetRateListingComponent,
    ],
    providers: [
        TimesheetAccessResource,
        AuthGuardService
    ],
    entryComponents: [
        PaymentListingComponent,
        TimesheetCategoryModalComponent,
        CurrencyRateListingComponent,
        DepartmentAccessBulkCategoriesAddModalComponent,
        DepartmentAccessBulkDepartmentsddModalComponent,
        TimesheetAccessBulkUsersAddModalComponent,
        TimesheetAccessBulkCategoriesAddModalComponent,
        ReminderModalComponent,
        LocalizationModalComponent,
        TimesheetRateListingComponent,
    ]
})
export class AdminModule {

    constructor(
        coalescingResolver: CoalescingComponentFactoryResolver,
        localResolver: ComponentFactoryResolver
    ) {
        coalescingResolver.registerResolver(localResolver);
    }
}
