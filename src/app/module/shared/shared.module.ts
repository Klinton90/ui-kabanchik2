import { TableModule } from 'primeng/table';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { FieldErrorsPipe } from './pipe/field-errors.pipe';
import { KeysPipe } from './pipe/keys.pipe';
import { LanguagePipe } from './pipe/language.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FocusFirstInputDirective } from './directive/focus-first-input.directive';
import { BooleanPipe } from './pipe/boolean.pipe';
import { AtCellDirective } from './directive/at-cell.directive';
import { ButtonListComponent } from './component/button-list/button-list.component';
import { GenericFieldRendererComponent } from './component/generic-field-renderer/generic-field-renderer.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { ModalComponent } from './component/modal/modal.component';
import { MatInputWrapperComponent } from './component/mat-input-wrapper/mat-input-wrapper.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MAT_DATE_FORMATS, DateAdapter } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectWrapperComponent } from './component/mat-select-wrapper/mat-select-wrapper.component';
import { MatDatepickerWrapperComponent } from './component/mat-datepicker-wrapper/mat-datepicker-wrapper.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { LoginModalComponent } from './component/login/login-modal.component';
import { LoginComponent } from './component/login/login.component';
import { GetLabelByPatternPipe } from './pipe/get-label-by-pattern.pipe';
import { EditorComponent } from './component/editor/editor.component';
import { UserNamePipe } from './pipe/user-name.pipe';
import { NgSelectInfiniteWrapperComponent } from './component/ng-select-infinite-wrapper/ng-select-infinite-wrapper.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ChartsModule } from 'ng2-charts';
import { NgSelectFormFieldControlDirective } from './directive/ng-select-mat.directive';
import { MatNgSelectInfiniteWrapperComponent } from './component/mat-ng-select-infinite-wrapper/mat-ng-select-infinite-wrapper.component';
import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { FilterPipe } from './pipe/filter.pipe';
import { SharedModule } from 'primeng/api';

export const providers = [

];

@NgModule({
    imports: [
        // angular
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        // material
        MatExpansionModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        MatDialogModule,
        MatListModule,
        MatButtonToggleModule,
        MatSlideToggleModule,
        MatCardModule,
        MatTooltipModule,
        MatToolbarModule,
        MatSidenavModule,
        MatGridListModule,
        DragDropModule,
        MatMenuModule,

        // ngx-bootstrap
        ChartsModule,

        // ng-select
        NgSelectModule,

        // primeNg
        TableModule,
        SharedModule,
    ],
    declarations: [
        // pipes
        LanguagePipe,
        KeysPipe,
        BooleanPipe,
        GetLabelByPatternPipe,
        UserNamePipe,

        // custom
        FieldErrorsPipe,
        FilterPipe,
        FocusFirstInputDirective,
        AtCellDirective,
        ButtonListComponent,
        GenericFieldRendererComponent,

        NgSelectInfiniteWrapperComponent,
        MatNgSelectInfiniteWrapperComponent,
        NgSelectFormFieldControlDirective,

        ModalComponent,
        MatInputWrapperComponent,
        MatSelectWrapperComponent,
        MatDatepickerWrapperComponent,
        LoginComponent,
        LoginModalComponent,

        EditorComponent
    ],
    exports: [
        // angular
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        // material
        MatExpansionModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        MatDialogModule,
        MatListModule,
        MatButtonToggleModule,
        MatSlideToggleModule,
        MatCardModule,
        MatTooltipModule,
        MatToolbarModule,
        MatSidenavModule,
        MatGridListModule,
        DragDropModule,
        MatMomentDateModule,
        MatMenuModule,

        // ngx-bootstrap
        ChartsModule,

        // ng-select
        NgSelectModule,

        // pipes
        LanguagePipe,
        KeysPipe,
        BooleanPipe,
        GetLabelByPatternPipe,
        UserNamePipe,

        // primeng
        TableModule,
        SharedModule,

        // custom
        FieldErrorsPipe,
        FilterPipe,
        FocusFirstInputDirective,
        AtCellDirective,
        ButtonListComponent,
        GenericFieldRendererComponent,

        NgSelectInfiniteWrapperComponent,
        NgSelectFormFieldControlDirective,
        MatNgSelectInfiniteWrapperComponent,

        ModalComponent,
        MatInputWrapperComponent,
        MatSelectWrapperComponent,
        MatDatepickerWrapperComponent,
        LoginComponent,

        EditorComponent
    ],
    providers: [
        {
            provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS,
            useValue: { useUtc: true }
        },
        { provide: DateAdapter, useClass: MomentDateAdapter },
        {
            provide: MAT_DATE_FORMATS,
            useValue: {
                parse: {
                    dateInput: ['x', 'X', 'l'],
                },
                display: {
                    dateInput: 'l',
                    monthYearLabel: 'MMM YYYY',
                    dateA11yLabel: 'LL',
                    monthYearA11yLabel: 'MMMM YYYY',
                },
            }
        }
    ],
    entryComponents: [
        LoginModalComponent
    ]
})
export class AppSharedModule {
    static forRoot(): ModuleWithProviders<AppSharedModule> {
        return {
          ngModule: AppSharedModule,
          providers: [...providers]
        };
    }
}
