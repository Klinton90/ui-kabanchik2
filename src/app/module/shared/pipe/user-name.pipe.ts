import { PipeTransform, Pipe } from '@angular/core';
import { UserService } from 'app/module/core/service/user.service';

@Pipe({name: 'userName'})
export class UserNamePipe implements PipeTransform {

    constructor(private userService: UserService) {}

    transform(value: any, ...args: any[]) {
        return this.userService.getUserNameForView(value);
    }

}
