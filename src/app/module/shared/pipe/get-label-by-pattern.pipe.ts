import { Pipe, PipeTransform } from '@angular/core';
import { ConverterService } from 'app/module/core/service/converter.service';

@Pipe({
    name: 'getLabelByPattern',
    pure: false
})
export class GetLabelByPatternPipe implements PipeTransform {

    constructor(private converterService: ConverterService) {}

    transform(value: object, args: any[] = null): any {
        if (!args || !args.length) {
            return '';
        } else {
            return this.converterService.getLabelByPatternAndId(args, value, ConverterService.DEFAULT_PATTERN);
        }
    }
}
