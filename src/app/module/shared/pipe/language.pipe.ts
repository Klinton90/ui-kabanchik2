import { Pipe, PipeTransform } from '@angular/core';
import { MessageService } from '../../core/service/message.service';
import { isObject } from 'lodash';

@Pipe({name: 'language'})
export class LanguagePipe implements PipeTransform {

    constructor(
        private messageService: MessageService
    ) {}

    transform(value: string, ...args: any[]): any {
        if (value) {
            let result: string = value;
            value.split(' ').forEach((_value: string) => {
                let _result: string;
                if (args.length === 1 && isObject(args[0])) {
                    _result = this.messageService.resolveMessage(_value, args[0]);
                } else if (args.length > 0) {
                    _result = this.messageService.resolveMessage(_value, args);
                } else {
                    _result = this.messageService.resolveMessage(_value);
                }
                result = result.replace(_value, _result);
            });
            return result;
        } else {
            return value;
        }
    }

}
