import { Pipe, PipeTransform } from '@angular/core';
import { isObject } from 'lodash';

@Pipe({
    name: 'keys',
    pure: false
})
export class KeysPipe implements PipeTransform {
    transform(value: object, args: any[] = null): any {
        if (value && isObject(value)) {
            return Object.keys(value);
        } else {
            return value;
        }
    }
}
