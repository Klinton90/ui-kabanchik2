import { Pipe, PipeTransform } from '@angular/core';
import { toPairs } from 'lodash';
import { MessageService } from '../../core/service/message.service';

@Pipe({name: 'fieldErrors'})
export class FieldErrorsPipe implements PipeTransform {

    constructor(
        private messageService: MessageService
    ) {}

    transform(value: object, ...args: any[]): any {
        if (value) {
            const result: string[] = [];
            toPairs(value).forEach((val: any[]) => {
                result.push(this.messageService.resolveMessage(val[0], val[1]));
            });
            return result;
        } else {
            return value;
        }
    }

}
