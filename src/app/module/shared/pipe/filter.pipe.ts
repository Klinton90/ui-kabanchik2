import { Pipe, PipeTransform } from '@angular/core';
import { get, isArray } from 'lodash';

// This doesn't work for grid
@Pipe({name: 'filter'})
export class FilterPipe implements PipeTransform {
    transform(items: any[], path: string, term: any, mode: 'contains' | 'equals' = 'contains'): any {
        if (!isArray(items)) {
            throw Error('`filter` pipe can be applied only to an Array');
        }
        if (!path) {
            throw Error('`filter` pipe require `path` parameter');
        }
        return term ? items.filter((item: any) => {
            const value: any = get(item, path);
            if (mode === 'contains') {
                return (value as string).indexOf(term) > -1;
            } else {
                return value === term;
            }
        }) : items;
    }
}
