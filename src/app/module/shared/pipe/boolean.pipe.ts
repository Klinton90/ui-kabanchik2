import { Pipe, PipeTransform } from '@angular/core';
import { MessageService } from 'app/module/core/service/message.service';

@Pipe({ name: 'booleanPipe' })
export class BooleanPipe implements PipeTransform {

    constructor(private messageService: MessageService) {}

    transform(value: any, ...args: any[]) {
        return this.messageService.resolveMessage(value ? 'ui.text.default.yes' : 'ui.text.default.no');
    }

}
