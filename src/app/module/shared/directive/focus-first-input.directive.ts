import {AfterViewInit, Directive, ElementRef} from '@angular/core';

@Directive({
    selector: '[focusFirstInput]'
})
export class FocusFirstInputDirective implements AfterViewInit {

    static focus(el: ElementRef): void {
        const val = el.nativeElement.querySelector(
            'input:not([type=checkbox])'
            + ':not([type=radio])'
            + ':not([type=submit])'
            + ':not([type=button])'
            + ':not([type=hidden])'
            + ':not([disabled])');
        if (val) {
            setTimeout(() => {
                val.focus();
            });
        }
    }

    constructor(protected el: ElementRef) {}

    ngAfterViewInit(): void {
        FocusFirstInputDirective.focus(this.el);
    }

}
