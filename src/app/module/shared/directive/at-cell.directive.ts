import { Directive, ElementRef, Inject, Renderer2, Input, AfterViewInit, AfterViewChecked } from '@angular/core';

@Directive({
    selector: '[atCell]'
})
export class AtCellDirective implements AfterViewInit, AfterViewChecked {

    @Input()
    private title: string;

    constructor(
        @Inject(ElementRef) private element: ElementRef,
        private _renderer: Renderer2
    ) {}

    ngAfterViewInit(): void {
        let title = this.title;
        if (!title) {
            title = this.element.nativeElement.innerText;
        }
        this._renderer.setAttribute(this.element.nativeElement, 'title', title);
    }

    ngAfterViewChecked(): void {
        let title = this.title;
        if (!title) {
            title = this.element.nativeElement.innerText;
        }
        this._renderer.setAttribute(this.element.nativeElement, 'title', title);
    }

}
