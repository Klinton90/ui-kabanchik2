import { ChangeDetectionStrategy, Component, Host, ChangeDetectorRef, Input, Optional } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { isEqual } from 'lodash';
import { MatInputWrapperBase } from '../../common/mat-input-wrapper-base.component';

@Component({
    selector: 'app-mat-select-wrapper',
    templateUrl: 'mat-select-wrapper.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatSelectWrapperComponent extends MatInputWrapperBase {

    @Input() options: any[];
    @Input() bindLabel: string;
    @Input() bindValue: string;

    compareWith = (option: any, selected: any) => isEqual(option, selected);

    constructor (
        @Optional() @Host() protected formGroup: FormGroupDirective,
        protected ref: ChangeDetectorRef
    ) {
        super();
    }

}
