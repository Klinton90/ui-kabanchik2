import { Component, Input, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import {
    GenericFieldEntity,
    GenericFieldWithOptionModel,
    GenericFieldType
} from 'app/module/admin/component/generic-field/generic-field.model';
import { GenericFieldResource } from 'app/module/admin/component/generic-field/generic-field.resource';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StaticService } from 'app/module/core/service/static.service';
import { GenericFieldValue, IGenericFieldAwareDomain } from 'app/module/admin/model/generic-field-value.model';
import * as moment from 'moment';
import { GenericFieldService } from 'app/module/admin/component/generic-field/generic-field.service';

export const GENERIC_FIELD_FORM_GROUP_NAME = 'genericFieldsFormGroupName';

@Component({
    moduleId: module.id,
    templateUrl: 'generic-field-renderer.component.html',
    selector: 'genericFieldRenderer',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GenericFieldRendererComponent implements OnInit {

    @Input() type: GenericFieldEntity;
    @Input() parentForm: FormGroup;
    @Input() fields: GenericFieldWithOptionModel[];

    _disabled: boolean;
    @Input() get disabled(): boolean {
        return this._disabled;
    }
    set disabled(_disabled: boolean) {
        this._disabled = _disabled;

        if (this.initialized) {
            if (_disabled) {
                this.form.disable();
            } else {
                this.form.enable();
            }
        }
    }

    _entity: IGenericFieldAwareDomain<any>;
    @Input() get entity(): IGenericFieldAwareDomain<any> {
        return this._entity;
    }
    set entity(_entity: IGenericFieldAwareDomain<any>) {
        this._entity = _entity;

        if (this.initialized) {
            this.updateFields();
        } else {
            this.setupFields();
        }
    }

    @Output() initialization: EventEmitter<any> = new EventEmitter<any>();

    GENERIC_FIELD_TYPE: typeof GenericFieldType = GenericFieldType;
    GENERIC_FIELD_SERVICE: typeof GenericFieldService = GenericFieldService;
    staticService: typeof StaticService = StaticService;
    form: FormGroup = new FormGroup({});
    initialized: boolean = false;

    constructor(
        private resource: GenericFieldResource,
        private ref: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        if (!this.fields) {
            this.resource.findAllByEntity(this.type).subscribe((response: RestResponseModel) => {
                this.fields = response.data;
                this.setupFields();
            });
        } else {
            this.setupFields();
        }
    }

    private setupFields(): void {
        if (!this.entity || !this.fields) {
            return;
        }

        this.fields.forEach((field: GenericFieldWithOptionModel) => {
            const type: GenericFieldType = StaticService.resolveEnumOnEntity(field.type, GenericFieldType);
            const validator: any[] = type !== GenericFieldType.BOOLEAN && field.required ? [Validators.required] : [];

            const genericFieldValue: GenericFieldValue = this.entity.genericFieldValues
                .find(_genericFieldValue => _genericFieldValue.genericFieldId === field.id);
            let value: any = genericFieldValue ? genericFieldValue.value : '';

            if (value && type === GenericFieldType.DATE) {
                value = moment(value, 'x');
            }

            this.form.addControl(GenericFieldService.getFormFieldName(field), new FormControl(value, validator));
        });

        this.parentForm.addControl(GENERIC_FIELD_FORM_GROUP_NAME, this.form);
        this.initialized = true;
        this.initialization.emit();
        this.ref.detectChanges();
    }

    private updateFields(): void {
        if (!this.entity) {
            return;
        }

        this.fields.forEach((field: GenericFieldWithOptionModel) => {
            const type: GenericFieldType = StaticService.resolveEnumOnEntity(field.type, GenericFieldType);

            const genericFieldValue: GenericFieldValue = this.entity.genericFieldValues
                .find(_genericFieldValue => _genericFieldValue.genericFieldId === field.id);
            let value: any = genericFieldValue ? genericFieldValue.value : '';

            if (value && type === GenericFieldType.DATE) {
                value = moment(value, 'x');
            }

            this.form.controls[GenericFieldService.getFormFieldName(field)].setValue(value);
        });

        this.ref.detectChanges();
    }

}
