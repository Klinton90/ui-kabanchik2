import {ChangeDetectionStrategy, Component} from '@angular/core';
import {AlertService} from '../../../core/component/alert/alert.service';
import { MatDialogRef } from '@angular/material/dialog';
import {AlertType} from '../../../core/component/alert/alert-custom-config';

@Component({
    templateUrl: 'login-modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginModalComponent {

    constructor(
        private alertService: AlertService,
        public dialogRef: MatDialogRef<LoginModalComponent>
    ) {}

    loginSuccessfull(): void {
        this.dialogRef.close();
        this.alertService.post({
            title: 'Session restored.',
            body: 'Please repeat your last action manually.',
            type: AlertType.INFO
        });
    }

}
