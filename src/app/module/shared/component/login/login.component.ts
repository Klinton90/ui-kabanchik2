import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UtilService} from '../../../core/service/util.service';
import {HttpErrorResponse} from '@angular/common/http';
import { AuthService } from 'app/service/auth.service';
import { UserResource } from 'app/module/admin/component/user/user.resource';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent {

    @Output() loginSuccessfull: EventEmitter<any> = new EventEmitter<any>();

    form: FormGroup = new FormGroup({
        login:      new FormControl('', [Validators.required]),
        password:   new FormControl('', [Validators.required])
    }, {updateOn: 'blur'});

    constructor(
        private auth: AuthService,
        private utilService: UtilService,
        private resource: UserResource,
        private alertService: AlertService,
    ) {}

    loginAction(): void {
        this.utilService.submitFormWithBlur(this.form, () => {
            this.auth.doLogin(this.form.value.login, this.form.value.password)
                .subscribe((response: HttpErrorResponse) => {
                    this.loginSuccessfull.emit(response);
                });
        });
    }

    forgetPassword(): void {
        this.resource.forgetPassword(this.form.value['login']).subscribe((data: RestResponseModel) => {
            this.alertService.post({
                type: AlertType.SUCCESS,
                body: 'ui.message.user.forgetPassword.body',
                title: 'ui.message.user.forgetPassword.title'
            });
        });
    }

}
