import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, ViewChild, OnDestroy } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, tap, switchMap, catchError } from 'rxjs/operators';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { isNil, isArray } from 'lodash';
import { AbstractControl, FormControl } from '@angular/forms';
import { NgSelectComponent, NgOption } from '@ng-select/ng-select';
import { AddTagFn } from '@ng-select/ng-select/lib/ng-select.component';

const OFFSET_SIZE = 10;

export type ngSelectInfiniteDataCallback = (term: string, limit: number, offset?: number) => Observable<any>;

@Component({
    moduleId: module.id,
    selector: 'ng-select-infinite-wrapper',
    templateUrl: 'ng-select-infinite-wrapper.component.html'
})
export class NgSelectInfiniteWrapperComponent implements OnInit, OnDestroy {

    private loadingOffset: number = 0;

    private initialized: boolean = false;

    isLoading: boolean = false;

    typeaheadValue$: Subject<string>;

    values: any[] = [];

    @ViewChild(NgSelectComponent, {static: true}) ngSelect: NgSelectComponent;

    @Input() control: AbstractControl;

    // TODO: add setter in order to track external changes
    @Input() value: any = null;

    @Input() clearable: boolean;

    @Input() required: boolean;

    @Input() disabled: boolean;

    _loadDataCallback: ngSelectInfiniteDataCallback;

    @Input()
    set loadDataCallback(columnApi: ngSelectInfiniteDataCallback) {
        this._loadDataCallback = columnApi;
        if (this.initialized) {
            this.reset();
        }
    }

    get loadDataCallback() {
        return this._loadDataCallback;
    }

    @Input() bindLabel: string;

    @Input() bindValue: string;

    @Input() defaultSearch: string;

    @Input() name: string;

    @Input() multiple: boolean;

    @Input() converter: (response: RestResponseModel) => any[];

    @Input() addTag?: boolean | AddTagFn;

    @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

    @Output() fullValueChange: EventEmitter<any> = new EventEmitter<any>();

    @Output() add: EventEmitter<any> = new EventEmitter<any>();

    @Output() remove: EventEmitter<any> = new EventEmitter<any>();

    @Output() dataLoaded: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        protected ref: ChangeDetectorRef
    ) {}

    ngOnInit(): void {
        if (!this.control) {
            this.control = new FormControl(this.value);
        }

        this.createSubscription();

        this.initialized = true;
    }

    ngOnDestroy(): void {
        this.typeaheadValue$ && this.typeaheadValue$.unsubscribe();
    }

    fetchMoreValues(term: string): void {
        if (this.loadingOffset >= 0) {
            this.isLoading = true;
            const nextOffset: number = this.loadingOffset + OFFSET_SIZE;
            this.loadDataCallback(!isNil(term) ? term : '', OFFSET_SIZE, nextOffset)
                .pipe(
                    catchError(() => of([])),
                    tap(() => this.isLoading = false),
                )
                .subscribe((response: RestResponseModel) => {
                    const data: any[] = this.tryConvert(response) || [];
                    this.loadingOffset = data.length === OFFSET_SIZE ? nextOffset : -1;
                    this.values = this.values.concat(data);
                    this.ref.detectChanges();
                    this.dataLoaded.emit(data);
                });
        }
    }

    onOpen(): void {
        this.typeaheadValue$.next('');
    }

    _valueChange(value: any): void {
        if (this.multiple) {
            let _value: any[] = [];
            if (value && this.bindValue && isArray(value)) {
                _value = value.map((_v: any) => {
                    return _v[this.bindValue] || null;
                });
            }
            this.valueChange.emit(_value);
        } else {
            this.valueChange.emit(value && this.bindValue && value[this.bindValue] ? value[this.bindValue] : value);
        }

        this.fullValueChange.emit(value);
    }

    _add($event: any): void {
        this.add.emit($event);
    }

    _remove($event: NgOption): void {
        this.remove.emit($event);
    }

    reset(): void {
        this.ngOnDestroy();
        this.createSubscription();
    }

    private tryConvert(response: RestResponseModel): any[] {
        return this.converter ? this.converter(response) : response.data;
    }

    private createSubscription(): void {
        this.typeaheadValue$ = new Subject<string>();
        this.typeaheadValue$
            .pipe(
                debounceTime(300),
                distinctUntilChanged(),
                tap(() => this.isLoading = true),
                switchMap(term => this.loadDataCallback(term, OFFSET_SIZE)
                    .pipe(
                        catchError(() => of([])),
                        tap(() => this.isLoading = false),
                    )
                )
            )
            .subscribe((response: RestResponseModel) => {
                this.values = this.tryConvert(response);
                this.loadingOffset = this.values.length === OFFSET_SIZE ? 0 : -1;
                this.ref.markForCheck();
                this.dataLoaded.emit(this.values);
            });

        if (this.defaultSearch) {
            this.typeaheadValue$.next(this.defaultSearch);
        } else if (!isNil(this.control.value)) {
            this.typeaheadValue$.next('');
        }
    }

}
