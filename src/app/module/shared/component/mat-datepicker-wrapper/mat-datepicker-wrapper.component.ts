import { ChangeDetectionStrategy, Component, Host, ChangeDetectorRef, Optional, Input } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { Moment } from 'moment';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatInputWrapperBase } from '../../common/mat-input-wrapper-base.component';

@Component({
    selector: 'app-mat-datepicker-wrapper',
    templateUrl: 'mat-datepicker-wrapper.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatDatepickerWrapperComponent extends MatInputWrapperBase {

    @Input() startView: 'month' | 'year' | 'multi-year' = 'month';
    @Input() mode: 'day' | 'month' | 'year' = 'day';

    _min: Moment;
    @Input()
    get min(): Moment {
        return this._min;
    }
    set min(value: Moment) {
        if (value && (!this._min || this._min.valueOf() !== value.valueOf())) {
            this._min = value;
        }
    }

    _max: Moment;
    @Input()
    get max(): Moment {
        return this._max;
    }
    set max(value: Moment) {
        if (value && (!this._max || this._max.valueOf() !== value.valueOf())) {
            this._max = value;
        }
    }

    constructor (
        @Optional() @Host() protected formGroup: FormGroupDirective,
        protected ref: ChangeDetectorRef
    ) {
        super();

        if (this.mode === 'year') {
            this.startView = 'multi-year';
        }
        if (this.mode === 'year' && this.startView === 'month') {
            this.startView = 'year';
        }
    }

    chosenYearHandler(normalizedYear: Moment, datepicker: MatDatepicker<Moment>): void {
        let ctrlValue: Moment = this.control.value;

        if (!ctrlValue) {
            ctrlValue = normalizedYear;
        } else {
            ctrlValue.year(normalizedYear.year());
        }

        this.preventValueChangeEvent = true;
        this.control.setValue(ctrlValue);
        if (this.mode === 'year') {
            this.valueChange.emit(ctrlValue);
            datepicker.close();
        }
    }

    chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>): void {
        let ctrlValue: Moment = this.control.value;

        if (!ctrlValue) {
            ctrlValue = normalizedMonth;
        } else {
            ctrlValue.month(normalizedMonth.month());
        }

        this.preventValueChangeEvent = true;
        this.control.setValue(ctrlValue);
        if (this.mode === 'month') {
            this.valueChange.emit(ctrlValue);
            datepicker.close();
        }
    }

}
