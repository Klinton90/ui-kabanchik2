import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, AfterViewInit, forwardRef } from '@angular/core';
import * as Quill from 'quill';
import { Editor } from 'primeng/editor';
import { DomHandler } from 'primeng/dom';

const Link: any = Quill.import('formats/link');
class LinkType extends Link {
    static create (value: string) {
        const node = super.create(value);
        value = this.sanitize(value);

        if (value.startsWith('https://') || value.startsWith('http://')) {
            node.className = 'link--external';
        } else if (value.indexOf('@') > 0) {
            node.setAttribute('href', 'mailto:' + value);
            node.className = 'link--mail';
        } else {
            node.removeAttribute('target');
        }

        return node;
    }
}

Quill.register(LinkType);

// Same as primeng/editor
// Just have to have it custom, as there is no way to register components into Quill before editor is added to the page.
@Component({
      selector: 'app-editor',
      templateUrl: 'editor.component.html',
      providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EditorComponent),
        multi: true
    }]
  })
  export class EditorComponent extends Editor implements AfterViewInit {

      ngAfterViewInit() {
          const editorElement = DomHandler.findSingle(this.el.nativeElement , 'div.ui-editor-content');
          const toolbarElement = DomHandler.findSingle(this.el.nativeElement , 'div.ui-editor-toolbar');
          const defaultModule  = {toolbar: toolbarElement};
          const modules = this.modules ? {...defaultModule, ...this.modules} : defaultModule;

          this.quill = new Quill(editorElement, {
            modules: modules,
            placeholder: this.placeholder,
            readOnly: this.readonly,
            theme: 'snow',
            formats: this.formats,
            bounds: this.bounds,
            debug: this.debug,
            scrollingContainer: this.scrollingContainer
          });

          if (this.value) {
              this.quill.pasteHTML(this.value);
          }

          this.quill.on('text-change', (delta, oldContents, source) => {
              if (source === 'user') {
                  let html = editorElement.children[0].innerHTML;
                  const text = this.quill.getText().trim();
                  if (html === '<p><br></p>') {
                      html = null;
                  }

                  this.onTextChange.emit({
                      htmlValue: html,
                      textValue: text,
                      delta: delta,
                      source: source
                  });

                  this.onModelChange(html);
                  this.onModelTouched();
              }
          });

          this.quill.on('selection-change', (range, oldRange, source) => {
              this.onSelectionChange.emit({
                  range: range,
                  oldRange: oldRange,
                  source: source
              });
          });

          this.onInit.emit({
              editor: this.quill
          });
      }

  }
