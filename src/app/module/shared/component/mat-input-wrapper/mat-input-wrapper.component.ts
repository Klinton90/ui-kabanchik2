import { ChangeDetectionStrategy, Component, Input, Host, ChangeDetectorRef, Optional } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { MatInputWrapperBase } from '../../common/mat-input-wrapper-base.component';

@Component({
    selector: 'app-mat-input-wrapper',
    templateUrl: 'mat-input-wrapper.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MatInputWrapperComponent extends MatInputWrapperBase {

    isTextArea: boolean;

    @Input() type: 'text' | 'number' | 'password' | 'textarea' = 'text';

    // @Input() prefixInputGroupText: string = "";
    // @Input() suffixInputGroupText: string = "";
    // @Input() prefixInputGroupIcon: string = "";
    // @Input() suffixInputGroupIcon: string = "";
    // @Input() prefixButtonClass: string = "";
    // @Input() prefixButtonIcon: string = "";
    // @Input() prefixButtonText: string = "";
    // @Input() suffixButtonClass: string = "";
    // @Input() suffixButtonIcon: string = "";
    // @Input() suffixButtonText: string = "";

    // @Output() prefixButtonClick: EventEmitter<any> = new EventEmitter<any>();
    // @Output() suffixButtonClick: EventEmitter<any> = new EventEmitter<any>();

    constructor (
        @Optional() @Host() protected formGroup: FormGroupDirective,
        protected ref: ChangeDetectorRef
    ) {
        super();
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.isTextArea = this.type === 'textarea';
    }

    // prefixButtonClicked(event: any): void{
    //     this.prefixButtonClick.emit(event);
    // }

    // suffixButtonClicked(event: any): void{
    //     this.suffixButtonClick.emit(event);
    // }

}
