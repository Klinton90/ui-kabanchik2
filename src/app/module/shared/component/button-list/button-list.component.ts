import { Component, Input, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { defaults } from 'lodash';
import { AtButton, BooleanFunction } from '../../model/at-button.model';

@Component({
  selector: 'appButtonList',
  templateUrl: './button-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
//   styleUrls: ['./button-list.component.scss']
})
export class ButtonListComponent implements OnInit {

    private defaultButton: AtButton = {
        title: '',
        click: () => {
            throw Error('Not Implemented');
        }
    };

    @Input() buttons: AtButton[];

    @Input() context: any;

    constructor() { }

    ngOnInit() {
        if (this.buttons) {
            for (const button of this.buttons) {
                defaults(button, this.defaultButton);
            }
        }
    }

    processBooleanFunction(input: boolean | BooleanFunction): boolean {
        if (input instanceof Function) {
            return input(this.context);
        } else {
            return input;
        }
    }

}
