import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    moduleId: module.id,
    selector: 'app-modal',
    templateUrl: 'modal.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent implements OnInit {

    @Input() dialogRef: MatDialogRef<any>;
    @Input() modalTitle: string;
    @Input() primaryDisable: boolean = false;
    @Input() primaryShow: boolean = true;
    @Input() primaryIcon: string = '';
    @Input() secondaryDisable: boolean = false;
    @Input() secondaryShow: boolean = true;
    @Input() secondaryIcon: string = '';
    @Input() hasX: boolean = true;

    private _primaryText: string;
    @Input()
    set primaryText(value: string) {
        this._primaryText = value;
    }
    get primaryText(): string {
        return this._primaryText || 'ui.text.default.yes';
    }

    private _secondaryText: string;
    @Input()
    set secondaryText(value: string) {
        this._secondaryText = value;
    }
    get secondaryText(): string {
        return this._secondaryText || 'ui.text.default.close';
    }

    @Output() primaryClick: EventEmitter<any> = new EventEmitter<any>();
    @Output() secondaryClick: EventEmitter<any> = new EventEmitter<any>();
    @Output() xClick: EventEmitter<any> = new EventEmitter<any>();
    @Output() escapePress: EventEmitter<any> = new EventEmitter<any>();
    @Output() backdropClick: EventEmitter<any> = new EventEmitter<any>();

    ngOnInit(): void {
        if (!this.dialogRef) {
            throw Error('`dialogRef` is required for `appModalNew`');
        }

        this.dialogRef.keydownEvents().subscribe(($event: KeyboardEvent) => {
            if ($event.key === 'Enter') {
                this.onPrimary();
            } else if ($event.key === 'Escape') {
                this.onEscape();
            }
        });

        this.dialogRef.backdropClick().subscribe(($event: MouseEvent) => {
            this.onBackdrop();
        });
    }

    onPrimary(): void {
        this.primaryClick.emit();
    }

    onSecondary(): void {
        if (this.secondaryClick.observers.length > 0) {
            this.secondaryClick.emit();
        } else {
            this.close();
        }
    }

    onX(): void {
        if (this.xClick.observers.length > 0) {
            this.xClick.emit();
        } else {
            this.onSecondary();
        }
    }

    onEscape(): void {
        if (this.escapePress.observers.length > 0) {
            this.escapePress.emit();
        } else {
            this.onSecondary();
        }
    }

    onBackdrop(): void {
        if (this.backdropClick.observers.length > 0) {
            this.backdropClick.emit();
        } else {
            this.onSecondary();
        }
    }

    close(): void {
        this.dialogRef.close();
    }

}
