import { AlertService } from 'app/module/core/component/alert/alert.service';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { IDomainModel } from 'app/module/core/model/i-domain.model';
import { HttpErrorResponse } from '@angular/common/http';
import { FormAwareModalResult } from './success-form-aware-modal-event.model';
import { AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { FormAwareResource } from 'app/module/core/resource/form-aware.resource';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { FormAwareModal } from './form-aware-modal.component';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/portal';
import { ModalResult } from './modal-result.model';

export class ListAwareParams<T extends IDomainModel<any>> {
    rows: T[];
    timesheetCategoryId: number;
}

export abstract class ListAwareModalComponent<
    M extends ListAwareModalComponent<any, any, any>,
    M2 extends FormAwareModal<any, any>,
    T extends IDomainModel<any>
> implements AfterViewInit {

    protected isRefreshRequired: boolean = false;

    protected abstract modal: ComponentType<M2>;

    public abstract data: ListAwareParams<T>;
    protected abstract dialogRef: MatDialogRef<M, ModalResult>;
    protected abstract notificationService: AlertService;
    protected abstract confirmationService: ConfirmationService;
    protected abstract resource: FormAwareResource;
    protected abstract dialog: MatDialog;
    protected abstract ref: ChangeDetectorRef;

    protected abstract getNewFormEntityInstance(): T;
    protected abstract getModalParams(): any;

    ngAfterViewInit(): void {
        if (!this.data.rows.length) {
            this.showAction(this.getNewFormEntityInstance());
        } else if (this.data.rows.length === 1) {
            this.showAction(this.data.rows[0]);
        }
    }

    showAction(entity: T): void {
        this.dialog
            .open(this.modal, {
                data: {
                    entity,
                    ...this.getModalParams()
                }
            })
            .afterClosed()
            .subscribe({
                next: (result: FormAwareModalResult) => {
                    if (result && result.isRefreshRequired) {
                        this.onSuccess(result);
                    }
                    if (this.data.rows.length === 0 || this.data.rows.length === 1 && this.isRefreshRequired) {
                        this.hideAction();
                    }
                }
            });
    }

    deleteAction(entity: T): void {
        this.confirmationService.show({
            onPrimaryClick: () => this.executeDeleteAction(entity),
            body: 'ui.confirmation.default.delete.body'
        });
    }

    executeDeleteAction(entity: T): void {
        this.resource.delete(entity.id).subscribe((response: T) => {
            const indexToRemove: number = this.data.rows.findIndex((row: T) => row.id === entity.id);
            this.data.rows.splice(indexToRemove, 1);
            this.isRefreshRequired = true;
            if (this.data.rows.length < 1) {
                this.hideAction();
            }

            this.notificationService.post({
                body: 'ui.message.default.grid.delete.body',
                type: AlertType.SUCCESS
            });
        }, (response: HttpErrorResponse) => {
            this.notificationService.processBackendValidation(response);
        });
    }

    onSuccess(event: FormAwareModalResult): void {
        const indexToUpdate: number = this.data.rows.findIndex((row: T) => row.id === event.entity.id);
        this.data.rows[indexToUpdate] = event.entity;
        this.isRefreshRequired = true;
        this.ref.detectChanges();
    }

    onHidden(): void {
        if (this.data.rows.length === 0 || this.data.rows.length === 1 && this.isRefreshRequired) {
            this.hideAction();
        }
    }

    hideAction(): void {
        this.dialogRef.close({ isRefreshRequired: this.isRefreshRequired });
    }

}
