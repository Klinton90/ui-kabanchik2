import { RestResponseModel } from '../../../core/model/rest-response.model';
import { ModalResult } from './modal-result.model';

export class FormAwareModalResult extends ModalResult {
    entity: any;
    isAddMode: boolean;
    response: RestResponseModel;
}
