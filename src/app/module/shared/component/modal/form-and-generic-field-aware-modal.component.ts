import { IGenericFieldAwareDomain } from 'app/module/admin/model/generic-field-value.model';
import { GenericFieldEntity, GenericFieldModel } from 'app/module/admin/component/generic-field/generic-field.model';
import { FormAwareParams, FormAwareModal } from './form-aware-modal.component';
import { GENERIC_FIELD_FORM_GROUP_NAME } from '../generic-field-renderer/generic-field-renderer.component';
import { FormGroup } from '@angular/forms';
import { GenericFieldService } from 'app/module/admin/component/generic-field/generic-field.service';

export interface FormAndGenericFieldAwareParams<E extends IGenericFieldAwareDomain<any>> extends FormAwareParams<E> {
    genericFields?: GenericFieldModel[];
}

export abstract class FormAndGenericFieldAwareModal<
    T extends FormAndGenericFieldAwareModal<T, E>,
    E extends IGenericFieldAwareDomain<any>
> extends FormAwareModal<T, E> {

    GENERIC_FIELD_ENTITY: typeof GenericFieldEntity = GenericFieldEntity;
    genericFields: GenericFieldModel[];

    show(params: FormAndGenericFieldAwareParams<E>): void {
        super.show(params);
        if (params.genericFields) {
            this.genericFields = params.genericFields;
        }
    }

    protected getDataForSave(): any {
        const result: any = super.getDataForSave();

        result.genericFieldValues = [];

        const genericFieldsFormGroup: FormGroup = <FormGroup> this.form.get(GENERIC_FIELD_FORM_GROUP_NAME);
        if (genericFieldsFormGroup) {
            result.genericFieldValues = GenericFieldService.processFormValues(this.genericFields, genericFieldsFormGroup, this.entity);
        }

        return result;
    }

}
