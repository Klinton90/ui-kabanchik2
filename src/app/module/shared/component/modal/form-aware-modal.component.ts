import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { FormAwareResource } from 'app/module/core/resource/form-aware.resource';
import { UtilService } from 'app/module/core/service/util.service';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { HttpErrorResponse } from '@angular/common/http';
import { IDomainModel } from 'app/module/core/model/i-domain.model';
import { cloneDeep, isNil } from 'lodash';
import { ConfirmationService } from 'app/module/core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { ChangeDetectorRef } from '@angular/core';
import { AlertCustomConfig } from 'app/module/core/component/alert/alert-custom-config';
import { FormAwareModalResult } from './success-form-aware-modal-event.model';

export interface FormAwareParams<E extends IDomainModel<any>> {
    entity?: E;
}

export abstract class FormAwareModal<T extends FormAwareModal<any, any>, E extends IDomainModel<any>> {

    abstract form: FormGroup;
    abstract dialogRef: MatDialogRef<T, FormAwareModalResult>;
    protected abstract resource: FormAwareResource;
    protected abstract newFormEntityInstance: E;
    protected abstract utilService: UtilService;
    protected abstract confirmationService: ConfirmationService;
    protected abstract notificationService: AlertService;
    protected abstract ref: ChangeDetectorRef;

    isAddMode: boolean;
    entity: E;
    params: FormAwareParams<E>;
    protected closeAfterSave: boolean = true;

    show(params: FormAwareParams<E>): void {
        this.params = Object.assign({}, params);
        this.isAddMode = !params.entity || isNil(params.entity.id);
        this.entity = Object.assign({}, this.newFormEntityInstance, params.entity);
        this.utilService.setFormGroupValues(this.form, this.entity);
    }

    hide(): void {
        if (this.form.dirty) {
            if (this.form.valid) {
                this.confirmationService.show({
                    body: 'ui.confirmation.default.dirty.valid.body',
                    onPrimaryClick: () => this.save(),
                    onSecondaryClick: () => this.dialogRef.close(),
                    onXClick: () => {},
                    onBackdropClick: () => {},
                    onEscapePress: () => {},
                });
            } else {
                this.confirmationService.show({
                    body: 'ui.confirmation.default.dirty.invalid.body',
                    onPrimaryClick: () => this.dialogRef.close()
                });
            }
        } else {
            this.dialogRef.close();
        }
    }

    getPrimaryText(): string {
        return this.isAddMode ? 'ui.text.default.create' : 'ui.text.default.save';
    }

    save(): void {
        if (this.form.valid) {
            const action = this.getSaveAction();
            this.resource[action](this.getDataForSave()).subscribe(
                (response: RestResponseModel) => {
                    this.successHandler(response);
                },
                (response: HttpErrorResponse) => {
                    this.errorHandler(response);
                }
            );
        }
    }

    protected getDataForSave(): any {
        return cloneDeep(this.form.getRawValue());
    }

    protected getSaveAction(): string {
        return this.isAddMode ? 'insert' : 'update';
    }

    protected successHandler(response: RestResponseModel, message?: AlertCustomConfig): void {
        this.form.markAsPristine();

        if (message) {
            this.notificationService.post(message);
        } else {
            this.notificationService.postSimple(this.isAddMode
                ? 'ui.message.default.grid.save.body.new'
                : 'ui.message.default.grid.save.body.edit');
        }

        if (this.closeAfterSave) {
            this.dialogRef.close({
                isRefreshRequired: true,
                isAddMode: this.isAddMode,
                entity: cloneDeep(this.form.getRawValue()),
                response: response,
            });
        }
    }

    protected errorHandler(response: HttpErrorResponse): void {
        this.notificationService.processFormFieldBackendValidation(response, this.form);
        this.ref.detectChanges();
    }

}
