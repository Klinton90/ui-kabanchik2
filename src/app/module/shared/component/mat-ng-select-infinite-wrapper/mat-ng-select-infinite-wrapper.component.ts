import { Component, Optional, Host, ChangeDetectorRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MatInputWrapperBase } from '../../common/mat-input-wrapper-base.component';
import { FormGroupDirective } from '@angular/forms';
import {
    ngSelectInfiniteDataCallback,
    NgSelectInfiniteWrapperComponent
} from '../ng-select-infinite-wrapper/ng-select-infinite-wrapper.component';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { NgOption } from '@ng-select/ng-select';

@Component({
    moduleId: module.id,
    selector: 'mat-ng-select-infinite-wrapper',
    templateUrl: 'mat-ng-select-infinite-wrapper.component.html',
    styleUrls: ['mat-ng-select-infinite-wrapper.component.scss']
})
export class MatNgSelectInfiniteWrapperComponent extends MatInputWrapperBase {

    @ViewChild(NgSelectInfiniteWrapperComponent, {static: true}) selectComponent: NgSelectInfiniteWrapperComponent;

    @Input() value: any;

    @Input() clearable: boolean;

    @Input() required: boolean;

    @Input() loadDataCallback: ngSelectInfiniteDataCallback;

    @Input() bindLabel: string;

    @Input() bindValue: string;

    @Input() defaultSearch: string;

    @Input() name: string;

    @Input() disabled: boolean;

    @Input() multiple: boolean;

    @Input() converter: (response: RestResponseModel) => any[];

    @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

    @Output() fullValueChange: EventEmitter<any> = new EventEmitter<any>();

    @Output() add: EventEmitter<any> = new EventEmitter<any>();

    @Output() remove: EventEmitter<any> = new EventEmitter<any>();

    @Output() dataLoaded: EventEmitter<any> = new EventEmitter<any>();

    constructor(
        @Optional() @Host() protected formGroup: FormGroupDirective,
        protected ref: ChangeDetectorRef
    ) {
        super();
    }

    _add($event: any): void {
        this.add.emit($event);
    }

    _remove($event: NgOption): void {
        this.remove.emit($event);
    }

    _dataLoaded($event: any): void {
        this.dataLoaded.emit($event);
    }

}
