import { FormGroupDirective, AbstractControl, FormControl } from '@angular/forms';
import { ChangeDetectorRef, OnInit, Input, AfterViewInit, Output, EventEmitter, OnDestroy, Directive } from '@angular/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { isNil } from 'lodash';
import { Subscription } from 'rxjs';

@Directive()
export abstract class MatInputWrapperBase implements OnInit, AfterViewInit, OnDestroy {

    protected abstract formGroup: FormGroupDirective;
    protected abstract ref: ChangeDetectorRef;

    @Input() name: string;
    @Input() required: boolean;
    @Input() validOnPristine: boolean;
    @Input() disabled: boolean;

    private _value: any;
    @Input()
    get value(): any {
        return this._value;
    }
    set value(value: any) {
        this._value = value;
        if (this.control) {
            this.preventValueChangeEvent = true;
            this.control.setValue(this._value);
        }
    }
    @Output()
    valueChange: EventEmitter<any> = new EventEmitter<any>();

    @Input() label: string;
    @Input() hint: string;
    @Input() placeholder: string;

    @Input() appearance: MatFormFieldAppearance = 'standard';

    control: AbstractControl;

    protected preventValueChangeEvent: boolean = false;

    private subscriptions: Subscription[] = [];

    ngOnInit(): void {
        if (this.formGroup) {
            this.control = this.formGroup.form.get(this.name);
        } else {
            this.control = new FormControl();
        }

        this.subscriptions.push(this.control.statusChanges.subscribe(() => this.ref.markForCheck()));

        if (this.valueChange.observers.length) {
            this.subscriptions.push(this.control.valueChanges.subscribe((value: any) => this.onValueChanges(value)));
        }

        if (this.value) {
            this.control.setValue(this.value);
        }

        this.isRequired();
    }

    ngAfterViewInit(): void {
        // this.control.markAsTouched();
        // this.ref.detectChanges();
        if (!this.validOnPristine && this.control.invalid) {
            this.control.markAsTouched();
            this.ref.detectChanges();
        }
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((subscription: Subscription) => subscription.unsubscribe());
    }

    isRequired(): void {
        if (isNil(this.required) && this.name && this.control.validator) {
            const validators: any = this.control.validator(<any>this.name);
            if (validators) {
                this.required = validators.hasOwnProperty('required');
            }
        }
    }

    protected onValueChanges(value: any): void {
        if (!this.preventValueChangeEvent) {
            this.valueChange.emit(value);
        }
        this.preventValueChangeEvent = false;
    }

}
