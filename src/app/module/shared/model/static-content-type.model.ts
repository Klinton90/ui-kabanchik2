export enum StaticContentType {
    CODE_OF_CONDUCT, HOME
}

export class StaticContent {
    type: string;
    text: string;
    localizationId: string;
}