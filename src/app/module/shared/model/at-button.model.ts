export declare type BooleanFunction = (context: any) => boolean;

export class AtButton {
    disabled?: BooleanFunction | boolean;
    hidden?: BooleanFunction | boolean;
    classList?: string;
    title?: string;
    label?: string;
    tabIndex?: number;
    click: (context: any) => void;
}
