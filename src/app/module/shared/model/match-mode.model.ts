export enum MatchMode {
    CONTAINS = <any>'contains',
    NOT_CONTAINS = <any>'notContains',
    STARTS_WITH = <any>'startsWith',
    ENDS_WITH = <any>'endsWith',
    EQUALS = <any>'equals',
    NOT_EQUAL = <any>'notEqual',
    IN = <any>'in',
    DATE = <any>'date',
    GREATER_THAN = <any>'greaterThan',
    GREATER_OR_EQUAL = <any>'greaterOrEqual',
    LESS_THAN = <any>'lessThan',
    LESS_OR_EQUAL = <any>'lessOrEqual',
    IN_RANGE = <any>'inRange'
}
