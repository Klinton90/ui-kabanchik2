import { SelectItem } from 'primeng/api/selectitem';

export class LocalizedEnum implements SelectItem {
    messageCode: string;
    label: string;
    value: string;
}
