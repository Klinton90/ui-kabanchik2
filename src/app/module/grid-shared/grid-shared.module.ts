import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AppSharedModule } from '../shared/shared.module';
import { GridButtonsComponent } from './component/grid-buttons/grid-buttons.component';
import { AgGridModule } from 'ag-grid-angular';
import { NgModule } from '@angular/core';
import { AtAgGridComponent } from './component/custom-ag-grid/custom-ag-grid.component';
import { SelectEditorComponent } from './component/select-editor/select-editor.component';
import { SelectFilterComponent } from './component/select-filter/select-filter.component';
import { SelectFloatingFilterComponent } from './component/select-floating-filter/select-floating-filter.component';
import { DatepickerEditorComponent } from './component/datepicker-editor/datepicker-editor.component';
import { DatepickerFilterComponent } from './component/datepicker-filter/datepicker-filter.component';
import { DatepickerFloatingFilterComponent } from './component/datepicker-floating-filter/datepicker-floating-filter.component';
import { CustomTextEditorComponent } from './component/custom-text-editor/custom-text-editor.component';
import { CustomCellRendererComponent } from './component/custom-cell-renderer/custom-cell-renderer.component';
import { InfiniteSelectFilterComponent } from './component/infinite-select-filter/infinite-select-filter.component';
import {
    InfiniteSelectFloatingFilterComponent
} from './component/infinite-select-floating-filter/infinite-select-floating-filter.component';
import { InfiniteSelectEditorComponent } from './component/infinite-select-editor/infinite-select-editor.component';
import { CustomNoRowsOverlayComponent } from './component/custom-no-rows-overlay/custom-no-rows-overlay.component';
import { SideBarComponent } from './component/side-bar/side-bar.component';

@NgModule({
    declarations: [
        GridButtonsComponent,
        AtAgGridComponent,
        CustomTextEditorComponent,
        SelectEditorComponent,
        SelectFilterComponent,
        SelectFloatingFilterComponent,
        DatepickerEditorComponent,
        DatepickerFilterComponent,
        DatepickerFloatingFilterComponent,
        CustomCellRendererComponent,
        InfiniteSelectFilterComponent,
        InfiniteSelectFloatingFilterComponent,
        InfiniteSelectEditorComponent,
        CustomNoRowsOverlayComponent,
        SideBarComponent
    ],
    imports: [
        // Angular
        CommonModule,
        FormsModule,
        // ag-grid
        AgGridModule.withComponents([]),
        // Custom
        AppSharedModule
    ],
    exports: [
        GridButtonsComponent,
        AtAgGridComponent
    ],
    entryComponents: [
        GridButtonsComponent,
        CustomTextEditorComponent,
        SelectEditorComponent,
        SelectFilterComponent,
        SelectFloatingFilterComponent,
        DatepickerEditorComponent,
        DatepickerFilterComponent,
        DatepickerFloatingFilterComponent,
        CustomCellRendererComponent,
        InfiniteSelectFilterComponent,
        InfiniteSelectFloatingFilterComponent,
        InfiniteSelectEditorComponent,
        CustomNoRowsOverlayComponent
    ]
})
export class GridSharedModule { }
