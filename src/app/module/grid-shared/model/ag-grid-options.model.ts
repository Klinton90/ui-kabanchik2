import {
    GridOptions,
    IFilterParams,
    IFloatingFilterParams,
    ICellRendererParams,
    ColGroupDef,
    ColDef,
    AgEvent,
    ICellEditorParams,
    IGetRowsParams,
    INoRowsOverlayParams,
    GridReadyEvent
} from 'ag-grid-community';
import { RestResource } from 'app/module/core/resource/rest.resource';
import { AtButton } from 'app/module/shared/model/at-button.model';
import { MatchMode } from 'app/module/shared/model/match-mode.model';
import { Observable } from 'rxjs';
import { ComponentType } from '@angular/cdk/portal';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { FormAwareParams, FormAwareModal } from 'app/module/shared/component/modal/form-aware-modal.component';
import { IDomainModel } from 'app/module/core/model/i-domain.model';
import { GenericFieldEntity, GenericFieldWithOptionModel } from 'app/module/admin/component/generic-field/generic-field.model';
import { ValidatorFn } from '@angular/forms';
import { AddTagFn } from '@ng-select/ng-select/lib/ng-select.component';
import { ngSelectInfiniteDataCallback } from 'app/module/shared/component/ng-select-infinite-wrapper/ng-select-infinite-wrapper.component';

export const SELECT_FILTER_OPTIONS_REFRESH = 'SELECT_FILTER_OPTIONS_REFRESH';
export const GRID_BUTTONS_REFRESH = 'GRID_BUTTONS_REFRESH';
export const GRID_EDITOR_REFRESH = 'GRID_EDITOR_REFRESH';

export declare type SelectGridValues = any[] | ((field: string) => any[]);

export declare type ModalDataBuilder = (context: ICellRendererParams, resolvedObservables: RestResponseModel[]) => FormAwareParams<any>;

export interface CustomColumnDef extends ColDef {
    autoGenerateContext?: ColDefObservable;
}

export interface CustomColumnGroupDef extends ColGroupDef {
    children: (CustomColumnDef | ColGroupDef)[];
}

export interface CustomAgGridOptionsModel extends GridOptions {
    context: CustomAgGridContext;
    beforeSave?: (data: any) => any;
    columnDefs?: (CustomColumnDef | CustomColumnGroupDef)[];
}

export interface CustomGridReadyEvent extends GridReadyEvent {
    customApi: CustomApi;
}

export interface CustomApi {
    refreshUi: () => void;
}

export interface CustomAgGridContext {
    stickyName?: string;
    resource?: RestResource;
    getDataMethod?: string | ((params: IGetRowsParams) => Observable<any>);
    prepareRowForSave?: (data: any) => any;
    extraFilter?: Map<string, AgFilterMetadata> | (() => Map<string, AgFilterMetadata>);
    actionsColumnContext?: ActionsColumnContext;
    hasFooter?: boolean;
    hasPagination?: boolean;
    hasSaveButton?: boolean;
    hasAddButton?: boolean;
    newItemConstructor: () => IDomainModel<any>;
    paginationPageSizes?: number[];
    buttons?: AtButton[];
    buttonsContext?: any;
    genericFieldEntity?: GenericFieldEntity;
    $dirtyRows?: Map<number, Set<string>>;
    $deletedRows?: Set<number>;
    $invalidRows?: Map<number, Map<string, string>>;
    gridOptions?: CustomAgGridOptionsModel;
    noRowsOverlayComponentParams?: CustomNoRowsComponentParams;
}

export interface CustomNoRowsComponentParams extends INoRowsOverlayParams {
    getMessage?: () => string;
}

export interface CustomAgGridModalContext<T extends FormAwareModal<any, E>, E extends IDomainModel<any>> {
    observables?: Observable<any>[];
    component: ComponentType<T>;
    modalDataBuilder: ModalDataBuilder;
}

export class ActionsColumnContext {
    minWidth?: number;
    isSaveAllowed?: boolean | ((rowIndex: number) => boolean);
    savePressedOverwrite?: (rowIndex: number, data: IDomainModel<any>) => void;
    actionColumnButtons?: AtButton[];
}

export class BaseEditorParams {
    disabled?: boolean | ((data: IDomainModel<any>) => boolean);
    hidden?: boolean | ((data: IDomainModel<any>) => boolean);
    change?: (tmpValue: any, data: IDomainModel<any>, isFirstLoad: boolean, params: ICellEditorParams) => boolean | Observable<any>;
    validators?: ValidatorFn[] | ((data: IDomainModel<any>) => ValidatorFn[]);
}

export class CustomTextEditorParams extends BaseEditorParams {
    type?: 'text' | 'number' | 'password';
}

export interface GenericFieldCellRendererParams extends ICellRendererParams {
    genericField: GenericFieldWithOptionModel;
}

export interface InfiniteSelectGridBaseParams {
    bindValue?: string;
    bindLabel?: string;
    dataCallback: ngSelectInfiniteDataCallback;
    converter?: (response: RestResponseModel) => any[];
    multiple?: boolean;
}

export interface InfiniteSelectEditorParams extends BaseEditorParams, InfiniteSelectGridBaseParams {
    addTag?: boolean | AddTagFn;
}

export interface InfiniteSelectFilterParams extends InfiniteSelectGridBaseParams, IFilterParams {
}

export interface SelectGridBaseParams {
    bindValue?: string;
    bindLabel?: string;
    values: SelectGridValues;
    multiple?: boolean;
}

export interface SelectCellRendererParams extends ICellRendererParams, SelectGridBaseParams {
    bindLabel: string;
    bindValue: string;
}

export interface SelectEditorParams extends BaseEditorParams, SelectGridBaseParams {
    addTag?: boolean | AddTagFn;
}

export interface SelectFilterParams extends SelectGridBaseParams, IFilterParams {
}

export interface AgFilterMetadata {
    filter: any;
    filterTo?: any;
    type: MatchMode;
    filterType: string;
}

export interface FloatingFilterChange {
    model: AgFilterMetadata;
}

export interface CustomFloatingFilterParams extends IFloatingFilterParams {
    value: any;
}

export interface SelectFloatingFilterParams extends CustomFloatingFilterParams, SelectGridBaseParams {
    values: SelectGridValues;
}

export interface InfiniteSelectFloatingFilterParams extends CustomFloatingFilterParams, InfiniteSelectGridBaseParams {
    values: SelectGridValues;
}

export enum ColumnObservableType {
    ARRAY,
    LOCALIZED_ENUM,
    DOMAIN,
    DOMAIN_MULTIPLE,
    DOMAIN2,
    DOMAIN_MULTIPLE2,
    BOOLEAN,
    TEXT,
    NUMBER,
    DATE
}

export interface ColDefObservable {
    type: ColumnObservableType;
    observable?: Observable<any>;
    bindLabel?: string;
    bindValue?: string;

    otherField?: string;
    infiniteProps?: InfiniteSelectColumnProps;
    // consider to remove these 3 fields, those are redundand
    filter?: boolean;
    floatingFilter?: boolean;
    editor?: boolean;
}

export interface InfiniteSelectColumnProps {
    name: string;   // field for `getUniqueValues`
    filterByFieldNames?: string[];
    filterMethod?: ngSelectInfiniteDataCallback;
    editorOptionsResource?: RestResource;
    editorOptionsMethod?: ngSelectInfiniteDataCallback;
    orderByFields?: string[];
    converter?: (response: RestResponseModel) => any[];
}

export interface CellFieldAwareAgEvent extends AgEvent {
    field: string;
}

export interface CustomTextFilterParams {
    caseSensitive?: boolean;
    suppressAndOrCondition?: boolean;
}
