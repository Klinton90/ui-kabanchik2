import {Component, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef} from '@angular/core';
import {ICellEditorParams} from 'ag-grid-community';
import {BaseEditorParams} from '../../model/ag-grid-options.model';
import {BaseCellEditorAngularComponent} from '../../common/ag-editor-base.component';
import { MessageService } from 'app/module/core/service/message.service';
import { ConverterService } from 'app/module/core/service/converter.service';
import { Moment } from 'moment';

@Component({
    selector: 'app-datepicker-editor',
    templateUrl: './datepicker-editor.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./datepicker-editor.component.scss']
  })
  export class DatepickerEditorComponent extends BaseCellEditorAngularComponent<BaseEditorParams> {

    constructor(
        public ref: ChangeDetectorRef,
        public messageService: MessageService,
        public element: ElementRef
    ) {
        super();
    }

    agInit(params: ICellEditorParams): void {
        super.agInit(params);
    }

    getValue() {
        return this.control.value ? (this.control.value as Moment).valueOf() : '';
    }

    protected valueInitializer(value: any): any {
        return value ? ConverterService.guessMoment(value) : null;
    }

  }
