import { Component, ChangeDetectionStrategy } from '@angular/core';
import { IFilterAngularComp } from 'ag-grid-angular';
import { IDoesFilterPassParams, IFilterParams } from 'ag-grid-community';
import { AgFilterMetadata, FloatingFilterChange } from '../../model/ag-grid-options.model';
import { isNil } from 'lodash';
import { MatchMode } from 'app/module/shared/model/match-mode.model';

@Component({
    selector: 'appDatepickerFilter',
    templateUrl: './datepicker-filter.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerFilterComponent implements IFilterAngularComp {

    params: IFilterParams;

    fromValue: any;

    toValue: any;

    isFilterActive(): boolean {
        return !isNil(this.fromValue) || !isNil(this.toValue);
    }

    doesFilterPass(params: IDoesFilterPassParams): boolean {
        return true;
    }

    getModel(): AgFilterMetadata {
        if (this.isFilterActive()) {
            return {
                filter: (!isNil(this.fromValue) ? this.fromValue.valueOf() : ''),
                filterTo: (!isNil(this.toValue) ? this.toValue.valueOf() : ''),
                type: MatchMode.DATE,
                filterType: 'datepicker'
            };
        } else {
            return null;
        }
    }

    setModel(model: AgFilterMetadata): void {
        if (model) {
            const values: string[] = model.filter.toString().split(',');
            this.fromValue = values[0];
            this.toValue = values.length > 1 ? values[1] : null;
        }
    }

    getModelAsString(model: any): string {
        return model.toString();
    }

    onFloatingFilterChanged(change: AgFilterMetadata | FloatingFilterChange): void {
        if (change) {
            if ((<AgFilterMetadata>change).filterType) {
                this.fromValue = (<AgFilterMetadata>change).filter;
            } else {
                this.fromValue = (<FloatingFilterChange>change).model.filter;
            }
        } else {
            this.fromValue = null;
        }

        this.params.filterChangedCallback();
    }

    agInit(params: IFilterParams): void {
        this.params = params;
    }

    onFromChange(tmpValue: any): void {
        if (this.fromValue !== tmpValue.value) {
            this.fromValue = tmpValue.value;
            this.params.filterChangedCallback();
        }
    }

    onToChange(tmpValue: any): void {
        if (this.toValue !== tmpValue.value) {
            this.toValue = tmpValue.value;
            this.params.filterChangedCallback();
        }
    }

    onFromClearClicked(): void {
        if (!isNil(this.fromValue)) {
            this.fromValue = null;
            this.params.filterChangedCallback();
        }
    }

    onToClearClicked(): void {
        if (!isNil(this.toValue)) {
            this.toValue = null;
            this.params.filterChangedCallback();
        }
    }

}
