import {Component, Input, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import {
    ColDef,
    IGetRowsParams,
    GridReadyEvent,
    GridApi,
    ColGroupDef,
    Events,
    RowValueChangedEvent,
    RowNode,
    CellValueChangedEvent,
    ICellEditorComp,
    PopupEditorWrapper,
    ColumnResizedEvent,
    IDatasource,
    ColumnApi,
} from 'ag-grid-community';
import { cloneDeep, isNil, isEqual, isArray, isUndefined, isString, omitBy, pickBy, toPlainObject } from 'lodash';
import { LoadCompleteEvent } from 'ag-grid-community/dist/lib/modules/rowNodeCache/rowNodeBlock';
import { CustomAgGridOptionsModel, CustomColumnDef, AgFilterMetadata } from '../../model/ag-grid-options.model';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { IDomainModel } from 'app/module/core/model/i-domain.model';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { MessageService, BACKEND_VALIDATION_KEY } from 'app/module/core/service/message.service';
import { ACTIONS_COLUMN_NAME, CustomAgGridService } from '../../service/custom-ag-grid.service';
import { HttpErrorResponse } from '@angular/common/http';
import { BaseCellEditorAngularComponent } from '../../common/ag-editor-base.component';
import { AlertType } from 'app/module/core/component/alert/alert-custom-config';
import { Observable, zip } from 'rxjs';
import { StickyService } from 'app/module/core/service/sticky.service';
import { BulkUpdateModel } from 'app/module/core/model/bulk-update.model';
import { StaticService } from 'app/module/core/service/static.service';
import { ListResponseModel } from 'app/module/core/model/list-response.model';
import { ColumnState } from 'ag-grid-community/dist/lib/columnController/columnController';

class GridSticky {
    filter: any;
    columns: ColumnState[];
    columnGroups: {
        groupId: string;
        open: boolean;
    }[];
    manualColumnSize: boolean;
    pageSize: number;
}

@Component({
  selector: 'appAtAgGrid',
  templateUrl: './custom-ag-grid.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./custom-ag-grid.component.scss']
})
export class AtAgGridComponent {

    testFlag: boolean = false;

    private calculatedHeaderHeight: number;

    private _gridOptions: CustomAgGridOptionsModel;

    private preventPaginationOnRowAdd: boolean = false;

    private manualColumnSize: boolean = false;

    private datasource: IDatasource = {
        getRows: this.getRows.bind(this)
    };

    private eventListeners = {
        modelUpdateListener: null
    };

    @Input()
    set gridOptions(gridOptions: CustomAgGridOptionsModel) {
        this._gridOptions = gridOptions;
        this.gridInit();
    }

    get gridOptions(): CustomAgGridOptionsModel {
        return this._gridOptions;
    }

    gridHeight: string;

    api: GridApi;
    columnApi: ColumnApi;

    constructor(
        private notificationService: AlertService,
        private messageService: MessageService,
        private ref: ChangeDetectorRef,
        private stickyService: StickyService
    ) { }

    test(a): void {
        console.log(a);
    }

    getCurrentPage(): number {
        return this.api ? this.api.paginationGetCurrentPage() + 1 : 1;
    }

    getTotalPages(): number {
        return this.api && this.api.paginationIsLastPageFound() ? this.api.paginationGetTotalPages() : 0;
    }

    getTotalPagesString(): string {
        return this.getTotalPages() > 0 ? this.getTotalPages().toString() : 'more';
    }

    getItemStart(): number {
        return this.api && this.api.paginationGetRowCount() > 0
            ? this.api.paginationGetCurrentPage() * this.api.paginationGetPageSize() + 1
            : 0;
    }

    getItemEnd(): number {
        let result: number = 0;
        if (this.api) {
            result = (this.api.paginationGetCurrentPage() + 1) * this.api.paginationGetPageSize();
            if (this.api.paginationIsLastPageFound() && this.api.paginationGetRowCount() < result) {
                result = this.api.paginationGetRowCount();
            }
        }
        return result;
    }

    getTotalItems(): number {
        return this.api && this.api.paginationIsLastPageFound() ? this.api.paginationGetRowCount() : 0;
    }

    getTotalItemsString(): string {
        return this.getTotalItems() > 0 ? this.getTotalItems().toString() : 'more';
    }

    getPaginationPageSize(): number {
        return this.api ? this.api.paginationGetPageSize() : this._gridOptions.context.paginationPageSizes[0];
    }

    addRow(): void {
        const hasEmptyRow: boolean = this._gridOptions.editType === 'fullRow'
            && !!this.getDisplayedRowsPage().find((row: RowNode) => isNil(row.data.id));

        if (!hasEmptyRow) {
            if (this.api.paginationIsLastPageFound()
                && this.api.paginationGetTotalPages() > 0
                && this.api.paginationGetCurrentPage() !== (this.api.paginationGetTotalPages() - 1)
            ) {
                this.eventListeners.modelUpdateListener = this.modelUpdateListener.bind(this);
                this.api.addEventListener(Events.EVENT_MODEL_UPDATED, this.eventListeners.modelUpdateListener);
                this.api.paginationGoToLastPage();
            } else {
                this.doAddRow();
            }
        }
    }

    setPaginationPageSize(size: number): void {
        this._gridOptions.cacheBlockSize = size;
        this._gridOptions.paginationPageSize = size;
        this.updateGridHeight();
        this.api.onSortChanged();
    }

    hasDirtyRows(): boolean {
        return this._gridOptions.context.$dirtyRows.size > 0 || this._gridOptions.context.$deletedRows.size > 0;
    }

    saveSingleRow(rowIndex: number, dataForSave: IDomainModel<any>): void {
        if (this._gridOptions.beforeSave) {
            dataForSave = this._gridOptions.beforeSave(dataForSave);
        }
        this._gridOptions.context.resource.save(dataForSave)
            .subscribe({
                next: this.saveSuccessHandler.bind(this, rowIndex),
                error: this.rowValueChangedErrorHandler.bind(this, rowIndex)
            });
    }

    saveMultiRows(): void {
        if (this.api.getEditingCells().length) {
            this.api.stopEditing();
            setTimeout(() => this._saveMultiRows());
        } else {
            this._saveMultiRows();
        }
    }

    setAutoSize(): void {
        this.manualColumnSize = false;
        this.api.sizeColumnsToFit();
    }

    setAutoSize2(): void {
        this.manualColumnSize = true;
        this.columnApi.autoSizeAllColumns();
    }

    restoreOriginalGridState(): void {
        this.manualColumnSize = false;
        this.columnApi.resetColumnState();
        this.columnApi.resetColumnGroupState();
        this.api.sizeColumnsToFit();
    }

    private _saveMultiRows(): void {
        const dataForSave: BulkUpdateModel<any> = new BulkUpdateModel<any>();

        if ((!this._gridOptions.context.$dirtyRows || !this._gridOptions.context.$dirtyRows.size)
            && (!this._gridOptions.context.$deletedRows || !this._gridOptions.context.$deletedRows.size)
        ) {
            return;
        }

        if (this._gridOptions.context.$dirtyRows && this._gridOptions.context.$dirtyRows.size) {
            dataForSave.rowsForUpdate = Array.from(this._gridOptions.context.$dirtyRows.keys())
                .map(index => this._gridOptions.api.getRenderedNodes().find(_rowNode => _rowNode.rowIndex === index))
                .filter((rowNode: RowNode) => !isEqual(pickBy(this.gridOptions.context.newItemConstructor()), pickBy(rowNode.data)))
                .map((rowNode: RowNode) => ({
                    data: this.gridOptions.context.prepareRowForSave
                        ? this.gridOptions.context.prepareRowForSave(rowNode.data)
                        : rowNode.data,
                    uiId: parseInt(rowNode.id)
                }));
        }

        if (this._gridOptions.context.$deletedRows && this._gridOptions.context.$deletedRows.size) {
            dataForSave.idsToDelete = Array.from(this._gridOptions.context.$deletedRows)
                .map(index => {
                    const rowNode: RowNode = this._gridOptions.api.getRenderedNodes()
                        .find(_rowNode => _rowNode.rowIndex === index);
                    return {
                        id: rowNode.data.id,
                        uiId: parseInt(rowNode.id)
                    };
                });
        }

        this._gridOptions.context.resource.saveAll(dataForSave)
            .subscribe({
                next: this.saveSuccessHandler.bind(this),
                error: this.processMultiRowSaveError.bind(this)
            });
    }

    private processMultiRowSaveError(response: HttpErrorResponse) {
        if (response.status === 422) {
            const json: any = response.error;

            if (isArray(json)) {
                const rowsToUpdate: Set<RowNode> = new Set<RowNode>();
                const messages: string[] = [];
                json.forEach(error => {
                    if (!isNil(error.id)) {
                        const field: string = error.field || 'id';
                        const rowNode: RowNode = this._gridOptions.api.getRowNode(error.id.toString());

                        if (!this._gridOptions.context.$invalidRows.has(rowNode.rowIndex)) {
                            this._gridOptions.context.$invalidRows.set(rowNode.rowIndex, new Map<string, string>());
                        }
                        this._gridOptions.context.$invalidRows.get(rowNode.rowIndex).set(field, error.message);

                        rowsToUpdate.add(rowNode);
                        messages.push('Row#' + (rowNode.rowIndex + 1) + ': ' + error.message);
                    }
                });

                if (messages.length) {
                    this.notificationService.post({
                        body: messages,
                        type: AlertType.DANGER
                    }, 0);
                }

                if (rowsToUpdate.size) {
                    this._gridOptions.api.refreshCells({
                        force: true,
                        rowNodes: Array.from(rowsToUpdate)
                    });
                }
            } else {
                this.notificationService.processBackendValidation(response);
            }
        }
    }

    private gridInit(): void {
        if (this._gridOptions) {
            this.gridPreInit();

            const original_onGridReady = this._gridOptions.onGridReady;
            this._gridOptions.onGridReady = (event: GridReadyEvent) => {
                this.gridPostInit(event);
                if (original_onGridReady) {
                    original_onGridReady(event);
                }

                this.ref.detectChanges();
            };
        }
    }

    private gridPreInit(): void {
        this.setupEditEvents();

        this._gridOptions.suppressPaginationPanel = true;

        if (!this._gridOptions.datasource && this._gridOptions.context.resource) {
            this._gridOptions.datasource = this.datasource;
        }

        this.recursivelyConvertColGroupHeader(this._gridOptions.columnDefs);

        const defaultSortCol: ColDef = this._gridOptions.columnDefs.find((col: ColDef | ColGroupDef) =>
            !CustomAgGridService.isColGroupDef(col) && !!col.sort);
        if (!defaultSortCol) {
            // TODO: improve this for `colGroups`
            const firstCol: ColDef = this._gridOptions.columnDefs.find((col: ColDef | ColGroupDef) =>
                !CustomAgGridService.isColGroupDef(col) && col.sortable);
            if (firstCol) {
                firstCol.sort = 'asc';
            }
        }
    }

    private setupEditEvents(): void {
        // TODO: think if this needs to be registered for `editType !== fullRow`
        if (this._gridOptions.context.resource) {
            this._gridOptions.onRowValueChanged = this.rowValueChanged.bind(this);
        }

        if (this._gridOptions.editType !== 'fullRow') {
            if (!this._gridOptions.context.actionsColumnContext) {
                this._gridOptions.context.actionsColumnContext = {};
            }
            this.recursiveAddValueChangedEvent(this._gridOptions.columnDefs);
        } else {
            this._gridOptions.onRowEditingStarted = (event) => {
                event.api.refreshCells({force: true, columns: [ACTIONS_COLUMN_NAME]});
            };
            this._gridOptions.onRowEditingStopped = (event) => {
                event.api.refreshCells({force: true, columns: [ACTIONS_COLUMN_NAME]});
            };
        }
    }

    private recursiveAddValueChangedEvent(columnDefs: (ColGroupDef | CustomColumnDef)[]): void {
        columnDefs.forEach((colDef: ColGroupDef | CustomColumnDef) => {
            if (CustomAgGridService.isColGroupDef(colDef)) {
                this.recursiveAddValueChangedEvent(colDef.children);
            } else {
                if (colDef.editable) {
                    colDef.onCellValueChanged = (e: CellValueChangedEvent) => {
                        if (e.newValue !== e.oldValue) {
                            if (this._gridOptions.context.$dirtyRows.has(e.node.rowIndex)) {
                                this._gridOptions.context.$dirtyRows.get(e.node.rowIndex).add(e.column.getId());
                            } else {
                                const set = new Set<string>();
                                set.add(e.column.getId());
                                this._gridOptions.context.$dirtyRows.set(e.node.rowIndex, set);
                            }

                            this.ref.detectChanges();
                            this._gridOptions.api.refreshCells({
                                rowNodes: [e.node],
                                columns: [e.column, ACTIONS_COLUMN_NAME],
                                force: true
                            });
                        }
                    };
                }
            }
        });
    }

    private recursivelyConvertColGroupHeader(columnDefs: (ColGroupDef | CustomColumnDef)[]): void {
        columnDefs.forEach((colDef: ColGroupDef | CustomColumnDef) => {
            colDef.headerName = this.messageService.resolveMessage(colDef.headerName);
            if (CustomAgGridService.isColGroupDef(colDef)) {
                this.recursivelyConvertColGroupHeader(colDef.children);
            }
        });
    }

    private gridPostInit(event: GridReadyEvent): void {
        this.api = event.api;
        this.columnApi = event.columnApi;
        (event as any).customApi = {
            refreshUi: () => this.ref.detectChanges(),
        };

        this.api.addEventListener('columnResized', (columnResizedvent: ColumnResizedEvent) => {
            if (columnResizedvent.source === 'uiColumnDragged') {
                this.manualColumnSize = true;
            }
        });

        this.setupStickySaveAndPostLoadSticky();
        this.calculateHeaderHeight();
        this.updateGridHeight();
    }

    private setupStickySaveAndPostLoadSticky(): void {
        if (this.gridOptions.context.stickyName) {
            this.stickyService.registerField(this.gridOptions.context.stickyName, (): GridSticky => {
                return {
                    filter: this.api.getFilterModel(),
                    pageSize: this.getPaginationPageSize(),
                    manualColumnSize: this.manualColumnSize,
                    columns: this.columnApi.getColumnState(),
                    columnGroups: this.columnApi.getColumnGroupState(),
                };
            });

            const stickyData: any = this.stickyService.getData();
            if (stickyData) {
                const gridStickyData: GridSticky = stickyData[this.gridOptions.context.stickyName];
                if (gridStickyData) {
                    if (gridStickyData.columns) {
                        this.columnApi.setColumnState(gridStickyData.columns);
                    }

                    if (gridStickyData.columnGroups) {
                        this.columnApi.setColumnGroupState(gridStickyData.columnGroups);
                    }

                    if (gridStickyData.filter) {
                        this.api.setFilterModel(gridStickyData.filter);
                    }

                    this.manualColumnSize = gridStickyData.manualColumnSize;
                    if (!gridStickyData.manualColumnSize) {
                        this.api.sizeColumnsToFit();
                    }
                }
            } else {
                this.api.sizeColumnsToFit();
            }
        } else {
            this.api.sizeColumnsToFit();
        }
    }

    private modelUpdateListener(event: LoadCompleteEvent): void {
        if (event) {
            if (this.eventListeners.modelUpdateListener) {
                this.api.removeEventListener(Events.EVENT_MODEL_UPDATED, this.eventListeners.modelUpdateListener);
                this.eventListeners.modelUpdateListener = null;
            }
            this.doAddRow();
        }
    }

    private doAddRow(): void {
        this.api.updateRowData({add: [this._gridOptions.context.newItemConstructor()], addIndex: this.getTotalItems()});
        if (this.api.paginationGetCurrentPage() !== (this.api.paginationGetTotalPages() - 1)) {
            this.preventPaginationOnRowAdd = true;
            this.api.paginationGoToLastPage();
        } else {
            this.startEditingFirstEditableCell();
        }
    }

    private updateGridHeight(): void {
        if (!this._gridOptions.paginationAutoPageSize) {
            this.gridHeight = (this._gridOptions.paginationPageSize * this._gridOptions.rowHeight + this.calculatedHeaderHeight + 3) + 'px';
        }
    }

    private calculateHeaderHeight(): void {
        const colGroupsCount = CustomAgGridService.calculateColGroups(this._gridOptions.columnDefs);
        this.calculatedHeaderHeight = this._gridOptions.headerHeight * (1.6 + colGroupsCount);
    }

    private rowValueChanged(event: RowValueChangedEvent): void {
        const _isEqual: boolean = isEqual(
            toPlainObject(this._gridOptions.context.newItemConstructor()),
            omitBy(event.data, isUndefined));

        if (!_isEqual) {
            this.saveSingleRow(event.rowIndex, event.data);
        }
    }

    private saveSuccessHandler(rowIndex: number, response: IDomainModel<any>): void {
        this.notificationService.postSimple('ui.message.default.grid.save.body.edit');
        this.api.refreshInfiniteCache();
    }

    private rowValueChangedErrorHandler(rowIndex: number, errorResponse: HttpErrorResponse): void {
        this.notificationService.processBackendValidation(errorResponse);

        let editableCol: ColDef;
        if (errorResponse.error.field) {
            editableCol = this._gridOptions.columnDefs
                .find((col: ColDef | ColGroupDef) =>
                    !CustomAgGridService.isColGroupDef(col)
                    && !!col.editable
                    && col.field === errorResponse.error.field);

            if (editableCol) {
                this.api.startEditingCell({
                    rowIndex: rowIndex,
                    colKey: editableCol.field
                });
            }

            if (this.api.getCellEditorInstances() !== null && this.api.getCellEditorInstances().length) {
                const editor: BaseCellEditorAngularComponent<any> = this.api.getCellEditorInstances()[0].getFrameworkComponentInstance();
                // In general, this check is useless, but lets keep it in order to prevent possible errors or 3rd party editors.
                if (editor.focus) {
                    editor.focus();
                }
                if (editor.control) {
                    editor.control.setErrors({[BACKEND_VALIDATION_KEY]: errorResponse.error});
                }
            }

            return;
        }

        editableCol = this.getFirstEditableCell();

        this.api.startEditingCell({
            rowIndex: rowIndex,
            colKey: editableCol.field
        });

        if (this.api.getCellEditorInstances() !== null && this.api.getCellEditorInstances().length) {
            const editor: BaseCellEditorAngularComponent<any> = this.api.getCellEditorInstances()[0].getFrameworkComponentInstance();
            // In general, this check is useless, but lets keep it in order to prevent possible errors or 3rd party editors.
            if (editor.focus) {
                editor.focus();
            }
        }
    }

    private getFirstEditableCell(): ColDef {
        return this._gridOptions.columnDefs
            .find((col: ColDef | ColGroupDef) => !CustomAgGridService.isColGroupDef(col) && !!col.editable);
    }

    private startEditingFirstEditableCell(): void {
        const editableCol: ColDef = this.getFirstEditableCell();
        if (this._gridOptions.editType !== 'fullRow') {
            // Save `editType` so we can restore it later
            const oldEditType: string = this._gridOptions.editType;

            // Init editors for all cells in order to get validation errors
            this._gridOptions.editType = 'fullRow';
            this.api.startEditingCell({rowIndex: this.getTotalItems() - 1, colKey: editableCol.field});

            // Run validation
            const initializers: Observable<any>[] = [];
            this.api.getCellEditorInstances().forEach((editor: ICellEditorComp | PopupEditorWrapper) => {
                let customEditor: BaseCellEditorAngularComponent<any>;
                if (editor.isPopup()) {
                    customEditor = (<any>editor).cellEditor.getFrameworkComponentInstance();
                } else {
                    customEditor = (<ICellEditorComp>editor).getFrameworkComponentInstance();
                }

                // In general, this check is useless, but lets keep it in order to prevent possible errors or 3rd party editors.
                if (customEditor.validate) {
                    customEditor.validate();
                }

                if (customEditor.$initialized) {
                    initializers.push(customEditor.$initialized());
                }
            });

            if (initializers.length) {
                zip(...initializers).subscribe({
                    next: () => this.startEditingFirstEditableCell_post(editableCol, oldEditType)
                });
            } else {
                this.startEditingFirstEditableCell_post(editableCol, oldEditType);
            }
        } else {
            this.api.startEditingCell({rowIndex: this.getTotalItems() - 1, colKey: editableCol.field});
        }
    }

    private startEditingFirstEditableCell_post(editableCol: ColDef, oldEditType: string) {
        // Stop validation, as we still want to edit 1 cell at time
        this.api.stopEditing();
        // Restore `editType`
        this._gridOptions.editType = oldEditType;

        // Refresh cells in order to show error icon
        this._gridOptions.api.refreshCells({
            rowNodes: [this.api.getModel().getRow(this.getTotalItems() - 1)],
            force: true
        });
        // Start editing first cell
        this.api.startEditingCell({rowIndex: this.getTotalItems() - 1, colKey: editableCol.field});
    }

    private getRows(params: IGetRowsParams): void {
        if (this.preventPaginationOnRowAdd) {
            this.preventPaginationOnRowAdd = false;
            params.successCallback([this._gridOptions.context.newItemConstructor()], this.getTotalItems());
            this.startEditingFirstEditableCell();
            return;
        }

        if (!this._gridOptions.context.resource) {
            return;
        }

        const newParams: IGetRowsParams = cloneDeep(params);
        if (this._gridOptions.context && this._gridOptions.context.extraFilter) {
            StaticService.resolveFunctor(this._gridOptions.context.extraFilter)
                .forEach((filter: AgFilterMetadata, key: string) => {
                    newParams.filterModel[key] = filter;
                });
        }

        let request: Observable<any> = this._gridOptions.context.resource.findAgByGenericFilters(newParams, this.api);
        if (this._gridOptions.context && this._gridOptions.context.getDataMethod) {
            if (isString(this._gridOptions.context.getDataMethod)) {
                request = this._gridOptions.context.resource[this._gridOptions.context.getDataMethod](newParams);
            } else {
                request = this._gridOptions.context.getDataMethod(newParams);
            }
        }

        // TODO: prevent going to next page on request failure
        request.subscribe({
            next: (response: RestResponseModel) => {
                if (this._gridOptions.editType !== 'fullRow') {
                    this._gridOptions.context.$dirtyRows.clear();
                    this._gridOptions.context.$deletedRows.clear();
                    this._gridOptions.context.$invalidRows.clear();
                }
                const data: ListResponseModel = response.data;
                params.successCallback(data.content, data.totalElements);
                this.ref.detectChanges();
            }
        });
    }

    private getDisplayedRowsPage(): RowNode[] {
        const result: RowNode[] = [];

        const lastGridIndex = this.api.getDisplayedRowCount() - 1;
        const currentPage = this.api.paginationGetCurrentPage();
        const pageSize = this.api.paginationGetPageSize();
        const startPageIndex = currentPage * pageSize;
        let endPageIndex = (currentPage + 1) * pageSize - 1;
        if (endPageIndex > lastGridIndex) {
            endPageIndex = lastGridIndex;
        }

        for (let i = startPageIndex; i <= endPageIndex; i++) {
            result.push(this.api.getDisplayedRowAtIndex(i));
        }

        return result;
    }

}
