import { Component, ChangeDetectionStrategy } from '@angular/core';
import { IFilterAngularComp } from 'ag-grid-angular';
import { AgFilterMetadata, FloatingFilterChange, InfiniteSelectFilterParams } from '../../model/ag-grid-options.model';
import { IDoesFilterPassParams } from 'ag-grid-community';
import { MatchMode } from 'app/module/shared/model/match-mode.model';
import { isNil } from 'lodash';

@Component({
    selector: 'app-infinite-select-filter',
    templateUrl: './infinite-select-filter.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfiniteSelectFilterComponent implements IFilterAngularComp {

    params: InfiniteSelectFilterParams;

    value: any;

    isFilterActive(): boolean {
        return !isNil(this.value);
    }

    doesFilterPass(params: IDoesFilterPassParams): boolean {
        return true;
    }

    getModel(): AgFilterMetadata {
        if (!isNil(this.value)) {
            return {
                filter: this.value,
                type: this.params.multiple ? MatchMode.IN : MatchMode.EQUALS,
                filterType: 'infiniteSelect'
            };
        } else {
            return null;
        }
    }

    setModel(model: AgFilterMetadata): void {
        if (model) {
            this.value = model.filter;
        }
    }

    getModelAsString(model: any): string {
        return model.toString();
    }

    onFloatingFilterChanged(change: AgFilterMetadata | FloatingFilterChange): void {
        if (change) {
            if ((<AgFilterMetadata>change).filterType) {
                this.value = (<AgFilterMetadata>change).filter;
            } else {
                this.value = (<FloatingFilterChange>change).model.filter;
            }
        } else {
            this.value = null;
        }

        this.params.filterChangedCallback();
    }

    agInit(params: InfiniteSelectFilterParams): void {
        this.params = params;
    }

    onChange(tmpValue: any): void {
        this.value = tmpValue;
        this.params.filterChangedCallback();
    }

}
