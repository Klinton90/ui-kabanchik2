import { ChangeDetectionStrategy, Component } from '@angular/core';
import { IFloatingFilter, FilterChangedEvent } from 'ag-grid-community';
import {
    AgFilterMetadata,
    InfiniteSelectFloatingFilterParams
} from '../../model/ag-grid-options.model';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { isNil } from 'lodash';
import { MatchMode } from 'app/module/shared/model/match-mode.model';
import { SelectFilterComponent } from '../select-filter/select-filter.component';

@Component({
    selector: 'app-select-floating-filter',
    templateUrl: './infinite-select-floating-filter.component.html',
    styleUrls: ['./infinite-select-floating-filter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfiniteSelectFloatingFilterComponent implements
    IFloatingFilter,
    AgFrameworkComponent<InfiniteSelectFloatingFilterParams> {

    params: InfiniteSelectFloatingFilterParams;

    value: any;

    agInit(params: InfiniteSelectFloatingFilterParams): void {
        this.params = params;
    }

    onParentModelChanged(parentModel: AgFilterMetadata, filterChangedEvent: FilterChangedEvent): void {
        if (parentModel && !isNil(parentModel.filter)) {
            this.value = parentModel.filter;
        } else {
            this.value = null;
        }
    }

    onChange(tmpValue: any): void {
        this.value = tmpValue;
        let newFilter = null;
        if (!isNil(tmpValue)) {
            newFilter = {
                filter: tmpValue,
                type: this.params.multiple ? MatchMode.IN : MatchMode.EQUALS,
                filterType: 'infiniteSelect'
            };
        }
        this.params.parentFilterInstance((filterInstance: unknown) => {
            (filterInstance as SelectFilterComponent).onFloatingFilterChanged(newFilter);
        });
    }

}
