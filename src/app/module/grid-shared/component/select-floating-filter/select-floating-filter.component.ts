import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { IFloatingFilter, FilterChangedEvent } from 'ag-grid-community';
import {
    AgFilterMetadata,
    CellFieldAwareAgEvent,
    SELECT_FILTER_OPTIONS_REFRESH,
    SelectFloatingFilterParams
} from '../../model/ag-grid-options.model';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { isNil } from 'lodash';
import { StaticService } from '../../../core/service/static.service';
import { MatchMode } from 'app/module/shared/model/match-mode.model';
import { SelectFilterComponent } from '../select-filter/select-filter.component';

@Component({
    selector: 'app-select-floating-filter',
    templateUrl: './select-floating-filter.component.html',
    styleUrls: ['./select-floating-filter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectFloatingFilterComponent implements
    OnDestroy,
    IFloatingFilter,
    AgFrameworkComponent<SelectFloatingFilterParams> {

    params: SelectFloatingFilterParams;

    values: any[];

    value: any;

    private eventListeners = {
        refreshValues: null
    };

    ngOnDestroy(): void {
        this.params.api.removeEventListener(SELECT_FILTER_OPTIONS_REFRESH, this.eventListeners.refreshValues);
    }

    agInit(params: SelectFloatingFilterParams): void {
        this.params = params;
        this.refreshValues();

        this.eventListeners.refreshValues = this.refreshValues.bind(this);
        this.params.api.addEventListener(SELECT_FILTER_OPTIONS_REFRESH, this.eventListeners.refreshValues);
    }

    onParentModelChanged(parentModel: AgFilterMetadata, filterChangedEvent: FilterChangedEvent): void {
        if (parentModel && !isNil(parentModel.filter)) {
            this.value = parentModel.filter;
        } else {
            this.value = null;
        }
    }

    onChange(tmpValue: any): void {
        this.value = tmpValue;
        let newFilter = null;
        if (!isNil(tmpValue)) {
            newFilter = {
                filter: tmpValue,
                type: this.params.multiple ? MatchMode.IN : MatchMode.EQUALS,
                filterType: 'ngSelect'
            };
        }
        this.params.parentFilterInstance((filterInstance: unknown) => {
            (filterInstance as SelectFilterComponent).onFloatingFilterChanged(newFilter);
        });
    }

    private refreshValues($event?: CellFieldAwareAgEvent): void {
        if (!$event || !$event.field || $event.field === this.params.column.getId()) {
            this.values = StaticService.resolveFunctor(this.params.values, this.params.column.getColDef().field);
        }
    }

}
