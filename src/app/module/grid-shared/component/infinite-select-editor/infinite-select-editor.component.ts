import { ChangeDetectionStrategy, Component, ChangeDetectorRef, ElementRef } from '@angular/core';
import { BaseCellEditorAngularComponent } from '../../common/ag-editor-base.component';
import { InfiniteSelectEditorParams } from '../../model/ag-grid-options.model';
import { MessageService } from 'app/module/core/service/message.service';

@Component({
    selector: 'app-infinite-select-editor',
    templateUrl: './infinite-select-editor.component.html',
    styleUrls: ['./infinite-select-editor.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfiniteSelectEditorComponent extends BaseCellEditorAngularComponent<InfiniteSelectEditorParams> {

    constructor(
        public ref: ChangeDetectorRef,
        public messageService: MessageService,
        public element: ElementRef
    ) {
        super();
    }

    customChange(selectedOption: any): void {
        if (this.colDef.autoGenerateContext && this.colDef.autoGenerateContext.otherField && this.editorParams.bindLabel) {
            this.params.data[this.colDef.autoGenerateContext.otherField] = selectedOption[this.editorParams.bindLabel];
        }
    }

}
