import { ChangeDetectionStrategy, Component, Input, ChangeDetectorRef } from '@angular/core';
import { GridOptions, GridApi, ColumnApi, Column, ColumnGroup } from 'ag-grid-community';
import { CustomAgGridService } from '../../service/custom-ag-grid.service';

interface SideBarCol {
    colOrGroup: Column | ColumnGroup;
    indetation: number;
    displayName: string;
}

@Component({
    selector: 'side-bar',
    templateUrl: './side-bar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent {

    private _columnApi: ColumnApi;

    @Input()
    gridOptions: GridOptions;

    @Input()
    api: GridApi;

    @Input()
    set columnApi(columnApi: ColumnApi) {
        this._columnApi = columnApi;
        this.initColumns();
    }

    get columnApi() {
        return this._columnApi;
    }

    isColumnsToolbarOpen: boolean = false;

    private allColumns: SideBarCol[] = [];
    visibleColumns: SideBarCol[] = [];

    private columnsFilterTimer;

    constructor(private changeDetectorRef: ChangeDetectorRef){}

    toggleColumnsToolbar(): void {
        this.isColumnsToolbarOpen = !this.isColumnsToolbarOpen;
    }

    toggleColumnVisibility(colOrGroup: Column | ColumnGroup): void {
        if (CustomAgGridService.isColGroup(colOrGroup)) {
            this.setColumnGroupVisible(colOrGroup, !this.getColumnGroupVisible(colOrGroup));
        } else {
            this.columnApi.setColumnVisible(colOrGroup, !colOrGroup.isVisible());
        }
    }

    getVisible(colOrGroup: Column | ColumnGroup): boolean | null {
        if (CustomAgGridService.isColGroup(colOrGroup)) {
            return this.getColumnGroupVisible(colOrGroup);
        } else {
            return colOrGroup.isVisible();
        }
    }

    getCheckboxClass(colOrGroup: Column | ColumnGroup): string {
        const isVisible: boolean = this.getVisible(colOrGroup);
        return isVisible === false
            ? ''
            : (isVisible === true ? 'ag-checked' : 'ag-indeterminate');
    }

    getPinClass(colOrGroup: Column | ColumnGroup): string {
        if (CustomAgGridService.isColGroup(colOrGroup)) {
            return 'hidden';
        } else {
            return colOrGroup.isPinned() ? '' : 'rotated';
        }
    }

    togglePin(colOrGroup: Column | ColumnGroup): void {
        if (!CustomAgGridService.isColGroup(colOrGroup)) {
            this.columnApi.setColumnPinned(colOrGroup, colOrGroup.isPinned() ? null : 'left');
        }
    }

    // type should be `InputEvent`, but TS for some reason gives error it cannot be found.
    columnsFilterChanged($event: any): void {
        if (this.columnsFilterTimer) {
            clearTimeout(this.columnsFilterTimer);
        }
        this.columnsFilterTimer = setTimeout(() => {
            if ($event) {
                this.visibleColumns = this.allColumns.filter((column: SideBarCol) =>
                    column.displayName.toLowerCase().indexOf($event.target.value.toLowerCase()) > -1);
            } else {
                this.visibleColumns = this.allColumns;
            }
            this.changeDetectorRef.detectChanges();
        }, 300);
    }

    // Tricky and "fastest" code as I can make to find state of group.
    //  - 0 child columns are shown - group is hidden
    //  - all child clumns are shown - group is all shown
    //  - 0 < x < all child columns are shown - group is half-shown, but special logic for checkbox icon.
    private getColumnGroupVisible(columnGroup: ColumnGroup): boolean | null {
        let counter: number = 0;

        const children: (Column | ColumnGroup)[] = columnGroup.getOriginalColumnGroup().getChildren() as any;
        for (const colOrGroup of children) {
            // Please notice trenary operator!!!
            if (CustomAgGridService.isColGroup(colOrGroup) ? this.getColumnGroupVisible(colOrGroup) : colOrGroup.isVisible()) {
                counter++;
            } else if (counter > 0) {
                // Before exiting this loop on found hidden `columnOrGroup` we need to make sure
                // that we've already found at least one visible item.
                // Otherwise, possible situation is: first column is hidden, but all other are shown.
                // This code will run faster for non-fully checked groups.
                break;
            }
        }

        return counter === 0 ? false : (counter === columnGroup.getOriginalColumnGroup().getChildren().length ? true : null);
    }

    private setColumnGroupVisible(columnGroup: ColumnGroup, newVisible: boolean): void {
        const children: (Column | ColumnGroup)[] = columnGroup.getOriginalColumnGroup().getChildren() as any;
        children.forEach((colOrGroup: Column | ColumnGroup) => {
            if (CustomAgGridService.isColGroup(colOrGroup)) {
                this.setColumnGroupVisible(colOrGroup, newVisible);
            } else {
                this.columnApi.setColumnVisible(colOrGroup, newVisible);
            }
        });
    }

    private initColumns(): void {
        if (this.columnApi) {
            const colGroups: (Column | ColumnGroup)[] = this.columnApi.getAllDisplayedColumnGroups() as any;
            this.allColumns = this.visibleColumns = this.recursiveIterateColGroups(colGroups);
        }
    }

    private recursiveIterateColGroups(colGroups: (Column | ColumnGroup)[], indetation: number = 0): SideBarCol[] {
        let result: SideBarCol[] = [];

        colGroups.forEach((colOrGroup: Column | ColumnGroup) => {
            result.push({
                colOrGroup: colOrGroup,
                indetation: indetation,
                displayName: this.getDisplayName(colOrGroup)
            });
            if (CustomAgGridService.isColGroup(colOrGroup)) {
                result = result.concat(
                    this.recursiveIterateColGroups(colOrGroup.getOriginalColumnGroup().getChildren() as any,
                    indetation + 1));
            }
        });

        return result;
    }

    private getDisplayName(colOrGroup: Column | ColumnGroup): string {
        if (CustomAgGridService.isColGroup(colOrGroup)) {
            return this.columnApi.getDisplayNameForColumnGroup(colOrGroup, null);
        } else {
            return this.columnApi.getDisplayNameForColumn(colOrGroup, null);
        }
    }

}
