import {ICellRendererAngularComp} from 'ag-grid-angular';
import {IAfterGuiAttachedParams, ICellRendererParams} from 'ag-grid-community';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import { CustomAgGridContext } from '../../model/ag-grid-options.model';

@Component({
    templateUrl: './custom-cell-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CustomCellRendererComponent implements ICellRendererAngularComp {

    dirty: boolean = false;

    invalid: boolean = false;

    message: string = 'ui.text.default.invalid';

    params: ICellRendererParams;

    constructor(private ref: ChangeDetectorRef) { }

    afterGuiAttached(params?: IAfterGuiAttachedParams): void {
    }

    agInit(params: ICellRendererParams): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        if (this.getContext().gridOptions.editType !== 'fullRow') {
            const newInvalid: boolean = this.getContext().$invalidRows
                && !!this.getContext().$invalidRows.size
                && this.getContext().$invalidRows.has(this.params.rowIndex)
                && this.getContext().$invalidRows.get(this.params.rowIndex).has(this.params.column.getId());

                if (this.invalid !== newInvalid) {
                    this.invalid = newInvalid;
                    if (newInvalid) {
                        this.message = this.getContext().$invalidRows.get(this.params.rowIndex).get(this.params.column.getId())
                            || 'ui.text.default.invalid';
                    }
                }

            const newDirty: boolean = this.getContext().$dirtyRows
                && !!this.getContext().$dirtyRows.size
                && this.getContext().$dirtyRows.has(this.params.rowIndex)
                && this.getContext().$dirtyRows.get(this.params.rowIndex).has(this.params.column.getId());

            if (newDirty && this.dirty !== newDirty) {
                this.dirty = newDirty;
            }

            if (newDirty || newInvalid) {
                this.ref.detectChanges();
                return true;
            }
        }

        return false;
    }

    private getContext(): CustomAgGridContext {
        return this.params.context;
    }

}
