import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererParams, CellPosition } from 'ag-grid-community';
import { isNil } from 'lodash';
import { CustomAgGridContext } from '../../model/ag-grid-options.model';
import { StaticService } from '../../../core/service/static.service';
import { ConfirmationService } from '../../../core/component/confirmation/confirmation.service';
import { AlertService } from 'app/module/core/component/alert/alert.service';
import { IDomainModel } from 'app/module/core/model/i-domain.model';

@Component({
  selector: 'app-grid-buttons',
  templateUrl: './grid-buttons.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
//   styleUrls: ['./grid-buttons.component.scss']
})
export class GridButtonsComponent implements AgRendererComponent {

    public params: ICellRendererParams;

    public isDeleteAllowed: boolean;

    constructor(
        private alertService: AlertService,
        private confirmationService: ConfirmationService,
        private ref: ChangeDetectorRef
    ) { }

    agInit(params: ICellRendererParams): void {
        this.params = params;
        this.isDeleteAllowed = this.getRowData() && !isNil(this.getRowData().id);
    }

    refresh(params: any): boolean {
        return false;
    }

    deleteRow(): void {
        if (this.getContext().gridOptions.editType === 'fullRow') {
            this.confirmationService.show({
                body: 'ui.confirmation.default.delete.body',
                onPrimaryClick: () => {
                    this.getContext().resource.delete(this.getRowData().id).subscribe(
                        value => this.params.api.refreshInfiniteCache(),
                        errorResponse => this.alertService.processBackendValidation(errorResponse)
                    );
                }
            });
        } else {
            if (this.getContext().$deletedRows.has(this.params.rowIndex)) {
                this.getContext().$deletedRows.delete(this.params.rowIndex);
            } else {
                this.getContext().$deletedRows.add(this.params.rowIndex);
            }
        }
    }

    isSaveAllowed(): boolean {
        if (this.getContext().actionsColumnContext && this.getContext().actionsColumnContext.isSaveAllowed) {
            const isSaveAllowed: boolean | ((rowIndex: number) => boolean) = this.getContext().actionsColumnContext.isSaveAllowed;
            return StaticService.resolveFunctor(isSaveAllowed, this.params.rowIndex);
        } else if (this.getContext().gridOptions.editType === 'fullRow') {
            return !!this.params.api.getEditingCells().find((cell: CellPosition) => cell.rowIndex === this.params.rowIndex);
        } else {
            // return this.isRowValid() && this.isRowDirty();
            return false;
        }
    }

    savePressed(): void {
        if (this.getContext().actionsColumnContext && this.getContext().actionsColumnContext.savePressedOverwrite) {
            this.getContext().actionsColumnContext.savePressedOverwrite(this.params.node.rowIndex, this.params.node.data);
        } else {
            this.params.api.stopEditing();
        }
    }

    private isRowValid(): boolean {
        return !this.getContext()
            || !this.getContext().$invalidRows
            || !this.getContext().$invalidRows.size
            || !this.getContext().$invalidRows.has(this.params.rowIndex)
            || !this.getContext().$invalidRows.get(this.params.rowIndex).size;
    }

    private isRowDirty(): boolean {
        return !!this.getContext()
            && !!this.getContext().$dirtyRows
            && !!this.getContext().$dirtyRows.size
            && this.getContext().$dirtyRows.has(this.params.rowIndex);
    }

    public getContext(): CustomAgGridContext {
        return this.params.context;
    }

    private getRowData(): IDomainModel<any> {
        return this.params.data;
    }

}
