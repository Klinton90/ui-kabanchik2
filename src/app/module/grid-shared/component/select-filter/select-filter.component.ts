import { ChangeDetectionStrategy, Component } from '@angular/core';
import { IFilterAngularComp } from 'ag-grid-angular';
import { IDoesFilterPassParams } from 'ag-grid-community';
import { AgFilterMetadata, FloatingFilterChange, SelectFilterParams } from '../../model/ag-grid-options.model';
import { isNil } from 'lodash';
import { StaticService } from '../../../core/service/static.service';
import { MatchMode } from 'app/module/shared/model/match-mode.model';

@Component({
    selector: 'app-select-filter',
    templateUrl: './select-filter.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectFilterComponent implements IFilterAngularComp {

    params: SelectFilterParams;

    value: any;

    values: any[] = [];

    isFilterActive(): boolean {
        return !isNil(this.value);
    }

    doesFilterPass(params: IDoesFilterPassParams): boolean {
        return true;
    }

    getModel(): AgFilterMetadata {
        if (!isNil(this.value)) {
            return {
                filter: this.value,
                type: this.params.multiple ? MatchMode.IN : MatchMode.EQUALS,
                filterType: 'ngSelect'
            };
        } else {
            return null;
        }
    }

    setModel(model: AgFilterMetadata): void {
        if (model) {
            this.value = model.filter;
        }
    }

    getModelAsString(model: any): string {
        return model.toString();
    }

    onFloatingFilterChanged(change: AgFilterMetadata | FloatingFilterChange): void {
        if (change) {
            if ((<AgFilterMetadata>change).filterType) {
                this.value = (<AgFilterMetadata>change).filter;
            } else {
                this.value = (<FloatingFilterChange>change).model.filter;
            }
        } else {
            this.value = null;
        }

        this.params.filterChangedCallback();
    }

    agInit(params: SelectFilterParams): void {
        this.params = params;
        this.values = StaticService.resolveFunctor(this.params.values, this.params.colDef.field);
    }

    onChange(tmpValue: any): void {
        this.value = tmpValue;
        this.params.filterChangedCallback();
    }

}
