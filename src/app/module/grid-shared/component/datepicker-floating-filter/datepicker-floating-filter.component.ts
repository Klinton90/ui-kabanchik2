import { Component, ChangeDetectionStrategy } from '@angular/core';
import { IFloatingFilter, FilterChangedEvent } from 'ag-grid-community';
import { AgFilterMetadata, CustomFloatingFilterParams } from '../../model/ag-grid-options.model';
import { AgFrameworkComponent } from 'ag-grid-angular';
import { isNil } from 'lodash';
import { MatchMode } from 'app/module/shared/model/match-mode.model';
import * as moment from 'moment';
import { DatepickerFilterComponent } from '../datepicker-filter/datepicker-filter.component';

@Component({
    selector: 'appDatepickerFloatingFilter',
    templateUrl: './datepicker-floating-filter.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    styleUrls: [
        './datepicker-floating-filter.component.scss'
    ]
})
export class DatepickerFloatingFilterComponent implements IFloatingFilter, AgFrameworkComponent<CustomFloatingFilterParams> {

    params: CustomFloatingFilterParams;

    value: any;

    agInit(params: CustomFloatingFilterParams): void {
        this.params = params;
    }

    onParentModelChanged(parentModel: AgFilterMetadata, filterChangedEvent?: FilterChangedEvent): void {
        if (parentModel && !isNil(parentModel.filter)) {
            const newValue: string = parentModel.filter.toString().split(',')[0];
            if (newValue) {
                this.value = moment(Number.parseInt(newValue));
                return;
            }
        }
        this.value = null;
    }

    onChange(tmpValue: any): void {
        this.value = tmpValue.value;
        let newFilter = null;
        if (!isNil(this.value)) {
            newFilter = {
                filter: this.value,
                type: MatchMode.DATE,
                filterType: 'datepicker'
            };
        }
        this.params.parentFilterInstance((instance: unknown) => {
            (instance as DatepickerFilterComponent).onFloatingFilterChanged(newFilter);
        });
    }

}
