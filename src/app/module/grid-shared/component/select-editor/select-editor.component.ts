import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef } from '@angular/core';
import { ICellEditorParams } from 'ag-grid-community';
import { isEqual } from 'lodash';
import { StaticService } from '../../../core/service/static.service';
import { BaseCellEditorAngularComponent } from '../../common/ag-editor-base.component';
import { SelectEditorParams } from '../../model/ag-grid-options.model';
import { MessageService } from 'app/module/core/service/message.service';

@Component({
    selector: 'app-select-editor',
    templateUrl: './select-editor.component.html',
    styleUrls: ['./select-editor.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectEditorComponent extends BaseCellEditorAngularComponent<SelectEditorParams> {

    values: any[] = [];

    compareWith = (o1, o2) => isEqual(o1, o2);

    constructor(
        public ref: ChangeDetectorRef,
        public messageService: MessageService,
        public element: ElementRef
    ) {
        super();
    }

    agInit(params: ICellEditorParams): void {
        super.agInit(params);
        if (!this.colDef.cellEditorParams) {
            throw Error('"colDef.cellEditorParams.values" is required property!');
        }
        this.values = StaticService.resolveFunctor(this.editorParams.values, this.params.node.data);
    }

    mapItems(items: any[]): string {
        return (this.editorParams.bindLabel ? items.map(item => item[this.editorParams.bindLabel]) : items).join(', ');
    }

}
