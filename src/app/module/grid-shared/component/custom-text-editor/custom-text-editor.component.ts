import { Component, ChangeDetectorRef, ElementRef } from '@angular/core';
import { ICellEditorParams } from 'ag-grid-community';
import { CustomTextEditorParams } from '../../model/ag-grid-options.model';
import { BaseCellEditorAngularComponent } from '../../common/ag-editor-base.component';
import { MessageService } from 'app/module/core/service/message.service';

@Component({
  templateUrl: './custom-text-editor.component.html',
  styleUrls: ['./custom-text-editor.component.scss']
})
export class CustomTextEditorComponent extends BaseCellEditorAngularComponent<CustomTextEditorParams> {

    type: 'text' | 'number' | 'password' = 'text';

    constructor(
        public ref: ChangeDetectorRef,
        public messageService: MessageService,
        public element: ElementRef
    ) {
        super();
    }

    agInit(params: ICellEditorParams): void {
        super.agInit(params);
        this.type = this.editorParams.type;
    }

}
