import { Component } from '@angular/core';
import { INoRowsOverlayAngularComp } from 'ag-grid-angular';
import { CustomNoRowsComponentParams } from '../../model/ag-grid-options.model';

@Component({
    moduleId: module.id,
    selector: 'custom-no-rows-overlay',
    templateUrl: 'custom-no-rows-overlay.component.html'
})
export class CustomNoRowsOverlayComponent implements INoRowsOverlayAngularComp {

    private params: CustomNoRowsComponentParams;

    message: string;

    agInit(params: CustomNoRowsComponentParams): void {
        this.params = params;
        this.message = this.params.getMessage ? this.params.getMessage() : 'ui.text.grid.noRowsFound';
    }

}
