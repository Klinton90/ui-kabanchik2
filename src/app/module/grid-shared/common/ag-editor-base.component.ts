import {ICellEditorAngularComp} from 'ag-grid-angular';
import {ICellEditorParams} from 'ag-grid-community';
import {
    BaseEditorParams,
    CellFieldAwareAgEvent,
    GRID_EDITOR_REFRESH,
    CustomAgGridContext,
    CustomColumnDef
} from '../model/ag-grid-options.model';
import {StaticService} from '../../core/service/static.service';
import {ChangeDetectorRef, OnDestroy, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import { FormControl } from '@angular/forms';
import { MessageService } from 'app/module/core/service/message.service';
import { isNil, toPairs } from 'lodash';
import { MatTooltip } from '@angular/material/tooltip';
import { FocusFirstInputDirective } from 'app/module/shared/directive/focus-first-input.directive';
import { Observable, isObservable } from 'rxjs';


export abstract class BaseCellEditorAngularComponent<T extends BaseEditorParams> implements
    ICellEditorAngularComp,
    OnDestroy,
    AfterViewInit {

    protected static REQUIRED_TEXT = 'ui.validation.default.required';

    abstract ref: ChangeDetectorRef;

    abstract messageService: MessageService;

    abstract element: ElementRef;

    @ViewChild(MatTooltip, {static: false}) tooltip: MatTooltip;

    params: ICellEditorParams;

    colDef: CustomColumnDef;

    editorParams: T;

    control: FormControl;

    prevValue: any;

    private eventListeners = {
        refreshUI: null
    };

    $initialized?(): Observable<any>;

    ngOnDestroy(): void {
        this.params.api.removeEventListener(GRID_EDITOR_REFRESH, this.eventListeners.refreshUI);
    }

    ngAfterViewInit(): void {
        if (this.getContext().gridOptions.editType !== 'fullRow') {
            this.focus();
        }
        this.control.markAsTouched();
        this.ref.detectChanges();
    }

    agInit(params: ICellEditorParams): void {
        this.params = params;
        this.colDef = this.params.column.getColDef();

        if (this.colDef.cellEditorParams) {
            this.editorParams = this.colDef.cellEditorParams;
        }

        this.control = new FormControl(this.valueInitializer(this.params.value));
        if (this.editorParams && this.editorParams.validators) {
            this.control.setValidators(StaticService.resolveFunctor(this.editorParams.validators, this.params.data));
        }
        this.control.valueChanges.subscribe(this.onChange.bind(this));
        this.control.statusChanges.subscribe(() => {
            if (this.tooltip) {
                if (this.control.invalid) {
                    this.ref.detectChanges();
                    this.tooltip.show();
                } else {
                    this.tooltip.hide();
                }
            }
        });

        this.prevValue = this.params.value;
        this.onChange(this.control.value, true);

        this.refreshUI();
        this.eventListeners.refreshUI = this.refreshUI.bind(this);
        this.params.api.addEventListener(GRID_EDITOR_REFRESH, this.eventListeners.refreshUI);
    }

    getValue(): any {
        return this.control.value;
    }

    isHidden(): boolean {
        return this.editorParams
            ? StaticService.resolveFunctor(this.editorParams.hidden, this.params.node.data, this.params)
            : false;
    }

    isPopup(): boolean {
        return false;
    }

    isCancelAfterEnd(): boolean {
        if (this.getContext().gridOptions.editType !== 'fullRow') {
            this.validate();
        }
        return false;
    }

    validate(): void {
        if (this.control.invalid) {
            if (!this.getContext().$invalidRows.has(this.params.rowIndex)) {
                this.getContext().$invalidRows.set(this.params.rowIndex, new Map<string, string>());
            }
            this.getContext().$invalidRows.get(this.params.rowIndex).set(this.params.column.getId(), null);
        } else {
            if (this.getContext().$invalidRows.has(this.params.rowIndex)) {
                this.getContext().$invalidRows.get(this.params.rowIndex).delete(this.params.column.getId());
            }
        }
    }

    isCancelBeforeStart(): boolean {
        return this.getContext().$deletedRows.has(this.params.rowIndex) || this.isDisabled();
    }

    getFirstErrorMessage(): string {
        if (this.control.invalid && this.control.errors) {
            const errors: Array<[string, any]> = toPairs(this.control.errors);
            return this.messageService.resolveMessage(errors[0][0], errors[0][1]);
        }
        return '';
    }

    focus(): void {
        FocusFirstInputDirective.focus(this.element);
    }

    // Is used only when `editType !== 'fullRow'`
    focusIn(): void {
        FocusFirstInputDirective.focus(this.element);
    }

    onChange(newValue: any, isFirstLoad: boolean = false): void {
        if (this.editorParams && this.editorParams.change) {
            const changeResult: boolean | Observable<any> = this.editorParams.change(newValue, this.params.data, isFirstLoad, this.params);

            if (isObservable(changeResult)) {
                changeResult.subscribe(_changeResult => {
                    this.decideValueRollback(!!_changeResult, newValue);
                });
            } else {
                if (!isNil(changeResult)) {
                    this.decideValueRollback(!!changeResult, newValue);
                }
            }
        }
    }

    protected valueInitializer(value: any): any {
        return value;
    }

    protected isDisabled(): boolean {
        return this.editorParams
            ? StaticService.resolveFunctor(this.editorParams.disabled, this.params.node.data)
            : false;
    }

    protected refreshUI($event?: CellFieldAwareAgEvent): void {
        if (!$event || !$event.field || $event.field === this.params.column.getId()) {
            if (this.isDisabled()) {
                this.control.disable();
            } else {
                this.control.enable();
            }
            this.ref.detectChanges();
        }
    }

    protected getContext(): CustomAgGridContext {
        return this.params.context;
    }

    private decideValueRollback(changeResult: boolean, newValue: any): void {
        if (changeResult) {
            this.prevValue = newValue;
        } else {
            this.control.setValue(this.prevValue);
            this.ref.detectChanges();
        }
    }

}
