import { Injectable } from '@angular/core';
import {
    CustomAgGridOptionsModel,
    ColumnObservableType,
    CustomColumnDef,
    SelectGridBaseParams,
    CustomAgGridModalContext,
    InfiniteSelectGridBaseParams,
    InfiniteSelectColumnProps,
    CustomTextEditorParams,
    CustomTextFilterParams
} from '../model/ag-grid-options.model';
import { Observable, zip, of } from 'rxjs';
import { RestResponseModel } from 'app/module/core/model/rest-response.model';
import { tap, map, shareReplay } from 'rxjs/operators';
import {
    ICellRendererParams,
    ValueSetterParams,
    ValueGetterParams,
    ColDef,
    ColGroupDef,
    ValueFormatterParams,
    CellPosition,
    IFloatingFilter,
    ITooltipParams,
    ColumnGroup,
    Column,
    ColumnMovedEvent
} from 'ag-grid-community';
import { LanguageService } from 'app/service/language.service';
import { IDomainModel } from 'app/module/core/model/i-domain.model';
import { StaticService, DEFAULT_DATE_FORMAT } from 'app/module/core/service/static.service';
import { SelectFilterComponent } from '../component/select-filter/select-filter.component';
import { SelectFloatingFilterComponent } from '../component/select-floating-filter/select-floating-filter.component';
import { SelectEditorComponent } from '../component/select-editor/select-editor.component';
import { get, defaultsDeep, isNil, isString, merge } from 'lodash';
import { GenericFieldResource } from 'app/module/admin/component/generic-field/generic-field.resource';
import {
    GenericFieldType,
    GenericFieldWithOptionModel,
    GenericFieldModel
} from 'app/module/admin/component/generic-field/generic-field.model';
import { GenericFieldValue, IGenericFieldAwareDomain } from 'app/module/admin/model/generic-field-value.model';
import { GenericFieldOptionModel } from 'app/module/admin/component/generic-field-option/generic-field-option.model';
import { DatepickerEditorComponent } from '../component/datepicker-editor/datepicker-editor.component';
import { DatepickerFilterComponent } from '../component/datepicker-filter/datepicker-filter.component';
import { DatepickerFloatingFilterComponent } from '../component/datepicker-floating-filter/datepicker-floating-filter.component';
import { GridButtonsComponent } from '../component/grid-buttons/grid-buttons.component';
import { MatDialog } from '@angular/material/dialog';
import { FormAwareModal, FormAwareParams } from 'app/module/shared/component/modal/form-aware-modal.component';
import { ModalResult } from 'app/module/shared/component/modal/modal-result.model';
import {
    FormAndGenericFieldAwareParams,
    FormAndGenericFieldAwareModal
} from 'app/module/shared/component/modal/form-and-generic-field-aware-modal.component';
import { GenericFieldService } from 'app/module/admin/component/generic-field/generic-field.service';
import { CustomTextEditorComponent } from '../component/custom-text-editor/custom-text-editor.component';
import { ValidatorFn, Validators } from '@angular/forms';
import { CustomCellRendererComponent } from '../component/custom-cell-renderer/custom-cell-renderer.component';
import { BaseColDefParams, SuppressKeyboardEventParams, CellClassParams } from 'ag-grid-community/dist/lib/entities/colDef';
import { ConverterService } from 'app/module/core/service/converter.service';
import { ComponentType } from '@angular/cdk/portal';
import { IFilterAngularComp } from 'ag-grid-angular';
import { BaseCellEditorAngularComponent } from '../common/ag-editor-base.component';
import { InfiniteSelectFilterComponent } from '../component/infinite-select-filter/infinite-select-filter.component';
import { RestResource } from 'app/module/core/resource/rest.resource';
import { ngSelectInfiniteDataCallback } from 'app/module/shared/component/ng-select-infinite-wrapper/ng-select-infinite-wrapper.component';
import {
    InfiniteSelectFloatingFilterComponent
} from '../component/infinite-select-floating-filter/infinite-select-floating-filter.component';
import { InfiniteSelectEditorComponent } from '../component/infinite-select-editor/infinite-select-editor.component';
import { CustomNoRowsOverlayComponent } from '../component/custom-no-rows-overlay/custom-no-rows-overlay.component';

export const ACTIONS_COLUMN_NAME = 'actions';
export const GENERIC_FIELD_GROUP_ID = 'genericField';

@Injectable({
    providedIn: 'root'
})
export class CustomAgGridService {

    constructor(
        private genericFieldResource: GenericFieldResource,
        private dialog: MatDialog,
        private converterService: ConverterService,
    ) {}

    private static defaultGridOptions: CustomAgGridOptionsModel = {
        suppressPropertyNamesCheck: true,
        // TODO: this breaks datepicker editor stopEditingWhenGridLosesFocus: true,
        suppressKeyboardEvent: (params: SuppressKeyboardEventParams) => {
            const column: CustomColumnDef = <CustomColumnDef>params.column.getColDef();
            return params.editing
                && params.event.code === 'Enter'
                && column.autoGenerateContext
                && (column.autoGenerateContext.type === ColumnObservableType.LOCALIZED_ENUM
                    || column.autoGenerateContext.type === ColumnObservableType.DOMAIN_MULTIPLE
                    || column.autoGenerateContext.type === ColumnObservableType.ARRAY
                    || column.autoGenerateContext.type === ColumnObservableType.BOOLEAN
                    || column.autoGenerateContext.type === ColumnObservableType.DOMAIN)
                && params.api.getCellEditorInstances()
                && params.api.getCellEditorInstances().length === 1
                && params.api.getCellEditorInstances()[0].getFrameworkComponentInstance
                && params.api.getCellEditorInstances()[0].getFrameworkComponentInstance().isOpen;
        },
        noRowsOverlayComponentFramework: CustomNoRowsOverlayComponent,
        rowData: [],
        floatingFilter: true,
        pagination: true,
        rowModelType: 'infinite',
        paginationPageSize: 10,
        cacheBlockSize: 10,
        maxBlocksInCache: 1,
        cacheOverflowSize: 1,
        headerHeight: 32,
        rowHeight: 28,
        blockLoadDebounceMillis: 100,
        editType: 'fullRow',
        enableBrowserTooltips: true,
        context: {
            newItemConstructor: () => {
                throw new Error('Not implemented');
            },
            $dirtyRows: new Map<number, Set<string>>(),
            $deletedRows: new Set<number>(),
            $invalidRows: new Map<number, Map<string, string>>(),
            paginationPageSizes: [10, 20, 50, 100],
            hasAddButton: true
        },
        defaultColDef: {
            sortable: true,
            resizable: true,
            cellClassRules: {
                'border-left': CustomAgGridService.checkBorderClass.bind(null, true),
                'border-right': CustomAgGridService.checkBorderClass.bind(null, false)
            }
        },
        onColumnMoved: (event: ColumnMovedEvent) => {
            if (isNil(event.column) || event.column.getParent()) {
                event.api.refreshCells({force: true});
            }
        },
    };

    public static idColumn: CustomColumnDef = {
        headerName: 'ui.text.default.id',
        field: 'id',
        autoGenerateContext: {
            type: ColumnObservableType.NUMBER,
            editor: false
        },
    };

    private static checkBorderClass(isLeftBorder: boolean, params: CellClassParams): boolean {
        const column: Column = params.columnApi.getColumn(params.colDef);
        if (column.getParent()) {
            const leafColumns: Column[] = column.getParent().getLeafColumns();
            const columnIndex: number = leafColumns.indexOf(column);

            return isLeftBorder ? columnIndex === 0 : columnIndex === leafColumns.length - 1;
        }
    }

    private static addActionsColumn(gridOptions: CustomAgGridOptionsModel): void {
        gridOptions.columnDefs.push({
            headerName: 'ui.text.default.actions',
            field: ACTIONS_COLUMN_NAME,
            sortable: false,
            minWidth: gridOptions.context && gridOptions.context.actionsColumnContext && gridOptions.context.actionsColumnContext.minWidth
                ? gridOptions.context.actionsColumnContext.minWidth
                : 0,
            cellRendererFramework: GridButtonsComponent
        });
    }

    private static getLocalizedEnumFormatter(params: ValueFormatterParams): string {
        return params.value && params.value.label ? params.value.label : '';
    }

    private static getBooleanFormatter(params: ValueFormatterParams): string {
        return params.value && (params.value === true || params.value === 'true')
            ? LanguageService.getMessage('ui.text.default.yes')
            : LanguageService.getMessage('ui.text.default.no');
    }

    private static getObjectValueFormatter(params: ValueFormatterParams): string {
        if (!isNil(params.value) && params.colDef.cellRendererParams.values) {
            const values: any[] = StaticService.resolveFunctor(params.colDef.cellRendererParams.values, params.colDef.field);

            if (values && values.length) {
                const option: any = values.find((obj: any) => {
                    return obj[params.colDef.cellRendererParams.bindValue] === params.value;
                });
                return option ? option[params.colDef.cellRendererParams.bindLabel] : '';
            }
        }

        return params.value ? params.value.toString() : '';
    }

    private static getYesNoArrayFormatter(params: ValueFormatterParams): string {
        return params.value && params.value.length ? 'Yes' : 'No';
    }

    private static otherFieldFormatter(otherField: string): (params: ValueFormatterParams) => string {
        return (params: ValueFormatterParams): string => {
            const value: any = get(params.data, otherField);
            return params.data && !isNil(value)
                ? value.toString()
                : (params.value ? params.value.toString() : '');
        };
    }

    private static getBooleanGridParams(asString: boolean = false): SelectGridBaseParams {
        return {
            bindLabel: 'label',
            bindValue: 'value',
            values: [
                {
                    label: LanguageService.getMessage('ui.text.default.yes'),
                    value: asString ? 'true' : true
                },
                {
                    label: LanguageService.getMessage('ui.text.default.no'),
                    value: asString ? 'false' : false
                }
            ]
        };
    }

    private static prepareFilter(
        column: CustomColumnDef,
        newColumn: CustomColumnDef,
        component: ComponentType<IFilterAngularComp> | string,
        params?: InfiniteSelectGridBaseParams | SelectGridBaseParams | CustomTextFilterParams
    ): void {
        if (!column.filter
            && !column.filterFramework
            && (column.autoGenerateContext.filter || isNil(column.autoGenerateContext.filter))
        ) {
            if (isString(component)) {
                newColumn.filter = component;
            } else {
                newColumn.filter = true;
                newColumn.filterFramework = component;
            }

            if (params) {
                newColumn.filterParams = params;
            }
        }
    }

    private static prepareFloatingFilter(
        column: CustomColumnDef,
        newColumn: CustomColumnDef,
        component: ComponentType<IFloatingFilter>,
        params?: InfiniteSelectGridBaseParams | SelectGridBaseParams
    ): void {
        if (!column.floatingFilterComponent
            && !column.floatingFilterComponentFramework
            && (column.autoGenerateContext.floatingFilter || isNil(column.autoGenerateContext.floatingFilter))
        ) {
            newColumn.floatingFilterComponentFramework = component;
            if (params) {
                newColumn.floatingFilterComponentParams = params;
            }
        }
    }

    private static prepareEditor(
        column: CustomColumnDef,
        newColumn: CustomColumnDef,
        component: ComponentType<BaseCellEditorAngularComponent<any>>,
        params?: InfiniteSelectGridBaseParams | SelectGridBaseParams | CustomTextEditorParams
    ): void {
        if (!column.cellEditor
            && !column.cellEditorFramework
            && (column.autoGenerateContext.editor || isNil(column.autoGenerateContext.editor))
        ) {
            newColumn.editable = true;
            newColumn.cellEditorFramework = component;
        }
        if (params) {
            newColumn.cellEditorParams = params;
        }
    }

    private static prepareDateColumn(column: CustomColumnDef): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }
        if (!column.valueFormatter) {
            newColumn.valueFormatter = CustomAgGridService.getGenericFieldDateFormatter;
        }

        CustomAgGridService.prepareFilter(column, newColumn, DatepickerFilterComponent);
        CustomAgGridService.prepareFloatingFilter(column, newColumn, DatepickerFloatingFilterComponent);
        CustomAgGridService.prepareEditor(column, newColumn, DatepickerEditorComponent);

        return newColumn;
    }

    private static prepareTextColumn(column: CustomColumnDef): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }

        CustomAgGridService.prepareFilter(column, newColumn, 'agTextColumnFilter', {
            caseSensitive: true,
            suppressAndOrCondition: true
        });

        CustomAgGridService.prepareEditor(column, newColumn, CustomTextEditorComponent, {
            type: 'text'
        });

        return newColumn;
    }

    private static prepareNumberColumn(column: CustomColumnDef): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }

        CustomAgGridService.prepareFilter(column, newColumn, 'agNumberColumnFilter', {
            suppressAndOrCondition: true
        });

        CustomAgGridService.prepareEditor(column, newColumn, CustomTextEditorComponent, {
            type: 'number'
        });

        return newColumn;
    }

    private static prepareArrayColumn(column: CustomColumnDef, resolvedObservable: any): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!resolvedObservable) {
            throw Error('`resolvedObservable` is required for type `ColumnObservableType.ARRAY`');
        }

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }

        const ngSelectParams: SelectGridBaseParams = {
            values: resolvedObservable
        };

        CustomAgGridService.prepareFilter(column, newColumn, SelectFilterComponent, ngSelectParams);
        CustomAgGridService.prepareFloatingFilter(column, newColumn, SelectFloatingFilterComponent, ngSelectParams);
        CustomAgGridService.prepareEditor(column, newColumn, SelectEditorComponent, ngSelectParams);

        return newColumn;
    }

    private static prepareDomainColumn(column: CustomColumnDef, resolvedObservable: any): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!resolvedObservable) {
            throw Error('`resolvedObservable` is required for type `ColumnObservableType.DOMAIN`');
        }
        if (!column.autoGenerateContext.bindLabel) {
            throw Error('`ColObservable.bindLabel` is required for `ColumnObservableType.DOMAIN`');
        }

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }
        if (!column.valueFormatter) {
            newColumn.valueFormatter = CustomAgGridService.getObjectValueFormatter;
        }

        const ngSelectParams: SelectGridBaseParams = {
            values: resolvedObservable,
            bindLabel: column.autoGenerateContext.bindLabel,
            bindValue: column.autoGenerateContext.bindValue || 'id'
        };

        const filterParams: SelectGridBaseParams = Object.assign({}, ngSelectParams, {multiple: true});

        newColumn.cellRendererParams = ngSelectParams;

        CustomAgGridService.prepareFilter(column, newColumn, SelectFilterComponent, filterParams);
        CustomAgGridService.prepareFloatingFilter(column, newColumn, SelectFloatingFilterComponent, filterParams);
        CustomAgGridService.prepareEditor(column, newColumn, SelectEditorComponent, ngSelectParams);

        return newColumn;
    }

    private static prepareDomainMultipleColumn(column: CustomColumnDef, resolvedObservable: any): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!resolvedObservable) {
            throw Error('`resolvedObservable` is required for type `ColumnObservableType.DOMAIN_MULTIPLE`');
        }
        if (!column.autoGenerateContext.bindLabel) {
            throw Error('`ColObservable.bindLabel` is required for `ColumnObservableType.DOMAIN_MULTIPLE`');
        }

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }
        if (!column.valueFormatter) {
            newColumn.valueFormatter = CustomAgGridService.getYesNoArrayFormatter;
        }

        const ngSelectParams: SelectGridBaseParams = {
            values: resolvedObservable,
            bindLabel: column.autoGenerateContext.bindLabel,
            bindValue:isNil(column.autoGenerateContext.bindValue) ? 'id' : column.autoGenerateContext.bindValue,
            multiple:  true
        };

        newColumn.sortable = false;
        newColumn.cellRendererParams = ngSelectParams;

        CustomAgGridService.prepareFilter(column, newColumn, SelectFilterComponent, ngSelectParams);
        CustomAgGridService.prepareFloatingFilter(column, newColumn, SelectFloatingFilterComponent, ngSelectParams);
        CustomAgGridService.prepareEditor(column, newColumn, SelectEditorComponent, ngSelectParams);

        return newColumn;
    }

    private static prepareLocalizedEnumColumn(column: CustomColumnDef, resolvedObservable: any): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!resolvedObservable) {
            throw Error('`resolvedObservable` is required for type `ColumnObservableType.LOCALIZED_ENUM`');
        }

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }
        if (!column.valueFormatter) {
            newColumn.valueFormatter = CustomAgGridService.getLocalizedEnumFormatter;
        }

        const ngSelectParams: SelectGridBaseParams = {
            values: resolvedObservable,
            bindLabel: column.autoGenerateContext.bindLabel || 'label',
            bindValue: column.autoGenerateContext.bindValue || 'value',
            multiple:  true
        };

        CustomAgGridService.prepareFilter(column, newColumn, SelectFilterComponent, ngSelectParams);
        CustomAgGridService.prepareFloatingFilter(column, newColumn, SelectFloatingFilterComponent, ngSelectParams);
        CustomAgGridService.prepareEditor(column, newColumn, SelectEditorComponent, {
            values: resolvedObservable,
            bindLabel: column.autoGenerateContext.bindLabel || 'label'
        });

        return newColumn;
    }

    private static prepareBooleanColumn(column: CustomColumnDef): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }
        if (!column.valueFormatter) {
            newColumn.valueFormatter = CustomAgGridService.getBooleanFormatter;
        }

        const ngSelectParams: SelectGridBaseParams = CustomAgGridService.getBooleanGridParams();

        CustomAgGridService.prepareFilter(column, newColumn, SelectFilterComponent, ngSelectParams);
        CustomAgGridService.prepareFloatingFilter(column, newColumn, SelectFloatingFilterComponent, ngSelectParams);
        CustomAgGridService.prepareEditor(column, newColumn, SelectEditorComponent, ngSelectParams);

        return newColumn;
    }

    private static prepareGenericFieldColumn(genericField: GenericFieldWithOptionModel): CustomColumnDef {
        const type: GenericFieldType = StaticService.resolveEnumOnEntity(genericField.type, GenericFieldType);

        const validators: ValidatorFn[] = [];
        if (genericField.required) {
            validators.push(Validators.required);
        }

        let newColumn: CustomColumnDef = null;

        switch (type) {
            case GenericFieldType.DATE:
                newColumn = Object.assign({
                    valueFormatter: CustomAgGridService.getGenericFieldDateFormatter,

                    filter: true,
                    filterFramework: DatepickerFilterComponent,

                    floatingFilterComponentFramework: DatepickerFloatingFilterComponent,

                    editable: true,
                    cellEditorFramework: DatepickerEditorComponent,
                    cellEditorParams: {
                        validators: validators
                    }
                }, CustomAgGridService.getGenericFieldBaseColDef(genericField));

                break;
            case GenericFieldType.BOOLEAN:
                const ngSelectBooleanParams: SelectGridBaseParams = CustomAgGridService.getBooleanGridParams();

                newColumn = Object.assign({
                    valueFormatter: CustomAgGridService.getBooleanFormatter,

                    filter: true,
                    filterFramework: SelectFilterComponent,
                    filterParams: ngSelectBooleanParams,

                    floatingFilterComponentFramework: SelectFloatingFilterComponent,
                    floatingFilterComponentParams: ngSelectBooleanParams,

                    editable: true,
                    cellEditorFramework: SelectEditorComponent,
                    cellEditorParams: CustomAgGridService.getBooleanGridParams(true)
                }, CustomAgGridService.getGenericFieldBaseColDef(genericField));

                break;
            case GenericFieldType.SELECT:
                const ngSelectParams: SelectGridBaseParams = {
                    values: genericField.genericFieldOptions,
                    bindLabel: 'description',
                    bindValue: 'value'
                };
                const filterParams: SelectGridBaseParams = Object.assign({}, ngSelectParams, {multiple: true});
                newColumn = Object.assign({
                    valueFormatter: CustomAgGridService.getGenericFieldSelectFormatter,

                    filter: true,
                    filterFramework: SelectFilterComponent,
                    filterParams: filterParams,

                    floatingFilterComponentFramework: SelectFloatingFilterComponent,
                    floatingFilterComponentParams: filterParams,

                    editable: true,
                    cellEditorFramework: SelectEditorComponent,
                    cellEditorParams: Object.assign(ngSelectParams, {validators: validators})
                }, CustomAgGridService.getGenericFieldBaseColDef(genericField));

                break;
            case GenericFieldType.TEXT:
                validators.push(Validators.maxLength(255));
                if (genericField.required) {
                    validators.push(Validators.minLength(1));
                }
                newColumn = Object.assign({
                    filter: 'agTextColumnFilter',
                    filterParams: {
                        caseSensitive: true,
                        suppressAndOrCondition: true
                    },

                    editable: true,
                    cellEditorFramework: CustomTextEditorComponent,
                    cellEditorParams: {
                        type: 'text',
                        validators: validators
                    }
                }, CustomAgGridService.getGenericFieldBaseColDef(genericField));

                break;
            case GenericFieldType.NUMBER:
                newColumn = Object.assign({
                    filter: 'agNumberColumnFilter',
                    filterParams: {
                        caseSensitive: true,
                        suppressAndOrCondition: true
                    },

                    editable: true,
                    cellEditorFramework: CustomTextEditorComponent,
                    cellEditorParams: {
                        type: 'number',
                        validators: validators
                    }
                }, CustomAgGridService.getGenericFieldBaseColDef(genericField));

                break;
            default:
                throw Error('Unexpected `GenericFieldType`');
        }

        return newColumn;
    }

    private static getGenericFieldBaseColDef(genericField: GenericFieldModel): CustomColumnDef {
        return {
            headerName: genericField.name,
            field: GenericFieldService.getFormFieldName(genericField),

            cellRendererFramework: CustomCellRendererComponent,
            cellRendererParams: {
                genericField: genericField
            },

            valueGetter: CustomAgGridService.getGenericFieldValueGetter,
            valueSetter: CustomAgGridService.getGenericFieldValueSetter
        };
    }

    private static findGenericFieldValueForColumn(params: BaseColDefParams): GenericFieldValue {
        if (params.data && params.data.genericFieldValues) {
            return params.data.genericFieldValues.find((_genericFieldValue: GenericFieldValue) => {
                return _genericFieldValue.genericFieldId === params.colDef.cellRendererParams.genericField.id;
            });
        }
    }

    private static getGenericFieldSelectFormatter(params: ValueFormatterParams): string {
        const genericField: GenericFieldWithOptionModel = params.colDef.cellRendererParams.genericField;

        if (genericField.genericFieldOptions && params.value) {
            const option: GenericFieldOptionModel = genericField.genericFieldOptions
                .find((genericFieldOption: GenericFieldOptionModel) => {
                    return genericFieldOption.value === params.value;
                });

            if (option) {
                return option.description;
            }
        }

        return '';
    }

    private static getGenericFieldDateFormatter(params: ValueFormatterParams): string {
        return params.value ? ConverterService.guessMoment(params.value).format(DEFAULT_DATE_FORMAT) : '';
    }

    private static getGenericFieldValueGetter(params: ValueGetterParams): string {
        const genericFieldValue: GenericFieldValue = CustomAgGridService.findGenericFieldValueForColumn(params);
        if (genericFieldValue) {
            return genericFieldValue.value;
        } else {
            return '';
        }
    }

    private static getGenericFieldValueSetter(params: ValueSetterParams): boolean {
        if (params.newValue !== params.oldValue) {
            let genericFieldValue: GenericFieldValue = CustomAgGridService.findGenericFieldValueForColumn(params);

            if (genericFieldValue) {
                genericFieldValue.value = params.newValue;
            } else {
                genericFieldValue = {
                    id: 0,
                    value: params.newValue,
                    genericFieldId: params.colDef.cellRendererParams.genericField.id,
                    parentId: params.data.id
                };
                params.data.genericFieldValues.push(genericFieldValue);
            }

            return true;
        } else {
            return false;
        }
    }

    private static defaultTooltipValueGetter(params: ITooltipParams): string {
        return params.valueFormatted || params.value;
    }

    static isColGroupDef(col: ColDef | ColGroupDef): col is ColGroupDef {
        return 'children' in col;
    }

    static isColGroup(col: Column | ColumnGroup): col is ColumnGroup {
        return 'children' in col;
    }

    static getCurrencyValueFormatter = (params: ValueFormatterParams): string => {
        return ConverterService.formatCurrency(params.value);
    }

    static getPercentageValueFormatter = (params: ValueFormatterParams): string => {
        return ConverterService.formatPercentage(params.value);
    }

    static calculateColGroups(columns: (ColGroupDef | CustomColumnDef)[]): number {
        let hasGroupOnThisLevel: boolean = false;
        let hasFilterOnThisLevel: boolean = false;
        let maxDeepGroups: number = 0;

        columns.forEach((column: ColGroupDef | CustomColumnDef) => {
            if (CustomAgGridService.isColGroupDef(column)) {
                hasGroupOnThisLevel = true;
                maxDeepGroups = Math.max(maxDeepGroups, CustomAgGridService.calculateColGroups(column.children));
            } else if (column.filter) {
                hasFilterOnThisLevel = true;
            }
        });

        return hasGroupOnThisLevel ? (1 + maxDeepGroups) : (hasFilterOnThisLevel ? 1 : 0);
    }

    static recursivelyFindColDef(colGroups: (CustomColumnDef | ColGroupDef)[], id: string): CustomColumnDef {
        let result: CustomColumnDef;

        for (const colOrGroup of colGroups) {
            if (CustomAgGridService.isColGroupDef(colOrGroup)) {
                result = CustomAgGridService.recursivelyFindColDef(colOrGroup.children, id);
                if (result) {
                    break;
                }
            } else if (colOrGroup.colId ? colOrGroup.colId === id : colOrGroup.field === id) {
                result = colOrGroup;
                break;
            }
        }

        return result;
    }

    addEditModalAction<T extends FormAwareModal<T, E>, E extends IDomainModel<any>>(
        gridOptions: CustomAgGridOptionsModel,
        modalParams: CustomAgGridModalContext<T, E>
    ): void {
        if (!gridOptions.context.actionsColumnContext) {
            gridOptions.context.actionsColumnContext = {};
        }
        if (!gridOptions.context.actionsColumnContext.actionColumnButtons) {
            gridOptions.context.actionsColumnContext.actionColumnButtons = [];
        }

        gridOptions.context.actionsColumnContext.actionColumnButtons.push({
            classList: 'btn btn-xs btn-primary fa fa-edit',
            click: (context: ICellRendererParams) => {
                zip(...modalParams.observables).subscribe((resolvedObservables: RestResponseModel[]) => {
                    const modalData: FormAwareParams<E> = modalParams.modalDataBuilder(context, resolvedObservables);
                    this.dialog.open(modalParams.component, {data: modalData})
                        .afterClosed()
                        .subscribe({
                            next: (result: ModalResult) => {
                                if (result && result.isRefreshRequired) {
                                    gridOptions.api.refreshInfiniteCache();
                                }
                            }
                        });
                });
            },
            hidden: (context: ICellRendererParams) => !!gridOptions.api.getEditingCells()
                .find((cell: CellPosition) => cell.rowIndex === context.rowIndex)
        });
    }

    addEditModalActionWithGenericFields<T extends FormAndGenericFieldAwareModal<T, E>, E extends IGenericFieldAwareDomain<any>>(
        gridOptions: CustomAgGridOptionsModel,
        modalParams: CustomAgGridModalContext<T, E>
    ): void {
        if (!gridOptions.context.actionsColumnContext) {
            gridOptions.context.actionsColumnContext = {};
        }
        if (!gridOptions.context.actionsColumnContext.actionColumnButtons) {
            gridOptions.context.actionsColumnContext.actionColumnButtons = [];
        }

        gridOptions.context.actionsColumnContext.actionColumnButtons.push({
            classList: 'btn btn-xs btn-primary fa fa-edit',
            click: (context: ICellRendererParams) => {
                zip(...modalParams.observables).subscribe((resolvedObservables: RestResponseModel[]) => {
                    const modalData: FormAndGenericFieldAwareParams<E> = modalParams.modalDataBuilder(context, resolvedObservables);

                    if (!modalData.genericFields) {
                        const _genericFields: any = resolvedObservables[resolvedObservables.length - 1];
                        if (_genericFields.data && _genericFields.data[0] && _genericFields.data[0].entity && _genericFields.data[0].type) {
                            modalData.genericFields = resolvedObservables[resolvedObservables.length - 1].data;
                        }
                    }

                    this.dialog.open(modalParams.component, {data: modalData})
                        .afterClosed()
                        .subscribe({
                            next: (result: ModalResult) => {
                                if (result && result.isRefreshRequired) {
                                    gridOptions.api.refreshInfiniteCache();
                                }
                            }
                        });
                });
            },
            hidden: (context: ICellRendererParams) => !!gridOptions.api.getEditingCells()
                .find((cell: CellPosition) => cell.rowIndex === context.rowIndex)
        });
    }

    prepareGrid<T extends FormAwareModal<any, any>, E extends IDomainModel<any>>(
        gridOptions: CustomAgGridOptionsModel,
        isActiveColumn: boolean = true,
        actionsColumn: boolean = true,
        modalParams?: CustomAgGridModalContext<T, E>,
        idColumn: boolean = true
    ): Observable<CustomAgGridOptionsModel> {
        if (idColumn && !gridOptions.columnDefs.some((colDef: CustomColumnDef) => colDef.field === 'id' || colDef.colId === 'id')) {
            gridOptions.columnDefs.unshift(CustomAgGridService.idColumn);
        }

        defaultsDeep(gridOptions, CustomAgGridService.defaultGridOptions);
        gridOptions.context.gridOptions = gridOptions;

        const hasGenericFields: boolean = !isNil(gridOptions.context.genericFieldEntity);
        const _observables: Observable<any>[] = this.recursiveGetColGroupObservables(gridOptions.columnDefs);

        if (hasGenericFields) {
            const $genericFields: Observable<GenericFieldWithOptionModel[]> = this.genericFieldResource
                    .findAllByEntity(gridOptions.context.genericFieldEntity)
                    .pipe(shareReplay(1));
            _observables.push($genericFields);
            if (modalParams) {
                if (!modalParams.observables) {
                    modalParams.observables = [];
                }
                modalParams.observables.push($genericFields);
            }
        } else {
            _observables.push(of(null));
        }

        return zip(..._observables).pipe(
            tap((resolvedObservables: RestResponseModel[]) => {
                this.recursivePrepareColGroups(gridOptions.columnDefs, resolvedObservables, gridOptions.context.resource);

                const genericFieldObservable: RestResponseModel = resolvedObservables[resolvedObservables.length - 1];
                if (genericFieldObservable && genericFieldObservable.data && genericFieldObservable.data.length) {
                    const colGroup: ColGroupDef = {
                        headerName: 'ui.text.genericField.title',
                        groupId: GENERIC_FIELD_GROUP_ID,
                        children: []
                    };
                    genericFieldObservable.data.forEach((genericField: GenericFieldWithOptionModel) => {
                        const newColumn: CustomColumnDef = CustomAgGridService.prepareGenericFieldColumn(genericField);
                        if (newColumn) {
                            colGroup.children.push(newColumn);
                        }
                    });
                    if (colGroup.children.length) {
                        gridOptions.columnDefs.push(colGroup);
                    }
                }
            }),
            map(() => {
                if (isActiveColumn) {
                    this.addIsActiveColumn(gridOptions);
                }

                if (actionsColumn) {
                    CustomAgGridService.addActionsColumn(gridOptions);
                }

                if (modalParams) {
                    if (hasGenericFields) {
                        this.addEditModalActionWithGenericFields(gridOptions, (<any>modalParams));
                    } else {
                        this.addEditModalAction(gridOptions, modalParams);
                    }
                }

                gridOptions.columnDefs.forEach((colDef: CustomColumnDef) => {
                    if (!colDef.tooltipValueGetter && !colDef.tooltipComponentFramework) {
                        colDef.tooltipValueGetter = CustomAgGridService.defaultTooltipValueGetter;
                    }
                });

                return gridOptions;
            })
        );
    }

    private recursiveGetColGroupObservables(columnDefs: (ColGroupDef | CustomColumnDef)[]): Observable<any>[] {
        let result: Observable<any>[] = [];

        columnDefs.forEach((colDef: ColGroupDef | CustomColumnDef) => {
            if (CustomAgGridService.isColGroupDef(colDef)) {
                result = result.concat(this.recursiveGetColGroupObservables(colDef.children));
            } else {
                result.push(colDef.autoGenerateContext && colDef.autoGenerateContext.observable
                    ? colDef.autoGenerateContext.observable
                    : of(null));
            }
        });

        return result;
    }

    private recursivePrepareColGroups(
        columnDefs: (ColGroupDef | CustomColumnDef)[],
        resolvedObservables: RestResponseModel[],
        resource: RestResource,
        recursiveIndex: number = 0
    ) {
        columnDefs.forEach((colDef: ColGroupDef | CustomColumnDef, index: number) => {
            if (CustomAgGridService.isColGroupDef(colDef)) {
                recursiveIndex = this.recursivePrepareColGroups(colDef.children, resolvedObservables, resource, recursiveIndex);
            } else {
                if (colDef.autoGenerateContext) {
                    const tmpData: any = resolvedObservables[recursiveIndex] ? resolvedObservables[recursiveIndex].data : null;
                    columnDefs[index] = this.prepareColumn(colDef, tmpData, resource);
                }
                recursiveIndex++;
            }
        });

        return recursiveIndex;
    }

    private prepareColumn(column: CustomColumnDef, resolvedObservable?: any, resource?: RestResource): CustomColumnDef {
        let newColumn: CustomColumnDef;

        switch (column.autoGenerateContext.type) {
            case ColumnObservableType.DATE:
                newColumn = CustomAgGridService.prepareDateColumn(column);
                break;
            case ColumnObservableType.ARRAY:
                newColumn = CustomAgGridService.prepareArrayColumn(column, resolvedObservable);
                break;
            case ColumnObservableType.DOMAIN:
                newColumn = CustomAgGridService.prepareDomainColumn(column, resolvedObservable);
                break;
            case ColumnObservableType.DOMAIN_MULTIPLE:
                newColumn = CustomAgGridService.prepareDomainMultipleColumn(column, resolvedObservable);
                break;
            case ColumnObservableType.LOCALIZED_ENUM:
                newColumn = CustomAgGridService.prepareLocalizedEnumColumn(column, resolvedObservable);
                break;
            case ColumnObservableType.BOOLEAN:
                newColumn = CustomAgGridService.prepareBooleanColumn(column);
                break;
            case ColumnObservableType.DOMAIN2:
                newColumn = this.prepareInfiniteSelectColumn(column, resource);
                break;
            case ColumnObservableType.DOMAIN_MULTIPLE2:
                newColumn = this.prepareInfiniteSelectMultiColumn(column, resource);
                break;
            case ColumnObservableType.NUMBER:
                newColumn = CustomAgGridService.prepareNumberColumn(column);
                break;
            case ColumnObservableType.TEXT:
                newColumn = CustomAgGridService.prepareTextColumn(column);
                break;
            default:
                throw Error('Unexpected `ColumnObservableType`');
        }

        return merge(newColumn, column);
    }

    private addIsActiveColumn(gridOptions: CustomAgGridOptionsModel): void {
        gridOptions.columnDefs.push(this.prepareColumn({
            headerName: 'ui.text.default.isActive',
            field: 'isActive',

            autoGenerateContext: {
                type: ColumnObservableType.BOOLEAN,
            },
        }));
    }

    private prepareInfiniteSelectColumn(column: CustomColumnDef, resource: RestResource): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!column.autoGenerateContext.infiniteProps) {
            throw Error('`InfiniteSelectColumnProps.infiniteProps` is required for `ColumnObservableType.DOMAIN2`');
        }

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }
        if (!column.valueFormatter && column.autoGenerateContext.otherField) {
            newColumn.valueFormatter = CustomAgGridService.otherFieldFormatter(column.autoGenerateContext.otherField);
        }

        const infiniteSelectBaseParams: Partial<InfiniteSelectGridBaseParams> = {
            converter: column.autoGenerateContext.infiniteProps.converter || this.converterService.defaultSelectItemsConverter,
            bindLabel: column.autoGenerateContext.bindLabel || 'label',
            bindValue: column.autoGenerateContext.bindValue || 'value'
        };

        const infiniteSelectFilterParams: InfiniteSelectGridBaseParams = {
            ...infiniteSelectBaseParams,
            multiple: true,
            dataCallback:
                column.autoGenerateContext.infiniteProps.filterMethod
                || this.getInfiniteSelectFilterCallback(column.autoGenerateContext.infiniteProps, resource),
        };

        CustomAgGridService.prepareFilter(
            column,
            newColumn,
            InfiniteSelectFilterComponent,
            infiniteSelectFilterParams);
        CustomAgGridService.prepareFloatingFilter(
            column,
            newColumn,
            InfiniteSelectFloatingFilterComponent,
            infiniteSelectFilterParams);

        if (column.autoGenerateContext.editor || isNil(column.autoGenerateContext.editor)) {
            if (
                !column.autoGenerateContext.infiniteProps.editorOptionsResource
                && !column.autoGenerateContext.infiniteProps.editorOptionsMethod
            ) {
                throw Error('`InfiniteSelectColumnProps.infiniteProps.editorOptionsResource` or '
                + '`column.autoGenerateContext.infiniteProps.editorOptionsMethod`'
                + ' is required for `ColumnObservableType.DOMAIN2` with `editable` option');
            } else {
                const infiniteSelectEditorParams: InfiniteSelectGridBaseParams = {
                    ...infiniteSelectBaseParams,
                    dataCallback:
                        column.autoGenerateContext.infiniteProps.editorOptionsMethod
                        || this.getInfiniteSelectEditorCallback(column.autoGenerateContext.infiniteProps),
                };
                CustomAgGridService.prepareEditor(column, newColumn, InfiniteSelectEditorComponent, infiniteSelectEditorParams);
            }
        }

        return newColumn;
    }

    private prepareInfiniteSelectMultiColumn(column: CustomColumnDef, resource: RestResource): CustomColumnDef {
        const newColumn: CustomColumnDef = {};

        if (!column.autoGenerateContext.infiniteProps) {
            throw Error('`InfiniteSelectColumnProps.infiniteProps` is required for `ColumnObservableType.DOMAIN_MULTIPLE2`');
        }

        if (!column.cellRendererFramework && !column.cellRenderer) {
            newColumn.cellRendererFramework = CustomCellRendererComponent;
        }
        if (!column.valueFormatter) {
            newColumn.valueFormatter = CustomAgGridService.getYesNoArrayFormatter;
        }

        const infiniteSelectFilterParams: InfiniteSelectGridBaseParams = {
            converter: column.autoGenerateContext.infiniteProps.converter || this.converterService.defaultSelectItemsConverter,
            bindLabel: column.autoGenerateContext.bindLabel || 'label',
            bindValue: column.autoGenerateContext.bindValue || 'value',
            multiple: true,
            dataCallback:
                column.autoGenerateContext.infiniteProps.filterMethod
                || this.getInfiniteSelectFilterCallback(column.autoGenerateContext.infiniteProps, resource),
        };

        CustomAgGridService.prepareFilter(
            column,
            newColumn,
            InfiniteSelectFilterComponent,
            infiniteSelectFilterParams);
        CustomAgGridService.prepareFloatingFilter(
            column,
            newColumn,
            InfiniteSelectFloatingFilterComponent,
            infiniteSelectFilterParams);

        if (column.autoGenerateContext.editor || isNil(column.autoGenerateContext.editor)) {
            if (
                !column.autoGenerateContext.infiniteProps.editorOptionsResource
                && !column.autoGenerateContext.infiniteProps.editorOptionsMethod
            ) {
                throw Error('`InfiniteSelectColumnProps.infiniteProps.editorOptionsResource` or '
                + '`column.autoGenerateContext.infiniteProps.editorOptionsMethod`'
                + ' is required for `ColumnObservableType.DOMAIN_MULTIPLE2` with `editable` option');
            } else {
                const infiniteSelectEditorParams: InfiniteSelectGridBaseParams = {
                    ...infiniteSelectFilterParams,
                    dataCallback:
                        column.autoGenerateContext.infiniteProps.editorOptionsMethod
                        || this.getInfiniteSelectEditorCallback(column.autoGenerateContext.infiniteProps),
                };
                CustomAgGridService.prepareEditor(column, newColumn, InfiniteSelectEditorComponent, infiniteSelectEditorParams);
            }
        }

        return newColumn;
    }

    private getInfiniteSelectFilterCallback(props: InfiniteSelectColumnProps, resource: RestResource): ngSelectInfiniteDataCallback {
        return (term: string, limit: number, offset?: number): Observable<any> => {
            return resource.getUniqueValues(
                props.name,
                props.orderByFields,
                term,
                props.filterByFieldNames || props.orderByFields,
                limit,
                offset);
        };
    }

    private getInfiniteSelectEditorCallback(props: InfiniteSelectColumnProps): ngSelectInfiniteDataCallback {
        return (term: string, limit: number, offset?: number): Observable<any> => {
            return props.editorOptionsResource
                .findLike(false, StaticService.createPageable(limit, offset, props.filterByFieldNames || props.orderByFields), term);
        };
    }

}
